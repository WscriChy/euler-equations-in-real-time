#ifndef EulerEquations_Simulation_Grid_hpp
#define EulerEquations_Simulation_Grid_hpp

#include "Cell.hpp"
#include "CellAccessor.hpp"

#include "PeanoExecutor/records/SubgridDescriptor.h"
#include "PeanoExecutor/records/Unknown.h"

#include <Uni/Logging/macros>
#include <Uni/StructuredGrid/Basic/Grid>

#include <Uni/Numerics/OneStepMethod/Controller>
#include <Uni/Numerics/OneStepMethod/ScalarCellIndices>
#include <Uni/Numerics/OneStepMethod/ScalarCellProvider>

#include <peano/heap/Heap.h>

#include <vector>

namespace EulerEquations {
namespace Simulation {
typedef peano::heap::PlainHeap<PeanoExecutor::records::SubgridDescriptor> GridHeap;

using GridType
  = Uni::StructuredGrid::Basic::Grid<Uni::StructuredGrid::Basic::DummyForThisGrid,
                                     CellAccessor,
                                     3>;

class Grid : public Uni::Numerics::OneStepMethod::ScalarCellProvider<Cell>,
             public GridType,
             public Uni::Numerics::OneStepMethod::Controller<double> {
private:
    typedef Uni::Numerics::OneStepMethod::ScalarCellProvider<Cell> Base1;
    typedef Uni::Numerics::OneStepMethod::Controller<double> Base3;

    typedef Cell::PersistentCell PersistentCell;
    typedef std::vector<PersistentCell> PersistentCells;

    typedef Uni::Numerics::OneStepMethod::ScalarCellIndices<GridType> ScalarCellIndices;

public:
    typedef peano::heap::PlainHeap<PeanoExecutor::records::Unknown> UnknownHeap;

    typedef PeanoExecutor::records::SubgridDescriptor SubgridDescriptor;

    using Iterator = typename GridType::Iterator;
    using Factory = typename GridType::Factory;
    using VectorDi = typename GridType::VectorDi;

    using InnerGrid = GridType;
    using LeftGhostGrid = GridType;
    using RightGhostGrid = GridType;
    using BottomGhostGrid = GridType;
    using TopGhostGrid = GridType;
    using BackGhostGrid = GridType;
    using FrontGhostGrid = GridType;

public:
    Grid(SubgridDescriptor* descriptor)
      : iteratorFactory(std::bind(
          &Grid::createCellAccessor, this, std::placeholders::_1, std::placeholders::_2)),
        _descriptor(descriptor),
        _cells(&UnknownHeap::getInstance().getData(
          _descriptor->getUnknowns(!_descriptor->getNewTimeStep()))),
        _newCells(&UnknownHeap::getInstance().getData(
          _descriptor->getUnknowns(_descriptor->getNewTimeStep()))),
        _scalarCellIndices(this) {

        VectorDi indent({_descriptor->getGhostSize(0),
                         _descriptor->getGhostSize(1),
                         _descriptor->getGhostSize(2)});

        this->initialize(VectorDi({_descriptor->getSize(0),
                                   _descriptor->getSize(1),
                                   _descriptor->getSize(2)}) + 2 * indent,
                         VectorDi::Ones(),
                         VectorDi::Ones(),
                         iteratorFactory);

        for (int i = 0; i < Dimensions; ++i) {
            VectorDi leftIndent(indent);
            VectorDi rightIndent(indent);
            leftIndent(i) = 0;
            rightIndent(i) = size(i) - rightIndent(i);

            if (i == 0) {
              leftGhostGrid.initialize(size(), leftIndent, rightIndent, iteratorFactory);
              rightGhostGrid.initialize(size(), rightIndent, leftIndent, iteratorFactory);
            } else if (i == 1) {
              bottomGhostGrid.initialize(size(), leftIndent, rightIndent, iteratorFactory);
              topGhostGrid.initialize(size(), rightIndent, leftIndent, iteratorFactory);
            } else if (i == 2) {
              backGhostGrid.initialize(size(), leftIndent, rightIndent, iteratorFactory);
              frontGhostGrid.initialize(size(), rightIndent, leftIndent, iteratorFactory);
            }
        }

        innerGrid.initialize(size(), indent, indent, iteratorFactory);
    }

    Grid(Grid const& other) = delete;

    Grid& operator=(Grid const& other) = delete;

    ~Grid() {}

    int xSize() const { return _descriptor->getSize(0) + indentSize(0); }

    int ySize() const { return _descriptor->getSize(1) + indentSize(1); }

    int zSize() const { return _descriptor->getSize(2) + indentSize(2); }

    double previousTimeStamp() const {
        return _descriptor->getTimeStamp(_descriptor->getNewTimeStep());
    }

    double currentTimeStamp() const {
        return _descriptor->getTimeStamp(!_descriptor->getNewTimeStep());
    }

    void newTimeStamp(double const& value) {
        _descriptor->setTimeStamp(_descriptor->getNewTimeStep(), value);
    }

    double timeStepSize() const { return _descriptor->getTimeStepSize(); }

    void timeStepSize(double const& value) { _descriptor->setTimeStepSize(value); }

    void flip() { _descriptor->setNewTimeStep(!_descriptor->getNewTimeStep()); }

    void resetTimeStep() { _descriptor->setNewTimeStep(0); }

    Cell leftCell(int const& index) const {
        return Cell(&_cells->at(_scalarCellIndices.leftIndex(index)));
    }

    Cell rightCell(int const& index) const {
        return Cell(&_cells->at(_scalarCellIndices.rightIndex(index)));
    }

    Cell bottomCell(int const& index) const {
        return Cell(&_cells->at(_scalarCellIndices.bottomIndex(index)));
    }

    Cell topCell(int const& index) const {
        return Cell(&_cells->at(_scalarCellIndices.topIndex(index)));
    }

    Cell backCell(int const& index) const {
        return Cell(&_cells->at(_scalarCellIndices.backIndex(index)));
    }

    Cell frontCell(int const& index) const {
        return Cell(&_cells->at(_scalarCellIndices.frontIndex(index)));
    }

    Cell currentCell(int const& index) const {
        return Cell(&_cells->at(_scalarCellIndices.currentIndex(index)));
    }

    Cell leftNewCell(int const& index) const {
        return Cell(&_newCells->at(_scalarCellIndices.leftIndex(index)));
    }

    Cell rightNewCell(int const& index) const {
        return Cell(&_newCells->at(_scalarCellIndices.rightIndex(index)));
    }

    Cell bottomNewCell(int const& index) const {
        return Cell(&_newCells->at(_scalarCellIndices.bottomIndex(index)));
    }

    Cell topNewCell(int const& index) const {
        return Cell(&_newCells->at(_scalarCellIndices.topIndex(index)));
    }

    Cell backNewCell(int const& index) const {
        return Cell(&_newCells->at(_scalarCellIndices.backIndex(index)));
    }

    Cell frontNewCell(int const& index) const {
        return Cell(&_newCells->at(_scalarCellIndices.frontIndex(index)));
    }

    Cell currentNewCell(int const& index) const {
        return Cell(&_newCells->at(_scalarCellIndices.currentIndex(index)));
    }

public:
    CellAccessor createCellAccessor(GridType const* grid,
                                    typename GridType::VectorDi const& i) {
        return CellAccessor(grid, i, this);
    }

    Factory iteratorFactory;

    InnerGrid innerGrid;
    LeftGhostGrid leftGhostGrid;
    RightGhostGrid rightGhostGrid;
    BottomGhostGrid bottomGhostGrid;
    TopGhostGrid topGhostGrid;
    BackGhostGrid backGhostGrid;
    FrontGhostGrid frontGhostGrid;

private:
    SubgridDescriptor* _descriptor;
    PersistentCells* _cells;
    PersistentCells* _newCells;

    ScalarCellIndices _scalarCellIndices;
};
}
}

#endif
