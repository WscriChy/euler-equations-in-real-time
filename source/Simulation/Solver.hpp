#ifndef EulerEquations_Simulation_Solver_hpp
#define EulerEquations_Simulation_Solver_hpp

#include "Scenario.hpp"

#include <Uni/IterativeTask>
#include <Uni/Observable>

#include <memory>
#include <vector>

namespace PeanoExecutor {
namespace repositories {
class Repository;
}
}

namespace EulerEquations {
namespace Simulation {
enum SolverProperties {
  SimulationTime  = 0,
  IterationNumber = 1,
  MinDensity      = 2,
  MaxDensity      = 3,
};
class Solver : public Uni::Observable<SolverProperties>,
               public Uni::IterativeTask {
public:
  typedef std::shared_ptr<Solver> Shared;

  typedef std::vector<float>           ScalarField;
  typedef std::shared_ptr<ScalarField> SharedScalarField;

private:
public:
  Solver(PeanoExecutor::repositories::Repository* repository);

  Solver(Solver const& other) = delete;

  virtual
  ~Solver();

  Solver const&
  operator=(Solver const& other) = delete;

  void
  scenario(Scenario::Shared scenario);

  float const&
  minDensity() const;

  float const&
  maxDensity() const;

  float const&
  minVelocityNorm() const;

  float const&
  maxVelocityNorm() const;

  float const&
  minTemperature() const;

  float const&
  maxTemperature() const;

  float const&
  minPressure() const;

  float const&
  maxPressure() const;

  SharedScalarField
  currentScalarField() const;

  void
  swapField();

  void
  synchronize(void* pointer = 0);

  virtual
  void
  initialize();

  virtual
  void
  iterate();

  void
  release();

private:
  void
  initializeField();

  bool
  advanceIndex(int&  index,
               bool& direction) const;

  void
  releaseIteration();

private:
  PeanoExecutor::repositories::Repository* _repository;
  Scenario::Shared                         _scenario;

  SharedScalarField _renderScalarField;
  SharedScalarField _taskScalarField;

  int   _iterationNumber;
  float _minDensity;
  float _maxDensity;
  float _minVelocityNorm;
  float _maxVelocityNorm;
  float _minTemperature;
  float _maxTemperature;
  float _minPressure;
  float _maxPressure;
};
}
}
#endif
