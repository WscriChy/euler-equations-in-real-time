#include "CellAccessor.hpp"

#include "Grid.hpp"

using EulerEquations::Simulation::CellAccessor;

CellAccessor::CellAccessor(GridType const* grid,
                           VectorDi const& i,
                           CellProvider* cellProvider)
  : Base(grid, i, cellProvider) {}

CellAccessor::CellAccessor(CellAccessor const& other) : Base(other) {}

CellAccessor::~CellAccessor() {}

CellAccessor const& CellAccessor::operator=(CellAccessor const& other) {
    Base::operator=(other);

    return *this;
}
