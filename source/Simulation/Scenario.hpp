#ifndef Solver_Scenario_hpp
#define Solver_Scenario_hpp

#include "Grid.hpp"

#include <Eigen/Core>

#include <memory>

namespace EulerEquations {
namespace Simulation {
class ScenarioData {
public:
  typedef std::shared_ptr<ScenarioData> Shared;

public:
  ScenarioData() {}

  virtual
  ~ScenarioData() {}

  virtual
  double const&
  currentTime() const = 0;

  virtual
  float const&
  minDensity() const = 0;

  virtual
  float const&
  maxDensity() const = 0;

  virtual
  float const&
  minVelocityNorm() const = 0;

  virtual
  float const&
  maxVelocityNorm() const = 0;

  virtual
  float const&
  minTemperature() const = 0;

  virtual
  float const&
  maxTemperature() const = 0;

  virtual
  float const&
  minPressure() const = 0;

  virtual
  float const&
  maxPressure() const = 0;

  virtual
  void
  join(ScenarioData::Shared scenarioData) = 0;
};

class ScenarioRunner {
public:
  typedef std::shared_ptr<ScenarioRunner> Shared;

  typedef Eigen::Matrix<double, 3, 1> Vector3;

private:
public:
  ScenarioRunner() {}

  virtual
  ~ScenarioRunner() {}

  virtual
  ScenarioData::Shared
  data() const = 0;

  virtual
  void
  initialize(Grid*          grid,
             Vector3 const& position,
             Vector3 const& gridWidth) = 0;

  virtual
  void
  iterate(Grid*          leftGrid,
          Grid*          rightGrid,
          Grid*          bottomGrid,
          Grid*          topGrid,
          Grid*          backGrid,
          Grid*          frontGrid,
          Grid*          grid,
          Vector3 const& position,
          Vector3 const& gridWidth) = 0;

private:
};

class Scenario {
private:
public:
  typedef std::shared_ptr<Scenario> Shared;

public:
  Scenario() {}

  virtual
  ~Scenario() {}

  virtual
  int const&
  xSize() const {
    return _xSize;
  }

  virtual
  void
  xSize(int const& value) {
    _xSize = value;
  }

  virtual
  int const&
  ySize() const {
    return _ySize;
  }

  virtual
  void
  ySize(int const& value) {
    _ySize = value;
  }

  virtual
  int const&
  zSize() const {
    return _zSize;
  }

  virtual
  void
  zSize(int const& value) {
    _zSize = value;
  }

  virtual
  ScenarioRunner::Shared
  createRunner(ScenarioData::Shared scenarioData = ScenarioData::Shared()) = 0;

  virtual
  ScenarioData::Shared
  createData() = 0;

public:
private:
  int _xSize;
  int _ySize;
  int _zSize;
};
}
}

#endif
