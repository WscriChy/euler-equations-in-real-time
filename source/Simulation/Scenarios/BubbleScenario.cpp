#include "BubbleScenario.hpp"

#include <Uni/EulerEquations/Basic/Cell>
#include <Uni/EulerEquations/RoeSolver>
#include <Uni/Numerics/FiniteVolume/utils>
#include <Uni/Numerics/LocalTimeStepping/utils>

#include <Uni/Logging/macros>

#include <tbb/blocked_range.h>
#include <tbb/parallel_for_each.h>
#include <tbb/parallel_reduce.h>

#include <limits>

#define EulerEquations_CFL 0.3

using EulerEquations::Simulation::Scenarios::BubbleScenario;
using EulerEquations::Simulation::Scenarios::BubbleScenarioData;
using EulerEquations::Simulation::Scenarios::BubbleScenarioRunner;

using Uni::EulerEquations::RoeSolver;

namespace EulerEquations {
namespace Simulation {
inline GridType getBoundaryGrid(unsigned const& dimension,
                                unsigned const& direction,
                                Grid* grid,
                                Grid* neighbor_grid) {
    using VectorDi = typename GridType::VectorDi;

    VectorDi left_indent(neighbor_grid->leftIndent());
    VectorDi right_indent(neighbor_grid->rightIndent());

    if (direction == 0) {
        right_indent(dimension) = neighbor_grid->size(dimension) - left_indent(dimension)
                                  - grid->rightIndent(dimension);
    } else {
        left_indent(dimension) = neighbor_grid->size(dimension) - right_indent(dimension)
                                 - grid->leftIndent(dimension);
    }

    GridType boundary_grid;
    boundary_grid.initialize(
      neighbor_grid->size(), left_indent, right_indent, neighbor_grid->iteratorFactory);
    return boundary_grid;
}
}
}

void BubbleScenarioRunner::initialize(Grid* grid,
                                      Vector3 const& position,
                                      Vector3 const& gridWidth) {
    double maxWaveSpeed = std::numeric_limits<double>::min();
    double minDensity = std::numeric_limits<double>::max();
    double maxDensity = std::numeric_limits<double>::min();
    double minVelocityNorm = std::numeric_limits<double>::max();
    double maxVelocityNorm = std::numeric_limits<double>::min();
    double minTemperature = std::numeric_limits<double>::max();
    double maxTemperature = std::numeric_limits<double>::min();
    double minPressure = std::numeric_limits<double>::max();
    double maxPressure = std::numeric_limits<double>::min();

    typedef Uni::EulerEquations::Basic::Cell<double, 3> StaticCell;

    StaticCell cell1;
    cell1.density(0.1);
    cell1.velocity({0.0, 0.0, 0.0});
    cell1.temperature(300.0);
    StaticCell cell2;
    cell2.density(0.8);
    cell2.velocity({0.0, 0.0, 0.0});
    cell2.temperature(300.0);

    for (auto const& accessor : grid->innerGrid) {
        double maxLambda;

        Vector3 currentPosition = position;

        currentPosition
          += gridWidth.cwiseProduct(accessor().cast<double>() + Vector3::Constant(0.5)
                                    - Vector3({grid->innerGrid.leftIndent(0),
                                               grid->innerGrid.leftIndent(1),
                                               grid->innerGrid.leftIndent(2)}));

        Vector3 bubbleCenter;
        bubbleCenter.fill(0.5);

        double radius = 0.30;

        if ((bubbleCenter - currentPosition).norm() < radius) {
            // Inside bubble
            accessor.currentCell().copy(&cell1);
            accessor.currentNewCell().copy(&cell1);
            maxLambda = cell1.computeMaxWaveVelocity();
        } else {
            // Outside bubble
            accessor.currentCell().copy(&cell2);
            accessor.currentNewCell().copy(&cell2);
            maxLambda = cell2.computeMaxWaveVelocity();
        }

        maxWaveSpeed = std::max(maxLambda, maxWaveSpeed);
        minDensity = std::min(minDensity, accessor.currentNewCell().density());
        maxDensity = std::max(maxDensity, accessor.currentNewCell().density());
        minVelocityNorm
          = std::min(minVelocityNorm, accessor.currentNewCell().velocity().norm());
        maxVelocityNorm
          = std::max(maxVelocityNorm, accessor.currentNewCell().velocity().norm());
        minTemperature
          = std::min(minTemperature, accessor.currentNewCell().temperature());
        maxTemperature
          = std::max(maxTemperature, accessor.currentNewCell().temperature());
        minPressure = std::min(minPressure, accessor.currentNewCell().pressure());
        maxPressure = std::max(maxPressure, accessor.currentNewCell().pressure());
    }

    _data->minDensity(minDensity);
    _data->maxDensity(maxDensity);

    _data->minVelocityNorm(minVelocityNorm);
    _data->maxVelocityNorm(maxVelocityNorm);

    _data->minTemperature(minTemperature);
    _data->maxTemperature(maxTemperature);

    _data->minPressure(minPressure);
    _data->maxPressure(maxPressure);

    double newTimeStepSize = Uni::Numerics::FiniteVolume::computeTimeStepSize(
      gridWidth, maxWaveSpeed, EulerEquations_CFL);

    // Local Time Stepping logic
    // grid->timeStepSize(newTimeStepSize);
    _data->minTimeStepSize(newTimeStepSize);

    grid->resetTimeStep();
    grid->newTimeStamp(0.0);
    grid->flip();
    grid->newTimeStamp(0.0);
    grid->flip();
    _data->currentTime(grid->currentTimeStamp());
}

namespace EulerEquations {
namespace Simulation {
namespace Scenarios {
void interpolate(Grid::Iterator& it1,
                 Grid& grid1,
                 Grid::Iterator& itEnd,
                 Grid::Iterator& it2,
                 Grid& grid2) {
    double coeff = Uni::Numerics::LocalTimeStepping::interpolate(
      grid2.previousTimeStamp(), grid2.currentTimeStamp(), grid1.currentTimeStamp());

    for (; it1 != itEnd; ++it1, ++it2) {
        auto ghostCellAccessor = *it1;
        auto boundaryCellAccessor = *it2;
        auto cell1 = boundaryCellAccessor.currentNewCell();
        auto cell2 = boundaryCellAccessor.currentCell();

        // ghostCellAccessor.currentCell().interpolate(
        // &cell1, &cell2, coeff);
        ghostCellAccessor.currentCell().copy(&cell2);
    }
}

void reflect(Grid::Iterator& it1, Grid::Iterator& itEnd, Grid::Iterator& it2) {
    for (; it1 != itEnd; ++it1, ++it2) {
        auto ghostCellAccessor = *it1;
        auto boundaryCellAccessor = *it2;
        auto cell = boundaryCellAccessor.currentCell();
        ghostCellAccessor.currentCell().reflect(&cell);
    }
}

class SchemeExecutor {
public:
    SchemeExecutor(SchemeExecutor& other, tbb::split)
      : scheme(other.scheme), totalMaxLambda(std::numeric_limits<double>::min()),
        maxDensity(std::numeric_limits<double>::min()),
        minDensity(std::numeric_limits<double>::max()),
        maxVelocityNorm(std::numeric_limits<double>::min()),
        minVelocityNorm(std::numeric_limits<double>::max()),
        maxTemperature(std::numeric_limits<double>::min()),
        minTemperature(std::numeric_limits<double>::max()),
        maxPressure(std::numeric_limits<double>::min()),
        minPressure(std::numeric_limits<double>::max()) {}

    SchemeExecutor(double const& timeStepSize,
                   double const& xDelta,
                   double const& yDelta,
                   double const& zDelta)
      : totalMaxLambda(std::numeric_limits<double>::min()),
        maxDensity(std::numeric_limits<double>::min()),
        minDensity(std::numeric_limits<double>::max()),
        maxVelocityNorm(std::numeric_limits<double>::min()),
        minVelocityNorm(std::numeric_limits<double>::max()),
        maxTemperature(std::numeric_limits<double>::min()),
        minTemperature(std::numeric_limits<double>::max()),
        maxPressure(std::numeric_limits<double>::min()),
        minPressure(std::numeric_limits<double>::max()) {
        scheme.courantNumber(timeStepSize, xDelta, yDelta, zDelta);
    }

    void operator()(tbb::blocked_range<Grid::InnerGrid::Iterator> const& range) {
        for (auto it = range.begin(); it != range.end(); ++it) {
            CellAccessor const& accessor = *it;
            double maxLambda;
            auto newCell = accessor.currentNewCell();

            scheme.apply(accessor.leftCell(),
                         accessor.rightCell(),
                         accessor.bottomCell(),
                         accessor.topCell(),
                         accessor.backCell(),
                         accessor.frontCell(),
                         accessor.currentCell(),
                         newCell,
                         maxLambda);
            totalMaxLambda = std::max(totalMaxLambda, maxLambda);
            maxDensity = std::max(maxDensity, newCell.density());
            minDensity = std::min(minDensity, newCell.density());
            maxVelocityNorm = std::max(maxVelocityNorm, newCell.velocity().norm());
            minVelocityNorm = std::min(minVelocityNorm, newCell.velocity().norm());
            maxTemperature = std::max(maxTemperature, newCell.temperature());
            minTemperature = std::min(minTemperature, newCell.temperature());
            maxPressure = std::max(maxPressure, newCell.pressure());
            minPressure = std::min(minPressure, newCell.pressure());
        }
    }

    void join(SchemeExecutor const& other) {
        totalMaxLambda = std::max(totalMaxLambda, other.totalMaxLambda);
        maxDensity = std::max(maxDensity, other.maxDensity);
        minDensity = std::min(minDensity, other.minDensity);
        maxVelocityNorm = std::max(maxVelocityNorm, other.maxVelocityNorm);
        minVelocityNorm = std::min(minVelocityNorm, other.minVelocityNorm);
        maxTemperature = std::max(maxTemperature, other.maxTemperature);
        minTemperature = std::min(minTemperature, other.minTemperature);
        maxPressure = std::max(maxPressure, other.maxPressure);
        minPressure = std::min(minPressure, other.minPressure);
    }

private:
    RoeSolver scheme;

public:
    double totalMaxLambda;
    double maxDensity;
    double minDensity;
    double maxVelocityNorm;
    double minVelocityNorm;
    double maxTemperature;
    double minTemperature;
    double maxPressure;
    double minPressure;
};
}
}
}

void BubbleScenarioRunner::iterate(Grid* leftGrid,
                                   Grid* rightGrid,
                                   Grid* bottomGrid,
                                   Grid* topGrid,
                                   Grid* backGrid,
                                   Grid* frontGrid,
                                   Grid* grid,
                                   Vector3 const& position,
                                   Vector3 const& gridWidth) {

    // Local Time Stepping logic
    // std::vector<double> neighbourTimeStamps;

    // if (leftGrid) {
    // neighbourTimeStamps.push_back(leftGrid->currentTimeStamp());
    // }

    // if (rightGrid) {
    // neighbourTimeStamps.push_back(rightGrid->currentTimeStamp());
    // }

    // if (bottomGrid) {
    // neighbourTimeStamps.push_back(bottomGrid->currentTimeStamp());
    // }

    // if (topGrid) {
    // neighbourTimeStamps.push_back(topGrid->currentTimeStamp());
    // }

    // if (backGrid) {
    // neighbourTimeStamps.push_back(backGrid->currentTimeStamp());
    // }

    // if (frontGrid) {
    // neighbourTimeStamps.push_back(frontGrid->currentTimeStamp());
    // }

    // bool needUpdate = Uni::Numerics::LocalTimeStepping::isLessOrEqual(
    // grid->currentTimeStamp(),
    // neighbourTimeStamps);

    // double timeStepSize = grid->timeStepSize();

    // int timeStepPlotCount;
    // Uni::Numerics::LocalTimeStepping::computeTimeStepPlotCount(
    // grid->currentTimeStamp(),
    // _plotInterval,
    // timeStepPlotCount,
    // timeStepSize);

    // if (!needUpdate) {
    // _data->currentTime(grid->currentTimeStamp());
    // _data->minPlotCount(timeStepPlotCount);

    // return;
    // }

    // auto minPlotTime = _input->minPlotCount() * _plotInterval;

    // if (minPlotTime > 0.0) {
    // logDebug("coutn %i\n", _input->minPlotCount() * _plotInterval);
    // }

    // timeStepSize += minPlotTime;

    double timeStepSize = _input->minTimeStepSize();

    // Uni_Logging_logDebug("Some %f", timeStepSize);

    if (leftGrid) {
        auto it1 = grid->leftGhostGrid.begin();
        auto itEnd = grid->leftGhostGrid.end();

        GridType boundary_grid = getBoundaryGrid(0, 1, grid, leftGrid);
        auto it2 = boundary_grid.begin();

        interpolate(it1, *grid, itEnd, it2, *leftGrid);
    } else {
        auto it1 = grid->leftGhostGrid.begin();
        auto itEnd = grid->leftGhostGrid.end();

        GridType boundary_grid = getBoundaryGrid(0, 0, grid, grid);
        auto it2 = boundary_grid.begin();
        reflect(it1, itEnd, it2);
    }

    if (rightGrid) {
        auto it1 = grid->rightGhostGrid.begin();
        auto itEnd = grid->rightGhostGrid.end();

        GridType boundary_grid = getBoundaryGrid(0, 0, grid, rightGrid);
        auto it2 = boundary_grid.begin();

        interpolate(it1, *grid, itEnd, it2, *rightGrid);
    } else {
        auto it1 = grid->rightGhostGrid.begin();
        auto itEnd = grid->rightGhostGrid.end();

        GridType boundary_grid = getBoundaryGrid(0, 1, grid, grid);
        auto it2 = boundary_grid.begin();

        reflect(it1, itEnd, it2);
    }

    if (bottomGrid) {
        auto it1 = grid->bottomGhostGrid.begin();
        auto itEnd = grid->bottomGhostGrid.end();

        GridType boundary_grid = getBoundaryGrid(1, 1, grid, bottomGrid);
        auto it2 = boundary_grid.begin();

        interpolate(it1, *grid, itEnd, it2, *bottomGrid);
    } else {
        auto it1 = grid->bottomGhostGrid.begin();
        auto itEnd = grid->bottomGhostGrid.end();

        GridType boundary_grid = getBoundaryGrid(1, 0, grid, grid);
        auto it2 = boundary_grid.begin();

        reflect(it1, itEnd, it2);
    }

    if (topGrid) {
        auto it1 = grid->topGhostGrid.begin();
        auto itEnd = grid->topGhostGrid.end();

        GridType boundary_grid = getBoundaryGrid(1, 0, grid, topGrid);
        auto it2 = boundary_grid.begin();

        interpolate(it1, *grid, itEnd, it2, *topGrid);
    } else {
        auto it1 = grid->topGhostGrid.begin();
        auto itEnd = grid->topGhostGrid.end();

        GridType boundary_grid = getBoundaryGrid(1, 1, grid, grid);
        auto it2 = boundary_grid.begin();

        reflect(it1, itEnd, it2);
    }

    if (backGrid) {
        auto it1 = grid->backGhostGrid.begin();
        auto itEnd = grid->backGhostGrid.end();

        GridType boundary_grid = getBoundaryGrid(2, 1, grid, backGrid);
        auto it2 = boundary_grid.begin();

        interpolate(it1, *grid, itEnd, it2, *backGrid);
    } else {
        auto it1 = grid->backGhostGrid.begin();
        auto itEnd = grid->backGhostGrid.end();

        GridType boundary_grid = getBoundaryGrid(2, 0, grid, grid);
        auto it2 = boundary_grid.begin();

        reflect(it1, itEnd, it2);

        // Input
        // typedef Uni::EulerEquations::Basic::Cell<double, 3> StaticCell;

        // StaticCell cell3;
        // cell3.density(0.8);
        // cell3.velocity({ 0.0, 0.0, 0.0 });
        // cell3.temperature(300.0);

        // for (auto const& accessor : grid->backGhostGrid) {
        // accessor.currentCell().copy(&cell3);
        // }
    }

    if (frontGrid) {
        auto it1 = grid->frontGhostGrid.begin();
        auto itEnd = grid->frontGhostGrid.end();

        GridType boundary_grid = getBoundaryGrid(2, 0, grid, frontGrid);
        auto it2 = boundary_grid.begin();

        interpolate(it1, *grid, itEnd, it2, *frontGrid);
    } else {
        auto it1 = grid->frontGhostGrid.begin();
        auto itEnd = grid->frontGhostGrid.end();

        GridType boundary_grid = getBoundaryGrid(2, 1, grid, grid);
        auto it2 = boundary_grid.begin();

        reflect(it1, itEnd, it2);
    }

    SchemeExecutor schemeExecutor(timeStepSize, gridWidth[0], gridWidth[1], gridWidth[2]);
    tbb::parallel_reduce(tbb::blocked_range<Grid::Iterator>(
                           grid->innerGrid.begin(), grid->innerGrid.end()),
                         schemeExecutor);

    double newTimeStepSize = Uni::Numerics::FiniteVolume::computeTimeStepSize(
      gridWidth, schemeExecutor.totalMaxLambda, EulerEquations_CFL);
    _data->minTimeStepSize(newTimeStepSize);

    _data->minDensity(schemeExecutor.minDensity);
    _data->maxDensity(schemeExecutor.maxDensity);
    _data->minVelocityNorm(schemeExecutor.minVelocityNorm);
    _data->maxVelocityNorm(schemeExecutor.maxVelocityNorm);
    _data->minTemperature(schemeExecutor.minTemperature);
    _data->maxTemperature(schemeExecutor.maxTemperature);
    _data->minPressure(schemeExecutor.minPressure);
    _data->maxPressure(schemeExecutor.maxPressure);

    grid->newTimeStamp(grid->currentTimeStamp() + timeStepSize);
    grid->timeStepSize(newTimeStepSize);
    grid->flip();
    _data->currentTime(grid->currentTimeStamp());

    // Local Time Stepping logic
    // Uni::Numerics::LocalTimeStepping::computeTimeStepPlotCount(
    // grid->currentTimeStamp(),
    // _plotInterval,
    // timeStepPlotCount,
    // newTimeStepSize);
    // _data->minPlotCount(timeStepPlotCount);
}
