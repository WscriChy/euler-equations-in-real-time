#ifndef EulerEquations_Simulation_Scenarios_BubbleScenario_hpp
#define EulerEquations_Simulation_Scenarios_BubbleScenario_hpp

#include "Simulation/Scenario.hpp"

namespace EulerEquations {
namespace Simulation {
namespace Scenarios {
class BubbleScenarioData : public ScenarioData {
public:
  typedef ScenarioData Base;

private:
public:
  BubbleScenarioData() : Base(),
                         _currentTime(std::numeric_limits<double>::max()),
                         _minPlotCount(std::numeric_limits<int>::max()),
                         _minTimeStepSize(std::numeric_limits<double>::max()),
                         _minDensity(std::numeric_limits<float>::max()),
                         _maxDensity(std::numeric_limits<float>::min()),
                         _minVelocityNorm(std::numeric_limits<float>::max()),
                         _maxVelocityNorm(std::numeric_limits<float>::min()),
                         _minTemperature(std::numeric_limits<float>::max()),
                         _maxTemperature(std::numeric_limits<float>::min()),
                         _minPressure(std::numeric_limits<float>::max()),
                         _maxPressure(std::numeric_limits<float>::min()) {}

  virtual
  ~BubbleScenarioData() {}

  virtual
  double const&
  currentTime() const {
    return _currentTime;
  }

  void
  currentTime(double const& currentTime) {
    _currentTime = currentTime;
  }

  int const&
  minPlotCount() const {
    return _minPlotCount;
  }

  void
  minPlotCount(int const& minPlotCount) {
    _minPlotCount = minPlotCount;
  }

  double const&
  minTimeStepSize() const {
    return _minTimeStepSize;
  }

  void
  minTimeStepSize(double const& minTimeStepSize) {
    _minTimeStepSize = minTimeStepSize;
  }

  virtual
  float const&
  minDensity() const {
    return _minDensity;
  }

  void
  minDensity(float const& minDensity) {
    _minDensity = minDensity;
  }

  virtual
  float const&
  maxDensity() const {
    return _maxDensity;
  }

  void
  maxDensity(float const& maxDensity) {
    _maxDensity = maxDensity;
  }

  virtual
  float const&
  minVelocityNorm() const {
    return _minVelocityNorm;
  }

  void
  minVelocityNorm(float const& minVelocityNorm) {
    _minVelocityNorm = minVelocityNorm;
  }

  virtual
  float const&
  maxVelocityNorm() const {
    return _maxVelocityNorm;
  }

  void
  maxVelocityNorm(float const& maxVelocityNorm) {
    _maxVelocityNorm = maxVelocityNorm;
  }

  virtual
  float const&
  minTemperature() const {
    return _minTemperature;
  }

  void
  minTemperature(float const& minTemperature) {
    _minTemperature = minTemperature;
  }

  virtual
  float const&
  maxTemperature() const {
    return _maxTemperature;
  }

  void
  maxTemperature(float const& maxTemperature) {
    _maxTemperature = maxTemperature;
  }


  virtual
  float const&
  minPressure() const {
    return _minPressure;
  }

  void
  minPressure(float const& minPressure) {
    _minPressure = minPressure;
  }

  virtual
  float const&
  maxPressure() const {
    return _maxPressure;
  }

  void
  maxPressure(float const& maxPressure) {
    _maxPressure = maxPressure;
  }

  virtual
  void
  join(ScenarioData::Shared scenarioData) {
    auto data = static_cast<BubbleScenarioData*>(scenarioData.get());

    _currentTime     = std::min(_currentTime, data->_currentTime);
    _minPlotCount    = std::min(_minPlotCount, data->_minPlotCount);
    _minTimeStepSize = std::min(_minTimeStepSize, data->_minTimeStepSize);

    _minDensity = std::min(_minDensity, data->_minDensity);
    _maxDensity = std::max(_maxDensity, data->_maxDensity);

    _minVelocityNorm = std::min(_minVelocityNorm, data->_minVelocityNorm);
    _maxVelocityNorm = std::max(_maxVelocityNorm, data->_maxVelocityNorm);

    _minTemperature = std::min(_minTemperature, data->_minTemperature);
    _maxTemperature = std::max(_maxTemperature, data->_maxTemperature);

    _minPressure = std::min(_minPressure, data->_minPressure);
    _maxPressure = std::max(_maxPressure, data->_maxPressure);
  }

private:
  double _currentTime;
  int    _minPlotCount;
  double _minTimeStepSize;

  float _maxDensity;
  float _minDensity;
  float _maxVelocityNorm;
  float _minVelocityNorm;
  float _maxTemperature;
  float _minTemperature;
  float _maxPressure;
  float _minPressure;
};

class BubbleScenarioRunner : public ScenarioRunner {
public:
  typedef ScenarioRunner         Base;
  typedef typename Base::Vector3 Vector3;

private:
public:
  BubbleScenarioRunner(ScenarioData::Shared input = ScenarioData::Shared())
    : Base(),
      _input(static_cast<BubbleScenarioData*>(input.get())),
      _inputScenarioData(input),
      _data(new BubbleScenarioData()),
      _scenarioData(_data),
      _plotInterval(0.01) {}

  virtual
  ~BubbleScenarioRunner() {}

  virtual
  ScenarioData::Shared
  data() const {
    return _scenarioData;
  }

  virtual
  void
  initialize(Grid*          grid,
             Vector3 const& position,
             Vector3 const& gridWidth);

  virtual
  void
  iterate(Grid*          leftGrid,
          Grid*          rightGrid,
          Grid*          bottomGrid,
          Grid*          topGrid,
          Grid*          backGrid,
          Grid*          frontGrid,
          Grid*          grid,
          Vector3 const& position,
          Vector3 const& gridWidth);

private:
  BubbleScenarioData*  _input;
  ScenarioData::Shared _inputScenarioData;
  BubbleScenarioData*  _data;
  ScenarioData::Shared _scenarioData;

  double _plotInterval;
};

class BubbleScenario : public Scenario {
public:
  typedef Scenario Base;

public:
  BubbleScenario() : Base() {
    xSize(3 * 26);
    ySize(3 * 26);
    zSize(3 * 26);
  }

  virtual
  ~BubbleScenario() {}

  virtual
  ScenarioRunner::Shared
  createRunner(ScenarioData::Shared scenarioData = ScenarioData::Shared()) {
    return ScenarioRunner::Shared(new BubbleScenarioRunner(scenarioData));
  }

  virtual
  ScenarioData::Shared
  createData() {
    return ScenarioData::Shared(new BubbleScenarioData());
  }
};
}
}
}

#endif
