#include "Solver.hpp"

#include "Grid.hpp"

#include "PeanoExecutor/repositories/Repository.h"

#include <Uni/Logging/macros>

#include <GL/glew.h>

using EulerEquations::Simulation::Solver;
using EulerEquations::Simulation::SolverProperties;

Solver::
Solver(PeanoExecutor::repositories::Repository* repository)
  : _repository(repository),
    _iterationNumber(0),
    _minDensity(0.0),
    _maxDensity(0.0) {}

Solver::
~Solver() {}

void
Solver::
scenario(Scenario::Shared scenario) {
  _scenario = scenario;
  _repository->getState().scenario(_scenario);
  initializeField();
}

float const&
Solver::
minDensity() const {
  return _minDensity;
}

float const&
Solver::
maxDensity() const {
  return _maxDensity;
}

float const&
Solver::
minVelocityNorm() const {
  return _minVelocityNorm;
}

float const&
Solver::
maxVelocityNorm() const {
  return _maxVelocityNorm;
}

float const&
Solver::
minTemperature() const {
  return _minTemperature;
}

float const&
Solver::
maxTemperature() const {
  return _maxTemperature;
}

float const&
Solver::
minPressure() const {
  return _minPressure;
}

float const&
Solver::
maxPressure() const {
  return _maxPressure;
}

Solver::SharedScalarField
Solver::
currentScalarField() const {
  return _renderScalarField;
}

void
Solver::
swapField() {
  auto temp = _renderScalarField;
  _renderScalarField = _taskScalarField;
  _taskScalarField   = temp;
}

void
Solver::
synchronize(void* pointer) {
  ((void)pointer);

  notify(SolverProperties::SimulationTime,
         (double const&)_repository->getState().scenarioData()->currentTime());

  notify(SolverProperties::IterationNumber,
         (int const&)_iterationNumber);

  notify(SolverProperties::MinDensity,
         (float const&)_minDensity);

  notify(SolverProperties::MaxDensity,
         (float const&)_maxDensity);
}

void
Solver::
initialize() {
  _repository->switchToSetupExperiment();
  _repository->iterate();
  releaseIteration();
  _repository->switchToPerformTimeStep();
}

void
Solver::
iterate() {
  _repository->iterate();
  releaseIteration();
}

void
Solver::
release() {}

void
Solver::
initializeField() {
  _renderScalarField = SharedScalarField(
    new ScalarField(_scenario->xSize() *
                    _scenario->ySize() *
                    _scenario->zSize(),
                    0.0));
  _taskScalarField = SharedScalarField(
    new ScalarField(_scenario->xSize() *
                    _scenario->ySize() *
                    _scenario->zSize(),
                    0.0));
}

bool
Solver::
advanceIndex(int&  index,
             bool& direction) const {
  if (direction) {
    if (index < 2) {
      ++index;
    } else {
      direction = !direction;

      return true;
    }
  } else {
    if (index > 0) {
      --index;
    } else {
      direction = !direction;

      return true;
    }
  }

  return false;
}

void
Solver::
releaseIteration() {
  ++_iterationNumber;
  _minDensity      = _repository->getState().scenarioData()->minDensity();
  _maxDensity      = _repository->getState().scenarioData()->maxDensity();
  _minVelocityNorm = _repository->getState().scenarioData()->minVelocityNorm();
  _maxVelocityNorm = _repository->getState().scenarioData()->maxVelocityNorm();
  _minTemperature  = _repository->getState().scenarioData()->minTemperature();
  _maxTemperature  = _repository->getState().scenarioData()->maxTemperature();
  _minPressure     = _repository->getState().scenarioData()->minPressure();
  _maxPressure     = _repository->getState().scenarioData()->maxPressure();

  int x = 0;

  bool xInc = true;
  int  y    = 0;
  bool yInc = true;
  int  z    = 0;
  bool zInc = true;

  for (int i = 0;
       i < GridHeap::getInstance().getNumberOfAllocatedEntries();
       ++i) {
    auto cellDescriptor = &GridHeap::getInstance().getData(
      i)[0];

    if (cellDescriptor->getSize(0) == 0) {
      continue;
    }

    Grid grid(cellDescriptor);

    for (auto const& accessor : grid.innerGrid) {
      int index =
        (accessor(0) - grid.innerGrid.leftIndent(0)) +
        (accessor(1) - grid.innerGrid.leftIndent(1)) * 3 *
        cellDescriptor->getSize(0) +
        (accessor(2) - grid.innerGrid.leftIndent(2)) * 9 *
        cellDescriptor->getSize(0) * cellDescriptor->getSize(1) +
        (x) * cellDescriptor->getSize(0) +
        (y) * 3 * cellDescriptor->getSize(0) * cellDescriptor->getSize(1) +
        (z) * 9 * cellDescriptor->getSize(0) *
        cellDescriptor->getSize(1) * cellDescriptor->getSize(2);
      _taskScalarField->at(index) = accessor.currentCell().density();

      // if (_minDensity > 0.4 &&
      // _maxDensity < 0.75 &&
      // _iterationNumber > 10000 &&
      // accessor.currentCell().density() < 0.65) {
      // INFO << accessor.currentCell();
      // INFO << accessor.currentCell().soundSpeed();
      // INFO << accessor.currentCell().pressure();
      // INFO << accessor.currentCell().enthalpy();
      // }
    }

    if (advanceIndex(x, xInc)) {
      if (advanceIndex(y, yInc)) {
        if (advanceIndex(z, zInc)) {}
      }
    }
  }
}
