#pragma once

#include "Cell.hpp"

#include <Uni/StructuredGrid/Basic/Grid>

#include <Uni/Numerics/OneStepMethod/ScalarCellAccessor>

namespace EulerEquations {
namespace Simulation {

class CellAccessor;

using GridType
  = Uni::StructuredGrid::Basic::Grid<Uni::StructuredGrid::Basic::DummyForThisGrid,
                                     CellAccessor,
                                     3>;

class CellAccessor : public Uni::Numerics::OneStepMethod::
                       ScalarCellAccessor<CellAccessor, GridType, Cell, 3> {

public:
    typedef Uni::Numerics::OneStepMethod::
      ScalarCellAccessor<CellAccessor, GridType, Cell, 3> Base;

    using VectorDi = Eigen::Matrix<int, 3, 1>;

    typedef typename Base::CellProvider CellProvider;

public:
    CellAccessor(GridType const* grid, VectorDi const& i, CellProvider* cellProvider);

    CellAccessor(CellAccessor const& other);

    ~CellAccessor();

    CellAccessor const& operator=(CellAccessor const& other);
};
}
}
