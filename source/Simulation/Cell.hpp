#ifndef EulerEquations_Simulation_Cell_hpp
#define EulerEquations_Simulation_Cell_hpp

#include "PeanoExecutor/records/Unknown.h"

#include <Uni/EulerEquations/Cell>

namespace EulerEquations {
namespace Simulation {
class Cell : public Uni::EulerEquations::Cell<double, 3> {
public:
  typedef Uni::EulerEquations::Cell<double, 3> Base;

public:
  typedef typename Base::Vector Vector;

  typedef PeanoExecutor::records::Unknown PersistentCell;

public:
  Cell(PersistentCell* persistentCell) : _persistentCell(persistentCell) {}

  Cell(Cell const& other) : _persistentCell(other._persistentCell) {}

  virtual
  ~Cell() {}

  Cell&
  operator=(Cell const& other) {
    _persistentCell = other._persistentCell;

    return *this;
  }

  double
  density() const {
    return _persistentCell->getDensity();
  }

  void
  density(double const& value) {
    _persistentCell->setDensity(value);
  }

  Vector
  velocity() const {
    return Vector(_persistentCell->getVelocity().data());
  }

  void
  velocity(Vector const& value) {
    _persistentCell->setVelocity(0, value(0));
    _persistentCell->setVelocity(1, value(1));
    _persistentCell->setVelocity(2, value(2));
  }

  void
  velocity(int const& index, double const& value) {
    _persistentCell->setVelocity(index, value);
  }

  double
  energy() const {
    return _persistentCell->getEnergy();
  }

  void
  energy(double const& value) {
    _persistentCell->setEnergy(value);
  }

  double
  marker() const {
    return _persistentCell->getMarker();
  }

  void
  marker(double const& value) {
    _persistentCell->setMarker(value);
  }

  double
  gasConstant() const {
    // R is a constant obtained by dividing the universal gas constant R
    // by the molecular weight of the gas

    return Base::gasConstant() / 18.0;
  }

  double
  adiabaticConstant() const {
    return 1.4;
  }

  double
  temperature() const {
    return Base::computeTemperatureFromDensityVelocityEnergy(
      density(),
      velocity(),
      energy(),
      adiabaticConstant(),
      gasConstant());
  }

  void
  temperature(double const& temperature) {
    energy(Base::computeEnergyFromDensityVelocityTemperature(
             density(),
             velocity(),
             temperature,
             adiabaticConstant(),
             gasConstant()));
  }

  double
  pressure() const {
    return Base::computePressureFromDensityVelocityEnergy(
      density(),
      velocity(),
      energy(),
      adiabaticConstant());
  }

  void
  pressure(double const& pressure) {
    energy(Base::computeEnergyFromDensityVelocityPressure(
             density(),
             velocity(),
             pressure,
             adiabaticConstant()));
  }

  double
  enthalpy() const {
    return Base::computeEnthalpyFromDensityVelocityEnergy(
      density(),
      velocity(),
      energy(),
      adiabaticConstant());
  }

  void
  enthalpy(double const& enthalpy) {
    energy(Base::computeEnergyFromDensityVelocityEnthalpy(
             density(),
             velocity(),
             enthalpy,
             adiabaticConstant()));
  }

  double
  soundSpeed() const {
    return Base::computeSoundSpeedFromDensityVelocityEnergy(
      density(),
      velocity(),
      energy(),
      adiabaticConstant());
  }

private:
  PersistentCell* _persistentCell;
};
}
}

#endif
