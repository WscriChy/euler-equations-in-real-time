#ifndef EulerEquations_Ui_Controller_hpp
#define EulerEquations_Ui_Controller_hpp

#include <Uni/InputMapping/InputMapper>
#include <Uni/InputMapping/MouseState>
#include <Uni/MotionSystem/MotionController>
#include <Uni/RingStack>
#include <Uni/Stopwatch>

#include <memory>

namespace EulerEquations {
namespace Ui {
/**
 * The main controller of the application.
 * Controlls Camera, Screen, Input, ...
 */
class Controller {
public:
  typedef std::shared_ptr<Controller>                Shared;
  typedef Uni::MotionSystem::MotionController<float> MotionController;
  typedef Uni::InputMapping::InputMapper             InputMapper;
  typedef Uni::InputMapping::MouseState              MouseState;

  typedef Stopwatch<Microseconds, SteadyClock> ConcreteClock;
  typedef typename ConcreteClock::Duration     Duration;

public:
  Controller();

  virtual
  ~Controller();

  MotionController::Shared
  motionController() const {
    return _motionController;
  }

  InputMapper::Shared
  inputMapper() const {
    return _inputMapper;
  }

  MouseState::Shared
  mouseState() const {
    return _mouseState;
  }

  /**
   * start not related to opengl context
   * start worke stopwatch and frame stopwatch
   * suppose that the work starts with the firs frame
   */
  void
  start();

  /**
   * elpsed time of the system in microseconds
   */
  Duration
  elapsedTime();

  /**
   *  slot to notify about new frame
   *  returns elapsed time in microseconds
   */
  Duration
  newFrame();

  /**
   * Compute and returns an average framte time in microseconds
   */
  Duration
  frameTime();

  float
  fps();

protected:
  MotionController::Shared _motionController;
  InputMapper::Shared      _inputMapper;
  MouseState::Shared       _mouseState;

  ConcreteClock       _workStopwatch;
  ConcreteClock       _frameStopwatch;
  RingStack<Duration> _fpsStack;
};
}
}

#endif
