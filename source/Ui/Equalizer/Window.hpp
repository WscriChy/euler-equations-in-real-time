#ifndef EulerEquations_Ui_Equalizer_Window
#define EulerEquations_Ui_Equalizer_Window

#include <eq/eq.h>

namespace EulerEquations {
namespace Ui {
namespace Equalizer {
/**
 * A window represent an OpenGL drawable and context
 *
 * Manages OpenGL-specific data, i.e., it creates the logo texture during
 * initialization and holds a state object for GL object creation. It
 * initializes the OpenGL state and draws the statistics overlay.
 */
class Window : public eq::Window {
public:
  Window(eq::Pipe* parent)
    : eq::
      Window(parent) {}

protected:
  virtual
  ~Window() {}

  virtual
  bool
  configInitSystemWindow(const eq::uint128_t& initID);

  virtual
  bool
  configInitGL(const eq::uint128_t& initID);

  virtual
  bool
  configExitGL();

  virtual
  void
  frameStart(const eq::uint128_t& frameID,
             const uint32_t       frameNumber);

private:
};
}
}
}

#endif
