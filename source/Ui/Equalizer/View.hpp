#ifndef EulerEquations_Ui_Equalizer_View
#define EulerEquations_Ui_Equalizer_View

#include <eq/eq.h>

#include <string>

namespace EulerEquations {
namespace Ui {
namespace Equalizer {
class View : public eq::View {
public:
  View(eq::Layout* parent);

  virtual
  ~View();
};
}
}
}

#endif
