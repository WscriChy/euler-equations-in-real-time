#include "Config.hpp"

#include <Uni/Logging/logging>

using Gui::Equalizer::Config;

Config::
Config(eq::ServerPtr                    parent,
       euler::repositories::Repository& repository)
  : eq::Config(parent),
  _controller(new Gui::Controller()),
  _worker(new IterativeWorker()),
  _settings(new GridRendererSceneSettings()) {
  // Task installation
  using namespace std::placeholders;

  _task = IterativeTask::Shared(new IterativeTask(repository));

  _worker->iteration(
    std::bind(&IterativeTask::iterate, _task));
  _worker->join(
    std::bind((void (IterativeTask::*)(void*)) & IterativeTask::settings, _task,
              _1));
}

bool
Config::
init() {
  // init config
  // lunchbox::uint128_t initID(1);

  _worker->synchronize(_settings.get());
  _worker->start();
  _worker->waitInitialization();
  _worker->waitIteration();

  _distributedObject.xSize = _settings->xSize();
  _distributedObject.ySize = _settings->ySize();
  _distributedObject.zSize = _settings->zSize();

  _distributedObject.scalarField = _task->currentScalarField();

  registerObject(&_distributedObject);

  if (!eq::Config::init(_distributedObject.getID())) {
    return false;
  }

  return true;
}

bool
Config::
exit() {
  const bool ret = eq::Config::exit();
  _closeAdminServer();

  return ret;
}

uint32_t
Config::
startFrame() {
  auto scalarField = _task->currentScalarField();

  if (_worker->synchronize(_settings.get())) {
    scalarField = _task->currentScalarField();
    _task->swapField();
    _worker->proceed();
  }

  _distributedObject.projection =
    _controller->motionController()->projection();

  _distributedObject.view = _controller->motionController()->view();

  _distributedObject.scalarField = scalarField;

  eq::uint128_t const& version = _distributedObject.update();

  return eq::Config::startFrame(version);
}

void
normalizerCoordinates(int const& width,
                      int const& height,
                      int const& currentWidth,
                      int const& currentHeight,
                      int const& x,
                      int const& y,
                      float&     xNormalized,
                      float&     yNormalized) {
  xNormalized = (float)x * (float)width / (float)currentWidth;
  yNormalized = (float)y * (float)height / (float)currentHeight;
}

bool
Config::
handleEvent(const eq::ConfigEvent* event) {
  using MotionSystem::Event;

  switch (event->data.type) {
  case eq::Event::KEY_PRESS: {
      if (event->data.keyPress.key == 'q') {
        _controller->inputMapper()->fire(Event::qDown);
      } else if (event->data.keyPress.key == 'w') {
        _controller->inputMapper()->fire(Event::wDown);
      } else if (event->data.keyPress.key == 'e') {
        _controller->inputMapper()->fire(Event::eDown);
      } else if (event->data.keyPress.key == 'a') {
        _controller->inputMapper()->fire(Event::aDown);
      } else if (event->data.keyPress.key == 's') {
        _controller->inputMapper()->fire(Event::sDown);
      } else if (event->data.keyPress.key == 'd') {
        _controller->inputMapper()->fire(Event::dDown);
      } else {
        break;
      }

      return true;

      break;
    }

  case eq::Event::KEY_RELEASE: {
      if (event->data.keyRelease.key == 'q') {
        _controller->inputMapper()->fire(Event::qUp);
      } else if (event->data.keyRelease.key == 'w') {
        _controller->inputMapper()->fire(Event::wUp);
      } else if (event->data.keyRelease.key == 'e') {
        _controller->inputMapper()->fire(Event::eUp);
      } else if (event->data.keyRelease.key == 'a') {
        _controller->inputMapper()->fire(Event::aUp);
      } else if (event->data.keyRelease.key == 's') {
        _controller->inputMapper()->fire(Event::sUp);
      } else if (event->data.keyRelease.key == 'd') {
        _controller->inputMapper()->fire(Event::dUp);
      } else {
        break;
      }

      return true;
    }

  case eq::Event::CHANNEL_POINTER_BUTTON_PRESS: {
      switch (event->data.pointerButtonPress.buttons) {
      case eq::PTR_BUTTON1:
        _controller->inputMapper()->fire(Event::mouseLeftButtionDown);

        return true;

      case eq::PTR_BUTTON2:
        _controller->inputMapper()->fire(Event::mouseRightButtionDown);

        return true;
      }
      break;
    }

  case eq::Event::CHANNEL_POINTER_BUTTON_RELEASE: {
      switch (event->data.pointerButtonRelease.buttons) {
      case eq::PTR_BUTTON1:
        _controller->inputMapper()->fire(Event::mouseLeftButtionUp);

        return true;

      case eq::PTR_BUTTON2:
        _controller->inputMapper()->fire(Event::mouseRightButtionUp);

        return true;
      }
      break;
    }

  case eq::Event::CHANNEL_POINTER_MOTION: {
      switch (event->data.pointerMotion.buttons) {
      case eq::PTR_BUTTON1:
        float x, y;
        normalizerCoordinates(400, 400,
                              event->data.context.pvp.w,
                              event->data.context.pvp.h,
                              event->data.pointerMotion.x,
                              event->data.pointerMotion.y,
                              x, y);
        _controller->inputMapper()->fire(Event::mouseMove,
                                         x, y);

        return true;

      case eq::PTR_BUTTON2:
        break;

      case eq::PTR_BUTTON3:
        break;
      }
      break;
    }

  case eq::Event::CHANNEL_POINTER_WHEEL: {
      return true;
    }

  case eq::Event::MAGELLAN_AXIS: {
      return true;
    }

  case eq::Event::MAGELLAN_BUTTON: {
      return true;
    }

  case eq::Event::WINDOW_EXPOSE:
  case eq::Event::WINDOW_RESIZE:
    _controller->inputMapper()->fire(Event::resize,
                                     400, 400);
  // event->data.resize.w,
  // event->data.resize.h);

  case eq::Event::WINDOW_CLOSE:
  case eq::Event::VIEW_RESIZE:
    break;

  default:
    break;
  }

  bool result = eq::Config::handleEvent(event);

  return result;
}

bool
Config::
handleEvent(eq::EventICommand command) {
  switch (command.getEventType()) {
  default:
    break;
  }

  bool result = eq::Config::handleEvent(command);

  return result;
}

bool
Config::
_handleKeyEvent(const eq::KeyEvent& event) {
  using MotionSystem::Event;

  if (event.key == 'q') {
    _controller->inputMapper()->fire(Event::qDown);
  } else if (event.key == 'w') {
    _controller->inputMapper()->fire(Event::wDown);
  } else if (event.key == 'e') {
    _controller->inputMapper()->fire(Event::eDown);
  } else if (event.key == 'a') {
    _controller->inputMapper()->fire(Event::aDown);
  } else if (event.key == 's') {
    _controller->inputMapper()->fire(Event::sDown);
  } else if (event.key == 'd') {
    _controller->inputMapper()->fire(Event::dDown);
  }

  return true;
}

co::uint128_t
Config::
sync(const co::uint128_t& version) {
  if (_admin.isValid() && _admin->isConnected()) {
    _admin->syncConfig(getID(), version);
  }

  return eq::Config::sync(version);
}

eq::admin::ServerPtr
Config::
_getAdminServer() {
  // Debug: _closeAdminServer();
  if (_admin.isValid() && _admin->isConnected()) {
    return _admin;
  }

  eq::admin::init(0, 0);
  eq::admin::ClientPtr client = new eq::admin::Client;

  if (!client->initLocal(0, 0)) {
    logFatal("Can't init admin client");
    eq::admin::exit();
  }

  _admin = new eq::admin::Server;

  if (!client->connectServer(_admin)) {
    logFatal("Can't open connection to administrate server");
    client->exitLocal();
    _admin = 0;
    eq::admin::exit();
  }

  return _admin;
}

void
Config::
_closeAdminServer() {
  if (!_admin) {
    return;
  }

  eq::admin::ClientPtr client = _admin->getClient();
  client->disconnectServer(_admin);
  client->exitLocal();

  if (client->getRefCount() != 1) {
    logFatal("Error");
  }

  if (_admin->getRefCount() != 1) {
    logFatal("Error");
  }

  _admin = 0;
  eq::admin::exit();
}
