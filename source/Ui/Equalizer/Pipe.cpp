#include "Pipe.hpp"

#include <Uni/Logging/logging>

#include <eq/eq.h>

using Gui::Equalizer::Pipe;

eq::WindowSystem
Pipe::
selectWindowSystem() const {
  return eq::WindowSystem();
}

bool
Pipe::
configInit(eq::uint128_t const& initID) {
  if (!eq::Pipe::configInit(initID)) {
    return false;
  }

  eq::Config* config = getConfig();

  return config->mapObject(&_distributedObject, initID);
}

bool
Pipe::
configExit() {
  eq::Config* config = getConfig();
  config->unmapObject(&_distributedObject);

  return eq::Pipe::configExit();
}

void
Pipe::
frameStart(eq::uint128_t const& frameID,
           uint32_t const       frameNumber) {
  eq::Pipe::frameStart(frameID, frameNumber);
  _distributedObject.sync(frameID);
}
