#ifndef EulerEquations_Ui_Equalizer_DistributedObject
#define EulerEquations_Ui_Equalizer_DistributedObject

#include <Gui/Controller.hpp>

#include <co/co.h>
#include <eq/eq.h>

#include <vector>

namespace EulerEquations {
namespace Ui {
namespace Equalizer {
/**
 * Frame-specific data.
 *
 * The frame-specific data is used as a per-config distributed object and
 * contains mutable, rendering-relevant data. Each rendering thread (pipe) keeps
 * its own instance synchronized with the frame currently being rendered. The
 * data is managed by the Config, which modifies it directly.
 */
class DistributedObject : public co::Serializable {
public:
  typedef typename Gui::Controller::MotionController::Matrix4x4 Matrix4x4;

  typedef std::vector<float> ScalarField;

public:
  DistributedObject();

  virtual
  ~DistributedObject() {}

  eq::uint128_t
  update();

protected:
  /** @sa Object::serialize() */
  virtual
  void
  serialize(co::DataOStream& os,
            uint64_t const   dirtyBits);

  /** @sa Object::deserialize() */
  virtual
  void
  deserialize(co::DataIStream& is,
              uint64_t const   dirtyBits);

  virtual
  ChangeType
  getChangeType() const { return DELTA; }

public:
  int xSize;
  int ySize;
  int zSize;

  Matrix4x4 projection;
  Matrix4x4 view;

  float densityMax;
  float densityMin;

  ScalarField* scalarField;
};
}
}
}

#endif
