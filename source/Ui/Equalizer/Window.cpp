#include "Window.hpp"

#include "Pipe.hpp"

using Gui::Equalizer::Window;

bool
Window::
configInitSystemWindow(const eq::uint128_t& initID) {
  return eq::Window::configInitSystemWindow(initID);
}

bool
Window::
configInitGL(const eq::uint128_t& initID) {
  if (!eq::Window::configInitGL(initID)) {
    return false;
  }

  return true;
}

bool
Window::
configExitGL() {
  return eq::Window::configExitGL();
}

void
Window::
frameStart(const eq::uint128_t& frameID, const uint32_t frameNumber) {
  eq::Window::frameStart(frameID, frameNumber);
}
