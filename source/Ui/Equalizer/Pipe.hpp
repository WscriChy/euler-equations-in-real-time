#ifndef EulerEquations_Ui_Equalizer_Pipe
#define EulerEquations_Ui_Equalizer_Pipe

#include "DistributedObject.hpp"

#include <eq/eq.h>

namespace EulerEquations {
namespace Ui {
namespace Equalizer {
/**
 * The representation of one GPU.
 *
 * The pipe object is responsible for maintaining GPU-specific and
 * frame-specific data. The identifier passed by the configuration contains
 * the version of the frame data corresponding to the rendered frame. The
 * pipe's start frame callback synchronizes the thread-local instance of the
 * frame data to this version.
 */
class Pipe : public eq::Pipe {
public:
  Pipe(eq::Node* parent) : eq::
                           Pipe(parent) {}

protected:
  virtual
  ~Pipe() {}

public:
  DistributedObject const&
  distributedObject() const {
    return _distributedObject;
  }

protected:
  virtual
  eq::WindowSystem
  selectWindowSystem() const;

  virtual
  bool
  configInit(eq::uint128_t const& initID);

  virtual
  bool
  configExit();

  virtual
  void
  frameStart(eq::uint128_t const& frameID,
             uint32_t const       frameNumber);

private:
  DistributedObject _distributedObject;
};
}
}
}

#endif
