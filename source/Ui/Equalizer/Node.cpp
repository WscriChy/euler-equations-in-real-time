#include "Node.hpp"

#include "Config.hpp"

using Gui::Equalizer::Node;

bool
Node::
configInit(const eq::uint128_t& initID) {
  // All render data is static or multi-buffered, we can run asynchronously
  if (getIAttribute(IATTR_THREAD_MODEL) == eq::UNDEFINED) {
    setIAttribute(IATTR_THREAD_MODEL, eq::ASYNC);
  }

  if (!eq::Node::configInit(initID)) {
    return false;
  }

  return true;
}
