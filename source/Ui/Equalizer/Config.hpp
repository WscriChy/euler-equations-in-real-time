#ifndef EulerEquations_Ui_Equalizer_Config
#define EulerEquations_Ui_Equalizer_Config

#include "DistributedObject.hpp"

#include "IterativeWorker.hpp"

#include "Gui/Controller.hpp"
#include "Gui/GridRendererSceneSettings.hpp"
#include "Gui/IterativeTask.hpp"

#include "repositories/Repository.h"

#include <eq/admin/base.h>
#include <eq/eq.h>

namespace EulerEquations {
namespace Ui {
namespace Equalizer {
class View;

/**
 * A configuration instance manages configuration-specific data: it
 * distributes the initialization and model data, updates frame-specific
 * data and manages frame generation based on event handling.
 */
class Config : public eq::Config {
public:
  Config(eq::ServerPtr                    parent,
         euler::repositories::Repository& repository);

protected:
  virtual
  ~Config() {}

public:
  /** @sa eq::Config::init. */
  virtual
  bool
  init();

  /** @sa eq::Config::exit. */
  virtual
  bool
  exit();

  /** @sa eq::Config::startFrame. */
  virtual
  uint32_t
  startFrame();

  /** @sa eq::Config::handleEvent */
  virtual
  bool
  handleEvent(const eq::ConfigEvent* event);

  virtual
  bool
  handleEvent(eq::EventICommand command);

protected:
  /** Synchronize config and admin copy. */
  virtual
  co::uint128_t
  sync(co::uint128_t const& version = co::VERSION_HEAD);

private:
  eq::admin::ServerPtr _admin;

  bool
  _handleKeyEvent(const eq::KeyEvent& event);

  /** @return a pointer to a connected admin server. */
  eq::admin::ServerPtr
  _getAdminServer();

  void
  _closeAdminServer();

  DistributedObject _distributedObject;

  Gui::Controller::Shared _controller;

  IterativeWorker::Shared _worker;
  IterativeTask::Shared   _task;

  GridRendererSceneSettings::Shared _settings;
};
}
}
}

#endif
