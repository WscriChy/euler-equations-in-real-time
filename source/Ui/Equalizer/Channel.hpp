#ifndef EulerEquations_Ui_Equalizer_Channel
#define EulerEquations_Ui_Equalizer_Channel

#include "RootSceneWithIterativeWorker.hpp"

#include "Gui/GridRendererSceneSettings.hpp"

#include <eq/eq.h>

class GridRendererScene2;

namespace EulerEquations {
namespace Ui {
namespace Equalizer {
/**
 * The rendering entity, updating a part of a Window.
 */
class Channel : public eq::Channel {
public:
  Channel(eq::Window* parent);

protected:
  virtual
  ~Channel();

  virtual
  bool
  configInit(const eq::uint128_t& initID);

  virtual
  bool
  configExit();

  virtual
  void
  frameClear(eq::uint128_t const& frameID);

  virtual
  void
  frameDraw(eq::uint128_t const& frameID);

  virtual
  void
  frameAssemble(eq::uint128_t const& frameID);

  virtual
  void
  frameReadback(eq::uint128_t const& frameID);

  virtual
  void
  frameStart(eq::uint128_t const& frameID,
             uint32_t const       frameNumber);

  virtual
  void
  frameFinish(eq::uint128_t const& frameID,
              uint32_t const       frameNumber);

  virtual
  void
  frameViewStart(eq::uint128_t const& frameID);

  virtual
  void
  frameViewFinish(eq::uint128_t const& frameID);

private:
  Gui::GridRendererSceneSettings::Shared _settings;
  GridRendererScene2*                    _scene;
  unsigned int                           _scalarFieldBuffer;
};
}
}
}

#endif
