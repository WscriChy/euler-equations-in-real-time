#include "DistributedObject.hpp"

#include <Uni/Logging/logging>

using Gui::Equalizer::DistributedObject;

DistributedObject::
DistributedObject() :
  scalarField(0) {}

eq::uint128_t
DistributedObject::
update() {
  setDirty(DIRTY_ALL);

  return commit();
}

void
DistributedObject::
serialize(co::DataOStream& os, const uint64_t dirtyBits) {
  co::Serializable::serialize(os, dirtyBits);

  os << xSize << ySize << zSize;

  os << projection(0, 0);
  os << projection(0, 1);
  os << projection(0, 2);
  os << projection(0, 3);
  os << projection(1, 0);
  os << projection(1, 1);
  os << projection(1, 2);
  os << projection(1, 3);
  os << projection(2, 0);
  os << projection(2, 1);
  os << projection(2, 2);
  os << projection(2, 3);
  os << projection(3, 0);
  os << projection(3, 1);
  os << projection(3, 2);
  os << projection(3, 3);

  os << view(0, 0);
  os << view(0, 1);
  os << view(0, 2);
  os << view(0, 3);
  os << view(1, 0);
  os << view(1, 1);
  os << view(1, 2);
  os << view(1, 3);
  os << view(2, 0);
  os << view(2, 1);
  os << view(2, 2);
  os << view(2, 3);
  os << view(3, 0);
  os << view(3, 1);
  os << view(3, 2);
  os << view(3, 3);

  os << scalarField->size();

  for (int i = 0; i < scalarField->size(); ++i) {
    auto value = scalarField->at(i);

    os << value;
  }
}

void
DistributedObject::
deserialize(co::DataIStream& is, const uint64_t dirtyBits) {
  co::Serializable::deserialize(is, dirtyBits);

  is >> xSize >> ySize >> zSize;

  is >> projection(0, 0);
  is >> projection(0, 1);
  is >> projection(0, 2);
  is >> projection(0, 3);
  is >> projection(1, 0);
  is >> projection(1, 1);
  is >> projection(1, 2);
  is >> projection(1, 3);
  is >> projection(2, 0);
  is >> projection(2, 1);
  is >> projection(2, 2);
  is >> projection(2, 3);
  is >> projection(3, 0);
  is >> projection(3, 1);
  is >> projection(3, 2);
  is >> projection(3, 3);

  is >> view(0, 0);
  is >> view(0, 1);
  is >> view(0, 2);
  is >> view(0, 3);
  is >> view(1, 0);
  is >> view(1, 1);
  is >> view(1, 2);
  is >> view(1, 3);
  is >> view(2, 0);
  is >> view(2, 1);
  is >> view(2, 2);
  is >> view(2, 3);
  is >> view(3, 0);
  is >> view(3, 1);
  is >> view(3, 2);
  is >> view(3, 3);

  ScalarField::size_type size;

  is >> size;

  if (scalarField == 0) {
    scalarField = new ScalarField(size);
  }

  if (scalarField->size() < size) {
    scalarField->resize(size);
  }

  for (int i = 0; i < size; ++i) {
    ScalarField::value_type value;
    is >> value;
    scalarField->at(i) = value;
  }
}
