#ifndef EulerEquations_Ui_Equalizer_Node
#define EulerEquations_Ui_Equalizer_Node

#include <eq/eq.h>

namespace EulerEquations {
namespace Ui {
namespace Equalizer {
/**
 * Representation of a node in the cluster
 *
 * Manages node-specific data, namely requesting the mapping of the
 * initialization data by the local Config instance.
 */
class Node : public eq::Node {
public:
  Node(eq::Config* parent)
    : eq::
      Node(parent) {}

protected:
  virtual
  ~Node() {}

  virtual
  bool
  configInit(const eq::uint128_t& initID);

private:
};
}
}
}

#endif
