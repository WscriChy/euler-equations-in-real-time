#ifndef EulerEquations_Ui_Equalizer_Application
#define EulerEquations_Ui_Equalizer_Application

#include <eq/eq.h>

namespace EulerEquations {
namespace Ui {
namespace Equalizer {
/** The Equalizer application instance */
class Application : public eq::Client {
public:
  Application();

private:
  virtual
  ~Application() {}

public:
  int
  run();

protected:
  /** @sa eq::Client::clientLoop. */
  virtual
  void
  clientLoop();
};
}
}
}
#endif
