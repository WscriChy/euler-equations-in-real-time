#include "Application.hpp"

#include "Config.hpp"

#include <Uni/Logging/logging>

using Gui::Equalizer::Application;

Application::
Application()
{}

int
Application::
run() {
  // 1. connect to server
  eq::ServerPtr server = new eq::Server;

  if (!connectServer(server)) {
    ERROR << "Can't open server";

    return EXIT_FAILURE;
  }

  // 2. choose config
  eq::fabric::ConfigParams configParams;
  Config*                  config = static_cast<Config*>(
    server->chooseConfig(configParams));

  if (!config) {
    FATAL << "No matching config on server";
    disconnectServer(server);

    return EXIT_FAILURE;
  }

  // 3. init config
  lunchbox::Clock clock;

  if (!config->init()) {
    server->releaseConfig(config);
    disconnectServer(server);

    return EXIT_FAILURE;
  }
  INFO << "Config init took " << clock.getTimef() << " ms";

  clock.reset();

  while (config->isRunning()) {
    config->startFrame();
    config->finishFrame();

    while (false) { // wait for an event requiring redraw
      if (hasCommands()) { // execute non-critical pending commands
        processCommand();
        config->handleEvents(); // non-blocking
      } else { // no pending commands, block on user event
        const eq::EventICommand& event = config->getNextEvent();

        if (!config->handleEvent(event)) {
          LBVERB << "Unhandled " << event;
        }
      }
    }

    config->handleEvents(); // process all pending events
  }

  const uint32_t frame   = config->finishAllFrames();

  // 5. exit config

  // 6. cleanup and exit
  server->releaseConfig(config);

  if (!disconnectServer(server)) {
    ERROR << "Client::disconnectServer failed";
  }

  return EXIT_SUCCESS;
}

void
Application::
clientLoop() {
  do {
    eq::Client::clientLoop();
    INFO << "Configuration run successfully executed";
  } while (true); // execute at least one config run
}
