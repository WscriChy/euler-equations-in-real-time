#include "Channel.hpp"

#include "Pipe.hpp"

#include "GridRendererScene2.hpp"
#include <Uni/Logging/logging>

using Gui::Equalizer::Channel;

Channel::
Channel(eq::Window* parent) : eq::Channel(parent),
  _settings(new GridRendererSceneSettings()),
  _scene(new GridRendererScene2()) {}

Channel::
~Channel() {
  delete _scene;
}

bool
Channel::
configInit(const eq::uint128_t& initID) {
  if (!eq::Channel::configInit(initID)) {
    return false;
  }

  Pipe const* pipe = static_cast<Pipe const*>(getPipe());

  auto const& distributedObject = pipe->distributedObject();

  _settings->xSize() = distributedObject.xSize;
  _settings->ySize() = distributedObject.ySize;
  _settings->zSize() = distributedObject.zSize;

  _scene->initialize(_settings->xSize(),
                     _settings->ySize(),
                     _settings->zSize());

  return true;
}

bool
Channel::
configExit() {
  _scene->release();

  return eq::Channel::configExit();
}

void
Channel::
frameClear(const eq::uint128_t& frameID) {
  //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
  //eq::Channel::frameClear(frameID);
}

void
Channel::
frameDraw(const eq::uint128_t& frameID) {
  Pipe const* pipe              = static_cast<Pipe const*>(getPipe());
  auto const& distributedObject = pipe->distributedObject();

  eq::Channel::frameDraw(frameID);

  eq::Matrix4f const vmmProjection = getFrustum().compute_matrix();

  DistributedObject::Matrix4x4 projection(
    vmmProjection);
  // distributedObject.projection);

  _scene->render(projection,
                 distributedObject.view,
                 distributedObject.scalarField);
}

void
Channel::
frameAssemble(const eq::uint128_t& frameID) {
  eq::Channel::frameAssemble(frameID);
}

void
Channel::
frameReadback(const eq::uint128_t& frameID) {
  eq::Channel::frameReadback(frameID);
}

void
Channel::
frameStart(const eq::uint128_t& frameID,
           const uint32_t       frameNumber) {
  eq::Channel::frameStart(frameID, frameNumber);
}

void
Channel::
frameViewStart(const eq::uint128_t& frameID) {
  eq::Channel::frameViewStart(frameID);
}

void
Channel::
frameFinish(const eq::uint128_t& frameID,
            const uint32_t       frameNumber) {
  eq::Channel::frameFinish(frameID, frameNumber);
}

void
Channel::
frameViewFinish(const eq::uint128_t& frameID) {
  eq::Channel::frameViewFinish(frameID);
}
