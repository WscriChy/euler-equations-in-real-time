#include "Controller.hpp"

#include <Uni/Logging/macros>

#include <Uni/MotionSystem/Basic/ContinuousKeyPress>
#include <Uni/MotionSystem/Basic/MouseRotation>
#include <Uni/MotionSystem/Basic/OneVector2TwoScalarsMotion>
#include <Uni/MotionSystem/Basic/SixScalarsMotion>
#include <Uni/MotionSystem/ScreenState>

using EulerEquations::Ui::Controller;

Controller::
Controller()
  : _motionController(new MotionController()),
    _inputMapper(new InputMapper()),
    _mouseState(new MouseState()) {
  _fpsStack.length(30);

  using ContinuousKeyPress = Uni::MotionSystem::Basic::ContinuousKeyPress<>;
  using MouseRotation      = Uni::MotionSystem::Basic::MouseRotation<float>;
  using ScreenState        = Uni::MotionSystem::ScreenState<float>;

  using SixScalarsMotion = Uni::MotionSystem::Basic::SixScalarsMotion<
          float, typename ContinuousKeyPress::Duration>;
  using ScalarProviderShared =
          typename SixScalarsMotion::ScalarProviderShared;

  using OneVector2TwoScalarsMotion =
          Uni::MotionSystem::Basic::OneVector2TwoScalarsMotion<
            float, typename ContinuousKeyPress::Duration, float>;

  ScreenState::Shared screenStateShared(new ScreenState());
  screenStateShared->fovY(3.1415926 / 2.0);
  screenStateShared->nearZ(0.1);
  screenStateShared->farZ(1000);

  std::shared_ptr<bool> isMouseRotationOn(new bool[1]);
  isMouseRotationOn.get()[0] = false;
  OneVector2TwoScalarsMotion::Vector2ProviderShared mrVector2Provider(
    new MouseRotation());
  auto mouseRotation =
    std::static_pointer_cast<MouseRotation>(mrVector2Provider);
  mouseRotation->screenState(screenStateShared);

  OneVector2TwoScalarsMotion::ScalarProviderShared rScalarProvider(
    new ContinuousKeyPress());
  OneVector2TwoScalarsMotion::ScalarProviderShared fScalarProvider(
    new ContinuousKeyPress());

  auto rContinuousPress =
    std::static_pointer_cast<ContinuousKeyPress>(rScalarProvider);
  auto fContinuousPress =
    std::static_pointer_cast<ContinuousKeyPress>(fScalarProvider);

  auto rotation = new OneVector2TwoScalarsMotion();
  rotation->speed(OneVector2TwoScalarsMotion::Vector(
                    { 0.03, 0.03, 0.0000000003 }));

  rotation->vector2Provider(mrVector2Provider);
  rotation->scalarProvider(0, rScalarProvider);
  rotation->scalarProvider(1, fScalarProvider);

  ScalarProviderShared aScalarProvider(new ContinuousKeyPress());
  ScalarProviderShared dScalarProvider(new ContinuousKeyPress());
  ScalarProviderShared eScalarProvider(new ContinuousKeyPress());
  ScalarProviderShared sScalarProvider(new ContinuousKeyPress());
  ScalarProviderShared qScalarProvider(new ContinuousKeyPress());
  ScalarProviderShared wScalarProvider(new ContinuousKeyPress());

  auto aContinuousPress = std::static_pointer_cast<ContinuousKeyPress>(
    aScalarProvider);
  auto dContinuousPress = std::static_pointer_cast<ContinuousKeyPress>(
    dScalarProvider);
  auto eContinuousPress = std::static_pointer_cast<ContinuousKeyPress>(
    eScalarProvider);
  auto sContinuousPress = std::static_pointer_cast<ContinuousKeyPress>(
    sScalarProvider);
  auto qContinuousPress = std::static_pointer_cast<ContinuousKeyPress>(
    qScalarProvider);
  auto wContinuousPress = std::static_pointer_cast<ContinuousKeyPress>(
    wScalarProvider);

  auto movement = new SixScalarsMotion();
  movement->speed(SixScalarsMotion::Vector(
                    { 0.000000005, 0.000000005, 0.000000005 }));
  movement->scalarProvider(0, aScalarProvider);
  movement->scalarProvider(1, dScalarProvider);
  movement->scalarProvider(2, qScalarProvider);
  movement->scalarProvider(3, eScalarProvider);
  movement->scalarProvider(4, sScalarProvider);
  movement->scalarProvider(5, wScalarProvider);

  _motionController->screenState(screenStateShared);
  _motionController->rotation(
    MotionController::MotionShared(rotation));
  _motionController->movement(
    MotionController::MotionShared(movement));

  int inputPriority = 2;
  using Uni::InputMapping::Event;
  using namespace std::placeholders;

  _inputMapper->handleAndReturnFalse(
    Event::resize,
    screenStateShared, &ScreenState::size);

  _inputMapper->handle(
    Event::mouseMove,
    [ = ] (float const&, float const&) {
      if (isMouseRotationOn.get()[0]) {
        mouseRotation->advance(-_mouseState->deltaX(),
                               -_mouseState->deltaY());
        //_mouseState->position(
        //  _mouseState->x() - _mouseState->deltaX(),
        //  _mouseState->y() - _mouseState->deltaY()
        //  );

        return true;
      }

      return false;
    }, inputPriority);

  _inputMapper->handleAndReturnTrue(
    Event::mouseLeftButtonDown,
    [ = ] () {
      isMouseRotationOn.get()[0] = true;
    }, inputPriority);

  _inputMapper->handleAndReturnFalse(
    Event::mouseLeftButtonUp,
    [ = ] () {
      isMouseRotationOn.get()[0] = false;
    }, 0);

  _inputMapper->handleAndReturnFalse(
    Event::focusOut,
    [ = ] () {
      isMouseRotationOn.get()[0] = false;
      aContinuousPress->stop();
      dContinuousPress->stop();
      eContinuousPress->stop();
      fContinuousPress->stop();
      rContinuousPress->stop();
      sContinuousPress->stop();
      qContinuousPress->stop();
      wContinuousPress->stop();
    });
  _inputMapper->handleAndReturnFalse(
    Event::catchedMouseLeftButtonUp,
    [ = ] () {
      isMouseRotationOn.get()[0] = false;
      aContinuousPress->stop();
      dContinuousPress->stop();
      eContinuousPress->stop();
      fContinuousPress->stop();
      rContinuousPress->stop();
      sContinuousPress->stop();
      qContinuousPress->stop();
      wContinuousPress->stop();
    },
    0);

  _inputMapper->handleAndReturnTrue(
    Event::rDown,
    [ = ] (Uni::Core::String const&) {
      rContinuousPress->ContinuousKeyPress::start();
    }, inputPriority);
  _inputMapper->handleAndReturnFalse(
    Event::rUp,
    rContinuousPress, &ContinuousKeyPress::stop, 0);

  _inputMapper->handleAndReturnTrue(
    Event::fDown,
    [ = ] (Uni::Core::String const&) {
      fContinuousPress->ContinuousKeyPress::start();
    }, inputPriority);
  _inputMapper->handleAndReturnFalse(
    Event::fUp,
    fContinuousPress, &ContinuousKeyPress::stop, 0);

  _inputMapper->handleAndReturnTrue(
    Event::aDown,
    [ = ] (Uni::Core::String const&) {
      aContinuousPress->ContinuousKeyPress::start();
    }, inputPriority);
  _inputMapper->handleAndReturnTrue(
    Event::dDown,
    [ = ] (Uni::Core::String const&) {
      dContinuousPress->ContinuousKeyPress::start();
    }, inputPriority);
  _inputMapper->handleAndReturnTrue(
    Event::eDown,
    [ = ] (Uni::Core::String const&) {
      eContinuousPress->ContinuousKeyPress::start();
    }, inputPriority);
  _inputMapper->handleAndReturnTrue(
    Event::sDown,
    [ = ] (Uni::Core::String const&) {
      sContinuousPress->ContinuousKeyPress::start();
    }, inputPriority);
  _inputMapper->handleAndReturnTrue(
    Event::qDown,
    [ = ] (Uni::Core::String const&) {
      qContinuousPress->ContinuousKeyPress::start();
    }, inputPriority);
  _inputMapper->handleAndReturnTrue(
    Event::wDown,
    [ = ] (Uni::Core::String const&) {
      wContinuousPress->ContinuousKeyPress::start();
    }, inputPriority);

  _inputMapper->handleAndReturnFalse(
    Event::aUp,
    aContinuousPress, &ContinuousKeyPress::stop, 0);
  _inputMapper->handleAndReturnFalse(
    Event::dUp,
    dContinuousPress, &ContinuousKeyPress::stop, 0);
  _inputMapper->handleAndReturnFalse(
    Event::eUp,
    eContinuousPress, &ContinuousKeyPress::stop, 0);
  _inputMapper->handleAndReturnFalse(
    Event::sUp,
    sContinuousPress, &ContinuousKeyPress::stop, 0);
  _inputMapper->handleAndReturnFalse(
    Event::qUp,
    qContinuousPress, &ContinuousKeyPress::stop, 0);
  _inputMapper->handleAndReturnFalse(
    Event::wUp,
    wContinuousPress, &ContinuousKeyPress::stop, 0);

  // Set up Mouse state object

  _inputMapper->handleAndReturnFalse(
    Event::mouseLeftButtonDown, [ = ] () {
      _mouseState->isLeftButttonPressed(true);
    });
  _inputMapper->handleAndReturnFalse(
    Event::mouseLeftButtonUp, [ = ] () {
      _mouseState->isLeftButttonPressed(false);
    });
  _inputMapper->handleAndReturnFalse(
    Event::mouseRightButtonDown, [ = ] () {
      _mouseState->isRightButttonPressed(true);
    });
  _inputMapper->handleAndReturnFalse(
    Event::mouseRightButtonUp, [ = ] () {
      _mouseState->isRightButttonPressed(false);
    });
  _inputMapper->handleAndReturnFalse(
    Event::mouseMove, [ = ] (float const& x, float const& y) {
      _mouseState->position(x, y);
    });
}

Controller::
~Controller() {}

void
Controller::
start() {
  _workStopwatch.start();
  _frameStopwatch.start();
}

Controller::Duration
Controller::
elapsedTime() {
  return _workStopwatch.elapsed();
}

Controller::Duration
Controller::
newFrame() {
  Duration elapsedTime = _frameStopwatch.elapsed();
  _frameStopwatch.reset();
  _fpsStack.add(elapsedTime);

  return elapsedTime;
}

Controller::Duration
Controller::
frameTime() {
  Duration sum(0);

  for (auto const& frameTime : _fpsStack) {
    sum += frameTime;
  }

  sum /= _fpsStack.size();

  return sum;
}

float
Controller::
fps() {
  float sum = 0.0f;

  for (auto const& frameTime : _fpsStack) {
    sum += (float)frameTime;
  }
  sum = ((float)_fpsStack.size() * 1000000.0f) / sum;

  return sum;
}
