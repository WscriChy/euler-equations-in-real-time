#ifndef EulerEquations_Ui_Qt_MainWindow_hpp
#define EulerEquations_Ui_Qt_MainWindow_hpp

#include "Ui/Controller.hpp"
#include "Ui/GuiHub.hpp"

#include "Simulation/Scenario.hpp"
#include "Simulation/Solver.hpp"

#include "Visualization/GridRenderer.hpp"

#include <Uni/IterativeWorker>
#include <Uni/Switch>

#include <QtCore/QPoint>
#include <QtGui/QWindow>

#include <condition_variable>
#include <mutex>

class QOpenGLContext;

namespace EulerEquations {
namespace Ui {
namespace Qt {
class MainWindow : public QWindow {
  Q_OBJECT

public:
  typedef QWindow Base;

  typedef Simulation::Scenario        Scenario;
  typedef Simulation::Solver          Solver;
  typedef Visualization::GridRenderer GridRenderer;

public:
  explicit MainWindow(QWindow* parent = 0);

  ~MainWindow();

  /**
   * Argument
   */
  void
  simulation(Solver::Shared task);

private:
  void
  initialize();

public slots:
  void
  renderNow();

  /**
   * Post the UpdateRequest event to the event loop to invoke renderNow().
   * Generate render calls in the application loop.
   */
  void
  renderLater();

protected:
  bool
  event(QEvent* event);

private:
  Switch          _updatePending;
  QOpenGLContext* _openglContext;
  QOpenGLContext* _sharedOpenglContext;
  QPoint          _cursorPosition;

  Controller::Shared _controller;

  GridRenderer::Shared _renderer;

  Uni::IterativeWorker::Shared _worker;
  Solver::Shared          _task;

  Scenario::Shared _scenario;

  GuiHub::Shared _guiHub;

  bool                    _isInitializationFinished;
  std::mutex              _initializationConditionMutex;
  std::condition_variable _waitWorkerInitializationCondition;
};
}
}
}

#endif
