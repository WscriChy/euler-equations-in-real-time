#include "MainWindow.hpp"

#include "Simulation/Scenarios/BubbleScenario.hpp"

#include <Uni/Logging/macros>

#include <Uni/InputMapping/Integration/Qt/qtkeyTo>

#include <QtCore/QCoreApplication>

#include <Qt>
#include <QtGui/QCursor>
#include <QtGui/QKeyEvent>
#include <QtGui/QMouseEvent>
#include <QtGui/QOpenGLContext>
#include <QtGui/QResizeEvent>

using EulerEquations::Ui::Qt::MainWindow;

MainWindow::
MainWindow(QWindow* parent)
  : Base(parent),
    _openglContext(new QOpenGLContext(this)),
    _sharedOpenglContext(new QOpenGLContext()),
    _controller(new Controller()),
    _renderer(new GridRenderer()),
    _worker(new Uni::IterativeWorker()),
    _scenario(new Simulation::Scenarios::BubbleScenario()),
    _guiHub(new GuiHub()) {
  this->setSurfaceType(QWindow::OpenGLSurface);
  this->resize(1280, 860);
  _worker->isWaitAtInitializationFinished(true);
  _worker->isWaitAtIterationFinished(true);
  _worker->isWaitAtReleaseFinished(false);
  this->setCursor(::Qt::BlankCursor);
}

MainWindow::~MainWindow() {
  _worker->stop();
}

void
MainWindow::
simulation(Solver::Shared task) {
  // Task installation
  using namespace std::placeholders;

  _task = task;
  _task->scenario(_scenario);
  _worker->stop();
  _worker->task(_task);

  _isInitializationFinished = false;

  _worker->observe(Uni::IterativeWorkerProperties::InitializationFinished,
                   [&, this] () {
                     std::unique_lock<std::mutex>
                     internalLock(_initializationConditionMutex);
                     _isInitializationFinished = true;
                     internalLock.unlock();
                     _waitWorkerInitializationCondition.notify_all();
                   });

  _worker->start();
}

void
MainWindow::
initialize() {
  _openglContext->setFormat(requestedFormat());

  if (!_openglContext->create()) {
    logFatal("Failed to create Qt context");
  }
  _openglContext->makeCurrent(this);

  _renderer->initialize(
    _scenario->xSize(),
    _scenario->ySize(),
    _scenario->zSize(),
    10.0f,
    10.0f,
    10.0f);

  // Need to initialize OpenGL viewport
  glViewport(0, 0, width(), height());
  //
  _controller->inputMapper()->handleAndReturnFalse(
    Uni::InputMapping::Event::resize,
    [] (float const& width, float const& height) {
      glViewport(0, 0, width, height);
    });

  _guiHub->initialize(_controller);

  using std::placeholders::_1;
  _task->observe(Simulation::SolverProperties::SimulationTime,
                 std::function<void(double const&)>(
                   std::bind(&GuiHub::simulationTime, _guiHub, _1)));

  _task->observe(Simulation::SolverProperties::IterationNumber,
                 std::function<void(int const&)>(
                   std::bind(&GuiHub::iterationNumber, _guiHub, _1)));

  _task->observe(Simulation::SolverProperties::MinDensity,
                 std::function<void(float const&)>(
                   std::bind(&GuiHub::minDensity, _guiHub, _1)));

  _task->observe(Simulation::SolverProperties::MaxDensity,
                 std::function<void(float const&)>(
                   std::bind(&GuiHub::maxDensity, _guiHub, _1)));

  _guiHub->color(GuiHubProperties::ColorRangeR1,
                 255 * _renderer->colorRangeMin()(0));
  _guiHub->color(GuiHubProperties::ColorRangeG1,
                 255 * _renderer->colorRangeMin()(1));
  _guiHub->color(GuiHubProperties::ColorRangeB1,
                 255 * _renderer->colorRangeMin()(2));
  _guiHub->color(GuiHubProperties::ColorRangeA1,
                 255 * _renderer->colorRangeMin()(3));

  _guiHub->color(GuiHubProperties::ColorRangeR2,
                 255 * _renderer->colorRangeMax()(0));
  _guiHub->color(GuiHubProperties::ColorRangeG2,
                 255 * _renderer->colorRangeMax()(1));
  _guiHub->color(GuiHubProperties::ColorRangeB2,
                 255 * _renderer->colorRangeMax()(2));
  _guiHub->color(GuiHubProperties::ColorRangeA2,
                 255 * _renderer->colorRangeMax()(3));

  _guiHub->scalarMin(_renderer->scalarRangeMin());
  _guiHub->scalarMax(_renderer->scalarRangeMax());

  _guiHub->observe(GuiHubProperties::ColorRangeR1,
                   [ = ] (int const& value) {
                     _renderer->colorRangeMin()(0) = ((float)value) / 255.0f;
                   });
  _guiHub->observe(GuiHubProperties::ColorRangeG1,
                   [ = ] (int const& value) {
                     _renderer->colorRangeMin()(1) = ((float)value) / 255.0f;
                   });
  _guiHub->observe(GuiHubProperties::ColorRangeB1,
                   [ = ] (int const& value) {
                     _renderer->colorRangeMin()(2) = ((float)value) / 255.0f;
                   });
  _guiHub->observe(GuiHubProperties::ColorRangeA1,
                   [ = ] (int const& value) {
                     _renderer->colorRangeMin()(3) = ((float)value) / 255.0f;
                   });

  _guiHub->observe(GuiHubProperties::ColorRangeR2,
                   [ = ] (int const& value) {
                     _renderer->colorRangeMax()(0) = ((float)value) / 255.0f;
                   });
  _guiHub->observe(GuiHubProperties::ColorRangeG2,
                   [ = ] (int const& value) {
                     _renderer->colorRangeMax()(1) = ((float)value) / 255.0f;
                   });
  _guiHub->observe(GuiHubProperties::ColorRangeB2,
                   [ = ] (int const& value) {
                     _renderer->colorRangeMax()(2) = ((float)value) / 255.0f;
                   });
  _guiHub->observe(GuiHubProperties::ColorRangeA2,
                   [ = ] (int const& value) {
                     _renderer->colorRangeMax()(3) = ((float)value) / 255.0f;
                   });

  _guiHub->observe(GuiHubProperties::ScalarMin,
                   [ = ] (float const& value) {
                     _renderer->scalarRangeMin() = value;
                   });
  _guiHub->observe(GuiHubProperties::ScalarMax,
                   [ = ] (float const& value) {
                     _renderer->scalarRangeMax() = value;
                   });

  // thread - unsafe operations with task
  _guiHub->observe(GuiHubProperties::SetMin,
                   [ = ] () {
                     _guiHub->scalarMin(_task->minDensity());
                   });
  _guiHub->observe(GuiHubProperties::SetMax,
                   [ = ] () {
                     _guiHub->scalarMax(_task->maxDensity());
                   });

  typedef std::unique_lock<std::mutex> UniqueLock;

  {
    UniqueLock lock(_initializationConditionMutex);

    if (!_isInitializationFinished) {
      _waitWorkerInitializationCondition.wait(lock);
    }
  }
  _task->synchronize();
  _task->swapField();
  _worker->endWaitAtInitializationFinished();

  _controller->inputMapper()->handleAndReturnFalse(
    Uni::InputMapping::Event::gDown, [ = ] (Uni::Core::String const&) {
      _renderer->makeScreenShot();
    });
}

void
MainWindow::
renderNow() {
  if (!isExposed()) {
    return;
  }

  static bool isInitialized = false;

  if (!isInitialized) {
    isInitialized = true;
    initialize();
    _controller->start();
    // Zero RingStack size protection (division by zero)
    _controller->newFrame();
  } else {
    _controller->newFrame();
    _guiHub->elapsedTime(((float)_controller->elapsedTime() / 1000000.0f));
    _guiHub->frameTime((float)_controller->frameTime() / 1000000.0f);
    _guiHub->fps(_controller->fps());

    if (_worker->state() ==
        Uni::IterativeWorker::State::WaitingAtIterationFinished) {
      _task->synchronize();
      _task->swapField();
      _worker->endWaitAtIterationFinished();
    }
  }
  auto scalarField = _task->currentScalarField();

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

  _renderer->render(
    _controller->motionController()->projection(),
    _controller->motionController()->view(),
    scalarField);

  _guiHub->render(_controller);

  _openglContext->swapBuffers(this);

  renderLater();
}

void
MainWindow::
renderLater() {
  if (!_updatePending) {
    _updatePending();
    QCoreApplication::postEvent(this, new QEvent(QEvent::UpdateRequest));
  }
}

bool
MainWindow::
event(QEvent* event) {
  using Uni::InputMapping::Event;
  using Uni::InputMapping::Integration::Qt::qtKeyToDownEvent;
  using Uni::InputMapping::Integration::Qt::qtKeyToUpEvent;

  if (event->type() == QEvent::UpdateRequest) {
    _updatePending(false);
    renderNow();

    return true;
  } else if (event->type() == QEvent::Expose) {
    renderLater();

    return true;
  } else if (event->type() == QEvent::Show) {
    renderLater();

    _controller->inputMapper()->fire(Event::resize,
                                     (float const&)width(),
                                     (float const&)height());

    QPoint cursorPoosition = QCursor::pos();
    _controller->mouseState()->position(cursorPoosition.x(),
                                        cursorPoosition.y());

    return true;
  } else if (event->type() == QEvent::MouseMove) {
    QMouseEvent* temp = reinterpret_cast<QMouseEvent*>(event);
    _controller->inputMapper()->fire(Event::mouseMove,
                                     (float const&)temp->x(),
                                     (float const&)temp->y());

    int buttons = temp->buttons();

    if (buttons & ::Qt::LeftButton) {
      // QCursor::setPos(_cursorPosition);
    }

    return true;
  } else if (event->type() == QEvent::MouseButtonPress) {
    QMouseEvent* temp = reinterpret_cast<QMouseEvent*>(event);

    if (temp->button() == ::Qt::LeftButton) {
      _controller->inputMapper()->fire(Event::mouseLeftButtonDown);
      _cursorPosition = QCursor::pos();
    } else if (temp->button() == ::Qt::RightButton) {
      _controller->inputMapper()->fire(Event::mouseRightButtonDown);
    }

    return true;
  } else if (event->type() == QEvent::MouseButtonRelease) {
    QMouseEvent* temp = reinterpret_cast<QMouseEvent*>(event);

    if (temp->button() == ::Qt::LeftButton) {
      _controller->inputMapper()->fire(Event::mouseLeftButtonUp);
    } else if (temp->button() == ::Qt::RightButton) {
      _controller->inputMapper()->fire(Event::mouseRightButtonUp);
    }

    return true;
  } else if (event->type() == QEvent::Wheel) {
    auto temp = reinterpret_cast<QWheelEvent*>(event);
    _controller->inputMapper()->fire(Event::mouseWheel,
                                     (float const&)temp->delta());

    return true;
  } else if (event->type() == QEvent::KeyPress) {
    QKeyEvent* temp = reinterpret_cast<QKeyEvent*>(event);

    auto tempEvent = qtKeyToDownEvent(temp->key());
    _controller->inputMapper()->fire(tempEvent,
                                     (String const&)String(
                                       temp->text().toStdString()));

    return true;
  } else if (event->type() == QEvent::KeyRelease) {
    QKeyEvent* temp = reinterpret_cast<QKeyEvent*>(event);

    auto tempEvent = qtKeyToUpEvent(temp->key());
    _controller->inputMapper()->fire(tempEvent);

    return true;
  } else if (event->type() == QEvent::Resize) {
    QResizeEvent* temp = reinterpret_cast<QResizeEvent*>(event);

    _controller->inputMapper()->fire(Event::resize,
                                     (float const&)width(),
                                     (float const&)height());

    return true;
  } else if (event->type() == QEvent::Leave ||
             event->type() == QEvent::DragLeave ||
             event->type() == QEvent::HoverLeave) {
    _controller->inputMapper()->fire(Event::leave);

    return true;
  } else if (event->type() == QEvent::FocusOut) {
    _controller->inputMapper()->fire(Event::focusOut);

    return true;
  } else {
    return QWindow::event(event);
  }
}
