#include "start.hpp"

#include "Qt/MainWindow.hpp"

#include <QGuiApplication>

int
EulerEquations::Ui::
start(int                                      argc,
      char**                                   argv,
      PeanoExecutor::repositories::Repository* repository) {
  using Solver = Qt::MainWindow::Solver;
  using MainWindow    = Qt::MainWindow;

  Solver::Shared task(new Solver(repository));

  QGuiApplication application(argc, argv);
  MainWindow      mainWindow;
  mainWindow.simulation(task);
  mainWindow.showMaximized();

  return application.exec();
}
