#ifndef EulerEquations_Ui_start_hpp
#define EulerEquations_Ui_start_hpp

#include "PeanoExecutor/repositories/Repository.h"

namespace EulerEquations {
namespace Ui {
int
start(int                                      argc,
      char**                                   argv,
      PeanoExecutor::repositories::Repository* repository);
}
}

#endif
