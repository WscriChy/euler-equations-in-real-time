#include "GuiHub.hpp"

#include <Uni/Logging/macros>

#include <QtCore/QCoreApplication>

#include <GL/glew.h>

#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>

#include <iomanip>
#include <sstream>

using EulerEquations::Ui::GuiHub;

struct RocketKeyIdentifierToUniInputMappingEvent {
  CEGUI::Key::Scan         rocketId;
  Uni::InputMapping::Event Up;
  Uni::InputMapping::Event Down;
};

void
GuiHub::
initialize(Controller::Shared controller) {
  using namespace CEGUI;

  OpenGL3Renderer* renderer = &OpenGL3Renderer::bootstrapSystem();

  std::string rootPathStr =
    QCoreApplication::applicationDirPath().toStdString().data();

  // initialise the required dirs for the DefaultResourceProvider
  DefaultResourceProvider* rp = static_cast<CEGUI::DefaultResourceProvider*>
                                (CEGUI::System::getSingleton().
                                 getResourceProvider());
  rp->setResourceGroupDirectory("schemes",
                                (rootPathStr + "/Gui/schemes/"));
  rp->setResourceGroupDirectory("imagesets",
                                (rootPathStr   + "/Gui/imagesets/"));
  rp->setResourceGroupDirectory("fonts",
                                (rootPathStr   + "/Gui/fonts/"));
  rp->setResourceGroupDirectory("layouts",
                                (rootPathStr   + "/Gui/layouts/"));
  rp->setResourceGroupDirectory("looknfeels",
                                (rootPathStr   + "/Gui/looknfeel/"));
  rp->setResourceGroupDirectory("xml_schemes",
                                (rootPathStr   + "/Gui/xml_schemes/"));
  ImageManager::setImagesetDefaultResourceGroup("imagesets");
  Font::setDefaultResourceGroup("fonts");
  Scheme::setDefaultResourceGroup("schemes");
  WidgetLookManager::setDefaultResourceGroup("looknfeels");
  WindowManager::setDefaultResourceGroup("layouts");
  ScriptModule::setDefaultResourceGroup("xml_schemes");

  SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");

  System::getSingleton().getDefaultGUIContext().
  getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");
  System::getSingleton().getDefaultGUIContext().
  setDefaultTooltipType("TaharezLook/Tooltip");

  WindowManager& wmgr = WindowManager::getSingleton();

  Window* myRoot = wmgr.createWindow("DefaultWindow", "root");
  myRoot->setMousePassThroughEnabled(true);
  System::getSingleton().getDefaultGUIContext().setRootWindow(myRoot);

  myRoot->addChild(wmgr.loadLayoutFromFile("MainWindow.layout"));

  TabControl* tc = static_cast<TabControl*>(
    myRoot->getChild("Frame/TabControl"));

  // Add some pages to tab control
  tc->addTab(WindowManager::getSingleton().loadLayoutFromFile(
               "TabPage1.layout"));
  tc->addTab(WindowManager::getSingleton().loadLayoutFromFile(
               "TabPage2.layout"));
  tc->addTab(WindowManager::getSingleton().loadLayoutFromFile(
               "TabPage3.layout"));

  myRoot->getChildRecursive("R1")->subscribeEvent(
    Spinner::EventValueChanged,
    Event::Subscriber(&GuiHub::spinnerR1Handler, this));
  myRoot->getChildRecursive("G1")->subscribeEvent(
    Spinner::EventValueChanged,
    Event::Subscriber(&GuiHub::spinnerG1Handler, this));
  myRoot->getChildRecursive("B1")->subscribeEvent(
    Spinner::EventValueChanged,
    Event::Subscriber(&GuiHub::spinnerB1Handler, this));
  myRoot->getChildRecursive("A1")->subscribeEvent(
    Spinner::EventValueChanged,
    Event::Subscriber(&GuiHub::spinnerA1Handler, this));
  myRoot->getChildRecursive("R2")->subscribeEvent(
    Spinner::EventValueChanged,
    Event::Subscriber(&GuiHub::spinnerR2Handler, this));
  myRoot->getChildRecursive("G2")->subscribeEvent(
    Spinner::EventValueChanged,
    Event::Subscriber(&GuiHub::spinnerG2Handler, this));
  myRoot->getChildRecursive("B2")->subscribeEvent(
    Spinner::EventValueChanged,
    Event::Subscriber(&GuiHub::spinnerB2Handler, this));
  myRoot->getChildRecursive("A2")->subscribeEvent(
    Spinner::EventValueChanged,
    Event::Subscriber(&GuiHub::spinnerA2Handler, this));

  myRoot->getChildRecursive("ScalarMin")->subscribeEvent(
    Spinner::EventValueChanged,
    Event::Subscriber(&GuiHub::scalarMinHandler, this));
  myRoot->getChildRecursive("ScalarMax")->subscribeEvent(
    Spinner::EventValueChanged,
    Event::Subscriber(&GuiHub::scalarMaxHandler, this));

  myRoot->getChildRecursive("SetMin")->subscribeEvent(
    PushButton::EventClicked,
    Event::Subscriber(&GuiHub::setMinHandler, this));
  myRoot->getChildRecursive("SetMax")->subscribeEvent(
    PushButton::EventClicked,
    Event::Subscriber(&GuiHub::setMaxHandler, this));

  int inputPriority = 1;
  using Uni::InputMapping::Event;
  using namespace std::placeholders;

  GUIContext* _context = &System::getSingleton().getDefaultGUIContext();

  controller->inputMapper()->handleAndReturnFalse(
    Event::mouseMove,
    [ = ] (float const& x, float const& y) {
      _context->injectMousePosition(x, y);
    });

  controller->inputMapper()->handle(
    Event::mouseLeftButtonUp,
    [ = ] () {
      return _context->injectMouseButtonUp(MouseButton::LeftButton);
    }, inputPriority);

  controller->inputMapper()->handle(
    Event::mouseLeftButtonDown,
    [ = ] () {
      return _context->injectMouseButtonDown(MouseButton::LeftButton);
    }, inputPriority);

  controller->inputMapper()->handle(
    Event::mouseRightButtonUp,
    [ = ] () {
      return _context->injectMouseButtonUp(MouseButton::RightButton);
    }, inputPriority);

  controller->inputMapper()->handle(
    Event::mouseRightButtonDown,
    [ = ] () {
      return _context->injectMouseButtonDown(MouseButton::RightButton);
    }, inputPriority);

  controller->inputMapper()->handle(
    Event::mouseWheel,
    [ = ] (float const& value) {
      return _context->injectMouseWheelChange(value);
    }, inputPriority);

  controller->inputMapper()->handleAndReturnFalse(
    Event::leave,
    [ = ] () {
      _context->injectMouseLeaves();
    });

  controller->inputMapper()->handleAndReturnFalse(
    Event::resize,
    [ = ] (float const& x, float const& y) {
      System::getSingleton().notifyDisplaySizeChanged(Size<float>(x, y));
    });

  std::vector<RocketKeyIdentifierToUniInputMappingEvent> vector;

  vector.push_back({ Key::Escape, Event::escapeUp, Event::escapeDown });
  vector.push_back({ Key::One, Event::oneUp, Event::oneDown });
  vector.push_back({ Key::Two, Event::twoUp, Event::twoDown });
  vector.push_back({ Key::Three, Event::threeUp, Event::threeDown });
  vector.push_back({ Key::Four, Event::fourUp, Event::fourDown });
  vector.push_back({ Key::Five, Event::fiveUp, Event::fiveDown });
  vector.push_back({ Key::Six, Event::sixUp, Event::sixDown });
  vector.push_back({ Key::Seven, Event::sevenUp, Event::sevenDown });
  vector.push_back({ Key::Eight, Event::eightUp, Event::eightDown });
  vector.push_back({ Key::Nine, Event::nineUp, Event::nineDown });
  vector.push_back({ Key::Zero, Event::zeroUp, Event::zeroDown });
  vector.push_back({ Key::Minus, Event::minusUp, Event::minusDown });
  vector.push_back({ Key::Equals, Event::equalUp, Event::equalDown });
  vector.push_back({ Key::Backspace, Event::backspaceUp,
                     Event::backspaceDown });
  vector.push_back({ Key::Tab, Event::tabUp, Event::tabDown });
  vector.push_back({ Key::Q, Event::qUp, Event::qDown });
  vector.push_back({ Key::W, Event::wUp, Event::wDown });
  vector.push_back({ Key::E, Event::eUp, Event::eDown });
  vector.push_back({ Key::R, Event::rUp, Event::rDown });
  vector.push_back({ Key::T, Event::tUp, Event::tDown });
  vector.push_back({ Key::Y, Event::yUp, Event::yDown });
  vector.push_back({ Key::U, Event::uUp, Event::uDown });
  vector.push_back({ Key::I, Event::iUp, Event::iDown });
  vector.push_back({ Key::O, Event::oUp, Event::oDown });
  vector.push_back({ Key::P, Event::pUp, Event::pDown });
  vector.push_back({ Key::LeftBracket, Event::bracketleftUp,
                     Event::bracketleftDown });
  vector.push_back({ Key::RightBracket, Event::bracketrightUp,
                     Event::bracketrightDown });
  vector.push_back({ Key::Return, Event::returnUp, Event::returnDown });
  vector.push_back({ Key::LeftControl, Event::controlUp,
                     Event::controlDown });
  vector.push_back({ Key::A, Event::aUp, Event::aDown });
  vector.push_back({ Key::S, Event::sUp, Event::sDown });
  vector.push_back({ Key::D, Event::dUp, Event::dDown });
  vector.push_back({ Key::F, Event::fUp, Event::fDown });
  vector.push_back({ Key::G, Event::gUp, Event::gDown });
  vector.push_back({ Key::H, Event::hUp, Event::hDown });
  vector.push_back({ Key::J, Event::jUp, Event::jDown });
  vector.push_back({ Key::K, Event::kUp, Event::kDown });
  vector.push_back({ Key::L, Event::lUp, Event::lDown });
  vector.push_back({ Key::Semicolon, Event::semicolonUp,
                     Event::semicolonDown });
  vector.push_back({ Key::Apostrophe, Event::apostropheUp,
                     Event::apostropheDown });
  // vector.push_back({ Key::Grave, Event::graveUp, Event::graveDown });
  vector.push_back({ Key::LeftShift, Event::shiftUp,
                     Event::shiftDown });
  vector.push_back({ Key::Backslash, Event::backslashUp,
                     Event::backslashDown });
  vector.push_back({ Key::Z, Event::zUp, Event::zDown });
  vector.push_back({ Key::X, Event::xUp, Event::xDown });
  vector.push_back({ Key::C, Event::cUp, Event::cDown });
  vector.push_back({ Key::V, Event::vUp, Event::vDown });
  vector.push_back({ Key::B, Event::bUp, Event::bDown });
  vector.push_back({ Key::N, Event::nUp, Event::nDown });
  vector.push_back({ Key::M, Event::mUp, Event::mDown });
  vector.push_back({ Key::Comma, Event::commaUp, Event::commaDown });
  vector.push_back({ Key::Period, Event::periodUp, Event::periodDown });
  vector.push_back({ Key::Slash, Event::slashUp, Event::slashDown });
  vector.push_back({ Key::RightShift, Event::shiftUp,
                     Event::shiftDown });
  vector.push_back({ Key::Multiply, Event::multiplyUp, Event::multiplyDown });
  vector.push_back({ Key::LeftAlt, Event::altUp, Event::altDown });
  vector.push_back({ Key::Space, Event::spaceUp, Event::spaceDown });
  // vector.push_back({ Key::Capital, Event::capitalUp, Event::capitalDown });
  vector.push_back({ Key::F1, Event::f1Up, Event::f1Down });
  vector.push_back({ Key::F2, Event::f2Up, Event::f2Down });
  vector.push_back({ Key::F3, Event::f3Up, Event::f3Down });
  vector.push_back({ Key::F4, Event::f4Up, Event::f4Down });
  vector.push_back({ Key::F5, Event::f5Up, Event::f5Down });
  vector.push_back({ Key::F6, Event::f6Up, Event::f6Down });
  vector.push_back({ Key::F7, Event::f7Up, Event::f7Down });
  vector.push_back({ Key::F8, Event::f8Up, Event::f8Down });
  vector.push_back({ Key::F9, Event::f9Up, Event::f9Down });
  vector.push_back({ Key::F10, Event::f10Up, Event::f10Down });
  vector.push_back({ Key::NumLock, Event::numlockUp, Event::numlockDown });
  vector.push_back({ Key::ScrollLock, Event::scrolllockUp,
                     Event::scrolllockDown });
  vector.push_back({ Key::F11, Event::f11Up, Event::f11Down });
  vector.push_back({ Key::F12, Event::f12Up, Event::f12Down });
  vector.push_back({ Key::F13, Event::f13Up, Event::f13Down });
  vector.push_back({ Key::F14, Event::f14Up, Event::f14Down });
  vector.push_back({ Key::F15, Event::f15Up, Event::f15Down });
  vector.push_back({ Key::Pause, Event::pauseUp, Event::pauseDown });
  vector.push_back({ Key::Home, Event::homeUp, Event::homeDown });
  vector.push_back({ Key::ArrowUp, Event::upUp, Event::upDown });
  vector.push_back({ Key::ArrowLeft, Event::leftUp,
                     Event::leftDown });
  vector.push_back({ Key::ArrowRight, Event::rightUp,
                     Event::rightDown });
  vector.push_back({ Key::End, Event::endUp, Event::endDown });
  vector.push_back({ Key::ArrowDown, Event::downUp,
                     Event::downDown });
  vector.push_back({ Key::PageUp, Event::pageupUp, Event::pageupDown });
  vector.push_back({ Key::PageDown, Event::pagedownUp, Event::pagedownDown });
  vector.push_back({ Key::Insert, Event::insertUp, Event::insertDown });
  vector.push_back({ Key::Delete, Event::deleteUp, Event::deleteDown });
  vector.push_back({ Key::LeftWindows, Event::metaUp,
                     Event::metaDown });
  // vector.push_back({ Key::RightWindows, Event::windowsUp,
  // Event::windowsDown });
  vector.push_back({ Key::AppMenu, Event::applicationleftUp,
                     Event::applicationleftDown });
  // vector.push_back({ Key::Power, Event::powerUp, Event::powerDown });
  // vector.push_back({ Key::Sleep, Event::sleepUp, Event::sleepDown });
  // vector.push_back({ Key::Wake, Event::wakeUp, Event::wakeDown });

  for (auto const& item : vector) {
    controller->inputMapper()->handleAndReturnFalse(
      item.Up,
      [ = ] () {
        _context->injectKeyUp(item.rocketId);
      }, inputPriority);
    controller->inputMapper()->handle(
      item.Down,
      [ = ] (Uni::Core::String const& text) {
        auto result = _context->injectKeyDown(item.rocketId);

        if (!text.isEmpty()) {
          return _context->injectChar(text.get(0));
        } else {
          return false;
        }
      }, inputPriority);
  }
}

void
GuiHub::
render(Controller::Shared controller) {
  using namespace CEGUI;

  System::getSingleton().renderAllGUIContexts();
}

namespace EulerEquations {
namespace Ui {
namespace Detail {
CEGUI::Window*
findWindowRecursively(char const* name) {
  using namespace CEGUI;
  GUIContext* _context = &System::getSingleton().getDefaultGUIContext();

  auto window = _context->getRootWindow()->getChildRecursive(name);

  return window;
}
void
findWindowRecursivelyAndSetText(char const* name, char const* text) {
  findWindowRecursively(name)->setText(text);
}

std::string
format(int const& value) {
  std::stringstream stringStream;
  stringStream << std::fixed << value;

  return stringStream.str();
}

std::string
format(float const& value) {
  std::stringstream stringStream;
  stringStream << std::setprecision(3) << std::fixed << value;

  return stringStream.str();
}

std::string
format(double const& value) {
  std::stringstream stringStream;
  stringStream << std::setprecision(3) << std::fixed << value;

  return stringStream.str();
}

std::string
convertColorToHexString(int const& value) {
  std::stringstream stream;
  stream << std::hex << value;
  std::string result(stream.str());

  if (result.size() == 1) {
    result = std::string("0") + result;
  }

  return result;
}

float
getSpinnerValue(CEGUI::EventArgs const& e) {
  using namespace CEGUI;
  auto args = static_cast<WindowEventArgs const*>(&e);

  auto spinner = static_cast<Spinner const*>(args->window);

  return spinner->getCurrentValue();
}
}
}
}

void
GuiHub::
elapsedTime(float const& elapsedTime) {
  Detail::findWindowRecursivelyAndSetText(
    "ElapsedTimeValue",
    Detail::format(elapsedTime).c_str());
}

void
GuiHub::
fps(float const& fps) {
  Detail::findWindowRecursivelyAndSetText(
    "FpsValue",
    Detail::format(fps).c_str());
}

void
GuiHub::
frameTime(float const& frameTime) {
  Detail::findWindowRecursivelyAndSetText(
    "FrameTimeValue",
    Detail::format(frameTime).c_str());
}

void
GuiHub::
simulationTime(double const& simulationTime) {
  Detail::findWindowRecursivelyAndSetText(
    "SimulationTimeValue",
    Detail::format(simulationTime).c_str());
}

void
GuiHub::
iterationNumber(int const& iterationNumber) {
  Detail::findWindowRecursivelyAndSetText(
    "IterationNumberValue",
    Detail::format(iterationNumber).c_str());
}

void
GuiHub::
minDensity(float const& minDensity) {
  Detail::findWindowRecursivelyAndSetText(
    "MinDensityValue",
    Detail::format(minDensity).c_str());
}

void
GuiHub::
maxDensity(float const& maxDensity) {
  Detail::findWindowRecursivelyAndSetText(
    "MaxDensityValue",
    Detail::format(maxDensity).c_str());
}

void
GuiHub::
color(GuiHubProperties property, int const& color) {
  if (property == GuiHubProperties::ColorRangeR1) {
    Detail::findWindowRecursivelyAndSetText("R1",
                                            Detail::format(color).c_str());
  } else if (property == GuiHubProperties::ColorRangeG1) {
    Detail::findWindowRecursivelyAndSetText("G1",
                                            Detail::format(color).c_str());
  } else if (property == GuiHubProperties::ColorRangeB1) {
    Detail::findWindowRecursivelyAndSetText("B1",
                                            Detail::format(color).c_str());
  } else if (property == GuiHubProperties::ColorRangeA1) {
    Detail::findWindowRecursivelyAndSetText("A1",
                                            Detail::format(color).c_str());
  } else if (property == GuiHubProperties::ColorRangeR2) {
    Detail::findWindowRecursivelyAndSetText("R2",
                                            Detail::format(color).c_str());
  } else if (property == GuiHubProperties::ColorRangeG2) {
    Detail::findWindowRecursivelyAndSetText("G2",
                                            Detail::format(color).c_str());
  } else if (property == GuiHubProperties::ColorRangeB2) {
    Detail::findWindowRecursivelyAndSetText("B2",
                                            Detail::format(color).c_str());
  } else if (property == GuiHubProperties::ColorRangeA2) {
    Detail::findWindowRecursivelyAndSetText("A2",
                                            Detail::format(color).c_str());
  }
}

void
GuiHub::
scalarMin(float const& value) {
  Detail::findWindowRecursivelyAndSetText("ScalarMin",
                                          Detail::format(value).c_str());
}

void
GuiHub::
scalarMax(float const& value) {
  Detail::findWindowRecursivelyAndSetText("ScalarMax",
                                          Detail::format(value).c_str());
}

bool
GuiHub::
spinnerR1Handler(CEGUI::EventArgs const& e) {
  int color = Detail::getSpinnerValue(e);
  notify(GuiHubProperties::ColorRangeR1, (int const&)color);
  updateColorEditbox1(GuiHubProperties::ColorRangeR1, color);

  return false;
}

bool
GuiHub::
spinnerG1Handler(CEGUI::EventArgs const& e) {
  int color = Detail::getSpinnerValue(e);
  notify(GuiHubProperties::ColorRangeG1, (int const&)color);
  updateColorEditbox1(GuiHubProperties::ColorRangeG1, color);

  return false;
}

bool
GuiHub::
spinnerB1Handler(CEGUI::EventArgs const& e) {
  int color = Detail::getSpinnerValue(e);
  notify(GuiHubProperties::ColorRangeB1, (int const&)color);
  updateColorEditbox1(GuiHubProperties::ColorRangeB1, color);

  return false;
}

bool
GuiHub::
spinnerA1Handler(CEGUI::EventArgs const& e) {
  int color = Detail::getSpinnerValue(e);
  notify(GuiHubProperties::ColorRangeA1, (int const&)color);
  updateColorEditbox1(GuiHubProperties::ColorRangeA1, color);

  return false;
}

bool
GuiHub::
spinnerR2Handler(CEGUI::EventArgs const& e) {
  int color = Detail::getSpinnerValue(e);
  notify(GuiHubProperties::ColorRangeR2, (int const&)color);
  updateColorEditbox2(GuiHubProperties::ColorRangeR2, color);

  return false;
}

bool
GuiHub::
spinnerG2Handler(CEGUI::EventArgs const& e) {
  int color = Detail::getSpinnerValue(e);
  notify(GuiHubProperties::ColorRangeG2, (int const&)color);
  updateColorEditbox2(GuiHubProperties::ColorRangeG2, color);

  return false;
}

bool
GuiHub::
spinnerB2Handler(CEGUI::EventArgs const& e) {
  int color = Detail::getSpinnerValue(e);
  notify(GuiHubProperties::ColorRangeB2, (int const&)color);
  updateColorEditbox2(GuiHubProperties::ColorRangeB2, color);

  return false;
}

bool
GuiHub::
spinnerA2Handler(CEGUI::EventArgs const& e) {
  int color = Detail::getSpinnerValue(e);
  notify(GuiHubProperties::ColorRangeA2, (int const&)color);
  updateColorEditbox2(GuiHubProperties::ColorRangeA2, color);

  return false;
}

bool
GuiHub::
scalarMinHandler(CEGUI::EventArgs const& e) {
  float value = Detail::getSpinnerValue(e);
  notify(GuiHubProperties::ScalarMin, (float const&)value);

  return false;
}

bool
GuiHub::
scalarMaxHandler(CEGUI::EventArgs const& e) {
  float value = Detail::getSpinnerValue(e);
  notify(GuiHubProperties::ScalarMax, (float const&)value);

  return false;
}

bool
GuiHub::
setMinHandler(CEGUI::EventArgs const& e) {
  notify(GuiHubProperties::SetMin);

  return false;
}
bool
GuiHub::
setMaxHandler(CEGUI::EventArgs const& e) {
  notify(GuiHubProperties::SetMax);

  return false;
}

void
GuiHub::
updateColorEditbox1(GuiHubProperties property, int const& color) {
  auto     window = Detail::findWindowRecursively("Rgba1");
  auto     text   = window->getText();
  ::String string(text.c_str());

  if (string.length() != 8) {
    string = "00000000";
  }
  auto colorSting = Detail::convertColorToHexString(color);
  ::String::Iterator itBegin;

  if (property == GuiHubProperties::ColorRangeR1) {
    itBegin = string.begin() + 0;
  } else if (property == GuiHubProperties::ColorRangeG1) {
    itBegin = string.begin() + 2;
  } else if (property == GuiHubProperties::ColorRangeB1) {
    itBegin = string.begin() + 4;
  } else if (property == GuiHubProperties::ColorRangeA1) {
    itBegin = string.begin() + 6;
  }
  *itBegin       = colorSting[0];
  *(itBegin + 1) = colorSting[1];
  window->setText(string.chars());
}

void
GuiHub::
updateColorEditbox2(GuiHubProperties property, int const& color) {
  auto     window = Detail::findWindowRecursively("Rgba2");
  auto     text   = window->getText();
  ::String string(text.c_str());

  if (string.length() != 8) {
    string = "00000000";
  }
  auto colorSting = Detail::convertColorToHexString(color);
  ::String::Iterator itBegin;

  if (property == GuiHubProperties::ColorRangeR2) {
    itBegin = string.begin() + 0;
  } else if (property == GuiHubProperties::ColorRangeG2) {
    itBegin = string.begin() + 2;
  } else if (property == GuiHubProperties::ColorRangeB2) {
    itBegin = string.begin() + 4;
  } else if (property == GuiHubProperties::ColorRangeA2) {
    itBegin = string.begin() + 6;
  }
  *itBegin       = colorSting[0];
  *(itBegin + 1) = colorSting[1];
  window->setText(string.chars());
}
