#include "start.hpp"

#include "Equalizer/Application.hpp"
#include "Equalizer/Channel.hpp"
#include "Equalizer/Config.hpp"
#include "Equalizer/Node.hpp"
#include "Equalizer/Pipe.hpp"
#include "Equalizer/View.hpp"
#include "Equalizer/Window.hpp"

#include <Uni/Logging/logging>

namespace EulerEquations {
namespace Ui {
namespace Equalizer {
class NodeFactory : public eq::NodeFactory {
public:
  NodeFactory(PeanoExecutor::repositories::Repository& repository)
    : eq::
      NodeFactory(),
      _repository(repository) {}

  virtual
  eq::Config*
  createConfig(eq::ServerPtr parent)
  { return new Config(parent, _repository); }

  virtual
  eq::Node*
  createNode(eq::Config* parent)
  { return new Node(parent); }

  virtual
  eq::Pipe*
  createPipe(eq::Node* parent)
  { return new Pipe(parent); }

  virtual
  eq::Window*
  createWindow(eq::Pipe* parent)
  { return new Window(parent); }

  virtual
  eq::Channel*
  createChannel(eq::Window* parent)
  { return new Channel(parent); }

  virtual
  eq::View*
  createView(eq::Layout* parent)
  { return new View(parent); }

private:
  PeanoExecutor::repositories::Repository& _repository;
};
}
}
}

int
EulerEquations::Ui::
start(int                                      argc,
      char**                                   argv,
      PeanoExecutor::repositories::Repository& repository) {
  namespace Equalizer;

  // Equalizer initialization
  NodeFactory nodeFactory(repository);

  if (!eq::init(argc, argv, &nodeFactory)) {
    ERROR << "Equalizer init failed";

    return EXIT_FAILURE;
  }

  // initialization of local client node
  lunchbox::RefPtr<Application> application = new Application();

  if (!application->initLocal(argc, argv)) {
    ERROR << "Can't init client";
    eq::exit();

    return EXIT_FAILURE;
  }

  // run client
  int const exitCode = application->run();

  // cleanup and exit
  application->exitLocal();

  if (application->getRefCount() != 1) {
    logFatal("Error");
  }

  application = 0;

  eq::exit();

  return exitCode;
}
