#ifndef EulerEquations_Ui_GuiHub_hpp
#define EulerEquations_Ui_GuiHub_hpp

#include <Ui/Controller.hpp>

#include <Uni/Observable>

#include <CEGUI/EventArgs.h>

#include <memory>

namespace Rocket {
namespace Core {
class Context;
}
}

namespace EulerEquations {
namespace Ui {
enum GuiHubProperties {
  ColorRangeR1 = 0,
  ColorRangeG1 = 1,
  ColorRangeB1 = 2,
  ColorRangeA1 = 3,
  ColorRangeR2 = 4,
  ColorRangeG2 = 5,
  ColorRangeB2 = 6,
  ColorRangeA2 = 7,
  ScalarMin    = 8,
  ScalarMax    = 9,
  SetMin       = 10,
  SetMax       = 11,
};
class GuiHub : public Uni::Observable<GuiHubProperties> {
public:
  typedef std::shared_ptr<GuiHub> Shared;

public:
  void
  initialize(Controller::Shared controller);

  void
  render(Controller::Shared controller);

  void
  elapsedTime(float const&);

  void
  frameTime(float const&);

  void
  fps(float const&);

  void
  simulationTime(double const&);

  void
  iterationNumber(int const&);

  void
  minDensity(float const&);

  void
  maxDensity(float const&);

  void
    color(GuiHubProperties, int const &);

  void
  scalarMin(float const&);

  void
  scalarMax(float const&);

  bool
  spinnerR1Handler(CEGUI::EventArgs const&);

  bool
  spinnerG1Handler(CEGUI::EventArgs const&);

  bool
  spinnerB1Handler(CEGUI::EventArgs const&);

  bool
  spinnerA1Handler(CEGUI::EventArgs const&);

  bool
  spinnerR2Handler(CEGUI::EventArgs const&);

  bool
  spinnerG2Handler(CEGUI::EventArgs const&);

  bool
  spinnerB2Handler(CEGUI::EventArgs const&);

  bool
  spinnerA2Handler(CEGUI::EventArgs const&);

  bool
  scalarMinHandler(CEGUI::EventArgs const&);

  bool
  scalarMaxHandler(CEGUI::EventArgs const&);

  bool
  setMinHandler(CEGUI::EventArgs const&);

  bool
  setMaxHandler(CEGUI::EventArgs const&);

  void
    updateColorEditbox1(GuiHubProperties, int const &);

  void
    updateColorEditbox2(GuiHubProperties, int const &);

private:
  Rocket::Core::Context* _context;
};
}
}

#endif
