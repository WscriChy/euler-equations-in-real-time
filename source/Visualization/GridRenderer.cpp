#include "GridRenderer.hpp"

#include <Uni/Logging/macros>

#include <GL/glew.h>

#include <FreeImage.h>

#include <Uni/Logging/format>

#include <cstring>
#include <map>
#include <set>
#include <vector>

using EulerEquations::Visualization::GridRenderer;

namespace EulerEquations {
namespace Visualization {
struct Private {
  typedef typename GridRenderer::VertexArray  VertexArray;
  typedef typename GridRenderer::ElementArray ElementArray;
  typedef typename GridRenderer::ColorVector  ColorVector;

  Private() :               scalarRangeMin(0.0),
    scalarRangeMax(1.0) {
    colorRangeMin << 96.0, 43.0, 135.0, 5.0;
    colorRangeMin /= 255.0;
    colorRangeMax << 237.0, 70.0, 61.0, 5.0;
    colorRangeMax /= 255.0;
  }

  VertexArray  _vertexPositionArray;
  ElementArray _elementArray;

  GLuint _scalarFieldBuffer;
  GLuint _vertexPositionArrayBuffer;
  GLuint _elementArrayBuffer;

  GLuint _program;
  GLuint _vertexShaderObject;
  GLuint _fragmentShaderObject;

  GLuint _vertexArrayObject;
  GLuint _positionAttributeIndex;
  GLuint _scalarAttributeIndex;

  GLint _programMVUniform;
  GLint _programPUniform;

  GLint colorRangeMinProgramParameter;
  GLint colorRangeMaxProgramParameter;

  GLint scalarRangeMinProgramParameter;
  GLint scalarRangeMaxProgramParameter;

  ColorVector colorRangeMin;
  ColorVector colorRangeMax;

  float scalarRangeMin;
  float scalarRangeMax;
};
}
}

GridRenderer::
GridRenderer() : im(new Private()) {}

GridRenderer::
~GridRenderer() {
  delete im;
}

GridRenderer::ColorVector const&
GridRenderer::
colorRangeMin() const {
  return im->colorRangeMin;
}

GridRenderer::ColorVector const&
GridRenderer::
colorRangeMax() const {
  return im->colorRangeMax;
}

GridRenderer::ColorVector&
GridRenderer::
colorRangeMin() {
  return im->colorRangeMin;
}

GridRenderer::ColorVector&
GridRenderer::
colorRangeMax() {
  return im->colorRangeMax;
}

float const&
GridRenderer::
scalarRangeMin() const {
  return im->scalarRangeMin;
}

float const&
GridRenderer::
scalarRangeMax() const {
  return im->scalarRangeMax;
}

float&
GridRenderer::
scalarRangeMin() {
  return im->scalarRangeMin;
}

float&
GridRenderer::
scalarRangeMax() {
  return im->scalarRangeMax;
}

void
initializeGLEW() {
  GLenum err = glewInit();

  Uni_Logging_logInfo("Using GLEW {1}", glewGetString(GLEW_VERSION));

  if (GLEW_OK != err) {
    logFatal("GLEW: %s", glewGetErrorString(err));
  }

  if (GLEW_VERSION_4_0) {
    Uni_Logging_logInfo("OpenGL version 4.0 is available");
  }

  GLenum status = glGetError();

  if (status == GL_NO_ERROR) {
    return;
  }

  logFatal(
    "OpenGL error (%i) occured during scene initilaization",
    status);
}

void
GridRenderer::
initialize(int const&   xSize,
           int const&   ySize,
           int const&   zSize,
           float const& domainSizeX,
           float const& domainSizeY,
           float const& domainSizeZ) {
  makeGrid(im->_vertexPositionArray,
           im->_elementArray,
           xSize,
           ySize,
           zSize,
           domainSizeX,
           domainSizeY,
           domainSizeZ);

  initializeGLEW();

  glGenBuffers(1, &(im->_scalarFieldBuffer));

  GLuint tempBuffer;
  glGenBuffers(1, &tempBuffer);
  im->_vertexPositionArrayBuffer = tempBuffer;

  glGenBuffers(1, &(im->_elementArrayBuffer));
  glBindBuffer(GL_ARRAY_BUFFER, im->_vertexPositionArrayBuffer);
  glBufferData(GL_ARRAY_BUFFER,
               sizeof (GLfloat) * im->_vertexPositionArray.size(),
               im->_vertexPositionArray.data(),
               GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER,         0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, im->_elementArrayBuffer);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,
               sizeof (GLuint) * im->_elementArray.size(),
               im->_elementArray.data(),
               GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  char const* vertexShaderSource
    = "#version 400\n"
      "uniform mat4 mv;"
      "uniform mat4 p;"
      "in vec3 VertexPosition;"
      "in float VertexScalar;"
      "smooth out float Scalar;"
      "void "
      "main() {"
      "  vec3 position = VertexPosition;"
      "  Scalar = VertexScalar;"
      "  gl_Position = p * mv  * vec4(position, 1.0);"
      "}";
  int const   vertexShaderSourceLength = std::strlen(vertexShaderSource);
  char const* fragmentShaderSource
    = "#version 400\n"
      "uniform float ScalarMin = 0.8;"
      "uniform float ScalarMax = 1.2;"
      "uniform vec4 ColorMin = vec4(0.0, 0.0, 0.0, 0.0);"
      "uniform vec4 ColorMax = vec4(1.0, 1.0, 1.0, 1.0);"
      "smooth in float Scalar;"
      "out vec4 FragColor;"
      "vec4 toColor2(float scalar) {"
      "  float k = (scalar - 0.5) / (1.5 - 0.5);"
      "  return vec4(-2 * k * k + 1 * k,"
      "              -4 * k * k + 4 * k,"
      "              -2 * k * k + k + 1,"
      "              (4 * k * k - 4 * k + 1)* 0.1);"
      "}"
      "vec4 toColor(float scalar) {"
      "  float min = ScalarMin;" // 0.8;"
      "  float max = ScalarMax;" // 1.0;"
      "  if (scalar <= min) {"
      // "    return ColorMin;"
      "    return vec4(0.0, 0.0, 0.0, 0.0);"
      // "           vec4(0.0, 0.0, 0.0, 0.0 * abs(min - scalar));"
      // "    return intensity *"
      // "           vec4(0.0, 0.0, 0.0, 0.0 * abs(min - scalar));"
      "  }"
      "  if (scalar >= max) {"
      // "    return ColorMax;"
      "    return vec4(0.0, 0.0, 0.0, 0.0);"
      // "    return intensity *"
      // "           vec4(1.0, 1.0, 1.0, 0.0 * (scalar - max));"
      "  }"
      "  float k = (scalar - min) / (max - min);"
      "  vec4 colorMin = (1.0 - k) * ColorMin;"
      "  vec4 colorMax = k * ColorMax;"
      "  vec4 finalColor = colorMin + colorMax;"
      "  float middle = abs(0.5 - k);"
      "  finalColor[3] = finalColor[3] * middle;"
      "  return finalColor;"
      // "  return intensity *"
      // "         vec4(2.0 * k - 1,"
      // "         vec4(-16.0 * k * k + 24.0 * k - 8.0,"
      // "         vec4(-4.0 * k * k + 8.0 * k - 3.0,"

      // "              abs(k - 0.5),"
      // "              4.0 * k * k - 4.0 * k + 1.0,"

      // "              -2 * k + 1,"
      // "              -16.0 * k * k + 8.0 * k,"
      // "              -4.0 * k * k + 1.0,"

      // "              abs(k - 0.5));"
      // "              4.0 * k * k - 4.0 * k + 1.0);"
      "}"
      "void main(){"
      "  FragColor = toColor(Scalar);"
      // "  FragColor = vec4(1.0, 0.0, 0.0, 1.0);"
      "}";
  int const fragmentShaderSourceLength = std::strlen(fragmentShaderSource);

  im->_vertexShaderObject   = glCreateShader(GL_VERTEX_SHADER);
  im->_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

  glShaderSource(im->_vertexShaderObject,   1,
                 &vertexShaderSource,
                 &vertexShaderSourceLength);
  glShaderSource(im->_fragmentShaderObject, 1,
                 &fragmentShaderSource,
                 &fragmentShaderSourceLength);

  glCompileShader(im->_vertexShaderObject);
  glCompileShader(im->_fragmentShaderObject);
  glReleaseShaderCompiler();

  im->_program = glCreateProgram();

  glAttachShader(im->_program, im->_vertexShaderObject);
  glAttachShader(im->_program, im->_fragmentShaderObject);
  glLinkProgram(im->_program);
  GLint status;
  glGetProgramiv(im->_program, GL_LINK_STATUS, &status);

  if (GL_FALSE == status) {
    Uni_Logging_logError("Failed to link shader program!");
    GLint logLen;
    glGetProgramiv(im->_program, GL_INFO_LOG_LENGTH, &logLen);

    if (logLen > 0) {
      char*   log = new char[logLen];
      GLsizei written;
      glGetProgramInfoLog(im->_program, logLen, &written, log);
      Uni_Logging_logError("Program log: \n%s", log);
      delete log;
    }

    return;
  } else {
    Uni_Logging_logInfo("GL Shader Program has been compiled successfully");
  }

  im->_positionAttributeIndex = 0;
  im->_scalarAttributeIndex   = 1;
  glBindAttribLocation(im->_program, im->_positionAttributeIndex,
                       "VertexPosition");
  glBindAttribLocation(im->_program, im->_scalarAttributeIndex,
                       "VertexScalar");

  im->_programMVUniform = glGetProgramResourceLocation(
    im->_program, GL_UNIFORM, "mv");
  im->_programPUniform = glGetProgramResourceLocation(
    im->_program, GL_UNIFORM, "p");

  im->colorRangeMinProgramParameter = glGetProgramResourceLocation(
    im->_program, GL_UNIFORM, "ColorMin");
  im->colorRangeMaxProgramParameter = glGetProgramResourceLocation(
    im->_program, GL_UNIFORM, "ColorMax");

  im->scalarRangeMinProgramParameter = glGetProgramResourceLocation(
    im->_program, GL_UNIFORM, "ScalarMin");
  im->scalarRangeMaxProgramParameter = glGetProgramResourceLocation(
    im->_program, GL_UNIFORM, "ScalarMax");

  glGenVertexArrays(1, &(im->_vertexArrayObject));
  glBindVertexArray(im->_vertexArrayObject);
  glVertexAttribFormat(im->_positionAttributeIndex, 3, GL_FLOAT, GL_FALSE, 0);
  glVertexAttribBinding(im->_positionAttributeIndex, 0);
  glEnableVertexAttribArray(im->_positionAttributeIndex);
  glVertexAttribFormat(im->_scalarAttributeIndex, 1, GL_FLOAT, GL_FALSE, 0);
  glVertexAttribBinding(im->_scalarAttributeIndex, 1);
  glEnableVertexAttribArray(im->_scalarAttributeIndex);
  glBindVertexArray(0);
}

void
GridRenderer::
release() {
  glDeleteBuffers(1, &(im->_vertexPositionArrayBuffer));
  glDeleteBuffers(1, &(im->_elementArrayBuffer));

  glDeleteShader(im->_vertexShaderObject);
  glDeleteShader(im->_fragmentShaderObject);
  glDeleteProgram(im->_program);
}

void
GridRenderer::
render(Matrix4x4 const&  projection,
       Matrix4x4 const&  view,
       SharedScalarField scalarField) {
  glUseProgram(im->_program);
  glBindBuffer(GL_ARRAY_BUFFER, im->_scalarFieldBuffer);
  glBufferData(GL_ARRAY_BUFFER,
               sizeof (GLfloat) * scalarField->size(),
               scalarField->data(),
               GL_STREAM_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glDisable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);

  glBlendFunc(GL_SRC_ALPHA, GL_ONE);

  glUniformMatrix4fv(im->_programPUniform,  1, false,
                     projection.data());
  glUniformMatrix4fv(im->_programMVUniform, 1, false,
                     view.data());

  glUniform4fv(im->colorRangeMinProgramParameter,
               1,
               im->colorRangeMin.data());
  glUniform4fv(im->colorRangeMaxProgramParameter,
               1,
               im->colorRangeMax.data());

  glUniform1f(im->scalarRangeMinProgramParameter, im->scalarRangeMin);
  glUniform1f(im->scalarRangeMaxProgramParameter, im->scalarRangeMax);

  glBindVertexArray(im->_vertexArrayObject);
  // version 4.3
  glBindVertexBuffer(0, im->_vertexPositionArrayBuffer,
                     0, sizeof (GLfloat) * 3);
  glBindVertexBuffer(1, im->_scalarFieldBuffer,
                     0, sizeof (GLfloat) * 1);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, im->_elementArrayBuffer);
  glDrawElements(GL_TRIANGLES,
                 im->_elementArray.size(),
                 GL_UNSIGNED_INT, 0);
  glDisable(GL_BLEND);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
  glUseProgram(0);
}

void
GridRenderer::
makeScreenShot() {
  static std::size_t id      = 1;
  GLint              dims[4] = { 0 };
  glGetIntegerv(GL_VIEWPORT, dims);
  GLint width  = dims[2];
  GLint height = dims[3];

  GLubyte* pixels = new GLubyte[3 * width * height];

  glReadPixels(0, 0, width, height, GL_BGR, GL_UNSIGNED_BYTE, pixels);

  FIBITMAP* image = FreeImage_ConvertFromRawBits(pixels, width, height, 3 * width, 24,
                                                 0xFF0000, 0x00FF00, 0x0000FF, false);
  auto number_str =  Uni::Logging::format("{1}", id);
  while (number_str.size() < 4) {
    number_str = std::string("0") +  number_str;
  }
  FreeImage_Save(FIF_PNG, image, Uni::Logging::format("/tmp/ee/Euler-Equations-{1}.png",
                                                      number_str).c_str(), 0);

  // FIMEMORY* hmem    = FreeImage_OpenMemory();
  // bool      success = FreeImage_SaveToMemory(FIF_PNG, image, hmem, PNG_Z_BEST_SPEED);

  FreeImage_Unload(image);
  // FreeImage_CloseMemory(hmem);
  delete[] pixels;
  ++id;
}

void
GridRenderer::
makeGrid(VertexArray&  vertexArray,
         ElementArray& elementArray,
         int const&    sizeX,
         int const&    sizeY,
         int const&    sizeZ,
         float const&  domainSizeX,
         float const&  domainSizeY,
         float const&  domainSizeZ) const {
  float domainCellSizeX = domainSizeX / sizeX;
  float domainCellSizeY = domainSizeY / sizeY;
  float domainCellSizeZ = domainSizeZ / sizeZ;

  for (int z = 0; z < sizeZ; ++z) {
    for (int y = 0; y < sizeY; ++y) {
      for (int x = 0; x < sizeX; ++x) {
        vertexArray.push_back((float)x * domainCellSizeX);
        vertexArray.push_back((float)y * domainCellSizeY);
        vertexArray.push_back((float)z * domainCellSizeZ);
      }
    }
  }

  for (int z = 0; z < sizeZ - 1; ++z) {
    for (int y = 0; y < sizeY - 1; ++y) {
      for (int x = 0; x < sizeX - 1; ++x) {
        if (x == 0) {
          // Left triangle 1
          elementArray.push_back(
            (y) * sizeX + (z) * sizeX * sizeY);
          elementArray.push_back(
            (y + 1) * sizeX + (z) * sizeX * sizeY);
          elementArray.push_back(
            (y) * sizeX + (z + 1) * sizeX * sizeY);

          // Left triangle 2
          elementArray.push_back(
            (y) * sizeX + (z + 1) * sizeX * sizeY);
          elementArray.push_back(
            (y + 1) * sizeX + (z) * sizeX * sizeY);
          elementArray.push_back(
            (y + 1) * sizeX + (z + 1) * sizeX * sizeY);
        }

        // Front triangle 1
        elementArray.push_back(x + (y) * sizeX + (z) * sizeX * sizeY);
        elementArray.push_back(x + 1 + (y) * sizeX + (z) * sizeX * sizeY);
        elementArray.push_back(x + (y + 1) * sizeX + (z) * sizeX * sizeY);

        // Front triangle 2
        elementArray.push_back(x + 1 + (y) * sizeX + (z) * sizeX * sizeY);
        elementArray.push_back(x + 1 + (y + 1) * sizeX + (z) * sizeX * sizeY);
        elementArray.push_back(x + (y + 1) * sizeX + (z) * sizeX * sizeY);

        // Bottom triangle 1
        elementArray.push_back(x + (y) * sizeX + (z) * sizeX * sizeY);
        elementArray.push_back(x + (y) * sizeX + (z + 1) * sizeX * sizeY);
        elementArray.push_back(x + 1 + (y) * sizeX + (z) * sizeX * sizeY);

        // Bottom triangle 2
        elementArray.push_back(x + 1 + (y) * sizeX + (z) * sizeX * sizeY);
        elementArray.push_back(x + (y) * sizeX + (z + 1) * sizeX * sizeY);
        elementArray.push_back(x + 1 + (y) * sizeX + (z + 1) * sizeX * sizeY);

        // Right triangle 1
        elementArray.push_back(x + 1 + (y) * sizeX + (z) * sizeX * sizeY);
        elementArray.push_back(x + 1 + (y) * sizeX + (z + 1) * sizeX * sizeY);
        elementArray.push_back(x + 1 + (y + 1) * sizeX + (z) * sizeX * sizeY);

        // Right triangle 2
        elementArray.push_back(x + 1 + (y) * sizeX + (z + 1) * sizeX * sizeY);
        elementArray.push_back(x + 1 + (y + 1) * sizeX + (z + 1) * sizeX *
                               sizeY);
        elementArray.push_back(x + 1 + (y + 1) * sizeX + (z) * sizeX * sizeY);

        if (y == sizeY - 2) {
          // Top triangle 1
          elementArray.push_back(
            x + (sizeY - 1) * sizeX + (z) * sizeX * sizeY);
          elementArray.push_back(
            x + (sizeY - 1) * sizeX + (z + 1) * sizeX * sizeY);
          elementArray.push_back(
            x + 1 + (sizeY - 1) * sizeX + (z) * sizeX * sizeY);

          // Top triangle 2
          elementArray.push_back(
            x + 1 + (sizeY - 1) * sizeX + (z) * sizeX * sizeY);
          elementArray.push_back(
            x + (sizeY - 1) * sizeX + (z + 1) * sizeX * sizeY);
          elementArray.push_back(
            x + 1 + (sizeY - 1) * sizeX + (z + 1) * sizeX * sizeY);
        }

        if (z == sizeZ - 2) {
          // Back triangle 1
          elementArray.push_back(
            x + (y) * sizeX + (sizeZ - 1) * sizeX * sizeY);
          elementArray.push_back(
            x + (y + 1) * sizeX + (sizeZ - 1) * sizeX * sizeY);
          elementArray.push_back(
            x + 1 + (y) * sizeX + (sizeZ - 1) * sizeX * sizeY);

          // Back triangle 2
          elementArray.push_back(
            x + 1 + (y) * sizeX + (sizeZ - 1) * sizeX * sizeY);
          elementArray.push_back(
            x + (y + 1) * sizeX + (sizeZ - 1) * sizeX * sizeY);
          elementArray.push_back(
            x + 1 + (y + 1) * sizeX + (sizeZ - 1) * sizeX * sizeY);
        }
      }
    }
  }
}
