#ifndef EulerEquations_Visualization_GridRenderer_hpp
#define EulerEquations_Visualization_GridRenderer_hpp

#include <Ui/Controller.hpp>

#include "Simulation/Solver.hpp"

#include <Eigen/Core>

#include <vector>
#include <memory>

namespace EulerEquations {
namespace Visualization {
struct Private;

class GridRenderer {
public:
  typedef std::shared_ptr<GridRenderer> Shared;

  typedef typename Ui::Controller::MotionController::Matrix4x4 Matrix4x4;

  typedef std::vector<float>        VertexArray;
  typedef std::vector<unsigned int> ElementArray;

  typedef Simulation::Solver::SharedScalarField SharedScalarField;

  typedef Eigen::Matrix<float, 4, 1> ColorVector;

public:
  GridRenderer();

  ~GridRenderer();

  ColorVector const&
  colorRangeMin() const;

  ColorVector const&
  colorRangeMax() const;

  ColorVector&
  colorRangeMin();

  ColorVector&
  colorRangeMax();

  float const&
  scalarRangeMin() const;

  float const&
  scalarRangeMax() const;

  float&
  scalarRangeMin();

  float&
  scalarRangeMax();

  void
  initialize(int const&,
             int const&,
             int const&,
             float const&,
             float const&,
             float const&);

  void
  release();

  void
  render(Matrix4x4 const&  projection,
         Matrix4x4 const&  view,
         SharedScalarField scalarField);

  void makeScreenShot();

private:
  void
  makeGrid(VertexArray&,
           ElementArray&,
           int const&,
           int const&,
           int const&,
           float const&,
           float const&,
           float const&) const;

private:
  friend struct Private;

  Private* im;
};
}
}

#endif
