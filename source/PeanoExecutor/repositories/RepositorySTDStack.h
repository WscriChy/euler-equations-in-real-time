// This file is part of the Peano project. For conditions of distribution and 
// use, please see the copyright notice at www.peano-framework.org
#ifndef _PEANOEXECUTOR_REPOSITORIES_REPOSITORY_ARRAY_STD_H_ 
#define _PEANOEXECUTOR_REPOSITORIES_REPOSITORY_ARRAY_STD_H_ 


#include "PeanoExecutor/repositories/Repository.h"
#include "PeanoExecutor/records/RepositoryState.h"

#include "PeanoExecutor/State.h"
#include "PeanoExecutor/Vertex.h"
#include "PeanoExecutor/Cell.h"

#include "peano/grid/Grid.h"
#include "peano/stacks/CellSTDStack.h"
#include "peano/stacks/VertexSTDStack.h"


 #include "PeanoExecutor/adapters/SetupExperiment.h" 
 #include "PeanoExecutor/adapters/PerformTimeStep.h" 
 #include "PeanoExecutor/adapters/GatherValues.h" 



namespace PeanoExecutor {
      namespace repositories {
        class RepositorySTDStack;  
      }
}


class PeanoExecutor::repositories::RepositorySTDStack: public PeanoExecutor::repositories::Repository {
  private:
    static tarch::logging::Log _log;
  
    peano::geometry::Geometry& _geometry;
    
    typedef peano::stacks::CellSTDStack<PeanoExecutor::Cell>       CellStack;
    typedef peano::stacks::VertexSTDStack<PeanoExecutor::Vertex>   VertexStack;

    CellStack    _cellStack;
    VertexStack  _vertexStack;
    PeanoExecutor::State          _solverState;
    peano::grid::RegularGridContainer<PeanoExecutor::Vertex,PeanoExecutor::Cell>  _regularGridContainer;
    peano::grid::TraversalOrderOnTopLevel                                         _traversalOrderOnTopLevel;

    peano::grid::Grid<PeanoExecutor::Vertex,PeanoExecutor::Cell,PeanoExecutor::State,VertexStack,CellStack,PeanoExecutor::adapters::SetupExperiment> _gridWithSetupExperiment;
    peano::grid::Grid<PeanoExecutor::Vertex,PeanoExecutor::Cell,PeanoExecutor::State,VertexStack,CellStack,PeanoExecutor::adapters::PerformTimeStep> _gridWithPerformTimeStep;
    peano::grid::Grid<PeanoExecutor::Vertex,PeanoExecutor::Cell,PeanoExecutor::State,VertexStack,CellStack,PeanoExecutor::adapters::GatherValues> _gridWithGatherValues;

     
   PeanoExecutor::records::RepositoryState               _repositoryState;
   
    tarch::timing::Measurement _measureSetupExperimentCPUTime;
    tarch::timing::Measurement _measurePerformTimeStepCPUTime;
    tarch::timing::Measurement _measureGatherValuesCPUTime;

    tarch::timing::Measurement _measureSetupExperimentCalendarTime;
    tarch::timing::Measurement _measurePerformTimeStepCalendarTime;
    tarch::timing::Measurement _measureGatherValuesCalendarTime;

   
  public:
    RepositorySTDStack(
      peano::geometry::Geometry&                   geometry,
      const tarch::la::Vector<DIMENSIONS,double>&  domainSize,
      const tarch::la::Vector<DIMENSIONS,double>&  computationalDomainOffset
    );
    
    /**
     * Parallel Constructor
     *
     * Used in parallel mode only where the size of the domain is not known 
     * when the type of repository is determined.  
     */
    RepositorySTDStack(
      peano::geometry::Geometry&                   geometry
    );
    
    virtual ~RepositorySTDStack();

    virtual void restart(
      const tarch::la::Vector<DIMENSIONS,double>&  domainSize,
      const tarch::la::Vector<DIMENSIONS,double>&  domainOffset,
      int                                          domainLevel,
      const tarch::la::Vector<DIMENSIONS,int>&     positionOfCentralElementWithRespectToCoarserRemoteLevel
    );
         
    virtual void terminate();
        
    virtual PeanoExecutor::State& getState();
    virtual const PeanoExecutor::State& getState() const;
	
    virtual void iterate(int numberOfIterations=1);

    virtual void writeCheckpoint(peano::grid::Checkpoint<PeanoExecutor::Vertex, PeanoExecutor::Cell> * const checkpoint); 
    virtual void readCheckpoint( peano::grid::Checkpoint<PeanoExecutor::Vertex, PeanoExecutor::Cell> const * const checkpoint );
    virtual peano::grid::Checkpoint<PeanoExecutor::Vertex, PeanoExecutor::Cell>* createEmptyCheckpoint(); 

    virtual void switchToSetupExperiment();    
    virtual void switchToPerformTimeStep();    
    virtual void switchToGatherValues();    

    virtual bool isActiveAdapterSetupExperiment() const;
    virtual bool isActiveAdapterPerformTimeStep() const;
    virtual bool isActiveAdapterGatherValues() const;

   
    #ifdef Parallel
    virtual ContinueCommand continueToIterate();
    virtual void runGlobalStep();
    #endif

    virtual void setMaximumMemoryFootprintForTemporaryRegularGrids(double value);
    virtual void logIterationStatistics() const;
    virtual void clearIterationStatistics();
};


#endif
