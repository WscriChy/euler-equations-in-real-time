#include "PeanoExecutor/repositories/RepositoryArrayStack.h"

#include "tarch/Assertions.h"
#include "tarch/timing/Watch.h"

#include "tarch/compiler/CompilerSpecificSettings.h"

#ifdef Parallel
#include "tarch/parallel/Node.h"
#include "tarch/parallel/NodePool.h"
#include "peano/parallel/SendReceiveBufferPool.h"
#include "peano/parallel/loadbalancing/Oracle.h"
#endif

#include "peano/datatraversal/autotuning/Oracle.h"

#include "tarch/compiler/CompilerSpecificSettings.h"

#if !defined(CompilerICC)
#include "peano/grid/Grid.cpph"
#endif


tarch::logging::Log PeanoExecutor::repositories::RepositoryArrayStack::_log( "PeanoExecutor::repositories::RepositoryArrayStack" );


PeanoExecutor::repositories::RepositoryArrayStack::RepositoryArrayStack(
  peano::geometry::Geometry&                   geometry,
  const tarch::la::Vector<DIMENSIONS,double>&  domainSize,
  const tarch::la::Vector<DIMENSIONS,double>&  domainOffset,
  int                                          maximumSizeOfCellInOutStack,
  int                                          maximumSizeOfVertexInOutStack,
  int                                          maximumSizeOfVertexTemporaryStack
):
  _geometry(geometry),
  _cellStack(maximumSizeOfCellInOutStack),
  _vertexStack(maximumSizeOfVertexInOutStack, maximumSizeOfVertexTemporaryStack),
  _solverState(),
  _gridWithSetupExperiment(_vertexStack,_cellStack,_geometry,_solverState,domainSize,domainOffset,_regularGridContainer,_traversalOrderOnTopLevel),
  _gridWithPerformTimeStep(_vertexStack,_cellStack,_geometry,_solverState,domainSize,domainOffset,_regularGridContainer,_traversalOrderOnTopLevel),
  _gridWithGatherValues(_vertexStack,_cellStack,_geometry,_solverState,domainSize,domainOffset,_regularGridContainer,_traversalOrderOnTopLevel),

  _repositoryState() {
  logTraceIn( "RepositoryArrayStack(...)" );
  
  _repositoryState.setAction( PeanoExecutor::records::RepositoryState::Terminate );

  peano::datatraversal::autotuning::Oracle::getInstance().setNumberOfOracles(PeanoExecutor::records::RepositoryState::NumberOfAdapters);
  #ifdef Parallel
  peano::parallel::loadbalancing::Oracle::getInstance().setNumberOfOracles(PeanoExecutor::records::RepositoryState::NumberOfAdapters);
  #endif
  
  logTraceOut( "RepositoryArrayStack(...)" );
}



PeanoExecutor::repositories::RepositoryArrayStack::RepositoryArrayStack(
  peano::geometry::Geometry&                   geometry,
  int                                          maximumSizeOfCellInOutStack,
  int                                          maximumSizeOfVertexInOutStack,
  int                                          maximumSizeOfVertexTemporaryStack
):
  _geometry(geometry),
  _cellStack(maximumSizeOfCellInOutStack),
  _vertexStack(maximumSizeOfVertexInOutStack,maximumSizeOfVertexTemporaryStack),
  _solverState(),
  _gridWithSetupExperiment(_vertexStack,_cellStack,_geometry,_solverState,_regularGridContainer,_traversalOrderOnTopLevel),
  _gridWithPerformTimeStep(_vertexStack,_cellStack,_geometry,_solverState,_regularGridContainer,_traversalOrderOnTopLevel),
  _gridWithGatherValues(_vertexStack,_cellStack,_geometry,_solverState,_regularGridContainer,_traversalOrderOnTopLevel),

  _repositoryState() {
  logTraceIn( "RepositoryArrayStack(Geometry&)" );
  
  _repositoryState.setAction( PeanoExecutor::records::RepositoryState::Terminate );

  peano::datatraversal::autotuning::Oracle::getInstance().setNumberOfOracles(PeanoExecutor::records::RepositoryState::NumberOfAdapters);
  #ifdef Parallel
  peano::parallel::loadbalancing::Oracle::getInstance().setNumberOfOracles(PeanoExecutor::records::RepositoryState::NumberOfAdapters);
  #endif
  
  logTraceOut( "RepositoryArrayStack(Geometry&)" );
}
    
   
PeanoExecutor::repositories::RepositoryArrayStack::~RepositoryArrayStack() {
  assertion( _repositoryState.getAction() == PeanoExecutor::records::RepositoryState::Terminate );
}


void PeanoExecutor::repositories::RepositoryArrayStack::restart(
  const tarch::la::Vector<DIMENSIONS,double>&  domainSize,
  const tarch::la::Vector<DIMENSIONS,double>&  domainOffset,
  int                                          domainLevel,
  const tarch::la::Vector<DIMENSIONS,int>&     positionOfCentralElementWithRespectToCoarserRemoteLevel
) {
  logTraceInWith4Arguments( "restart(...)", domainSize, domainOffset, domainLevel, positionOfCentralElementWithRespectToCoarserRemoteLevel );
  #ifdef Parallel
  assertion( !tarch::parallel::Node::getInstance().isGlobalMaster());
  #endif
  
  logInfo( "restart(...)", "start node for subdomain " << domainOffset << "x" << domainSize << " on level " << domainLevel );
  
  assertion( _repositoryState.getAction() == PeanoExecutor::records::RepositoryState::Terminate );

  _vertexStack.clear();
  _cellStack.clear();

  _gridWithSetupExperiment.restart(domainSize,domainOffset,domainLevel,positionOfCentralElementWithRespectToCoarserRemoteLevel);
  _gridWithPerformTimeStep.restart(domainSize,domainOffset,domainLevel,positionOfCentralElementWithRespectToCoarserRemoteLevel);
  _gridWithGatherValues.restart(domainSize,domainOffset,domainLevel,positionOfCentralElementWithRespectToCoarserRemoteLevel);

 
   _solverState.restart();
 
  logTraceOut( "restart(...)" );
}


void PeanoExecutor::repositories::RepositoryArrayStack::terminate() {
  logTraceIn( "terminate()" );
  
  _repositoryState.setAction( PeanoExecutor::records::RepositoryState::Terminate );
  
  #ifdef Parallel
  if (tarch::parallel::Node::getInstance().isGlobalMaster()) {
    tarch::parallel::NodePool::getInstance().broadcastToWorkingNodes(
      _repositoryState,
      peano::parallel::SendReceiveBufferPool::getInstance().getIterationManagementTag()
    );
  }
  peano::parallel::SendReceiveBufferPool::getInstance().terminate();
  #endif
  
  _gridWithSetupExperiment.terminate();
  _gridWithPerformTimeStep.terminate();
  _gridWithGatherValues.terminate();

 
  logTraceOut( "terminate()" );
}


PeanoExecutor::State& PeanoExecutor::repositories::RepositoryArrayStack::getState() {
  return _solverState;
}


const PeanoExecutor::State& PeanoExecutor::repositories::RepositoryArrayStack::getState() const {
  return _solverState;
}

   
void PeanoExecutor::repositories::RepositoryArrayStack::iterate(int numberOfIterations) {
  tarch::timing::Watch watch( "PeanoExecutor::repositories::RepositoryArrayStack", "iterate(bool)", false);
  
  #ifdef Parallel
  if (tarch::parallel::Node::getInstance().isGlobalMaster()) {
    _repositoryState.setNumberOfIterations(numberOfIterations);
    tarch::parallel::NodePool::getInstance().broadcastToWorkingNodes(
      _repositoryState,
      peano::parallel::SendReceiveBufferPool::getInstance().getIterationManagementTag()
    );
  }
  else {
    assertionEquals( numberOfIterations, 1 );
    numberOfIterations = _repositoryState.getNumberOfIterations();
  }

  if ( numberOfIterations > 1 && ( peano::parallel::loadbalancing::Oracle::getInstance().isLoadBalancingActivated() || _solverState.isInvolvedInJoinOrFork() )) {
    logWarning( "iterate()", "iterate invoked for multiple traversals though load balancing is switched on or grid is not balanced globally. Use activateLoadBalancing(false) to deactivate the load balancing before" );
  }

  peano::datatraversal::autotuning::Oracle::getInstance().switchToOracle(_repositoryState.getAction());

  peano::parallel::loadbalancing::Oracle::getInstance().switchToOracle(_repositoryState.getAction());
  peano::parallel::loadbalancing::Oracle::getInstance().activateLoadBalancing(_repositoryState.getNumberOfIterations()==1);  
  
  _solverState.currentlyRunsMultipleIterations(_repositoryState.getNumberOfIterations()>1);
  #else
  peano::datatraversal::autotuning::Oracle::getInstance().switchToOracle(_repositoryState.getAction());
  #endif
  
  for (int i=0; i<numberOfIterations; i++) {
    switch ( _repositoryState.getAction()) {
      case PeanoExecutor::records::RepositoryState::UseAdapterSetupExperiment: watch.startTimer(); _gridWithSetupExperiment.iterate(); watch.stopTimer(); _measureSetupExperimentCPUTime.setValue( watch.getCPUTime() ); _measureSetupExperimentCalendarTime.setValue( watch.getCalendarTime() ); break;
      case PeanoExecutor::records::RepositoryState::UseAdapterPerformTimeStep: watch.startTimer(); _gridWithPerformTimeStep.iterate(); watch.stopTimer(); _measurePerformTimeStepCPUTime.setValue( watch.getCPUTime() ); _measurePerformTimeStepCalendarTime.setValue( watch.getCalendarTime() ); break;
      case PeanoExecutor::records::RepositoryState::UseAdapterGatherValues: watch.startTimer(); _gridWithGatherValues.iterate(); watch.stopTimer(); _measureGatherValuesCPUTime.setValue( watch.getCPUTime() ); _measureGatherValuesCalendarTime.setValue( watch.getCalendarTime() ); break;

      case PeanoExecutor::records::RepositoryState::Terminate:
        assertionMsg( false, "this branch/state should never be reached" ); 
        break;
      case PeanoExecutor::records::RepositoryState::NumberOfAdapters:
        assertionMsg( false, "this branch/state should never be reached" ); 
        break;
      case PeanoExecutor::records::RepositoryState::RunOnAllNodes:
        assertionMsg( false, "this branch/state should never be reached" ); 
        break;
      case PeanoExecutor::records::RepositoryState::ReadCheckpoint:
        assertionMsg( false, "not implemented yet" );
        break;
      case PeanoExecutor::records::RepositoryState::WriteCheckpoint:
        assertionMsg( false, "not implemented yet" );
        break;
    }
  }
    
  #ifdef Parallel
  if (_solverState.isJoiningWithMaster()) {
    _repositoryState.setAction( PeanoExecutor::records::RepositoryState::Terminate );
  }
  #endif
}

 void PeanoExecutor::repositories::RepositoryArrayStack::switchToSetupExperiment() { _repositoryState.setAction(PeanoExecutor::records::RepositoryState::UseAdapterSetupExperiment); }
 void PeanoExecutor::repositories::RepositoryArrayStack::switchToPerformTimeStep() { _repositoryState.setAction(PeanoExecutor::records::RepositoryState::UseAdapterPerformTimeStep); }
 void PeanoExecutor::repositories::RepositoryArrayStack::switchToGatherValues() { _repositoryState.setAction(PeanoExecutor::records::RepositoryState::UseAdapterGatherValues); }



 bool PeanoExecutor::repositories::RepositoryArrayStack::isActiveAdapterSetupExperiment() const { return _repositoryState.getAction() == PeanoExecutor::records::RepositoryState::UseAdapterSetupExperiment; }
 bool PeanoExecutor::repositories::RepositoryArrayStack::isActiveAdapterPerformTimeStep() const { return _repositoryState.getAction() == PeanoExecutor::records::RepositoryState::UseAdapterPerformTimeStep; }
 bool PeanoExecutor::repositories::RepositoryArrayStack::isActiveAdapterGatherValues() const { return _repositoryState.getAction() == PeanoExecutor::records::RepositoryState::UseAdapterGatherValues; }



peano::grid::Checkpoint<PeanoExecutor::Vertex, PeanoExecutor::Cell>* PeanoExecutor::repositories::RepositoryArrayStack::createEmptyCheckpoint() {
  return new peano::grid::Checkpoint<PeanoExecutor::Vertex, PeanoExecutor::Cell>();
} 


void PeanoExecutor::repositories::RepositoryArrayStack::writeCheckpoint(peano::grid::Checkpoint<PeanoExecutor::Vertex, PeanoExecutor::Cell> * const checkpoint) {
  _solverState.writeToCheckpoint( *checkpoint );
  _vertexStack.writeToCheckpoint( *checkpoint );
  _cellStack.writeToCheckpoint( *checkpoint );
} 


void PeanoExecutor::repositories::RepositoryArrayStack::setMaximumMemoryFootprintForTemporaryRegularGrids(double value) {
  _regularGridContainer.setMaximumMemoryFootprintForTemporaryRegularGrids(value);
}


void PeanoExecutor::repositories::RepositoryArrayStack::readCheckpoint( peano::grid::Checkpoint<PeanoExecutor::Vertex, PeanoExecutor::Cell> const * const checkpoint ) {
  assertionMsg( checkpoint->isValid(), "checkpoint has to be valid if you call this operation" );

  _solverState.readFromCheckpoint( *checkpoint );
  _vertexStack.readFromCheckpoint( *checkpoint );
  _cellStack.readFromCheckpoint( *checkpoint );
}


#ifdef Parallel
void PeanoExecutor::repositories::RepositoryArrayStack::runGlobalStep() {
  assertion(tarch::parallel::Node::getInstance().isGlobalMaster());

  PeanoExecutor::records::RepositoryState intermediateStateForWorkingNodes;
  intermediateStateForWorkingNodes.setAction( PeanoExecutor::records::RepositoryState::RunOnAllNodes );
  
  tarch::parallel::NodePool::getInstance().broadcastToWorkingNodes(
    intermediateStateForWorkingNodes,
    peano::parallel::SendReceiveBufferPool::getInstance().getIterationManagementTag()
  );
  tarch::parallel::NodePool::getInstance().activateIdleNodes();
}


PeanoExecutor::repositories::RepositoryArrayStack::ContinueCommand PeanoExecutor::repositories::RepositoryArrayStack::continueToIterate() {
  logTraceIn( "continueToIterate()" );

  assertion( !tarch::parallel::Node::getInstance().isGlobalMaster());

  ContinueCommand result;
  if ( _solverState.hasJoinedWithMaster() ) {
    result = Terminate;
  }
  else {
    int masterNode = tarch::parallel::Node::getInstance().getGlobalMasterRank();
    assertion( masterNode != -1 );

    _repositoryState.receive( masterNode, peano::parallel::SendReceiveBufferPool::getInstance().getIterationManagementTag(), true, ReceiveIterationControlMessagesBlocking );

    result = Continue;
    if (_repositoryState.getAction()==PeanoExecutor::records::RepositoryState::Terminate) {
      result = Terminate;
    } 
    if (_repositoryState.getAction()==PeanoExecutor::records::RepositoryState::RunOnAllNodes) {
      result = RunGlobalStep;
    } 
  }
   
  logTraceOutWith1Argument( "continueToIterate()", result );
  return result;
}
#endif


void PeanoExecutor::repositories::RepositoryArrayStack::logIterationStatistics() const {
  logInfo( "logIterationStatistics()", "|| adapter name \t || iterations \t || total CPU time [t]=s \t || average CPU time [t]=s \t || total user time [t]=s \t || average user time [t]=s  || CPU time properties  || user time properties " );  
   logInfo( "logIterationStatistics()", "| SetupExperiment \t |  " << _measureSetupExperimentCPUTime.getNumberOfMeasurements() << " \t |  " << _measureSetupExperimentCPUTime.getAccumulatedValue() << " \t |  " << _measureSetupExperimentCPUTime.getValue()  << " \t |  " << _measureSetupExperimentCalendarTime.getAccumulatedValue() << " \t |  " << _measureSetupExperimentCalendarTime.getValue() << " \t |  " << _measureSetupExperimentCPUTime.toString() << " \t |  " << _measureSetupExperimentCalendarTime.toString() );
   logInfo( "logIterationStatistics()", "| PerformTimeStep \t |  " << _measurePerformTimeStepCPUTime.getNumberOfMeasurements() << " \t |  " << _measurePerformTimeStepCPUTime.getAccumulatedValue() << " \t |  " << _measurePerformTimeStepCPUTime.getValue()  << " \t |  " << _measurePerformTimeStepCalendarTime.getAccumulatedValue() << " \t |  " << _measurePerformTimeStepCalendarTime.getValue() << " \t |  " << _measurePerformTimeStepCPUTime.toString() << " \t |  " << _measurePerformTimeStepCalendarTime.toString() );
   logInfo( "logIterationStatistics()", "| GatherValues \t |  " << _measureGatherValuesCPUTime.getNumberOfMeasurements() << " \t |  " << _measureGatherValuesCPUTime.getAccumulatedValue() << " \t |  " << _measureGatherValuesCPUTime.getValue()  << " \t |  " << _measureGatherValuesCalendarTime.getAccumulatedValue() << " \t |  " << _measureGatherValuesCalendarTime.getValue() << " \t |  " << _measureGatherValuesCPUTime.toString() << " \t |  " << _measureGatherValuesCalendarTime.toString() );

}


void PeanoExecutor::repositories::RepositoryArrayStack::clearIterationStatistics() {
   _measureSetupExperimentCPUTime.erase();
   _measurePerformTimeStepCPUTime.erase();
   _measureGatherValuesCPUTime.erase();

   _measureSetupExperimentCalendarTime.erase();
   _measurePerformTimeStepCalendarTime.erase();
   _measureGatherValuesCalendarTime.erase();

}
