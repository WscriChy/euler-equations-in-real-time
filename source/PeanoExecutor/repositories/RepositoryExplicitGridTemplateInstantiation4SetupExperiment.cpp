#include "PeanoExecutor/repositories/Repository.h"
#include "PeanoExecutor/records/RepositoryState.h"

#include "PeanoExecutor/State.h"
#include "PeanoExecutor/Vertex.h"
#include "PeanoExecutor/Cell.h"

#include "peano/grid/Grid.h"

#include "peano/stacks/CellArrayStack.h"
#include "peano/stacks/CellSTDStack.h"

#include "peano/stacks/VertexArrayStack.h"
#include "peano/stacks/VertexSTDStack.h"

 #include "PeanoExecutor/adapters/SetupExperiment.h" 
 #include "PeanoExecutor/adapters/PerformTimeStep.h" 
 #include "PeanoExecutor/adapters/GatherValues.h" 


namespace peano {
  namespace grid {
    template class Grid<PeanoExecutor::Vertex,PeanoExecutor::Cell,PeanoExecutor::State, peano::stacks::VertexArrayStack<PeanoExecutor::Vertex> ,peano::stacks::CellArrayStack<PeanoExecutor::Cell> ,PeanoExecutor::adapters::SetupExperiment>;
    template class Grid<PeanoExecutor::Vertex,PeanoExecutor::Cell,PeanoExecutor::State, peano::stacks::VertexSTDStack<  PeanoExecutor::Vertex> ,peano::stacks::CellSTDStack<  PeanoExecutor::Cell> ,PeanoExecutor::adapters::SetupExperiment>;
  }
}

#include "peano/grid/Grid.cpph"
