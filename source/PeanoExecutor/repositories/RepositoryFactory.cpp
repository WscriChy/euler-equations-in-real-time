#include "PeanoExecutor/repositories/RepositoryFactory.h"

#include "PeanoExecutor/repositories/RepositoryArrayStack.h"
#include "PeanoExecutor/repositories/RepositorySTDStack.h"

#include "PeanoExecutor/records/RepositoryState.h"

#ifdef Parallel
#include "tarch/parallel/NodePool.h"
#include "peano/parallel/Partitioner.h"
#endif


PeanoExecutor::repositories::RepositoryFactory::RepositoryFactory() {
  #ifdef Parallel
  peano::parallel::Partitioner::initDatatypes();

  PeanoExecutor::State::initDatatype();
  PeanoExecutor::Vertex::initDatatype();
  PeanoExecutor::Cell::initDatatype();

  if (PeanoExecutor::records::RepositoryState::Datatype==0) {
    PeanoExecutor::records::RepositoryState::initDatatype();
  }
  #endif
}


PeanoExecutor::repositories::RepositoryFactory::~RepositoryFactory() {
}


void PeanoExecutor::repositories::RepositoryFactory::shutdownAllParallelDatatypes() {
  #ifdef Parallel
  peano::parallel::Partitioner::shutdownDatatypes();

  PeanoExecutor::State::shutdownDatatype();
  PeanoExecutor::Vertex::shutdownDatatype();
  PeanoExecutor::Cell::shutdownDatatype();

  if (PeanoExecutor::records::RepositoryState::Datatype!=0) {
    PeanoExecutor::records::RepositoryState::shutdownDatatype();
    PeanoExecutor::records::RepositoryState::Datatype = 0;
  }
  #endif
}


PeanoExecutor::repositories::RepositoryFactory& PeanoExecutor::repositories::RepositoryFactory::getInstance() {
  static PeanoExecutor::repositories::RepositoryFactory singleton;
  return singleton;
}

    
PeanoExecutor::repositories::Repository* 
PeanoExecutor::repositories::RepositoryFactory::createWithArrayStackImplementation(
  peano::geometry::Geometry&                   geometry,
  const tarch::la::Vector<DIMENSIONS,double>&  domainSize,
  const tarch::la::Vector<DIMENSIONS,double>&  computationalDomainOffset,
  int                                          maxCellStackSize,    
  int                                          maxVertexStackSize,    
  int                                          maxTemporaryVertexStackSize    
) {
  #ifdef Parallel
  if (!tarch::parallel::Node::getInstance().isGlobalMaster()) {
    return new PeanoExecutor::repositories::RepositoryArrayStack(geometry, domainSize, computationalDomainOffset,maxCellStackSize,maxVertexStackSize,maxTemporaryVertexStackSize);
  }
  else
  #endif
  return new PeanoExecutor::repositories::RepositoryArrayStack(geometry, domainSize, computationalDomainOffset,maxCellStackSize,maxVertexStackSize,maxTemporaryVertexStackSize);
}    


PeanoExecutor::repositories::Repository* 
PeanoExecutor::repositories::RepositoryFactory::createWithSTDStackImplementation(
  peano::geometry::Geometry&                   geometry,
  const tarch::la::Vector<DIMENSIONS,double>&  domainSize,
  const tarch::la::Vector<DIMENSIONS,double>&  computationalDomainOffset
) {
  #ifdef Parallel
  if (!tarch::parallel::Node::getInstance().isGlobalMaster()) {
    return new PeanoExecutor::repositories::RepositorySTDStack(geometry);
  }
  else
  #endif
  return new PeanoExecutor::repositories::RepositorySTDStack(geometry, domainSize, computationalDomainOffset);
}
