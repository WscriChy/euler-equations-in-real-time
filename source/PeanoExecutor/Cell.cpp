#include "PeanoExecutor/Cell.h"

PeanoExecutor::Cell::
Cell()
  : Base() {
  // @todo Insert your code here
}

PeanoExecutor::Cell::
Cell(const Base::DoNotCallStandardConstructor& value)
  : Base(value) {
  // Please do not insert anything here
}

PeanoExecutor::Cell::
Cell(const Base::PersistentCell& argument)
  : Base(argument) {
  // @todo Insert your code here
}

int
PeanoExecutor::Cell::
getIndex() const {
  return _cellData.getIndex();
}

void
PeanoExecutor::Cell::
setIndex(int index) {
  _cellData.setIndex(index);
}
