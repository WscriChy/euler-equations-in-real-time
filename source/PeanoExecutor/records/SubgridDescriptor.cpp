#include "SubgridDescriptor.h"

#include "tarch/Assertions.h"

PeanoExecutor::records::SubgridDescriptor::PersistentRecords::PersistentRecords():
_size(DIMENSIONS),
_ghostSize(DIMENSIONS),
_timeStamp(2),
_unknowns(2) {

}


PeanoExecutor::records::SubgridDescriptor::PersistentRecords::PersistentRecords(const std::vector<int>& size, const std::vector<int>& ghostSize, const int& level, const int& newTimeStep, const std::vector<double>& timeStamp, const std::vector<int>& unknowns, const double& timeStepSize):
_size(size),
_ghostSize(ghostSize),
_level(level),
_newTimeStep(newTimeStep),
_timeStamp(timeStamp),
_unknowns(unknowns),
_timeStepSize(timeStepSize) {

}


 std::vector<int> PeanoExecutor::records::SubgridDescriptor::PersistentRecords::getSize() const  {
   return _size;
}



 void PeanoExecutor::records::SubgridDescriptor::PersistentRecords::setSize(const std::vector<int>& size)  {
   _size = (size);
}



 std::vector<int> PeanoExecutor::records::SubgridDescriptor::PersistentRecords::getGhostSize() const  {
   return _ghostSize;
}



 void PeanoExecutor::records::SubgridDescriptor::PersistentRecords::setGhostSize(const std::vector<int>& ghostSize)  {
   _ghostSize = (ghostSize);
}



 int PeanoExecutor::records::SubgridDescriptor::PersistentRecords::getLevel() const  {
   return _level;
}



 void PeanoExecutor::records::SubgridDescriptor::PersistentRecords::setLevel(const int& level)  {
   _level = level;
}



 int PeanoExecutor::records::SubgridDescriptor::PersistentRecords::getNewTimeStep() const  {
   return _newTimeStep;
}



 void PeanoExecutor::records::SubgridDescriptor::PersistentRecords::setNewTimeStep(const int& newTimeStep)  {
   _newTimeStep = newTimeStep;
}



 std::vector<double> PeanoExecutor::records::SubgridDescriptor::PersistentRecords::getTimeStamp() const  {
   return _timeStamp;
}



 void PeanoExecutor::records::SubgridDescriptor::PersistentRecords::setTimeStamp(const std::vector<double>& timeStamp)  {
   _timeStamp = (timeStamp);
}



 std::vector<int> PeanoExecutor::records::SubgridDescriptor::PersistentRecords::getUnknowns() const  {
   return _unknowns;
}



 void PeanoExecutor::records::SubgridDescriptor::PersistentRecords::setUnknowns(const std::vector<int>& unknowns)  {
   _unknowns = (unknowns);
}



 double PeanoExecutor::records::SubgridDescriptor::PersistentRecords::getTimeStepSize() const  {
   return _timeStepSize;
}



 void PeanoExecutor::records::SubgridDescriptor::PersistentRecords::setTimeStepSize(const double& timeStepSize)  {
   _timeStepSize = timeStepSize;
}


PeanoExecutor::records::SubgridDescriptor::SubgridDescriptor() {

}


PeanoExecutor::records::SubgridDescriptor::SubgridDescriptor(const PersistentRecords& persistentRecords):
_persistentRecords(persistentRecords._size, persistentRecords._ghostSize, persistentRecords._level, persistentRecords._newTimeStep, persistentRecords._timeStamp, persistentRecords._unknowns, persistentRecords._timeStepSize) {

}


PeanoExecutor::records::SubgridDescriptor::SubgridDescriptor(const std::vector<int>& size, const std::vector<int>& ghostSize, const int& level, const int& newTimeStep, const std::vector<double>& timeStamp, const std::vector<int>& unknowns, const double& timeStepSize):
_persistentRecords(size, ghostSize, level, newTimeStep, timeStamp, unknowns, timeStepSize) {

}


PeanoExecutor::records::SubgridDescriptor::~SubgridDescriptor() { }


 std::vector<int> PeanoExecutor::records::SubgridDescriptor::getSize() const  {
   return _persistentRecords._size;
}



 void PeanoExecutor::records::SubgridDescriptor::setSize(const std::vector<int>& size)  {
   _persistentRecords._size = (size);
}



 int PeanoExecutor::records::SubgridDescriptor::getSize(int elementIndex) const  {
   assertion(elementIndex>=0);
   assertion(elementIndex<DIMENSIONS);
   return _persistentRecords._size[elementIndex];

}



 void PeanoExecutor::records::SubgridDescriptor::setSize(int elementIndex, const int& size)  {
   assertion(elementIndex>=0);
   assertion(elementIndex<DIMENSIONS);
   _persistentRecords._size[elementIndex]= size;

}



 std::vector<int> PeanoExecutor::records::SubgridDescriptor::getGhostSize() const  {
   return _persistentRecords._ghostSize;
}



 void PeanoExecutor::records::SubgridDescriptor::setGhostSize(const std::vector<int>& ghostSize)  {
   _persistentRecords._ghostSize = (ghostSize);
}



 int PeanoExecutor::records::SubgridDescriptor::getGhostSize(int elementIndex) const  {
   assertion(elementIndex>=0);
   assertion(elementIndex<DIMENSIONS);
   return _persistentRecords._ghostSize[elementIndex];

}



 void PeanoExecutor::records::SubgridDescriptor::setGhostSize(int elementIndex, const int& ghostSize)  {
   assertion(elementIndex>=0);
   assertion(elementIndex<DIMENSIONS);
   _persistentRecords._ghostSize[elementIndex]= ghostSize;

}



 int PeanoExecutor::records::SubgridDescriptor::getLevel() const  {
   return _persistentRecords._level;
}



 void PeanoExecutor::records::SubgridDescriptor::setLevel(const int& level)  {
   _persistentRecords._level = level;
}



 int PeanoExecutor::records::SubgridDescriptor::getNewTimeStep() const  {
   return _persistentRecords._newTimeStep;
}



 void PeanoExecutor::records::SubgridDescriptor::setNewTimeStep(const int& newTimeStep)  {
   _persistentRecords._newTimeStep = newTimeStep;
}



 std::vector<double> PeanoExecutor::records::SubgridDescriptor::getTimeStamp() const  {
   return _persistentRecords._timeStamp;
}



 void PeanoExecutor::records::SubgridDescriptor::setTimeStamp(const std::vector<double>& timeStamp)  {
   _persistentRecords._timeStamp = (timeStamp);
}



 double PeanoExecutor::records::SubgridDescriptor::getTimeStamp(int elementIndex) const  {
   assertion(elementIndex>=0);
   assertion(elementIndex<2);
   return _persistentRecords._timeStamp[elementIndex];

}



 void PeanoExecutor::records::SubgridDescriptor::setTimeStamp(int elementIndex, const double& timeStamp)  {
   assertion(elementIndex>=0);
   assertion(elementIndex<2);
   _persistentRecords._timeStamp[elementIndex]= timeStamp;

}



 std::vector<int> PeanoExecutor::records::SubgridDescriptor::getUnknowns() const  {
   return _persistentRecords._unknowns;
}



 void PeanoExecutor::records::SubgridDescriptor::setUnknowns(const std::vector<int>& unknowns)  {
   _persistentRecords._unknowns = (unknowns);
}



 int PeanoExecutor::records::SubgridDescriptor::getUnknowns(int elementIndex) const  {
   assertion(elementIndex>=0);
   assertion(elementIndex<2);
   return _persistentRecords._unknowns[elementIndex];

}



 void PeanoExecutor::records::SubgridDescriptor::setUnknowns(int elementIndex, const int& unknowns)  {
   assertion(elementIndex>=0);
   assertion(elementIndex<2);
   _persistentRecords._unknowns[elementIndex]= unknowns;

}



 double PeanoExecutor::records::SubgridDescriptor::getTimeStepSize() const  {
   return _persistentRecords._timeStepSize;
}



 void PeanoExecutor::records::SubgridDescriptor::setTimeStepSize(const double& timeStepSize)  {
   _persistentRecords._timeStepSize = timeStepSize;
}




std::string PeanoExecutor::records::SubgridDescriptor::toString() const {
   std::ostringstream stringstr;
   toString(stringstr);
   return stringstr.str();
}

void PeanoExecutor::records::SubgridDescriptor::toString (std::ostream& out) const {
   out << "(";
   out << "size:[";
   for (int i = 0; i < DIMENSIONS-1; i++) {
      out << getSize(i) << ",";
   }
   out << getSize(DIMENSIONS-1) << "]";
   out << ",";
   out << "ghostSize:[";
   for (int i = 0; i < DIMENSIONS-1; i++) {
      out << getGhostSize(i) << ",";
   }
   out << getGhostSize(DIMENSIONS-1) << "]";
   out << ",";
   out << "level:" << getLevel();
   out << ",";
   out << "newTimeStep:" << getNewTimeStep();
   out << ",";
   out << "timeStamp:[";
   for (int i = 0; i < 2-1; i++) {
      out << getTimeStamp(i) << ",";
   }
   out << getTimeStamp(2-1) << "]";
   out << ",";
   out << "unknowns:[";
   for (int i = 0; i < 2-1; i++) {
      out << getUnknowns(i) << ",";
   }
   out << getUnknowns(2-1) << "]";
   out << ",";
   out << "timeStepSize:" << getTimeStepSize();
   out <<  ")";
}


PeanoExecutor::records::SubgridDescriptor::PersistentRecords PeanoExecutor::records::SubgridDescriptor::getPersistentRecords() const {
   return _persistentRecords;
}

PeanoExecutor::records::SubgridDescriptorPacked PeanoExecutor::records::SubgridDescriptor::convert() const{
   return SubgridDescriptorPacked(
      getSize(),
      getGhostSize(),
      getLevel(),
      getNewTimeStep(),
      getTimeStamp(),
      getUnknowns(),
      getTimeStepSize()
   );
}

PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::PersistentRecords():
_size(DIMENSIONS),
_ghostSize(DIMENSIONS),
_timeStamp(2),
_unknowns(2) {

}


PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::PersistentRecords(const std::vector<int>& size, const std::vector<int>& ghostSize, const int& level, const int& newTimeStep, const std::vector<double>& timeStamp, const std::vector<int>& unknowns, const double& timeStepSize):
_size(size),
_ghostSize(ghostSize),
_level(level),
_newTimeStep(newTimeStep),
_timeStamp(timeStamp),
_unknowns(unknowns),
_timeStepSize(timeStepSize) {

}


 std::vector<int> PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::getSize() const  {
   return _size;
}



 void PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::setSize(const std::vector<int>& size)  {
   _size = (size);
}



 std::vector<int> PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::getGhostSize() const  {
   return _ghostSize;
}



 void PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::setGhostSize(const std::vector<int>& ghostSize)  {
   _ghostSize = (ghostSize);
}



 int PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::getLevel() const  {
   return _level;
}



 void PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::setLevel(const int& level)  {
   _level = level;
}



 int PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::getNewTimeStep() const  {
   return _newTimeStep;
}



 void PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::setNewTimeStep(const int& newTimeStep)  {
   _newTimeStep = newTimeStep;
}



 std::vector<double> PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::getTimeStamp() const  {
   return _timeStamp;
}



 void PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::setTimeStamp(const std::vector<double>& timeStamp)  {
   _timeStamp = (timeStamp);
}



 std::vector<int> PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::getUnknowns() const  {
   return _unknowns;
}



 void PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::setUnknowns(const std::vector<int>& unknowns)  {
   _unknowns = (unknowns);
}



 double PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::getTimeStepSize() const  {
   return _timeStepSize;
}



 void PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords::setTimeStepSize(const double& timeStepSize)  {
   _timeStepSize = timeStepSize;
}


PeanoExecutor::records::SubgridDescriptorPacked::SubgridDescriptorPacked() {

}


PeanoExecutor::records::SubgridDescriptorPacked::SubgridDescriptorPacked(const PersistentRecords& persistentRecords):
_persistentRecords(persistentRecords._size, persistentRecords._ghostSize, persistentRecords._level, persistentRecords._newTimeStep, persistentRecords._timeStamp, persistentRecords._unknowns, persistentRecords._timeStepSize) {

}


PeanoExecutor::records::SubgridDescriptorPacked::SubgridDescriptorPacked(const std::vector<int>& size, const std::vector<int>& ghostSize, const int& level, const int& newTimeStep, const std::vector<double>& timeStamp, const std::vector<int>& unknowns, const double& timeStepSize):
_persistentRecords(size, ghostSize, level, newTimeStep, timeStamp, unknowns, timeStepSize) {

}


PeanoExecutor::records::SubgridDescriptorPacked::~SubgridDescriptorPacked() { }


 std::vector<int> PeanoExecutor::records::SubgridDescriptorPacked::getSize() const  {
   return _persistentRecords._size;
}



 void PeanoExecutor::records::SubgridDescriptorPacked::setSize(const std::vector<int>& size)  {
   _persistentRecords._size = (size);
}



 int PeanoExecutor::records::SubgridDescriptorPacked::getSize(int elementIndex) const  {
   assertion(elementIndex>=0);
   assertion(elementIndex<DIMENSIONS);
   return _persistentRecords._size[elementIndex];

}



 void PeanoExecutor::records::SubgridDescriptorPacked::setSize(int elementIndex, const int& size)  {
   assertion(elementIndex>=0);
   assertion(elementIndex<DIMENSIONS);
   _persistentRecords._size[elementIndex]= size;

}



 std::vector<int> PeanoExecutor::records::SubgridDescriptorPacked::getGhostSize() const  {
   return _persistentRecords._ghostSize;
}



 void PeanoExecutor::records::SubgridDescriptorPacked::setGhostSize(const std::vector<int>& ghostSize)  {
   _persistentRecords._ghostSize = (ghostSize);
}



 int PeanoExecutor::records::SubgridDescriptorPacked::getGhostSize(int elementIndex) const  {
   assertion(elementIndex>=0);
   assertion(elementIndex<DIMENSIONS);
   return _persistentRecords._ghostSize[elementIndex];

}



 void PeanoExecutor::records::SubgridDescriptorPacked::setGhostSize(int elementIndex, const int& ghostSize)  {
   assertion(elementIndex>=0);
   assertion(elementIndex<DIMENSIONS);
   _persistentRecords._ghostSize[elementIndex]= ghostSize;

}



 int PeanoExecutor::records::SubgridDescriptorPacked::getLevel() const  {
   return _persistentRecords._level;
}



 void PeanoExecutor::records::SubgridDescriptorPacked::setLevel(const int& level)  {
   _persistentRecords._level = level;
}



 int PeanoExecutor::records::SubgridDescriptorPacked::getNewTimeStep() const  {
   return _persistentRecords._newTimeStep;
}



 void PeanoExecutor::records::SubgridDescriptorPacked::setNewTimeStep(const int& newTimeStep)  {
   _persistentRecords._newTimeStep = newTimeStep;
}



 std::vector<double> PeanoExecutor::records::SubgridDescriptorPacked::getTimeStamp() const  {
   return _persistentRecords._timeStamp;
}



 void PeanoExecutor::records::SubgridDescriptorPacked::setTimeStamp(const std::vector<double>& timeStamp)  {
   _persistentRecords._timeStamp = (timeStamp);
}



 double PeanoExecutor::records::SubgridDescriptorPacked::getTimeStamp(int elementIndex) const  {
   assertion(elementIndex>=0);
   assertion(elementIndex<2);
   return _persistentRecords._timeStamp[elementIndex];

}



 void PeanoExecutor::records::SubgridDescriptorPacked::setTimeStamp(int elementIndex, const double& timeStamp)  {
   assertion(elementIndex>=0);
   assertion(elementIndex<2);
   _persistentRecords._timeStamp[elementIndex]= timeStamp;

}



 std::vector<int> PeanoExecutor::records::SubgridDescriptorPacked::getUnknowns() const  {
   return _persistentRecords._unknowns;
}



 void PeanoExecutor::records::SubgridDescriptorPacked::setUnknowns(const std::vector<int>& unknowns)  {
   _persistentRecords._unknowns = (unknowns);
}



 int PeanoExecutor::records::SubgridDescriptorPacked::getUnknowns(int elementIndex) const  {
   assertion(elementIndex>=0);
   assertion(elementIndex<2);
   return _persistentRecords._unknowns[elementIndex];

}



 void PeanoExecutor::records::SubgridDescriptorPacked::setUnknowns(int elementIndex, const int& unknowns)  {
   assertion(elementIndex>=0);
   assertion(elementIndex<2);
   _persistentRecords._unknowns[elementIndex]= unknowns;

}



 double PeanoExecutor::records::SubgridDescriptorPacked::getTimeStepSize() const  {
   return _persistentRecords._timeStepSize;
}



 void PeanoExecutor::records::SubgridDescriptorPacked::setTimeStepSize(const double& timeStepSize)  {
   _persistentRecords._timeStepSize = timeStepSize;
}




std::string PeanoExecutor::records::SubgridDescriptorPacked::toString() const {
   std::ostringstream stringstr;
   toString(stringstr);
   return stringstr.str();
}

void PeanoExecutor::records::SubgridDescriptorPacked::toString (std::ostream& out) const {
   out << "(";
   out << "size:[";
   for (int i = 0; i < DIMENSIONS-1; i++) {
      out << getSize(i) << ",";
   }
   out << getSize(DIMENSIONS-1) << "]";
   out << ",";
   out << "ghostSize:[";
   for (int i = 0; i < DIMENSIONS-1; i++) {
      out << getGhostSize(i) << ",";
   }
   out << getGhostSize(DIMENSIONS-1) << "]";
   out << ",";
   out << "level:" << getLevel();
   out << ",";
   out << "newTimeStep:" << getNewTimeStep();
   out << ",";
   out << "timeStamp:[";
   for (int i = 0; i < 2-1; i++) {
      out << getTimeStamp(i) << ",";
   }
   out << getTimeStamp(2-1) << "]";
   out << ",";
   out << "unknowns:[";
   for (int i = 0; i < 2-1; i++) {
      out << getUnknowns(i) << ",";
   }
   out << getUnknowns(2-1) << "]";
   out << ",";
   out << "timeStepSize:" << getTimeStepSize();
   out <<  ")";
}


PeanoExecutor::records::SubgridDescriptorPacked::PersistentRecords PeanoExecutor::records::SubgridDescriptorPacked::getPersistentRecords() const {
   return _persistentRecords;
}

PeanoExecutor::records::SubgridDescriptor PeanoExecutor::records::SubgridDescriptorPacked::convert() const{
   return SubgridDescriptor(
      getSize(),
      getGhostSize(),
      getLevel(),
      getNewTimeStep(),
      getTimeStamp(),
      getUnknowns(),
      getTimeStepSize()
   );
}


