#include "Unknown.h"

#include "tarch/Assertions.h"

PeanoExecutor::records::Unknown::PersistentRecords::PersistentRecords():
_velocity(DIMENSIONS) {

}


PeanoExecutor::records::Unknown::PersistentRecords::PersistentRecords(const std::vector<double>& velocity, const double& density, const double& energy, const double& marker):
_velocity(velocity),
_density(density),
_energy(energy),
_marker(marker) {

}


 std::vector<double> PeanoExecutor::records::Unknown::PersistentRecords::getVelocity() const  {
   return _velocity;
}



 void PeanoExecutor::records::Unknown::PersistentRecords::setVelocity(const std::vector<double>& velocity)  {
   _velocity = (velocity);
}



 double PeanoExecutor::records::Unknown::PersistentRecords::getDensity() const  {
   return _density;
}



 void PeanoExecutor::records::Unknown::PersistentRecords::setDensity(const double& density)  {
   _density = density;
}



 double PeanoExecutor::records::Unknown::PersistentRecords::getEnergy() const  {
   return _energy;
}



 void PeanoExecutor::records::Unknown::PersistentRecords::setEnergy(const double& energy)  {
   _energy = energy;
}



 double PeanoExecutor::records::Unknown::PersistentRecords::getMarker() const  {
   return _marker;
}



 void PeanoExecutor::records::Unknown::PersistentRecords::setMarker(const double& marker)  {
   _marker = marker;
}


PeanoExecutor::records::Unknown::Unknown() {

}


PeanoExecutor::records::Unknown::Unknown(const PersistentRecords& persistentRecords):
_persistentRecords(persistentRecords._velocity, persistentRecords._density, persistentRecords._energy, persistentRecords._marker) {

}


PeanoExecutor::records::Unknown::Unknown(const std::vector<double>& velocity, const double& density, const double& energy, const double& marker):
_persistentRecords(velocity, density, energy, marker) {

}


PeanoExecutor::records::Unknown::~Unknown() { }


 std::vector<double> PeanoExecutor::records::Unknown::getVelocity() const  {
   return _persistentRecords._velocity;
}



 void PeanoExecutor::records::Unknown::setVelocity(const std::vector<double>& velocity)  {
   _persistentRecords._velocity = (velocity);
}



 double PeanoExecutor::records::Unknown::getVelocity(int elementIndex) const  {
   assertion(elementIndex>=0);
   assertion(elementIndex<DIMENSIONS);
   return _persistentRecords._velocity[elementIndex];

}



 void PeanoExecutor::records::Unknown::setVelocity(int elementIndex, const double& velocity)  {
   assertion(elementIndex>=0);
   assertion(elementIndex<DIMENSIONS);
   _persistentRecords._velocity[elementIndex]= velocity;

}



 double PeanoExecutor::records::Unknown::getDensity() const  {
   return _persistentRecords._density;
}



 void PeanoExecutor::records::Unknown::setDensity(const double& density)  {
   _persistentRecords._density = density;
}



 double PeanoExecutor::records::Unknown::getEnergy() const  {
   return _persistentRecords._energy;
}



 void PeanoExecutor::records::Unknown::setEnergy(const double& energy)  {
   _persistentRecords._energy = energy;
}



 double PeanoExecutor::records::Unknown::getMarker() const  {
   return _persistentRecords._marker;
}



 void PeanoExecutor::records::Unknown::setMarker(const double& marker)  {
   _persistentRecords._marker = marker;
}




std::string PeanoExecutor::records::Unknown::toString() const {
   std::ostringstream stringstr;
   toString(stringstr);
   return stringstr.str();
}

void PeanoExecutor::records::Unknown::toString (std::ostream& out) const {
   out << "(";
   out << "velocity:[";
   for (int i = 0; i < DIMENSIONS-1; i++) {
      out << getVelocity(i) << ",";
   }
   out << getVelocity(DIMENSIONS-1) << "]";
   out << ",";
   out << "density:" << getDensity();
   out << ",";
   out << "energy:" << getEnergy();
   out << ",";
   out << "marker:" << getMarker();
   out <<  ")";
}


PeanoExecutor::records::Unknown::PersistentRecords PeanoExecutor::records::Unknown::getPersistentRecords() const {
   return _persistentRecords;
}

PeanoExecutor::records::UnknownPacked PeanoExecutor::records::Unknown::convert() const{
   return UnknownPacked(
      getVelocity(),
      getDensity(),
      getEnergy(),
      getMarker()
   );
}

PeanoExecutor::records::UnknownPacked::PersistentRecords::PersistentRecords():
_velocity(DIMENSIONS) {

}


PeanoExecutor::records::UnknownPacked::PersistentRecords::PersistentRecords(const std::vector<double>& velocity, const double& density, const double& energy, const double& marker):
_velocity(velocity),
_density(density),
_energy(energy),
_marker(marker) {

}


 std::vector<double> PeanoExecutor::records::UnknownPacked::PersistentRecords::getVelocity() const  {
   return _velocity;
}



 void PeanoExecutor::records::UnknownPacked::PersistentRecords::setVelocity(const std::vector<double>& velocity)  {
   _velocity = (velocity);
}



 double PeanoExecutor::records::UnknownPacked::PersistentRecords::getDensity() const  {
   return _density;
}



 void PeanoExecutor::records::UnknownPacked::PersistentRecords::setDensity(const double& density)  {
   _density = density;
}



 double PeanoExecutor::records::UnknownPacked::PersistentRecords::getEnergy() const  {
   return _energy;
}



 void PeanoExecutor::records::UnknownPacked::PersistentRecords::setEnergy(const double& energy)  {
   _energy = energy;
}



 double PeanoExecutor::records::UnknownPacked::PersistentRecords::getMarker() const  {
   return _marker;
}



 void PeanoExecutor::records::UnknownPacked::PersistentRecords::setMarker(const double& marker)  {
   _marker = marker;
}


PeanoExecutor::records::UnknownPacked::UnknownPacked() {

}


PeanoExecutor::records::UnknownPacked::UnknownPacked(const PersistentRecords& persistentRecords):
_persistentRecords(persistentRecords._velocity, persistentRecords._density, persistentRecords._energy, persistentRecords._marker) {

}


PeanoExecutor::records::UnknownPacked::UnknownPacked(const std::vector<double>& velocity, const double& density, const double& energy, const double& marker):
_persistentRecords(velocity, density, energy, marker) {

}


PeanoExecutor::records::UnknownPacked::~UnknownPacked() { }


 std::vector<double> PeanoExecutor::records::UnknownPacked::getVelocity() const  {
   return _persistentRecords._velocity;
}



 void PeanoExecutor::records::UnknownPacked::setVelocity(const std::vector<double>& velocity)  {
   _persistentRecords._velocity = (velocity);
}



 double PeanoExecutor::records::UnknownPacked::getVelocity(int elementIndex) const  {
   assertion(elementIndex>=0);
   assertion(elementIndex<DIMENSIONS);
   return _persistentRecords._velocity[elementIndex];

}



 void PeanoExecutor::records::UnknownPacked::setVelocity(int elementIndex, const double& velocity)  {
   assertion(elementIndex>=0);
   assertion(elementIndex<DIMENSIONS);
   _persistentRecords._velocity[elementIndex]= velocity;

}



 double PeanoExecutor::records::UnknownPacked::getDensity() const  {
   return _persistentRecords._density;
}



 void PeanoExecutor::records::UnknownPacked::setDensity(const double& density)  {
   _persistentRecords._density = density;
}



 double PeanoExecutor::records::UnknownPacked::getEnergy() const  {
   return _persistentRecords._energy;
}



 void PeanoExecutor::records::UnknownPacked::setEnergy(const double& energy)  {
   _persistentRecords._energy = energy;
}



 double PeanoExecutor::records::UnknownPacked::getMarker() const  {
   return _persistentRecords._marker;
}



 void PeanoExecutor::records::UnknownPacked::setMarker(const double& marker)  {
   _persistentRecords._marker = marker;
}




std::string PeanoExecutor::records::UnknownPacked::toString() const {
   std::ostringstream stringstr;
   toString(stringstr);
   return stringstr.str();
}

void PeanoExecutor::records::UnknownPacked::toString (std::ostream& out) const {
   out << "(";
   out << "velocity:[";
   for (int i = 0; i < DIMENSIONS-1; i++) {
      out << getVelocity(i) << ",";
   }
   out << getVelocity(DIMENSIONS-1) << "]";
   out << ",";
   out << "density:" << getDensity();
   out << ",";
   out << "energy:" << getEnergy();
   out << ",";
   out << "marker:" << getMarker();
   out <<  ")";
}


PeanoExecutor::records::UnknownPacked::PersistentRecords PeanoExecutor::records::UnknownPacked::getPersistentRecords() const {
   return _persistentRecords;
}

PeanoExecutor::records::Unknown PeanoExecutor::records::UnknownPacked::convert() const{
   return Unknown(
      getVelocity(),
      getDensity(),
      getEnergy(),
      getMarker()
   );
}


