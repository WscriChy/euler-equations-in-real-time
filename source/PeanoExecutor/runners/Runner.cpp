#include "PeanoExecutor/runners/Runner.h"

#include "PeanoExecutor/repositories/Repository.h"
#include "PeanoExecutor/repositories/RepositoryFactory.h"

#include "peano/utils/UserInterface.h"

#include "tarch/Assertions.h"

#include "tarch/parallel/Node.h"
#include "tarch/parallel/NodePool.h"

#include "peano/geometry/Hexahedron.h"

#include "Ui/start.hpp"

PeanoExecutor::runners::Runner::
Runner() {}

PeanoExecutor::runners::Runner::~Runner() {}

int
PeanoExecutor::runners::Runner::
run(int argc, char* argv[]) {
  // @todo Insert your geometry generation here and adopt the repository
  // generation to your needs. There is a dummy implementation to allow
  // for a quick start, but this is really very dummy (it generates
  // solely a sphere computational domain and basically does nothing with
  // it).

  // Start of dummy implementation
  peano::geometry::Hexahedron geometry(
    tarch::la::Vector<DIMENSIONS, double>(1.0),
    tarch::la::Vector<DIMENSIONS, double>(0.0)
    );
  PeanoExecutor::repositories::Repository* repository =
    PeanoExecutor::repositories::RepositoryFactory::getInstance().
    createWithSTDStackImplementation(
      geometry,
      tarch::la::Vector<DIMENSIONS, double>(1.0), // domainSize,
      tarch::la::Vector<DIMENSIONS, double>(0.0) // computationalDomainOffset
      );
  // End of dummy implementation

  int result = 0;

  if (tarch::parallel::Node::getInstance().isGlobalMaster()) {
    result = runAsMaster(repository, argc, argv);
  }
#ifdef Parallel
  else {
    result = runAsWorker(repository);
  }
#endif

  delete repository;

  return result;
}

int
PeanoExecutor::runners::Runner::
runAsMaster(PeanoExecutor::repositories::Repository* repository,
            int                                      argc,
            char*                                    argv[]) {
  peano::utils::UserInterface userInterface;
  userInterface.writeHeader();

  int exitCode = EulerEquations::Ui::start(argc, argv, repository);

  repository->logIterationStatistics();
  repository->terminate();

  return exitCode;
}
