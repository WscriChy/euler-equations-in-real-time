#ifndef _EULER_RUNNERS_RUNNER_H_
#define _EULER_RUNNERS_RUNNER_H_

#include "tarch/logging/Log.h"

namespace PeanoExecutor {
namespace runners {
class Runner;
}

namespace repositories {
class Repository;
}
}

class PeanoExecutor::runners::Runner {
private:
  static tarch::logging::Log _log;

  int
  runAsMaster(PeanoExecutor::repositories::Repository* repository,
              int                                      argc,
              char*                                    argv[]);

#ifdef Parallel
  int
  runAsWorker(PeanoExecutor::repositories::Repository* repository);

  /**
   * If the master node calls runGlobalStep() on the repository, all MPI
   * ranks besides rank 0 invoke this operation no matter whether they are
   * idle or not.
   */
  void
  runGlobalStep();

#endif

public:
  Runner();

  virtual ~Runner();

  /**
   * Run
   */
  int
  run(int argc, char* argv[]);
};

#endif
