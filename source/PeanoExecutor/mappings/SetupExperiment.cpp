#include "PeanoExecutor/mappings/SetupExperiment.h"

#include "Simulation/Grid.hpp"

#include <Uni/Logging/macros>

#include <peano/grid/aspects/VertexStateAnalysis.h>

#include <multiscalelinkedcell/SAMRTools.h>

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::CommunicationSpecification
PeanoExecutor::mappings::SetupExperiment::
communicationSpecification() {
  return peano::CommunicationSpecification(
    peano::CommunicationSpecification::
    SendDataAndStateBeforeFirstTouchVertexFirstTime,
    peano::CommunicationSpecification::
    SendDataAndStateAfterLastTouchVertexLastTime);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::SetupExperiment::
touchVertexLastTimeSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::Nop,
                                     peano::MappingSpecification::
                                     AvoidFineGridRaces);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::SetupExperiment::
touchVertexFirstTimeSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::Nop,
                                     peano::MappingSpecification::
                                     AvoidFineGridRaces);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::SetupExperiment::
enterCellSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::Nop,
                                     peano::MappingSpecification::
                                     AvoidFineGridRaces);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::SetupExperiment::
leaveCellSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::Nop,
                                     peano::MappingSpecification::
                                     AvoidFineGridRaces);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::SetupExperiment::
ascendSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::Nop,
                                     peano::MappingSpecification::
                                     AvoidCoarseGridRaces);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::SetupExperiment::
descendSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::Nop,
                                     peano::MappingSpecification::
                                     AvoidCoarseGridRaces);
}

tarch::logging::Log PeanoExecutor::mappings::SetupExperiment::_log(
  "PeanoExecutor::mappings::SetupExperiment");

PeanoExecutor::mappings::SetupExperiment::
SetupExperiment() {
  logTraceIn("SetupExperiment()");
  // @todo Insert your code here
  logTraceOut("SetupExperiment()");
}

PeanoExecutor::mappings::SetupExperiment::~SetupExperiment() {
  logTraceIn("~SetupExperiment()");
  // @todo Insert your code here
  logTraceOut("~SetupExperiment()");
}

#if defined (SharedMemoryParallelisation)
PeanoExecutor::mappings::SetupExperiment::
SetupExperiment(const SetupExperiment& masterThread) {
  logTraceIn("SetupExperiment(SetupExperiment)");
  // @todo Insert your code here
  logTraceOut("SetupExperiment(SetupExperiment)");
}

void
PeanoExecutor::mappings::SetupExperiment::
mergeWithWorkerThread(const SetupExperiment& workerThread) {
  logTraceIn("mergeWithWorkerThread(SetupExperiment)");
  // @todo Insert your code here
  logTraceOut("mergeWithWorkerThread(SetupExperiment)");
}
#endif

void
PeanoExecutor::mappings::SetupExperiment::
createHangingVertex(
  PeanoExecutor::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                fineGridH,
  PeanoExecutor::Vertex* const   coarseGridVertices,
  const peano::grid::VertexEnumerator&      coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&       coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                   fineGridPositionOfVertex) {
  logTraceInWith6Arguments("createHangingVertex(...)", fineGridVertex,
                           fineGridX, fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("createHangingVertex(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::SetupExperiment::
destroyHangingVertex(
  const PeanoExecutor::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  PeanoExecutor::Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("destroyHangingVertex(...)", fineGridVertex,
                           fineGridX, fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyHangingVertex(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::SetupExperiment::
createInnerVertex(
  PeanoExecutor::Vertex&           fineGridVertex,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridX,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridH,
  PeanoExecutor::Vertex* const     coarseGridVertices,
  const peano::grid::VertexEnumerator&
  coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&             coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&    fineGridPositionOfVertex) {
  logTraceInWith6Arguments("createInnerVertex(...)", fineGridVertex, fineGridX,
                           fineGridH, coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);

  const int CoarsestRegularLevel = 1;

  if (coarseGridVerticesEnumerator.getLevel() < CoarsestRegularLevel) {
    fineGridVertex.refine();
  }
  logTraceOutWith1Argument("createInnerVertex(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::SetupExperiment::
createBoundaryVertex(
  PeanoExecutor::Vertex&           fineGridVertex,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridX,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridH,
  PeanoExecutor::Vertex* const     coarseGridVertices,
  const peano::grid::VertexEnumerator&
  coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&             coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&    fineGridPositionOfVertex) {
  logTraceInWith6Arguments("createBoundaryVertex(...)", fineGridVertex,
                           fineGridX, fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);

  const int CoarsestRegularLevel = 1;

  if (coarseGridVerticesEnumerator.getLevel() < CoarsestRegularLevel) {
    fineGridVertex.refine();
  }

  logTraceOutWith1Argument("createBoundaryVertex(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::SetupExperiment::
destroyVertex(
  const PeanoExecutor::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  PeanoExecutor::Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("destroyVertex(...)", fineGridVertex, fineGridX,
                           fineGridH, coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyVertex(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::SetupExperiment::
createCell(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell) {
  logTraceInWith4Arguments("createCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);

  using Grid           = EulerEquations::Simulation::Grid;
  using GridHeap       = EulerEquations::Simulation::GridHeap;
  using ScenarioRunner = EulerEquations::Simulation::ScenarioRunner;

  typedef Eigen::Matrix<int, DIMENSIONS, 1> Vector3;

  Vector3 cellsPerDimension({ _state->scenario()->xSize() / 3,
                              _state->scenario()->ySize() / 3,
                              _state->scenario()->zSize() / 3 });

  Vector3 ghostCellsPerDimension({ 1, 1, 1 });

  int const newHeapIndex = GridHeap::getInstance().createData(1);
  fineGridCell.setIndex(newHeapIndex);

  if (peano::grid::aspects::VertexStateAnalysis::doesNoVertexCarryRefinementFlag(
        fineGridVertices,
        fineGridVerticesEnumerator,
        Vertex::Records::Refining) &&
      peano::grid::aspects::VertexStateAnalysis::doesNoVertexCarryRefinementFlag(
        fineGridVertices,
        fineGridVerticesEnumerator,
        Vertex::Records::RefinementTriggered)) {
    //

    int const numberOfCells = (cellsPerDimension +
                               2 * ghostCellsPerDimension).colwise().prod()(0);

    GridHeap::getInstance()
    .getData(newHeapIndex)[0].setSize(
      std::vector<int>(cellsPerDimension.data(), cellsPerDimension.data() +
                       DIMENSIONS));
    GridHeap::getInstance()
    .getData(newHeapIndex)[0].setGhostSize(
      std::vector<int>(ghostCellsPerDimension.data(),
                       ghostCellsPerDimension.data() + DIMENSIONS));
    GridHeap::getInstance()
    .getData(newHeapIndex)[0].setLevel(fineGridVerticesEnumerator.getLevel());
    GridHeap::getInstance()
    .getData(newHeapIndex)[0].setNewTimeStep(0);
    GridHeap::getInstance()
    .getData(newHeapIndex)[0].setUnknowns(0,
                                          Grid::UnknownHeap::getInstance()
                                          .createData(numberOfCells));
    GridHeap::getInstance()
    .getData(newHeapIndex)[0].setUnknowns(1,
                                          Grid::UnknownHeap::getInstance()
                                          .createData(numberOfCells));

    ScenarioRunner::Vector3 gridWidth(
      {
        fineGridVerticesEnumerator.getCellSize()(0) / cellsPerDimension[0],
        fineGridVerticesEnumerator.getCellSize()(1) / cellsPerDimension[1],
        fineGridVerticesEnumerator.getCellSize()(2) / cellsPerDimension[2]
      });

    ScenarioRunner::Vector3 position(
      {
        fineGridVerticesEnumerator.getVertexPosition()[0],
        fineGridVerticesEnumerator.getVertexPosition()[1],
        fineGridVerticesEnumerator.getVertexPosition()[2]
      });

    Grid grid(
      &GridHeap::getInstance().getData(newHeapIndex)[0]);

    auto scenarioRunner = _state->scenario()->createRunner();
    scenarioRunner->initialize(&grid, position, gridWidth);
    auto scenarioData = scenarioRunner->data();
    _state->scenarioData()->join(scenarioData);
  } else {
    GridHeap::getInstance()
    .getData(newHeapIndex)[0].setSize(0, 0);
    GridHeap::getInstance()
    .getData(newHeapIndex)[0].setSize(1, 0);
    GridHeap::getInstance()
    .getData(newHeapIndex)[0].setGhostSize(0, 0);
    GridHeap::getInstance()
    .getData(newHeapIndex)[0].setGhostSize(1, 0);
    GridHeap::getInstance()
    .getData(newHeapIndex)[0].setNewTimeStep(-1);
    GridHeap::getInstance()
    .getData(newHeapIndex)[0].setLevel(fineGridVerticesEnumerator.getLevel());
    GridHeap::getInstance()
    .getData(newHeapIndex)[0].setTimeStamp(0, 0.0);
    GridHeap::getInstance()
    .getData(newHeapIndex)[0].setTimeStamp(1, 0.0);
  }

  logTraceOutWith1Argument("createCell(...)", fineGridCell);
}

void
PeanoExecutor::mappings::SetupExperiment::
destroyCell(
  const PeanoExecutor::Cell&           fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell) {
  logTraceInWith4Arguments("destroyCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyCell(...)", fineGridCell);
}

#ifdef Parallel
void
PeanoExecutor::mappings::SetupExperiment::
mergeWithNeighbour(
  PeanoExecutor::Vertex& vertex,
  const PeanoExecutor::Vertex& neighbour,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&   fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&   fineGridH,
  int                                           level) {
  logTraceInWith6Arguments("mergeWithNeighbour(...)", vertex, neighbour,
                           fromRank, fineGridX, fineGridH, level);
  // @todo Insert your code here
  logTraceOut("mergeWithNeighbour(...)");
}

void
PeanoExecutor::mappings::SetupExperiment::
prepareSendToNeighbour(
  PeanoExecutor::Vertex& vertex,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level) {
  logTraceInWith3Arguments("prepareSendToNeighbour(...)", vertex, toRank,
                           level);
  // @todo Insert your code here
  logTraceOut("prepareSendToNeighbour(...)");
}

void
PeanoExecutor::mappings::SetupExperiment::
prepareCopyToRemoteNode(
  PeanoExecutor::Vertex& localVertex,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level) {
  logTraceInWith5Arguments("prepareCopyToRemoteNode(...)", localVertex, toRank,
                           x, h, level);
  // @todo Insert your code here
  logTraceOut("prepareCopyToRemoteNode(...)");
}

void
PeanoExecutor::mappings::SetupExperiment::
prepareCopyToRemoteNode(
  PeanoExecutor::Cell& localCell,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&   cellSize,
  int                                           level) {
  logTraceInWith5Arguments("prepareCopyToRemoteNode(...)", localCell, toRank,
                           cellCentre, cellSize, level);
  // @todo Insert your code here
  logTraceOut("prepareCopyToRemoteNode(...)");
}

void
PeanoExecutor::mappings::SetupExperiment::
mergeWithRemoteDataDueToForkOrJoin(
  PeanoExecutor::Vertex& localVertex,
  const PeanoExecutor::Vertex& masterOrWorkerVertex,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&  x,
  const tarch::la::Vector<DIMENSIONS, double>&  h,
  int                                       level) {
  logTraceInWith6Arguments("mergeWithRemoteDataDueToForkOrJoin(...)",
                           localVertex, masterOrWorkerVertex, fromRank, x, h,
                           level);
  // @todo Insert your code here
  logTraceOut("mergeWithRemoteDataDueToForkOrJoin(...)");
}

void
PeanoExecutor::mappings::SetupExperiment::
mergeWithRemoteDataDueToForkOrJoin(
  PeanoExecutor::Cell& localCell,
  const PeanoExecutor::Cell& masterOrWorkerCell,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&  cellSize,
  int                                       level) {
  logTraceInWith3Arguments("mergeWithRemoteDataDueToForkOrJoin(...)", localCell,
                           masterOrWorkerCell, fromRank);
  // @todo Insert your code here
  logTraceOut("mergeWithRemoteDataDueToForkOrJoin(...)");
}

bool
PeanoExecutor::mappings::SetupExperiment::
prepareSendToWorker(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell,
  int                                  worker) {
  logTraceIn("prepareSendToWorker(...)");
  // @todo Insert your code here
  logTraceOutWith1Argument("prepareSendToWorker(...)", true);

  return true;
}

void
PeanoExecutor::mappings::SetupExperiment::
prepareSendToMaster(
  PeanoExecutor::Cell& localCell,
  PeanoExecutor::Vertex* vertices,
  const peano::grid::VertexEnumerator& verticesEnumerator,
  const PeanoExecutor::Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  const PeanoExecutor::Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&   fineGridPositionOfCell) {
  logTraceInWith2Arguments("prepareSendToMaster(...)", localCell,
                           verticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("prepareSendToMaster(...)");
}

void
PeanoExecutor::mappings::SetupExperiment::
mergeWithMaster(
  const PeanoExecutor::Cell&           workerGridCell,
  PeanoExecutor::Vertex* const         workerGridVertices,
  const peano::grid::VertexEnumerator& workerEnumerator,
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell,
  int                                  worker,
  const PeanoExecutor::State&          workerState,
  PeanoExecutor::State&                masterState) {
  logTraceIn("mergeWithMaster(...)");
  // @todo Insert your code here
  logTraceOut("mergeWithMaster(...)");
}

void
PeanoExecutor::mappings::SetupExperiment::
receiveDataFromMaster(
  PeanoExecutor::Cell& receivedCell,
  PeanoExecutor::Vertex* receivedVertices,
  const peano::grid::VertexEnumerator& receivedVerticesEnumerator,
  PeanoExecutor::Vertex* const receivedCoarseGridVertices,
  const peano::grid::VertexEnumerator& receivedCoarseGridVerticesEnumerator,
  PeanoExecutor::Cell& receivedCoarseGridCell,
  PeanoExecutor::Vertex* const workersCoarseGridVertices,
  const peano::grid::VertexEnumerator& workersCoarseGridVerticesEnumerator,
  PeanoExecutor::Cell& workersCoarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&    fineGridPositionOfCell) {
  logTraceIn("receiveDataFromMaster(...)");
  // @todo Insert your code here
  logTraceOut("receiveDataFromMaster(...)");
}

void
PeanoExecutor::mappings::SetupExperiment::
mergeWithWorker(
  PeanoExecutor::Cell& localCell,
  const PeanoExecutor::Cell& receivedMasterCell,
  const tarch::la::Vector<DIMENSIONS, double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&  cellSize,
  int                                          level) {
  logTraceInWith2Arguments("mergeWithWorker(...)", localCell.toString(),
                           receivedMasterCell.toString());
  // @todo Insert your code here
  logTraceOutWith1Argument("mergeWithWorker(...)", localCell.toString());
}

void
PeanoExecutor::mappings::SetupExperiment::
mergeWithWorker(
  PeanoExecutor::Vertex& localVertex,
  const PeanoExecutor::Vertex& receivedMasterVertex,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level) {
  logTraceInWith2Arguments("mergeWithWorker(...)", localVertex.toString(),
                           receivedMasterVertex.toString());
  // @todo Insert your code here
  logTraceOutWith1Argument("mergeWithWorker(...)", localVertex.toString());
}
#endif

void
PeanoExecutor::mappings::SetupExperiment::
touchVertexFirstTime(
  PeanoExecutor::Vertex&           fineGridVertex,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridX,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridH,
  PeanoExecutor::Vertex* const     coarseGridVertices,
  const peano::grid::VertexEnumerator&
  coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&             coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&    fineGridPositionOfVertex) {
  logTraceInWith6Arguments("touchVertexFirstTime(...)", fineGridVertex,
                           fineGridX, fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("touchVertexFirstTime(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::SetupExperiment::
touchVertexLastTime(
  PeanoExecutor::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  PeanoExecutor::Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("touchVertexLastTime(...)", fineGridVertex,
                           fineGridX, fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("touchVertexLastTime(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::SetupExperiment::
enterCell(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell) {
  logTraceInWith4Arguments("enterCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("enterCell(...)", fineGridCell);
}

void
PeanoExecutor::mappings::SetupExperiment::
leaveCell(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell) {
  logTraceInWith4Arguments("leaveCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("leaveCell(...)", fineGridCell);
}

void
PeanoExecutor::mappings::SetupExperiment::
beginIteration(
  PeanoExecutor::State& solverState) {
  logTraceInWith1Argument("beginIteration(State)", solverState);

  _state = &solverState;
  _state->resetScenarioData();

  logTraceOutWith1Argument("beginIteration(State)", solverState);
}

void
PeanoExecutor::mappings::SetupExperiment::
endIteration(
  PeanoExecutor::State& solverState) {
  logTraceInWith1Argument("endIteration(State)", solverState);
  // @todo Insert your code here
  logTraceOutWith1Argument("endIteration(State)", solverState);
}

void
PeanoExecutor::mappings::SetupExperiment::
descend(
  PeanoExecutor::Cell* const           fineGridCells,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell) {
  logTraceInWith2Arguments("descend(...)", coarseGridCell.toString(),
                           coarseGridVerticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("descend(...)");
}

void
PeanoExecutor::mappings::SetupExperiment::
ascend(
  PeanoExecutor::Cell* const           fineGridCells,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell) {
  logTraceInWith2Arguments("ascend(...)", coarseGridCell.toString(),
                           coarseGridVerticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("ascend(...)");
}
