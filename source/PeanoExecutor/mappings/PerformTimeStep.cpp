#include "PeanoExecutor/mappings/PerformTimeStep.h"

#include "Simulation/Grid.hpp"

#include <Uni/Logging/macros>

#include <multiscalelinkedcell/HangingVertexBookkeeper.h>

#include <memory>

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::CommunicationSpecification
PeanoExecutor::mappings::PerformTimeStep::
communicationSpecification() {
  return peano::CommunicationSpecification(
    peano::CommunicationSpecification::
    SendDataAndStateBeforeFirstTouchVertexFirstTime,
    peano::CommunicationSpecification::
    SendDataAndStateAfterLastTouchVertexLastTime);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::PerformTimeStep::
touchVertexLastTimeSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::
                                     AvoidFineGridRaces);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::PerformTimeStep::
touchVertexFirstTimeSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::
                                     AvoidFineGridRaces);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::PerformTimeStep::
enterCellSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::OnlyLeaves,
                                     peano::MappingSpecification::
                                     AvoidFineGridRaces);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::PerformTimeStep::
leaveCellSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::
                                     AvoidFineGridRaces);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::PerformTimeStep::
ascendSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::
                                     AvoidCoarseGridRaces);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::PerformTimeStep::
descendSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::
                                     AvoidCoarseGridRaces);
}

tarch::logging::Log PeanoExecutor::mappings::PerformTimeStep::_log(
  "PeanoExecutor::mappings::PerformTimeStep");

PeanoExecutor::mappings::PerformTimeStep::
PerformTimeStep() {
  logTraceIn("PerformTimeStep()");
  // @todo Insert your code here
  logTraceOut("PerformTimeStep()");
}

PeanoExecutor::mappings::PerformTimeStep::~PerformTimeStep() {
  logTraceIn("~PerformTimeStep()");
  // @todo Insert your code here
  logTraceOut("~PerformTimeStep()");
}

#if defined (SharedMemoryParallelisation)
PeanoExecutor::mappings::PerformTimeStep::
PerformTimeStep(const PerformTimeStep& masterThread) {
  logTraceIn("PerformTimeStep(PerformTimeStep)");
  // @todo Insert your code here
  logTraceOut("PerformTimeStep(PerformTimeStep)");
}

void
PeanoExecutor::mappings::PerformTimeStep::
mergeWithWorkerThread(const PerformTimeStep& workerThread) {
  logTraceIn("mergeWithWorkerThread(PerformTimeStep)");
  // @todo Insert your code here
  logTraceOut("mergeWithWorkerThread(PerformTimeStep)");
}
#endif

void
PeanoExecutor::mappings::PerformTimeStep::
createHangingVertex(
  PeanoExecutor::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                fineGridH,
  PeanoExecutor::Vertex* const   coarseGridVertices,
  const peano::grid::VertexEnumerator&      coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&       coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                   fineGridPositionOfVertex) {
  logTraceInWith6Arguments("createHangingVertex(...)", fineGridVertex,
                           fineGridX, fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("createHangingVertex(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::PerformTimeStep::
destroyHangingVertex(
  const PeanoExecutor::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  PeanoExecutor::Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("destroyHangingVertex(...)", fineGridVertex,
                           fineGridX, fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyHangingVertex(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::PerformTimeStep::
createInnerVertex(
  PeanoExecutor::Vertex&           fineGridVertex,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridX,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridH,
  PeanoExecutor::Vertex* const     coarseGridVertices,
  const peano::grid::VertexEnumerator&
  coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&             coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&    fineGridPositionOfVertex) {
  logTraceInWith6Arguments("createInnerVertex(...)", fineGridVertex, fineGridX,
                           fineGridH, coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("createInnerVertex(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::PerformTimeStep::
createBoundaryVertex(
  PeanoExecutor::Vertex&           fineGridVertex,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridX,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridH,
  PeanoExecutor::Vertex* const     coarseGridVertices,
  const peano::grid::VertexEnumerator&
  coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&             coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&    fineGridPositionOfVertex) {
  logTraceInWith6Arguments("createBoundaryVertex(...)", fineGridVertex,
                           fineGridX, fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("createBoundaryVertex(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::PerformTimeStep::
destroyVertex(
  const PeanoExecutor::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  PeanoExecutor::Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("destroyVertex(...)", fineGridVertex, fineGridX,
                           fineGridH, coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyVertex(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::PerformTimeStep::
createCell(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell) {
  logTraceInWith4Arguments("createCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("createCell(...)", fineGridCell);
}

void
PeanoExecutor::mappings::PerformTimeStep::
destroyCell(
  const PeanoExecutor::Cell&           fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell) {
  logTraceInWith4Arguments("destroyCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyCell(...)", fineGridCell);
}

#ifdef Parallel
void
PeanoExecutor::mappings::PerformTimeStep::
mergeWithNeighbour(
  PeanoExecutor::Vertex& vertex,
  const PeanoExecutor::Vertex& neighbour,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&   fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&   fineGridH,
  int                                           level) {
  logTraceInWith6Arguments("mergeWithNeighbour(...)", vertex, neighbour,
                           fromRank, fineGridX, fineGridH, level);
  // @todo Insert your code here
  logTraceOut("mergeWithNeighbour(...)");
}

void
PeanoExecutor::mappings::PerformTimeStep::
prepareSendToNeighbour(
  PeanoExecutor::Vertex& vertex,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level) {
  logTraceInWith3Arguments("prepareSendToNeighbour(...)", vertex, toRank,
                           level);
  // @todo Insert your code here
  logTraceOut("prepareSendToNeighbour(...)");
}

void
PeanoExecutor::mappings::PerformTimeStep::
prepareCopyToRemoteNode(
  PeanoExecutor::Vertex& localVertex,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level) {
  logTraceInWith5Arguments("prepareCopyToRemoteNode(...)", localVertex, toRank,
                           x, h, level);
  // @todo Insert your code here
  logTraceOut("prepareCopyToRemoteNode(...)");
}

void
PeanoExecutor::mappings::PerformTimeStep::
prepareCopyToRemoteNode(
  PeanoExecutor::Cell& localCell,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&   cellSize,
  int                                           level) {
  logTraceInWith5Arguments("prepareCopyToRemoteNode(...)", localCell, toRank,
                           cellCentre, cellSize, level);
  // @todo Insert your code here
  logTraceOut("prepareCopyToRemoteNode(...)");
}

void
PeanoExecutor::mappings::PerformTimeStep::
mergeWithRemoteDataDueToForkOrJoin(
  PeanoExecutor::Vertex& localVertex,
  const PeanoExecutor::Vertex& masterOrWorkerVertex,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&  x,
  const tarch::la::Vector<DIMENSIONS, double>&  h,
  int                                       level) {
  logTraceInWith6Arguments("mergeWithRemoteDataDueToForkOrJoin(...)",
                           localVertex, masterOrWorkerVertex, fromRank, x, h,
                           level);
  // @todo Insert your code here
  logTraceOut("mergeWithRemoteDataDueToForkOrJoin(...)");
}

void
PeanoExecutor::mappings::PerformTimeStep::
mergeWithRemoteDataDueToForkOrJoin(
  PeanoExecutor::Cell& localCell,
  const PeanoExecutor::Cell& masterOrWorkerCell,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&  cellSize,
  int                                       level) {
  logTraceInWith3Arguments("mergeWithRemoteDataDueToForkOrJoin(...)", localCell,
                           masterOrWorkerCell, fromRank);
  // @todo Insert your code here
  logTraceOut("mergeWithRemoteDataDueToForkOrJoin(...)");
}

bool
PeanoExecutor::mappings::PerformTimeStep::
prepareSendToWorker(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell,
  int                                  worker) {
  logTraceIn("prepareSendToWorker(...)");
  // @todo Insert your code here
  logTraceOutWith1Argument("prepareSendToWorker(...)", true);

  return true;
}

void
PeanoExecutor::mappings::PerformTimeStep::
prepareSendToMaster(
  PeanoExecutor::Cell& localCell,
  PeanoExecutor::Vertex* vertices,
  const peano::grid::VertexEnumerator& verticesEnumerator,
  const PeanoExecutor::Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  const PeanoExecutor::Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&   fineGridPositionOfCell) {
  logTraceInWith2Arguments("prepareSendToMaster(...)", localCell,
                           verticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("prepareSendToMaster(...)");
}

void
PeanoExecutor::mappings::PerformTimeStep::
mergeWithMaster(
  const PeanoExecutor::Cell&           workerGridCell,
  PeanoExecutor::Vertex* const         workerGridVertices,
  const peano::grid::VertexEnumerator& workerEnumerator,
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell,
  int                                  worker,
  const PeanoExecutor::State&          workerState,
  PeanoExecutor::State&                masterState) {
  logTraceIn("mergeWithMaster(...)");
  // @todo Insert your code here
  logTraceOut("mergeWithMaster(...)");
}

void
PeanoExecutor::mappings::PerformTimeStep::
receiveDataFromMaster(
  PeanoExecutor::Cell& receivedCell,
  PeanoExecutor::Vertex* receivedVertices,
  const peano::grid::VertexEnumerator& receivedVerticesEnumerator,
  PeanoExecutor::Vertex* const receivedCoarseGridVertices,
  const peano::grid::VertexEnumerator& receivedCoarseGridVerticesEnumerator,
  PeanoExecutor::Cell& receivedCoarseGridCell,
  PeanoExecutor::Vertex* const workersCoarseGridVertices,
  const peano::grid::VertexEnumerator& workersCoarseGridVerticesEnumerator,
  PeanoExecutor::Cell& workersCoarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&    fineGridPositionOfCell) {
  logTraceIn("receiveDataFromMaster(...)");
  // @todo Insert your code here
  logTraceOut("receiveDataFromMaster(...)");
}

void
PeanoExecutor::mappings::PerformTimeStep::
mergeWithWorker(
  PeanoExecutor::Cell& localCell,
  const PeanoExecutor::Cell& receivedMasterCell,
  const tarch::la::Vector<DIMENSIONS, double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&  cellSize,
  int                                          level) {
  logTraceInWith2Arguments("mergeWithWorker(...)", localCell.toString(),
                           receivedMasterCell.toString());
  // @todo Insert your code here
  logTraceOutWith1Argument("mergeWithWorker(...)", localCell.toString());
}

void
PeanoExecutor::mappings::PerformTimeStep::
mergeWithWorker(
  PeanoExecutor::Vertex& localVertex,
  const PeanoExecutor::Vertex& receivedMasterVertex,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level) {
  logTraceInWith2Arguments("mergeWithWorker(...)", localVertex.toString(),
                           receivedMasterVertex.toString());
  // @todo Insert your code here
  logTraceOutWith1Argument("mergeWithWorker(...)", localVertex.toString());
}
#endif

void
PeanoExecutor::mappings::PerformTimeStep::
touchVertexFirstTime(
  PeanoExecutor::Vertex&           fineGridVertex,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridX,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridH,
  PeanoExecutor::Vertex* const     coarseGridVertices,
  const peano::grid::VertexEnumerator&
  coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&             coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&    fineGridPositionOfVertex) {
  logTraceInWith6Arguments("touchVertexFirstTime(...)", fineGridVertex,
                           fineGridX, fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("touchVertexFirstTime(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::PerformTimeStep::
touchVertexLastTime(
  PeanoExecutor::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  PeanoExecutor::Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("touchVertexLastTime(...)", fineGridVertex,
                           fineGridX, fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("touchVertexLastTime(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::PerformTimeStep::
enterCell(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell) {
  logTraceInWith4Arguments("enterCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);
  using Grid           = EulerEquations::Simulation::Grid;
  using GridHeap       = EulerEquations::Simulation::GridHeap;
  using ScenarioRunner = EulerEquations::Simulation::ScenarioRunner;

  if (!fineGridCell.isRefined()) {
    //

    const tarch::la::Vector<THREE_POWER_D, int> neighbourCellIndices =
      multiscalelinkedcell::getIndicesAroundCell(
        Vertex::readIndex(fineGridVerticesEnumerator, fineGridVertices));

    Grid grid(
      &GridHeap::getInstance().getData(fineGridCell.getIndex())[0]);

    using UniqueGrid = std::unique_ptr<Grid>;

    UniqueGrid leftGrid   = 0;
    UniqueGrid rightGrid  = 0;
    UniqueGrid bottomGrid = 0;
    UniqueGrid topGrid    = 0;
    UniqueGrid backGrid   = 0;
    UniqueGrid frontGrid  = 0;

    if (neighbourCellIndices[12] >= 0) {
      leftGrid = UniqueGrid(
        new Grid(&GridHeap::getInstance().getData(
                   neighbourCellIndices[12])[0]));
    }

    if (neighbourCellIndices[14] >= 0) {
      rightGrid = UniqueGrid(
        new Grid(&GridHeap::getInstance().getData(
                   neighbourCellIndices[14])[0]));
    }

    if (neighbourCellIndices[10] >= 0) {
      bottomGrid = UniqueGrid(
        new Grid(&GridHeap::getInstance().getData(
                   neighbourCellIndices[10])[0]));
    }

    if (neighbourCellIndices[16] >= 0) {
      topGrid = UniqueGrid(
        new Grid(&GridHeap::getInstance().getData(
                   neighbourCellIndices[16])[0]));
    }

    if (neighbourCellIndices[4] >= 0) {
      backGrid = UniqueGrid(
        new Grid(&GridHeap::getInstance().getData(
                   neighbourCellIndices[4])[0]));
    }

    if (neighbourCellIndices[22] >= 0) {
      frontGrid = UniqueGrid(
        new Grid(&GridHeap::getInstance().getData(
                   neighbourCellIndices[22])[0]));
    }

    ScenarioRunner::Vector3 gridWidth(
      {
        fineGridVerticesEnumerator.getCellSize()(0) / (double)grid.innerSize(0),
        fineGridVerticesEnumerator.getCellSize()(1) / (double)grid.innerSize(1),
        fineGridVerticesEnumerator.getCellSize()(2) / (double)grid.innerSize(2)
      });

    ScenarioRunner::Vector3 position(
      {
        fineGridVerticesEnumerator.getVertexPosition(0)[0],
        fineGridVerticesEnumerator.getVertexPosition(0)[1],
        fineGridVerticesEnumerator.getVertexPosition(0)[2]
      });

    auto scenarioRunner = _state->scenario()->createRunner(_scenarioData);
    scenarioRunner->iterate(leftGrid.get(),
                            rightGrid.get(),
                            bottomGrid.get(),
                            topGrid.get(),
                            backGrid.get(),
                            frontGrid.get(),
                            &grid,
                            position,
                            gridWidth);
    auto scenarioData = scenarioRunner->data();
    _state->scenarioData()->join(scenarioData);
  }

  logTraceOutWith1Argument("enterCell(...)", fineGridCell);
}

void
PeanoExecutor::mappings::PerformTimeStep::
leaveCell(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell) {
  logTraceInWith4Arguments("leaveCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("leaveCell(...)", fineGridCell);
}

void
PeanoExecutor::mappings::PerformTimeStep::
beginIteration(
  PeanoExecutor::State& solverState) {
  logTraceInWith1Argument("beginIteration(State)", solverState);
  _state        = &solverState;
  _scenarioData = _state->scenarioData();
  _state->resetScenarioData();
  logTraceOutWith1Argument("beginIteration(State)", solverState);
}

void
PeanoExecutor::mappings::PerformTimeStep::
endIteration(
  PeanoExecutor::State& solverState) {
  logTraceInWith1Argument("endIteration(State)", solverState);
  // @todo Insert your code here
  logTraceOutWith1Argument("endIteration(State)", solverState);
}

void
PeanoExecutor::mappings::PerformTimeStep::
descend(
  PeanoExecutor::Cell* const           fineGridCells,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell) {
  logTraceInWith2Arguments("descend(...)", coarseGridCell.toString(),
                           coarseGridVerticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("descend(...)");
}

void
PeanoExecutor::mappings::PerformTimeStep::
ascend(
  PeanoExecutor::Cell* const           fineGridCells,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell) {
  logTraceInWith2Arguments("ascend(...)", coarseGridCell.toString(),
                           coarseGridVerticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("ascend(...)");
}
