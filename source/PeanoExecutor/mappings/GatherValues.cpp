#include "PeanoExecutor/mappings/GatherValues.h"

#include "Simulation/Grid.hpp"

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::CommunicationSpecification
PeanoExecutor::mappings::GatherValues::
communicationSpecification() {
  return peano::CommunicationSpecification(
    peano::CommunicationSpecification::
    SendDataAndStateBeforeFirstTouchVertexFirstTime,
    peano::CommunicationSpecification::
    SendDataAndStateAfterLastTouchVertexLastTime);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::GatherValues::
touchVertexLastTimeSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::
                                     AvoidFineGridRaces);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::GatherValues::
touchVertexFirstTimeSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::
                                     AvoidFineGridRaces);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::GatherValues::
enterCellSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::
                                     AvoidFineGridRaces);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::GatherValues::
leaveCellSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::
                                     AvoidFineGridRaces);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::GatherValues::
ascendSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::
                                     AvoidCoarseGridRaces);
}

/**
 * @todo Please tailor the parameters to your mapping's properties.
 */
peano::MappingSpecification
PeanoExecutor::mappings::GatherValues::
descendSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,
                                     peano::MappingSpecification::
                                     AvoidCoarseGridRaces);
}

tarch::logging::Log PeanoExecutor::mappings::GatherValues::_log(
  "PeanoExecutor::mappings::GatherValues");

PeanoExecutor::mappings::GatherValues::
GatherValues() {
  logTraceIn("GatherValues()");
  // @todo Insert your code here
  logTraceOut("GatherValues()");
}

PeanoExecutor::mappings::GatherValues::~GatherValues() {
  logTraceIn("~GatherValues()");
  // @todo Insert your code here
  logTraceOut("~GatherValues()");
}

#if defined (SharedMemoryParallelisation)
PeanoExecutor::mappings::GatherValues::
GatherValues(const GatherValues& masterThread) {
  logTraceIn("GatherValues(GatherValues)");
  // @todo Insert your code here
  logTraceOut("GatherValues(GatherValues)");
}

void
PeanoExecutor::mappings::GatherValues::
mergeWithWorkerThread(const GatherValues& workerThread) {
  logTraceIn("mergeWithWorkerThread(GatherValues)");
  // @todo Insert your code here
  logTraceOut("mergeWithWorkerThread(GatherValues)");
}
#endif

void
PeanoExecutor::mappings::GatherValues::
createHangingVertex(
  PeanoExecutor::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                fineGridH,
  PeanoExecutor::Vertex* const   coarseGridVertices,
  const peano::grid::VertexEnumerator&      coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&       coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                   fineGridPositionOfVertex) {
  logTraceInWith6Arguments("createHangingVertex(...)", fineGridVertex,
                           fineGridX, fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("createHangingVertex(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::GatherValues::
destroyHangingVertex(
  const PeanoExecutor::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  PeanoExecutor::Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("destroyHangingVertex(...)", fineGridVertex,
                           fineGridX, fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyHangingVertex(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::GatherValues::
createInnerVertex(
  PeanoExecutor::Vertex&           fineGridVertex,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridX,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridH,
  PeanoExecutor::Vertex* const     coarseGridVertices,
  const peano::grid::VertexEnumerator&
  coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&             coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&    fineGridPositionOfVertex) {
  logTraceInWith6Arguments("createInnerVertex(...)", fineGridVertex, fineGridX,
                           fineGridH, coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("createInnerVertex(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::GatherValues::
createBoundaryVertex(
  PeanoExecutor::Vertex&           fineGridVertex,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridX,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridH,
  PeanoExecutor::Vertex* const     coarseGridVertices,
  const peano::grid::VertexEnumerator&
  coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&             coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&    fineGridPositionOfVertex) {
  logTraceInWith6Arguments("createBoundaryVertex(...)", fineGridVertex,
                           fineGridX, fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("createBoundaryVertex(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::GatherValues::
destroyVertex(
  const PeanoExecutor::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  PeanoExecutor::Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("destroyVertex(...)", fineGridVertex, fineGridX,
                           fineGridH, coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyVertex(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::GatherValues::
createCell(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell) {
  logTraceInWith4Arguments("createCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("createCell(...)", fineGridCell);
}

void
PeanoExecutor::mappings::GatherValues::
destroyCell(
  const PeanoExecutor::Cell&           fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell) {
  logTraceInWith4Arguments("destroyCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("destroyCell(...)", fineGridCell);
}

#ifdef Parallel
void
PeanoExecutor::mappings::GatherValues::
mergeWithNeighbour(
  PeanoExecutor::Vertex& vertex,
  const PeanoExecutor::Vertex& neighbour,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&   fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&   fineGridH,
  int                                           level) {
  logTraceInWith6Arguments("mergeWithNeighbour(...)", vertex, neighbour,
                           fromRank, fineGridX, fineGridH, level);
  // @todo Insert your code here
  logTraceOut("mergeWithNeighbour(...)");
}

void
PeanoExecutor::mappings::GatherValues::
prepareSendToNeighbour(
  PeanoExecutor::Vertex& vertex,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level) {
  logTraceInWith3Arguments("prepareSendToNeighbour(...)", vertex, toRank,
                           level);
  // @todo Insert your code here
  logTraceOut("prepareSendToNeighbour(...)");
}

void
PeanoExecutor::mappings::GatherValues::
prepareCopyToRemoteNode(
  PeanoExecutor::Vertex& localVertex,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level) {
  logTraceInWith5Arguments("prepareCopyToRemoteNode(...)", localVertex, toRank,
                           x, h, level);
  // @todo Insert your code here
  logTraceOut("prepareCopyToRemoteNode(...)");
}

void
PeanoExecutor::mappings::GatherValues::
prepareCopyToRemoteNode(
  PeanoExecutor::Cell& localCell,
  int toRank,
  const tarch::la::Vector<DIMENSIONS, double>&   cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&   cellSize,
  int                                           level) {
  logTraceInWith5Arguments("prepareCopyToRemoteNode(...)", localCell, toRank,
                           cellCentre, cellSize, level);
  // @todo Insert your code here
  logTraceOut("prepareCopyToRemoteNode(...)");
}

void
PeanoExecutor::mappings::GatherValues::
mergeWithRemoteDataDueToForkOrJoin(
  PeanoExecutor::Vertex& localVertex,
  const PeanoExecutor::Vertex& masterOrWorkerVertex,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&  x,
  const tarch::la::Vector<DIMENSIONS, double>&  h,
  int                                       level) {
  logTraceInWith6Arguments("mergeWithRemoteDataDueToForkOrJoin(...)",
                           localVertex, masterOrWorkerVertex, fromRank, x, h,
                           level);
  // @todo Insert your code here
  logTraceOut("mergeWithRemoteDataDueToForkOrJoin(...)");
}

void
PeanoExecutor::mappings::GatherValues::
mergeWithRemoteDataDueToForkOrJoin(
  PeanoExecutor::Cell& localCell,
  const PeanoExecutor::Cell& masterOrWorkerCell,
  int fromRank,
  const tarch::la::Vector<DIMENSIONS, double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&  cellSize,
  int                                       level) {
  logTraceInWith3Arguments("mergeWithRemoteDataDueToForkOrJoin(...)", localCell,
                           masterOrWorkerCell, fromRank);
  // @todo Insert your code here
  logTraceOut("mergeWithRemoteDataDueToForkOrJoin(...)");
}

bool
PeanoExecutor::mappings::GatherValues::
prepareSendToWorker(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell,
  int                                  worker) {
  logTraceIn("prepareSendToWorker(...)");
  // @todo Insert your code here
  logTraceOutWith1Argument("prepareSendToWorker(...)", true);

  return true;
}

void
PeanoExecutor::mappings::GatherValues::
prepareSendToMaster(
  PeanoExecutor::Cell& localCell,
  PeanoExecutor::Vertex* vertices,
  const peano::grid::VertexEnumerator& verticesEnumerator,
  const PeanoExecutor::Vertex* const coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  const PeanoExecutor::Cell& coarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&   fineGridPositionOfCell) {
  logTraceInWith2Arguments("prepareSendToMaster(...)", localCell,
                           verticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("prepareSendToMaster(...)");
}

void
PeanoExecutor::mappings::GatherValues::
mergeWithMaster(
  const PeanoExecutor::Cell&           workerGridCell,
  PeanoExecutor::Vertex* const         workerGridVertices,
  const peano::grid::VertexEnumerator& workerEnumerator,
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell,
  int                                  worker,
  const PeanoExecutor::State&          workerState,
  PeanoExecutor::State&                masterState) {
  logTraceIn("mergeWithMaster(...)");
  // @todo Insert your code here
  logTraceOut("mergeWithMaster(...)");
}

void
PeanoExecutor::mappings::GatherValues::
receiveDataFromMaster(
  PeanoExecutor::Cell& receivedCell,
  PeanoExecutor::Vertex* receivedVertices,
  const peano::grid::VertexEnumerator& receivedVerticesEnumerator,
  PeanoExecutor::Vertex* const receivedCoarseGridVertices,
  const peano::grid::VertexEnumerator& receivedCoarseGridVerticesEnumerator,
  PeanoExecutor::Cell& receivedCoarseGridCell,
  PeanoExecutor::Vertex* const workersCoarseGridVertices,
  const peano::grid::VertexEnumerator& workersCoarseGridVerticesEnumerator,
  PeanoExecutor::Cell& workersCoarseGridCell,
  const tarch::la::Vector<DIMENSIONS, int>&    fineGridPositionOfCell) {
  logTraceIn("receiveDataFromMaster(...)");
  // @todo Insert your code here
  logTraceOut("receiveDataFromMaster(...)");
}

void
PeanoExecutor::mappings::GatherValues::
mergeWithWorker(
  PeanoExecutor::Cell& localCell,
  const PeanoExecutor::Cell& receivedMasterCell,
  const tarch::la::Vector<DIMENSIONS, double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS, double>&  cellSize,
  int                                          level) {
  logTraceInWith2Arguments("mergeWithWorker(...)", localCell.toString(),
                           receivedMasterCell.toString());
  // @todo Insert your code here
  logTraceOutWith1Argument("mergeWithWorker(...)", localCell.toString());
}

void
PeanoExecutor::mappings::GatherValues::
mergeWithWorker(
  PeanoExecutor::Vertex& localVertex,
  const PeanoExecutor::Vertex& receivedMasterVertex,
  const tarch::la::Vector<DIMENSIONS, double>&   x,
  const tarch::la::Vector<DIMENSIONS, double>&   h,
  int                                           level) {
  logTraceInWith2Arguments("mergeWithWorker(...)", localVertex.toString(),
                           receivedMasterVertex.toString());
  // @todo Insert your code here
  logTraceOutWith1Argument("mergeWithWorker(...)", localVertex.toString());
}
#endif

void
PeanoExecutor::mappings::GatherValues::
touchVertexFirstTime(
  PeanoExecutor::Vertex&           fineGridVertex,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridX,
  const tarch::la::Vector<DIMENSIONS,
                          double>& fineGridH,
  PeanoExecutor::Vertex* const     coarseGridVertices,
  const peano::grid::VertexEnumerator&
  coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&             coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&    fineGridPositionOfVertex) {
  logTraceInWith6Arguments("touchVertexFirstTime(...)", fineGridVertex,
                           fineGridX, fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("touchVertexFirstTime(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::GatherValues::
touchVertexLastTime(
  PeanoExecutor::Vertex& fineGridVertex,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS, double>&                    fineGridH,
  PeanoExecutor::Vertex* const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&                       fineGridPositionOfVertex)
{
  logTraceInWith6Arguments("touchVertexLastTime(...)", fineGridVertex,
                           fineGridX, fineGridH,
                           coarseGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfVertex);
  // @todo Insert your code here
  logTraceOutWith1Argument("touchVertexLastTime(...)", fineGridVertex);
}

void
PeanoExecutor::mappings::GatherValues::
enterCell(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell) {
  logTraceInWith4Arguments("enterCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);

  using Grid     = EulerEquations::Simulation::Grid;
  using GridHeap = EulerEquations::Simulation::GridHeap;

  if (!fineGridCell.isRefined()) {
    //

    // auto scalarField;

    Grid grid(
      &GridHeap::getInstance().getData(fineGridCell.getIndex())[0]);

    for (auto const& accessor : grid.innerGrid) {
      int index =
        (accessor(0) - grid.innerGrid.leftIndent(0)) +
        (accessor(1) - grid.innerGrid.leftIndent(1)) * 3 *
        grid.innerSize(0) +
        // (x) * cellDescriptor->getSize(0) +
        (accessor(2) - grid.innerGrid.leftIndent(2)) * 9 *
        grid.innerSize(0) *
        grid.innerSize(1) +
        // (y) * 3 * grid.xInnerSize() * grid.yInnerSize() +
        // (z) * 9 * grid.xInnerSize() *
        grid.innerSize(1) * grid.innerSize(2);
      // _taskScalarField->at(index) = accessor.currentCell().density();
    }
  }

  logTraceOutWith1Argument("enterCell(...)", fineGridCell);
}

void
PeanoExecutor::mappings::GatherValues::
leaveCell(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,
                          int>&        fineGridPositionOfCell) {
  logTraceInWith4Arguments("leaveCell(...)", fineGridCell,
                           fineGridVerticesEnumerator.toString(),
                           coarseGridCell,
                           fineGridPositionOfCell);
  // @todo Insert your code here
  logTraceOutWith1Argument("leaveCell(...)", fineGridCell);
}

void
PeanoExecutor::mappings::GatherValues::
beginIteration(
  PeanoExecutor::State& solverState) {
  logTraceInWith1Argument("beginIteration(State)", solverState);
  // @todo Insert your code here
  logTraceOutWith1Argument("beginIteration(State)", solverState);
}

void
PeanoExecutor::mappings::GatherValues::
endIteration(
  PeanoExecutor::State& solverState) {
  logTraceInWith1Argument("endIteration(State)", solverState);
  // @todo Insert your code here
  logTraceOutWith1Argument("endIteration(State)", solverState);
}

void
PeanoExecutor::mappings::GatherValues::
descend(
  PeanoExecutor::Cell* const           fineGridCells,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell) {
  logTraceInWith2Arguments("descend(...)", coarseGridCell.toString(),
                           coarseGridVerticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("descend(...)");
}

void
PeanoExecutor::mappings::GatherValues::
ascend(
  PeanoExecutor::Cell* const           fineGridCells,
  PeanoExecutor::Vertex* const         fineGridVertices,
  const peano::grid::VertexEnumerator& fineGridVerticesEnumerator,
  PeanoExecutor::Vertex* const         coarseGridVertices,
  const peano::grid::VertexEnumerator& coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell) {
  logTraceInWith2Arguments("ascend(...)", coarseGridCell.toString(),
                           coarseGridVerticesEnumerator.toString());
  // @todo Insert your code here
  logTraceOut("ascend(...)");
}
