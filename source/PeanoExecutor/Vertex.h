// This file is part of the Peano project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#ifndef _PEANOEXECUTOR_VERTEX_H_
#define _PEANOEXECUTOR_VERTEX_H_

#include "PeanoExecutor/records/Vertex.h"
#include "peano/grid/Vertex.h"
#include "peano/grid/VertexEnumerator.h"
#include "peano/utils/Globals.h"

namespace PeanoExecutor {
class Vertex;

/**
 * Forward declaration
 */
class VertexOperations;
}

/**
 * Blueprint for grid vertex.
 *
 * This file has originally been created by the PDT and may be manually extended
 * to
 * the needs of your application. We do not recommend to remove anything!
 */
class PeanoExecutor::Vertex : public
                              peano::grid::Vertex<PeanoExecutor::records::Vertex>
{
private:
  typedef class peano::grid::Vertex<PeanoExecutor::records::Vertex> Base;

  friend class VertexOperations;

public:
  /**
   * Default Constructor
   *
   * This constructor is required by the framework's data container. Do not
   * remove it.
   */
  Vertex();

  /**
   * This constructor should not set any attributes. It is used by the
   * traversal algorithm whenever it allocates an array whose elements
   * will be overwritten later anyway.
   */
  Vertex(const Base::DoNotCallStandardConstructor&);

  /**
   * Constructor
   *
   * This constructor is required by the framework's data container. Do not
   * remove it. It is kind of a copy constructor that converts an object which
   * comprises solely persistent attributes into a full attribute. This very
   * functionality is implemented within the super type, i.e. this constructor
   * has to invoke the correponsing super type's constructor and not the super
   * type standard constructor.
   */
  Vertex(const Base::PersistentVertex& argument);

  tarch::la::Vector<TWO_POWER_D, int>&
  getIndex();

  const tarch::la::Vector<TWO_POWER_D, int>&
  getIndex() const;

  static tarch::la::Vector<TWO_POWER_D_TIMES_TWO_POWER_D, int>
  readIndex(
    const peano::grid::VertexEnumerator& enumerator,
    const Vertex* const                  vertices);

  static void
  writeIndex(const peano::grid::VertexEnumerator& enumerator,
             Vertex* const                        vertices,
             const tarch::la::Vector<TWO_POWER_D_TIMES_TWO_POWER_D,
                                     int>&        values);
};

#endif
