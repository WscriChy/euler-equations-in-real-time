#include "PeanoExecutor/Vertex.h"
#include "peano/grid/Checkpoint.h"
#include "peano/utils/Loop.h"

PeanoExecutor::Vertex::
Vertex()
  : Base() {
  // @todo Insert your code here
}

PeanoExecutor::Vertex::
Vertex(const Base::DoNotCallStandardConstructor& value)
  : Base(value) {
  // Please do not insert anything here
}

PeanoExecutor::Vertex::
Vertex(const Base::PersistentVertex& argument)
  : Base(argument) {
  // @todo Insert your code here
}

tarch::la::Vector<TWO_POWER_D_TIMES_TWO_POWER_D, int>
PeanoExecutor::Vertex::
readIndex(const peano::grid::VertexEnumerator& enumerator,
          const Vertex* const                  vertices) {
  tarch::la::Vector<TWO_POWER_D_TIMES_TWO_POWER_D, int> result; dfor2(
    x) tarch::la::slice(result, vertices[enumerator(x)]._vertexData.getIndex(),
                        xScalar * TWO_POWER_D); enddforx return result;
}

void
PeanoExecutor::Vertex::
writeIndex(const peano::grid::VertexEnumerator& enumerator,
           Vertex* const                        vertices,
           const tarch::la::Vector<TWO_POWER_D_TIMES_TWO_POWER_D,
                                   int>&        values) {
  dfor2(x) tarch::la::Vector<TWO_POWER_D, int> temp =
    tarch::la::slice<TWO_POWER_D>(values, xScalar * TWO_POWER_D);
  vertices[enumerator(x)]._vertexData.setIndex(temp); enddforx
}

tarch::la::Vector<TWO_POWER_D, int>&
PeanoExecutor::Vertex::
getIndex() {
  return _vertexData._persistentRecords._index;
}

const tarch::la::Vector<TWO_POWER_D, int>&
PeanoExecutor::Vertex::
getIndex() const {
  return _vertexData._persistentRecords._index;
}
