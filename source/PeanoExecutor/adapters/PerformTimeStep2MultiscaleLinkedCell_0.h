// This file is part of the Peano project. For conditions of distribution and 
// use, please see the copyright notice at www.peano-framework.org
#ifndef PEANOEXECUTOR_ADAPTERS_PerformTimeStep2MultiscaleLinkedCell_0_H_
#define PEANOEXECUTOR_ADAPTERS_PerformTimeStep2MultiscaleLinkedCell_0_H_


#include "tarch/logging/Log.h"
#include "tarch/multicore/MulticoreDefinitions.h"

#include "peano/MappingSpecification.h"
#include "peano/grid/VertexEnumerator.h"

#include "PeanoExecutor/Vertex.h"
#include "PeanoExecutor/Cell.h"
#include "PeanoExecutor/State.h"


namespace PeanoExecutor {
      namespace adapters {
        class PerformTimeStep2MultiscaleLinkedCell_0;
      } 
}


/**
 * This is an adapter providing a multiscale linked-cell data structure
 *
 * Index   Name of the index used for the cell indices within the vertex and 
 *          the cell
 *
 * @author Tobias Weinzierl
 * @version $Revision: 1.4 $
 */
class PeanoExecutor::adapters::PerformTimeStep2MultiscaleLinkedCell_0 {
  public:
    static peano::MappingSpecification   touchVertexLastTimeSpecification();
    static peano::MappingSpecification   touchVertexFirstTimeSpecification();
    static peano::MappingSpecification   enterCellSpecification();
    static peano::MappingSpecification   leaveCellSpecification();
    static peano::MappingSpecification   ascendSpecification();
    static peano::MappingSpecification   descendSpecification();

    PerformTimeStep2MultiscaleLinkedCell_0();

    #if defined(SharedMemoryParallelisation)
    PerformTimeStep2MultiscaleLinkedCell_0(const PerformTimeStep2MultiscaleLinkedCell_0& masterThread);
    #endif

    virtual ~PerformTimeStep2MultiscaleLinkedCell_0();
  
    #if defined(SharedMemoryParallelisation)
    void mergeWithWorkerThread(const PerformTimeStep2MultiscaleLinkedCell_0& workerThread);
    #endif

    void createInnerVertex(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
    );


    void createBoundaryVertex(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
    );


    /**
     * @todo Only half of the documentation
     * @todo Enumeration has changed
     *
     * In an adaptive grid, not all of the $2^d$ adjacent cells exist for hanging
     * vertices. Since each vertex is supposed to hold the adjacent vertices in
     * order to fill the ghostlayers of the patches appropriately, the adjacent
     * indices of hanging vertices need to be filled by the data of the vertices
     * on the next coarser grid. This filling is implemented in this method.
     *
     * !!! The idea
     * Each vertex holds $2^d$ indices. In the vertices they are numbered from 0
     * to $2^d-1$. However, in this method they are considered to exist in a
     * n-dimensional array. In 2d this would look like
     *
     * (0,1)|(1,1)
     * -----v-----
     * (0,0)|(1,0)
     *
     * The linearization looks as follow:
     *
     *   1  |  0
     * -----v-----
     *   3  |  2
     *
     * In the following the term "fine grid" refers to the $4^d$ vertices
     * belonging to the $3^d$ fine grid cells which overlap with the coars grid
     * cell.
     *
     * On the coarse grid cell we again consider the vertices being arranged in a
     * n-dimensional array:
     *
     * (0,1)-----(1,1)
     *   |          |
     *   |          |
     *   |          |
     * (0,0)-----(1,0)
     *
     * Each of them hold again the $2^d$ adjacent indices, while those which refer
     * to a refined cell are set to -1. A hanging vertex therefore gets the
     * adjacent indices from the nearest coarse grid vertex. If they coincide the
     * data can just be used directly. If not, it depends on which boundary of the
     * coarse grid cell the hanging vertex resides. Here the (single) index
     * outside of the coarse grid cell is assigned for all indices of the hanging
     * vertex pointing in the direction of this neighboring coarse grid cell.
     *
     * !!! The algorithm
     * It gets a hanging vertex and performs a loop over the $2^d$ adjacent-patch-
     * indices.
     * In each loop iteration it computes the n-dimensional index of the coarse
     * grid vertex (fromCoarseGridVertex) from which the data has to be copied.
     * For each dimension d with $0\le d <n$:
     *  - If the fine grid position of the hanging vertex in dimension $d$ is 0 set
     *    $fromCoarseGridVertex(d)$ to 0. If it is equals 3 then set
     *    $fromCoarseGridVertex(d)$ to 1. By this we ensure that we always choose
     *    the nearest coarse grid vertex in dimension $d$. If the hanging vertex
     *    resides in a corner of the fine grid this approach always chooses the
     *    coarse grid vertex that is located on the same position.
     *  - If the fine grid position of the hanging vertex in dimension $d$ is
     *    neither 0 nor 3 then the value of $fromCoarseGridVertex(d)$ depends on
     *    the adjacent-patch-index $k$ that has to be set currently. $k(d)$ can
     *    either be 0 or 1. If $k(d)$ is 0 than we want to get data from the
     *    in this dimension "lower" coarse grid vertex, so we set
     *    $fromCoarseGridVertex(d)$ to 0 as well. In the case of $k(d)=1$ we set
     *    $fromCoarseGridVertex(d)$ to 1, accordingly. This actually doesn't
     *    matter since the appropriate adjacent-patch-indices of the to coarse
     *    grid vertices have to be the same, since they are pointing to the same
     *    adjacent cell.
     * The determination of the correct adjacent-patch-index of the coarse grid
     * vertex (coarseGridVertexAdjacentPatchIndex) is done in a similar way. So,
     * for the adjacent-patch-index $k$ on the hanging vertex:
     *  - As stated before, if the fine and coarse grid vertices coincide we can
     *    just copy the adjacent-patch-index. Therefore, if the fine grid position
     *    of the hanging vertex in dimension $d$ is equal to 0 or to 3, we set
     *    $coarseGridVertexAdjacentPatchIndex(d)$ to $k(d)$.
     *  - Otherwise, we just set $coarseGridVertexAdjacentPatchIndex(d)$ to the
     *    inverted $k(d)$. I.e. if $k(d) = 0$ we set
     *    $coarseGridVertexAdjacentPatchIndex(d)$ to 1 and the other way around.
     *
     */
    void createHangingVertex(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
    );


    void destroyHangingVertex(
      const PeanoExecutor::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
    );


    void destroyVertex(
      const PeanoExecutor::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
    );


    void createCell(
      PeanoExecutor::Cell&                 fineGridCell,
      PeanoExecutor::Vertex * const         fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
    );


    void destroyCell(
      const PeanoExecutor::Cell&           fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
    );
        
    #ifdef Parallel
    void mergeWithNeighbour(
      PeanoExecutor::Vertex&  vertex,
      const PeanoExecutor::Vertex&  neighbour,
      int                                           fromRank,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
    );

    void prepareSendToNeighbour(
      PeanoExecutor::Vertex&  vertex,
      int                                           toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
    );

    void prepareCopyToRemoteNode(
      PeanoExecutor::Vertex&  localVertex,
      int                                           toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
    );

    void prepareCopyToRemoteNode(
      PeanoExecutor::Cell&  localCell,
      int                                           toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   cellCentre,
      const tarch::la::Vector<DIMENSIONS,double>&   cellSize,
      int                                           level
    );

    void mergeWithRemoteDataDueToForkOrJoin(
      PeanoExecutor::Vertex&  localVertex,
      const PeanoExecutor::Vertex&  masterOrWorkerVertex,
      int                                       fromRank,
      const tarch::la::Vector<DIMENSIONS,double>&  x,
      const tarch::la::Vector<DIMENSIONS,double>&  h,
      int                                       level
    );

    void mergeWithRemoteDataDueToForkOrJoin(
      PeanoExecutor::Cell&  localCell,
      const PeanoExecutor::Cell&  masterOrWorkerCell,
      int                                       fromRank,
      const tarch::la::Vector<DIMENSIONS,double>&  cellCentre,
      const tarch::la::Vector<DIMENSIONS,double>&  cellSize,
      int                                       level
    );

    bool prepareSendToWorker(
      PeanoExecutor::Cell&                 fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
      int                                                                  worker
    );

    void prepareSendToMaster(
      PeanoExecutor::Cell&                       localCell,
      PeanoExecutor::Vertex *                    vertices,
      const peano::grid::VertexEnumerator&       verticesEnumerator, 
      const PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&       coarseGridVerticesEnumerator,
      const PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&   fineGridPositionOfCell
    );

    void mergeWithMaster(
      const PeanoExecutor::Cell&           workerGridCell,
      PeanoExecutor::Vertex * const        workerGridVertices,
      const peano::grid::VertexEnumerator& workerEnumerator,
      PeanoExecutor::Cell&                 fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
      int                                                                  worker,
      const PeanoExecutor::State&          workerState,
      PeanoExecutor::State&                masterState
    );


    void receiveDataFromMaster(
      PeanoExecutor::Cell&                        receivedCell, 
      PeanoExecutor::Vertex *                     receivedVertices,
      const peano::grid::VertexEnumerator&        receivedVerticesEnumerator,
      PeanoExecutor::Vertex * const               receivedCoarseGridVertices,
      const peano::grid::VertexEnumerator&        receivedCoarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                        receivedCoarseGridCell,
      PeanoExecutor::Vertex * const               workersCoarseGridVertices,
      const peano::grid::VertexEnumerator&        workersCoarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                        workersCoarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&    fineGridPositionOfCell
    );


    void mergeWithWorker(
      PeanoExecutor::Cell&           localCell, 
      const PeanoExecutor::Cell&     receivedMasterCell,
      const tarch::la::Vector<DIMENSIONS,double>&  cellCentre,
      const tarch::la::Vector<DIMENSIONS,double>&  cellSize,
      int                                          level
    );


    void mergeWithWorker(
      PeanoExecutor::Vertex&        localVertex,
      const PeanoExecutor::Vertex&  receivedMasterVertex,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
    );
    #endif


    void touchVertexFirstTime(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
    );


    void touchVertexLastTime(
      PeanoExecutor::Vertex&         fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
    );
    

    void enterCell(
      PeanoExecutor::Cell&                 fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
    );


    void leaveCell(
      PeanoExecutor::Cell&                          fineGridCell,
      PeanoExecutor::Vertex * const                 fineGridVertices,
      const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const                 coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                          coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&      fineGridPositionOfCell
    );


    void beginIteration(
      PeanoExecutor::State&  solverState
    );


    void endIteration(
      PeanoExecutor::State&  solverState
    );

    void descend(
      PeanoExecutor::Cell * const          fineGridCells,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell
    );


    void ascend(
      PeanoExecutor::Cell * const    fineGridCells,
      PeanoExecutor::Vertex * const  fineGridVertices,
      const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell
    );    
};


#endif
