// This file is part of the Peano project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#ifndef PEANOEXECUTOR_ADAPTERS_SetupExperiment_H_
#define PEANOEXECUTOR_ADAPTERS_SetupExperiment_H_


#include "tarch/logging/Log.h"
#include "tarch/la/Vector.h"

#include "peano/grid/VertexEnumerator.h"
#include "peano/MappingSpecification.h"
#include "peano/CommunicationSpecification.h"

#include "tarch/multicore/MulticoreDefinitions.h"

#include "PeanoExecutor/Vertex.h"
#include "PeanoExecutor/Cell.h"
#include "PeanoExecutor/State.h"

 #include "PeanoExecutor/mappings/SetupExperiment.h"

 #include "PeanoExecutor/adapters/SetupExperiment2MultiscaleLinkedCell_0.h"



namespace PeanoExecutor {
      namespace adapters {
        class SetupExperiment;
      }
}


/**
 * This is a mapping from the spacetree traversal events to your user-defined activities.
 * The latter are realised within the mappings.
 *
 * @author Peano Development Toolkit (PDT) by  Tobias Weinzierl
 * @version $Revision: 1.10 $
 */
class PeanoExecutor::adapters::SetupExperiment {
  private:
    PeanoExecutor::mappings::SetupExperiment _map2SetupExperiment;

    PeanoExecutor::adapters::SetupExperiment2MultiscaleLinkedCell_0 _map2SetupExperiment2MultiscaleLinkedCell_0;

  public:
    static peano::MappingSpecification         touchVertexLastTimeSpecification();
    static peano::MappingSpecification         touchVertexFirstTimeSpecification();
    static peano::MappingSpecification         enterCellSpecification();
    static peano::MappingSpecification         leaveCellSpecification();
    static peano::MappingSpecification         ascendSpecification();
    static peano::MappingSpecification         descendSpecification();
    static peano::CommunicationSpecification   communicationSpecification();

    SetupExperiment();

    #if defined(SharedMemoryParallelisation)
    SetupExperiment(const SetupExperiment& masterThread);
    #endif

    virtual ~SetupExperiment();

    #if defined(SharedMemoryParallelisation)
    void mergeWithWorkerThread(const SetupExperiment& workerThread);
    #endif

    void createInnerVertex(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
    );


    void createBoundaryVertex(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
    );


    void createHangingVertex(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
    );


    void destroyHangingVertex(
      const PeanoExecutor::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
    );


    void destroyVertex(
      const PeanoExecutor::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
    );


    void createCell(
      PeanoExecutor::Cell&                 fineGridCell,
      PeanoExecutor::Vertex * const         fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
    );


    void destroyCell(
      const PeanoExecutor::Cell&           fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
    );

    #ifdef Parallel
    void mergeWithNeighbour(
      PeanoExecutor::Vertex&  vertex,
      const PeanoExecutor::Vertex&  neighbour,
      int                                           fromRank,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
    );

    void prepareSendToNeighbour(
      PeanoExecutor::Vertex&  vertex,
      int                                           toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
    );

    void prepareCopyToRemoteNode(
      PeanoExecutor::Vertex&  localVertex,
      int                                           toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
    );

    void prepareCopyToRemoteNode(
      PeanoExecutor::Cell&  localCell,
      int  toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   cellCentre,
      const tarch::la::Vector<DIMENSIONS,double>&   cellSize,
      int                                           level
    );

    void mergeWithRemoteDataDueToForkOrJoin(
      PeanoExecutor::Vertex&  localVertex,
      const PeanoExecutor::Vertex&  masterOrWorkerVertex,
      int                                       fromRank,
      const tarch::la::Vector<DIMENSIONS,double>&  x,
      const tarch::la::Vector<DIMENSIONS,double>&  h,
      int                                       level
    );

    void mergeWithRemoteDataDueToForkOrJoin(
      PeanoExecutor::Cell&  localCell,
      const PeanoExecutor::Cell&  masterOrWorkerCell,
      int                                       fromRank,
      const tarch::la::Vector<DIMENSIONS,double>&  cellCentre,
      const tarch::la::Vector<DIMENSIONS,double>&  cellSize,
      int                                       level
    );

    bool prepareSendToWorker(
      PeanoExecutor::Cell&                 fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
      int                                                                  worker
    );

    void prepareSendToMaster(
      PeanoExecutor::Cell&                       localCell,
      PeanoExecutor::Vertex *                    vertices,
      const peano::grid::VertexEnumerator&       verticesEnumerator,
      const PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&       coarseGridVerticesEnumerator,
      const PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&   fineGridPositionOfCell
    );

    void mergeWithMaster(
      const PeanoExecutor::Cell&           workerGridCell,
      PeanoExecutor::Vertex * const        workerGridVertices,
      const peano::grid::VertexEnumerator& workerEnumerator,
      PeanoExecutor::Cell&                 fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
      int                                                                  worker,
      const PeanoExecutor::State&           workerState,
      PeanoExecutor::State&                 masterState
    );


    void receiveDataFromMaster(
      PeanoExecutor::Cell&                        receivedCell,
      PeanoExecutor::Vertex *                     receivedVertices,
      const peano::grid::VertexEnumerator&        receivedVerticesEnumerator,
      PeanoExecutor::Vertex * const               receivedCoarseGridVertices,
      const peano::grid::VertexEnumerator&        receivedCoarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                        receivedCoarseGridCell,
      PeanoExecutor::Vertex * const               workersCoarseGridVertices,
      const peano::grid::VertexEnumerator&        workersCoarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                        workersCoarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&    fineGridPositionOfCell
    );


    void mergeWithWorker(
      PeanoExecutor::Cell&           localCell,
      const PeanoExecutor::Cell&     receivedMasterCell,
      const tarch::la::Vector<DIMENSIONS,double>&  cellCentre,
      const tarch::la::Vector<DIMENSIONS,double>&  cellSize,
      int                                          level
    );


    void mergeWithWorker(
      PeanoExecutor::Vertex&        localVertex,
      const PeanoExecutor::Vertex&  receivedMasterVertex,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
    );
    #endif


    void touchVertexFirstTime(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
    );


    void touchVertexLastTime(
      PeanoExecutor::Vertex&         fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
    );


    void enterCell(
      PeanoExecutor::Cell&                 fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
    );


    void leaveCell(
      PeanoExecutor::Cell&                          fineGridCell,
      PeanoExecutor::Vertex * const                 fineGridVertices,
      const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const                 coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                          coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&      fineGridPositionOfCell
    );


    void beginIteration(
      PeanoExecutor::State&  solverState
    );


    void endIteration(
      PeanoExecutor::State&  solverState
    );

    void descend(
      PeanoExecutor::Cell * const          fineGridCells,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell
    );


    void ascend(
      PeanoExecutor::Cell * const    fineGridCells,
      PeanoExecutor::Vertex * const  fineGridVertices,
      const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell
    );
};


#endif
