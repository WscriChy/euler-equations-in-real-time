#include "PeanoExecutor/adapters/SetupExperiment.h"


peano::CommunicationSpecification   PeanoExecutor::adapters::SetupExperiment::communicationSpecification() {
  return peano::CommunicationSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::SetupExperiment::communicationSpecification()

   //& PeanoExecutor::adapters::SetupExperiment2MultiscaleLinkedCell_0::communicationSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::SetupExperiment::touchVertexLastTimeSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::SetupExperiment::touchVertexLastTimeSpecification()

   & PeanoExecutor::adapters::SetupExperiment2MultiscaleLinkedCell_0::touchVertexLastTimeSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::SetupExperiment::touchVertexFirstTimeSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::SetupExperiment::touchVertexFirstTimeSpecification()

   & PeanoExecutor::adapters::SetupExperiment2MultiscaleLinkedCell_0::touchVertexFirstTimeSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::SetupExperiment::enterCellSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::SetupExperiment::enterCellSpecification()

   & PeanoExecutor::adapters::SetupExperiment2MultiscaleLinkedCell_0::enterCellSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::SetupExperiment::leaveCellSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::SetupExperiment::leaveCellSpecification()

   & PeanoExecutor::adapters::SetupExperiment2MultiscaleLinkedCell_0::leaveCellSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::SetupExperiment::ascendSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::SetupExperiment::ascendSpecification()

   & PeanoExecutor::adapters::SetupExperiment2MultiscaleLinkedCell_0::ascendSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::SetupExperiment::descendSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::SetupExperiment::descendSpecification()

   & PeanoExecutor::adapters::SetupExperiment2MultiscaleLinkedCell_0::descendSpecification()

;
}


PeanoExecutor::adapters::SetupExperiment::SetupExperiment() {
}


PeanoExecutor::adapters::SetupExperiment::~SetupExperiment() {
}


#if defined(SharedMemoryParallelisation)
PeanoExecutor::adapters::SetupExperiment::SetupExperiment(const SetupExperiment&  masterThread):
  _map2SetupExperiment(masterThread._map2SetupExperiment)

 ,
  _map2SetupExperiment2MultiscaleLinkedCell_0(masterThread._map2SetupExperiment2MultiscaleLinkedCell_0)

{
}


void PeanoExecutor::adapters::SetupExperiment::mergeWithWorkerThread(const SetupExperiment& workerThread) {

  _map2SetupExperiment.mergeWithWorkerThread(workerThread._map2SetupExperiment);

  _map2SetupExperiment2MultiscaleLinkedCell_0.mergeWithWorkerThread(workerThread._map2SetupExperiment2MultiscaleLinkedCell_0);

}
#endif


void PeanoExecutor::adapters::SetupExperiment::createHangingVertex(
      PeanoExecutor::Vertex&     fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                fineGridH,
      PeanoExecutor::Vertex * const   coarseGridVertices,
      const peano::grid::VertexEnumerator&      coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&       coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                   fineGridPositionOfVertex
) {
  _map2SetupExperiment2MultiscaleLinkedCell_0.createHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


  _map2SetupExperiment.createHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void PeanoExecutor::adapters::SetupExperiment::destroyHangingVertex(
      const PeanoExecutor::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {
  _map2SetupExperiment.destroyHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


  _map2SetupExperiment2MultiscaleLinkedCell_0.destroyHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::SetupExperiment::createInnerVertex(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {
  _map2SetupExperiment2MultiscaleLinkedCell_0.createInnerVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


  _map2SetupExperiment.createInnerVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::SetupExperiment::createBoundaryVertex(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {
  _map2SetupExperiment2MultiscaleLinkedCell_0.createBoundaryVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


  _map2SetupExperiment.createBoundaryVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::SetupExperiment::destroyVertex(
      const PeanoExecutor::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2SetupExperiment.destroyVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

  _map2SetupExperiment2MultiscaleLinkedCell_0.destroyVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::SetupExperiment::createCell(
      PeanoExecutor::Cell&                 fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {
  _map2SetupExperiment2MultiscaleLinkedCell_0.createCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


  _map2SetupExperiment.createCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

}


void PeanoExecutor::adapters::SetupExperiment::destroyCell(
      const PeanoExecutor::Cell&           fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {

  _map2SetupExperiment.destroyCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

  _map2SetupExperiment2MultiscaleLinkedCell_0.destroyCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

}

#ifdef Parallel
void PeanoExecutor::adapters::SetupExperiment::mergeWithNeighbour(
  PeanoExecutor::Vertex&  vertex,
  const PeanoExecutor::Vertex&  neighbour,
  int                                           fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridX,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridH,
  int                                           level
) {
   _map2SetupExperiment2MultiscaleLinkedCell_0.mergeWithNeighbour( vertex, neighbour, fromRank, fineGridX, fineGridH, level );


   _map2SetupExperiment.mergeWithNeighbour( vertex, neighbour, fromRank, fineGridX, fineGridH, level );

}

void PeanoExecutor::adapters::SetupExperiment::prepareSendToNeighbour(
  PeanoExecutor::Vertex&  vertex,
  int                                           toRank,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {
   _map2SetupExperiment.prepareSendToNeighbour( vertex, toRank, x, h, level );

   _map2SetupExperiment2MultiscaleLinkedCell_0.prepareSendToNeighbour( vertex, toRank, x, h, level );

}

void PeanoExecutor::adapters::SetupExperiment::prepareCopyToRemoteNode(
  PeanoExecutor::Vertex&  localVertex,
  int                                           toRank,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {
   _map2SetupExperiment.prepareCopyToRemoteNode( localVertex, toRank, x, h, level );

   _map2SetupExperiment2MultiscaleLinkedCell_0.prepareCopyToRemoteNode( localVertex, toRank, x, h, level );

}

void PeanoExecutor::adapters::SetupExperiment::prepareCopyToRemoteNode(
  PeanoExecutor::Cell&  localCell,
      int                                           toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
) {
   _map2SetupExperiment.prepareCopyToRemoteNode( localCell, toRank, x, h, level );

   _map2SetupExperiment2MultiscaleLinkedCell_0.prepareCopyToRemoteNode( localCell, toRank, x, h, level );

}

void PeanoExecutor::adapters::SetupExperiment::mergeWithRemoteDataDueToForkOrJoin(
  PeanoExecutor::Vertex&  localVertex,
  const PeanoExecutor::Vertex&  masterOrWorkerVertex,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {
   _map2SetupExperiment2MultiscaleLinkedCell_0.mergeWithRemoteDataDueToForkOrJoin( localVertex, masterOrWorkerVertex, fromRank, x, h, level );


   _map2SetupExperiment.mergeWithRemoteDataDueToForkOrJoin( localVertex, masterOrWorkerVertex, fromRank, x, h, level );

}

void PeanoExecutor::adapters::SetupExperiment::mergeWithRemoteDataDueToForkOrJoin(
  PeanoExecutor::Cell&  localCell,
  const PeanoExecutor::Cell&  masterOrWorkerCell,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {
   _map2SetupExperiment2MultiscaleLinkedCell_0.mergeWithRemoteDataDueToForkOrJoin( localCell, masterOrWorkerCell, fromRank, x, h, level );


   _map2SetupExperiment.mergeWithRemoteDataDueToForkOrJoin( localCell, masterOrWorkerCell, fromRank, x, h, level );

}

bool PeanoExecutor::adapters::SetupExperiment::prepareSendToWorker(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker
) {
  bool result = false;
   result |= _map2SetupExperiment.prepareSendToWorker( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker );

   result |= _map2SetupExperiment2MultiscaleLinkedCell_0.prepareSendToWorker( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker );

  return result;
}

void PeanoExecutor::adapters::SetupExperiment::prepareSendToMaster(
  PeanoExecutor::Cell&                       localCell,
  PeanoExecutor::Vertex *                    vertices,
  const peano::grid::VertexEnumerator&       verticesEnumerator,
  const PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&       coarseGridVerticesEnumerator,
  const PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&   fineGridPositionOfCell
) {

   _map2SetupExperiment.prepareSendToMaster( localCell, vertices, verticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

   _map2SetupExperiment2MultiscaleLinkedCell_0.prepareSendToMaster( localCell, vertices, verticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

}

void PeanoExecutor::adapters::SetupExperiment::mergeWithMaster(
  const PeanoExecutor::Cell&           workerGridCell,
  PeanoExecutor::Vertex * const        workerGridVertices,
  const peano::grid::VertexEnumerator& workerEnumerator,
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker,
    const PeanoExecutor::State&          workerState,
  PeanoExecutor::State&                masterState
) {
   _map2SetupExperiment2MultiscaleLinkedCell_0.mergeWithMaster( workerGridCell, workerGridVertices, workerEnumerator, fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker, workerState, masterState );


   _map2SetupExperiment.mergeWithMaster( workerGridCell, workerGridVertices, workerEnumerator, fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker, workerState, masterState );

}

void PeanoExecutor::adapters::SetupExperiment::receiveDataFromMaster(
      PeanoExecutor::Cell&                        receivedCell,
      PeanoExecutor::Vertex *                     receivedVertices,
      const peano::grid::VertexEnumerator&        receivedVerticesEnumerator,
      PeanoExecutor::Vertex * const               receivedCoarseGridVertices,
      const peano::grid::VertexEnumerator&        receivedCoarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                        receivedCoarseGridCell,
      PeanoExecutor::Vertex * const               workersCoarseGridVertices,
      const peano::grid::VertexEnumerator&        workersCoarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                        workersCoarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&    fineGridPositionOfCell
) {
   _map2SetupExperiment2MultiscaleLinkedCell_0.receiveDataFromMaster( receivedCell, receivedVertices, receivedVerticesEnumerator, receivedCoarseGridVertices, receivedCoarseGridVerticesEnumerator, receivedCoarseGridCell, workersCoarseGridVertices, workersCoarseGridVerticesEnumerator, workersCoarseGridCell, fineGridPositionOfCell );


   _map2SetupExperiment.receiveDataFromMaster( receivedCell, receivedVertices, receivedVerticesEnumerator, receivedCoarseGridVertices, receivedCoarseGridVerticesEnumerator, receivedCoarseGridCell, workersCoarseGridVertices, workersCoarseGridVerticesEnumerator, workersCoarseGridCell, fineGridPositionOfCell );

}


void PeanoExecutor::adapters::SetupExperiment::mergeWithWorker(
  PeanoExecutor::Cell&           localCell,
  const PeanoExecutor::Cell&     receivedMasterCell,
  const tarch::la::Vector<DIMENSIONS,double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS,double>&  cellSize,
  int                                          level
) {
   _map2SetupExperiment2MultiscaleLinkedCell_0.mergeWithWorker( localCell, receivedMasterCell, cellCentre, cellSize, level );


   _map2SetupExperiment.mergeWithWorker( localCell, receivedMasterCell, cellCentre, cellSize, level );

}

void PeanoExecutor::adapters::SetupExperiment::mergeWithWorker(
  PeanoExecutor::Vertex&        localVertex,
  const PeanoExecutor::Vertex&  receivedMasterVertex,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {

   _map2SetupExperiment.mergeWithWorker( localVertex, receivedMasterVertex, x, h, level );

   _map2SetupExperiment2MultiscaleLinkedCell_0.mergeWithWorker( localVertex, receivedMasterVertex, x, h, level );

}
#endif

void PeanoExecutor::adapters::SetupExperiment::touchVertexFirstTime(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {
  _map2SetupExperiment2MultiscaleLinkedCell_0.touchVertexFirstTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


  _map2SetupExperiment.touchVertexFirstTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::SetupExperiment::touchVertexLastTime(
      PeanoExecutor::Vertex&         fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2SetupExperiment.touchVertexLastTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

  _map2SetupExperiment2MultiscaleLinkedCell_0.touchVertexLastTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::SetupExperiment::enterCell(
      PeanoExecutor::Cell&                 fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {
  _map2SetupExperiment2MultiscaleLinkedCell_0.enterCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


  _map2SetupExperiment.enterCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

}


void PeanoExecutor::adapters::SetupExperiment::leaveCell(
      PeanoExecutor::Cell&           fineGridCell,
      PeanoExecutor::Vertex * const  fineGridVertices,
      const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfCell
) {

  _map2SetupExperiment.leaveCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

  _map2SetupExperiment2MultiscaleLinkedCell_0.leaveCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

}


void PeanoExecutor::adapters::SetupExperiment::beginIteration(
  PeanoExecutor::State&  solverState
) {
  _map2SetupExperiment2MultiscaleLinkedCell_0.beginIteration( solverState );


  _map2SetupExperiment.beginIteration( solverState );

}


void PeanoExecutor::adapters::SetupExperiment::endIteration(
  PeanoExecutor::State&  solverState
) {

  _map2SetupExperiment.endIteration( solverState );

  _map2SetupExperiment2MultiscaleLinkedCell_0.endIteration( solverState );

}




void PeanoExecutor::adapters::SetupExperiment::descend(
  PeanoExecutor::Cell * const          fineGridCells,
  PeanoExecutor::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell
) {
  _map2SetupExperiment2MultiscaleLinkedCell_0.descend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );


  _map2SetupExperiment.descend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );

}


void PeanoExecutor::adapters::SetupExperiment::ascend(
  PeanoExecutor::Cell * const    fineGridCells,
  PeanoExecutor::Vertex * const  fineGridVertices,
  const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&           coarseGridCell
) {

  _map2SetupExperiment.ascend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );

  _map2SetupExperiment2MultiscaleLinkedCell_0.ascend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );

}
