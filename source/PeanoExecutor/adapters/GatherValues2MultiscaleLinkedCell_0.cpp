#include "PeanoExecutor/adapters/GatherValues2MultiscaleLinkedCell_0.h"

#include <sstream>

#include "peano/utils/Loop.h"
#include "peano/grid/CellFlags.h"


#include "multiscalelinkedcell/HangingVertexBookkeeper.h"


peano::MappingSpecification   PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::touchVertexLastTimeSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::Nop,peano::MappingSpecification::AvoidFineGridRaces);
}


peano::MappingSpecification   PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::touchVertexFirstTimeSpecification() { 
  return peano::MappingSpecification(peano::MappingSpecification::Nop,peano::MappingSpecification::AvoidFineGridRaces);
}


peano::MappingSpecification   PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::enterCellSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::WholeTree,peano::MappingSpecification::AvoidFineGridRaces);
}


peano::MappingSpecification   PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::leaveCellSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::Nop,peano::MappingSpecification::AvoidFineGridRaces);
}


peano::MappingSpecification   PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::ascendSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::Nop,peano::MappingSpecification::AvoidFineGridRaces);
}


peano::MappingSpecification   PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::descendSpecification() {
  return peano::MappingSpecification(peano::MappingSpecification::Nop,peano::MappingSpecification::AvoidFineGridRaces);
}


PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::GatherValues2MultiscaleLinkedCell_0() {
}


PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::~GatherValues2MultiscaleLinkedCell_0() {
}


#if defined(SharedMemoryParallelisation)
PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::GatherValues2MultiscaleLinkedCell_0(const GatherValues2MultiscaleLinkedCell_0&  masterThread) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::mergeWithWorkerThread(const GatherValues2MultiscaleLinkedCell_0& workerThread) {
}
#endif


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::createHangingVertex(
  PeanoExecutor::Vertex&     fineGridVertex,
  const tarch::la::Vector<DIMENSIONS,double>&                fineGridX,
  const tarch::la::Vector<DIMENSIONS,double>&                fineGridH,
  PeanoExecutor::Vertex * const   coarseGridVertices,
  const peano::grid::VertexEnumerator&      coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&       coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                   fineGridPositionOfVertex
) {
  const int level = coarseGridVerticesEnumerator.getLevel()+1;
  
  fineGridVertex.getIndex() = 
    multiscalelinkedcell::HangingVertexBookkeeper::getInstance().createHangingVertex(
      fineGridX,level,
      fineGridPositionOfVertex,
      Vertex::readIndex(coarseGridVerticesEnumerator,coarseGridVertices)
    );
}



void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::destroyHangingVertex(
  const PeanoExecutor::Vertex&   fineGridVertex,
  const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
  const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
  PeanoExecutor::Vertex * const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&           coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::createInnerVertex(
  PeanoExecutor::Vertex&               fineGridVertex,
  const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
  const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {
  fineGridVertex.getIndex() = multiscalelinkedcell::HangingVertexBookkeeper::getInstance().createVertexLinkMapForNewVertex();
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::createBoundaryVertex(
  PeanoExecutor::Vertex&               fineGridVertex,
  const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
  const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {
  fineGridVertex.getIndex() = multiscalelinkedcell::HangingVertexBookkeeper::getInstance().createVertexLinkMapForBoundaryVertex();
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::destroyVertex(
      const PeanoExecutor::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::createCell(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::destroyCell(
  const PeanoExecutor::Cell&           fineGridCell,
  PeanoExecutor::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {
  multiscalelinkedcell::HangingVertexBookkeeper::getInstance().destroyCell(fineGridCell.getIndex());
}


#ifdef Parallel
void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::mergeWithNeighbour(
  PeanoExecutor::Vertex&  vertex,
  const PeanoExecutor::Vertex&  neighbour,
  int                                           fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridX,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridH,
  int                                           level
) {
  multiscalelinkedcell::HangingVertexBookkeeper::getInstance().updateCellIndicesInMergeWithNeighbour(vertex);
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::prepareSendToNeighbour(
      PeanoExecutor::Vertex&  vertex,
      int                                           toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::prepareCopyToRemoteNode(
      PeanoExecutor::Vertex&  localVertex,
      int                                           toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::prepareCopyToRemoteNode(
      PeanoExecutor::Cell&  localCell,
      int                                           toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   cellCentre,
      const tarch::la::Vector<DIMENSIONS,double>&   cellSize,
      int                                           level
) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::mergeWithRemoteDataDueToForkOrJoin(
  PeanoExecutor::Vertex&  localVertex,
  const PeanoExecutor::Vertex&  masterOrWorkerVertex,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::mergeWithRemoteDataDueToForkOrJoin(
  PeanoExecutor::Cell&  localCell,
  const PeanoExecutor::Cell&  masterOrWorkerCell,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {
}


bool PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::prepareSendToWorker(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker
) {
  return false;
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::prepareSendToMaster(
      PeanoExecutor::Cell&                       localCell,
      PeanoExecutor::Vertex *                    vertices,
      const peano::grid::VertexEnumerator&       verticesEnumerator, 
      const PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&       coarseGridVerticesEnumerator,
      const PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&   fineGridPositionOfCell
) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::mergeWithMaster(
  const PeanoExecutor::Cell&           workerGridCell,
  PeanoExecutor::Vertex * const        workerGridVertices,
  const peano::grid::VertexEnumerator& workerEnumerator,
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker,
  const PeanoExecutor::State&          workerState,
  PeanoExecutor::State&                masterState
) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::receiveDataFromMaster(
      PeanoExecutor::Cell&                        receivedCell, 
      PeanoExecutor::Vertex *                     receivedVertices,
      const peano::grid::VertexEnumerator&        receivedVerticesEnumerator,
      PeanoExecutor::Vertex * const               receivedCoarseGridVertices,
      const peano::grid::VertexEnumerator&        receivedCoarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                        receivedCoarseGridCell,
      PeanoExecutor::Vertex * const               workersCoarseGridVertices,
      const peano::grid::VertexEnumerator&        workersCoarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                        workersCoarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&    fineGridPositionOfCell
) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::mergeWithWorker(
      PeanoExecutor::Cell&           localCell, 
      const PeanoExecutor::Cell&     receivedMasterCell,
      const tarch::la::Vector<DIMENSIONS,double>&  cellCentre,
      const tarch::la::Vector<DIMENSIONS,double>&  cellSize,
      int                                          level
) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::mergeWithWorker(
      PeanoExecutor::Vertex&        localVertex,
      const PeanoExecutor::Vertex&  receivedMasterVertex,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
) {
}
#endif


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::touchVertexFirstTime(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::touchVertexLastTime(
      PeanoExecutor::Vertex&         fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::enterCell(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {
  dfor2(k)
    if (fineGridVertices[fineGridVerticesEnumerator(k)].isHangingNode()) {
      multiscalelinkedcell::HangingVertexBookkeeper::getInstance().getAdjacencyEntriesOfVertex( 
        fineGridVerticesEnumerator.getVertexPosition(k),
        fineGridVerticesEnumerator.getLevel()
      )(TWO_POWER_D-kScalar-1) = fineGridCell.getIndex();
    }
    else {
      fineGridVertices[fineGridVerticesEnumerator(k)].getIndex()(TWO_POWER_D-kScalar-1) = fineGridCell.getIndex();
    }
  enddforx
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::leaveCell(
      PeanoExecutor::Cell&           fineGridCell,
      PeanoExecutor::Vertex * const  fineGridVertices,
      const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfCell
) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::beginIteration(
  PeanoExecutor::State&  solverState
) {
  multiscalelinkedcell::HangingVertexBookkeeper::getInstance().beginIteration();
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::endIteration(
  PeanoExecutor::State&  solverState
) {
  multiscalelinkedcell::HangingVertexBookkeeper::getInstance().endIteration();
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::descend(
  PeanoExecutor::Cell * const          fineGridCells,
  PeanoExecutor::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell
) {
}


void PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::ascend(
  PeanoExecutor::Cell * const    fineGridCells,
  PeanoExecutor::Vertex * const  fineGridVertices,
  const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&           coarseGridCell
) {
}
