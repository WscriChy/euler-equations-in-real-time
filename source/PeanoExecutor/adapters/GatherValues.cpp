#include "PeanoExecutor/adapters/GatherValues.h"


peano::CommunicationSpecification   PeanoExecutor::adapters::GatherValues::communicationSpecification() {
  return peano::CommunicationSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::GatherValues::communicationSpecification()

   //& PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::communicationSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::GatherValues::touchVertexLastTimeSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::GatherValues::touchVertexLastTimeSpecification()

   & PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::touchVertexLastTimeSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::GatherValues::touchVertexFirstTimeSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::GatherValues::touchVertexFirstTimeSpecification()

   & PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::touchVertexFirstTimeSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::GatherValues::enterCellSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::GatherValues::enterCellSpecification()

   & PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::enterCellSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::GatherValues::leaveCellSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::GatherValues::leaveCellSpecification()

   & PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::leaveCellSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::GatherValues::ascendSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::GatherValues::ascendSpecification()

   & PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::ascendSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::GatherValues::descendSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::GatherValues::descendSpecification()

   & PeanoExecutor::adapters::GatherValues2MultiscaleLinkedCell_0::descendSpecification()

;
}


PeanoExecutor::adapters::GatherValues::GatherValues() {
}


PeanoExecutor::adapters::GatherValues::~GatherValues() {
}


#if defined(SharedMemoryParallelisation)
PeanoExecutor::adapters::GatherValues::GatherValues(const GatherValues&  masterThread):
  _map2GatherValues(masterThread._map2GatherValues)

 ,
  _map2GatherValues2MultiscaleLinkedCell_0(masterThread._map2GatherValues2MultiscaleLinkedCell_0)

{
}


void PeanoExecutor::adapters::GatherValues::mergeWithWorkerThread(const GatherValues& workerThread) {

  _map2GatherValues.mergeWithWorkerThread(workerThread._map2GatherValues);

  _map2GatherValues2MultiscaleLinkedCell_0.mergeWithWorkerThread(workerThread._map2GatherValues2MultiscaleLinkedCell_0);

}
#endif


void PeanoExecutor::adapters::GatherValues::createHangingVertex(
      PeanoExecutor::Vertex&     fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                fineGridH,
      PeanoExecutor::Vertex * const   coarseGridVertices,
      const peano::grid::VertexEnumerator&      coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&       coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                   fineGridPositionOfVertex
) {
  _map2GatherValues2MultiscaleLinkedCell_0.createHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


  _map2GatherValues.createHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void PeanoExecutor::adapters::GatherValues::destroyHangingVertex(
      const PeanoExecutor::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {
  _map2GatherValues.destroyHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


  _map2GatherValues2MultiscaleLinkedCell_0.destroyHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::GatherValues::createInnerVertex(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {
  _map2GatherValues2MultiscaleLinkedCell_0.createInnerVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


  _map2GatherValues.createInnerVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::GatherValues::createBoundaryVertex(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {
  _map2GatherValues2MultiscaleLinkedCell_0.createBoundaryVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


  _map2GatherValues.createBoundaryVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::GatherValues::destroyVertex(
      const PeanoExecutor::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2GatherValues.destroyVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

  _map2GatherValues2MultiscaleLinkedCell_0.destroyVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::GatherValues::createCell(
      PeanoExecutor::Cell&                 fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {
  _map2GatherValues2MultiscaleLinkedCell_0.createCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


  _map2GatherValues.createCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

}


void PeanoExecutor::adapters::GatherValues::destroyCell(
      const PeanoExecutor::Cell&           fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {

  _map2GatherValues.destroyCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

  _map2GatherValues2MultiscaleLinkedCell_0.destroyCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

}

#ifdef Parallel
void PeanoExecutor::adapters::GatherValues::mergeWithNeighbour(
  PeanoExecutor::Vertex&  vertex,
  const PeanoExecutor::Vertex&  neighbour,
  int                                           fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridX,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridH,
  int                                           level
) {
   _map2GatherValues2MultiscaleLinkedCell_0.mergeWithNeighbour( vertex, neighbour, fromRank, fineGridX, fineGridH, level );


   _map2GatherValues.mergeWithNeighbour( vertex, neighbour, fromRank, fineGridX, fineGridH, level );

}

void PeanoExecutor::adapters::GatherValues::prepareSendToNeighbour(
  PeanoExecutor::Vertex&  vertex,
  int                                           toRank,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {
   _map2GatherValues.prepareSendToNeighbour( vertex, toRank, x, h, level );

   _map2GatherValues2MultiscaleLinkedCell_0.prepareSendToNeighbour( vertex, toRank, x, h, level );

}

void PeanoExecutor::adapters::GatherValues::prepareCopyToRemoteNode(
  PeanoExecutor::Vertex&  localVertex,
  int                                           toRank,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {
   _map2GatherValues.prepareCopyToRemoteNode( localVertex, toRank, x, h, level );

   _map2GatherValues2MultiscaleLinkedCell_0.prepareCopyToRemoteNode( localVertex, toRank, x, h, level );

}

void PeanoExecutor::adapters::GatherValues::prepareCopyToRemoteNode(
  PeanoExecutor::Cell&  localCell,
      int                                           toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
) {
   _map2GatherValues.prepareCopyToRemoteNode( localCell, toRank, x, h, level );

   _map2GatherValues2MultiscaleLinkedCell_0.prepareCopyToRemoteNode( localCell, toRank, x, h, level );

}

void PeanoExecutor::adapters::GatherValues::mergeWithRemoteDataDueToForkOrJoin(
  PeanoExecutor::Vertex&  localVertex,
  const PeanoExecutor::Vertex&  masterOrWorkerVertex,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {
   _map2GatherValues2MultiscaleLinkedCell_0.mergeWithRemoteDataDueToForkOrJoin( localVertex, masterOrWorkerVertex, fromRank, x, h, level );


   _map2GatherValues.mergeWithRemoteDataDueToForkOrJoin( localVertex, masterOrWorkerVertex, fromRank, x, h, level );

}

void PeanoExecutor::adapters::GatherValues::mergeWithRemoteDataDueToForkOrJoin(
  PeanoExecutor::Cell&  localCell,
  const PeanoExecutor::Cell&  masterOrWorkerCell,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {
   _map2GatherValues2MultiscaleLinkedCell_0.mergeWithRemoteDataDueToForkOrJoin( localCell, masterOrWorkerCell, fromRank, x, h, level );


   _map2GatherValues.mergeWithRemoteDataDueToForkOrJoin( localCell, masterOrWorkerCell, fromRank, x, h, level );

}

bool PeanoExecutor::adapters::GatherValues::prepareSendToWorker(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker
) {
  bool result = false;
   result |= _map2GatherValues.prepareSendToWorker( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker );

   result |= _map2GatherValues2MultiscaleLinkedCell_0.prepareSendToWorker( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker );

  return result;
}

void PeanoExecutor::adapters::GatherValues::prepareSendToMaster(
  PeanoExecutor::Cell&                       localCell,
  PeanoExecutor::Vertex *                    vertices,
  const peano::grid::VertexEnumerator&       verticesEnumerator,
  const PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&       coarseGridVerticesEnumerator,
  const PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&   fineGridPositionOfCell
) {

   _map2GatherValues.prepareSendToMaster( localCell, vertices, verticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

   _map2GatherValues2MultiscaleLinkedCell_0.prepareSendToMaster( localCell, vertices, verticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

}

void PeanoExecutor::adapters::GatherValues::mergeWithMaster(
  const PeanoExecutor::Cell&           workerGridCell,
  PeanoExecutor::Vertex * const        workerGridVertices,
  const peano::grid::VertexEnumerator& workerEnumerator,
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker,
    const PeanoExecutor::State&          workerState,
  PeanoExecutor::State&                masterState
) {
   _map2GatherValues2MultiscaleLinkedCell_0.mergeWithMaster( workerGridCell, workerGridVertices, workerEnumerator, fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker, workerState, masterState );


   _map2GatherValues.mergeWithMaster( workerGridCell, workerGridVertices, workerEnumerator, fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker, workerState, masterState );

}

void PeanoExecutor::adapters::GatherValues::receiveDataFromMaster(
      PeanoExecutor::Cell&                        receivedCell,
      PeanoExecutor::Vertex *                     receivedVertices,
      const peano::grid::VertexEnumerator&        receivedVerticesEnumerator,
      PeanoExecutor::Vertex * const               receivedCoarseGridVertices,
      const peano::grid::VertexEnumerator&        receivedCoarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                        receivedCoarseGridCell,
      PeanoExecutor::Vertex * const               workersCoarseGridVertices,
      const peano::grid::VertexEnumerator&        workersCoarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                        workersCoarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&    fineGridPositionOfCell
) {
   _map2GatherValues2MultiscaleLinkedCell_0.receiveDataFromMaster( receivedCell, receivedVertices, receivedVerticesEnumerator, receivedCoarseGridVertices, receivedCoarseGridVerticesEnumerator, receivedCoarseGridCell, workersCoarseGridVertices, workersCoarseGridVerticesEnumerator, workersCoarseGridCell, fineGridPositionOfCell );


   _map2GatherValues.receiveDataFromMaster( receivedCell, receivedVertices, receivedVerticesEnumerator, receivedCoarseGridVertices, receivedCoarseGridVerticesEnumerator, receivedCoarseGridCell, workersCoarseGridVertices, workersCoarseGridVerticesEnumerator, workersCoarseGridCell, fineGridPositionOfCell );

}


void PeanoExecutor::adapters::GatherValues::mergeWithWorker(
  PeanoExecutor::Cell&           localCell,
  const PeanoExecutor::Cell&     receivedMasterCell,
  const tarch::la::Vector<DIMENSIONS,double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS,double>&  cellSize,
  int                                          level
) {
   _map2GatherValues2MultiscaleLinkedCell_0.mergeWithWorker( localCell, receivedMasterCell, cellCentre, cellSize, level );


   _map2GatherValues.mergeWithWorker( localCell, receivedMasterCell, cellCentre, cellSize, level );

}

void PeanoExecutor::adapters::GatherValues::mergeWithWorker(
  PeanoExecutor::Vertex&        localVertex,
  const PeanoExecutor::Vertex&  receivedMasterVertex,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {

   _map2GatherValues.mergeWithWorker( localVertex, receivedMasterVertex, x, h, level );

   _map2GatherValues2MultiscaleLinkedCell_0.mergeWithWorker( localVertex, receivedMasterVertex, x, h, level );

}
#endif

void PeanoExecutor::adapters::GatherValues::touchVertexFirstTime(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {
  _map2GatherValues2MultiscaleLinkedCell_0.touchVertexFirstTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


  _map2GatherValues.touchVertexFirstTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::GatherValues::touchVertexLastTime(
      PeanoExecutor::Vertex&         fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2GatherValues.touchVertexLastTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

  _map2GatherValues2MultiscaleLinkedCell_0.touchVertexLastTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::GatherValues::enterCell(
      PeanoExecutor::Cell&                 fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {
  _map2GatherValues2MultiscaleLinkedCell_0.enterCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


  _map2GatherValues.enterCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

}


void PeanoExecutor::adapters::GatherValues::leaveCell(
      PeanoExecutor::Cell&           fineGridCell,
      PeanoExecutor::Vertex * const  fineGridVertices,
      const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfCell
) {

  _map2GatherValues.leaveCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

  _map2GatherValues2MultiscaleLinkedCell_0.leaveCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

}


void PeanoExecutor::adapters::GatherValues::beginIteration(
  PeanoExecutor::State&  solverState
) {
  _map2GatherValues2MultiscaleLinkedCell_0.beginIteration( solverState );


  _map2GatherValues.beginIteration( solverState );

}


void PeanoExecutor::adapters::GatherValues::endIteration(
  PeanoExecutor::State&  solverState
) {

  _map2GatherValues.endIteration( solverState );

  _map2GatherValues2MultiscaleLinkedCell_0.endIteration( solverState );

}




void PeanoExecutor::adapters::GatherValues::descend(
  PeanoExecutor::Cell * const          fineGridCells,
  PeanoExecutor::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell
) {
  _map2GatherValues2MultiscaleLinkedCell_0.descend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );


  _map2GatherValues.descend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );

}


void PeanoExecutor::adapters::GatherValues::ascend(
  PeanoExecutor::Cell * const    fineGridCells,
  PeanoExecutor::Vertex * const  fineGridVertices,
  const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&           coarseGridCell
) {

  _map2GatherValues.ascend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );

  _map2GatherValues2MultiscaleLinkedCell_0.ascend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );

}
