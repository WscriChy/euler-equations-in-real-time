#include "PeanoExecutor/adapters/PerformTimeStep.h"


peano::CommunicationSpecification   PeanoExecutor::adapters::PerformTimeStep::communicationSpecification() {
  return peano::CommunicationSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::PerformTimeStep::communicationSpecification()

   //& PeanoExecutor::adapters::PerformTimeStep2MultiscaleLinkedCell_0::communicationSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::PerformTimeStep::touchVertexLastTimeSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::PerformTimeStep::touchVertexLastTimeSpecification()

   & PeanoExecutor::adapters::PerformTimeStep2MultiscaleLinkedCell_0::touchVertexLastTimeSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::PerformTimeStep::touchVertexFirstTimeSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::PerformTimeStep::touchVertexFirstTimeSpecification()

   & PeanoExecutor::adapters::PerformTimeStep2MultiscaleLinkedCell_0::touchVertexFirstTimeSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::PerformTimeStep::enterCellSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::PerformTimeStep::enterCellSpecification()

   & PeanoExecutor::adapters::PerformTimeStep2MultiscaleLinkedCell_0::enterCellSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::PerformTimeStep::leaveCellSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::PerformTimeStep::leaveCellSpecification()

   & PeanoExecutor::adapters::PerformTimeStep2MultiscaleLinkedCell_0::leaveCellSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::PerformTimeStep::ascendSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::PerformTimeStep::ascendSpecification()

   & PeanoExecutor::adapters::PerformTimeStep2MultiscaleLinkedCell_0::ascendSpecification()

;
}


peano::MappingSpecification   PeanoExecutor::adapters::PerformTimeStep::descendSpecification() {
  return peano::MappingSpecification::getMinimalSpecification()
   & PeanoExecutor::mappings::PerformTimeStep::descendSpecification()

   & PeanoExecutor::adapters::PerformTimeStep2MultiscaleLinkedCell_0::descendSpecification()

;
}


PeanoExecutor::adapters::PerformTimeStep::PerformTimeStep() {
}


PeanoExecutor::adapters::PerformTimeStep::~PerformTimeStep() {
}


#if defined(SharedMemoryParallelisation)
PeanoExecutor::adapters::PerformTimeStep::PerformTimeStep(const PerformTimeStep&  masterThread):
  _map2PerformTimeStep(masterThread._map2PerformTimeStep)

 ,
  _map2PerformTimeStep2MultiscaleLinkedCell_0(masterThread._map2PerformTimeStep2MultiscaleLinkedCell_0)

{
}


void PeanoExecutor::adapters::PerformTimeStep::mergeWithWorkerThread(const PerformTimeStep& workerThread) {

  _map2PerformTimeStep.mergeWithWorkerThread(workerThread._map2PerformTimeStep);

  _map2PerformTimeStep2MultiscaleLinkedCell_0.mergeWithWorkerThread(workerThread._map2PerformTimeStep2MultiscaleLinkedCell_0);

}
#endif


void PeanoExecutor::adapters::PerformTimeStep::createHangingVertex(
      PeanoExecutor::Vertex&     fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                fineGridH,
      PeanoExecutor::Vertex * const   coarseGridVertices,
      const peano::grid::VertexEnumerator&      coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&       coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                   fineGridPositionOfVertex
) {
  _map2PerformTimeStep2MultiscaleLinkedCell_0.createHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


  _map2PerformTimeStep.createHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


}


void PeanoExecutor::adapters::PerformTimeStep::destroyHangingVertex(
      const PeanoExecutor::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {
  _map2PerformTimeStep.destroyHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


  _map2PerformTimeStep2MultiscaleLinkedCell_0.destroyHangingVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::PerformTimeStep::createInnerVertex(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {
  _map2PerformTimeStep2MultiscaleLinkedCell_0.createInnerVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


  _map2PerformTimeStep.createInnerVertex(fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::PerformTimeStep::createBoundaryVertex(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {
  _map2PerformTimeStep2MultiscaleLinkedCell_0.createBoundaryVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


  _map2PerformTimeStep.createBoundaryVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::PerformTimeStep::destroyVertex(
      const PeanoExecutor::Vertex&   fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2PerformTimeStep.destroyVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

  _map2PerformTimeStep2MultiscaleLinkedCell_0.destroyVertex( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::PerformTimeStep::createCell(
      PeanoExecutor::Cell&                 fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {
  _map2PerformTimeStep2MultiscaleLinkedCell_0.createCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


  _map2PerformTimeStep.createCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

}


void PeanoExecutor::adapters::PerformTimeStep::destroyCell(
      const PeanoExecutor::Cell&           fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {

  _map2PerformTimeStep.destroyCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

  _map2PerformTimeStep2MultiscaleLinkedCell_0.destroyCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

}

#ifdef Parallel
void PeanoExecutor::adapters::PerformTimeStep::mergeWithNeighbour(
  PeanoExecutor::Vertex&  vertex,
  const PeanoExecutor::Vertex&  neighbour,
  int                                           fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridX,
  const tarch::la::Vector<DIMENSIONS,double>&   fineGridH,
  int                                           level
) {
   _map2PerformTimeStep2MultiscaleLinkedCell_0.mergeWithNeighbour( vertex, neighbour, fromRank, fineGridX, fineGridH, level );


   _map2PerformTimeStep.mergeWithNeighbour( vertex, neighbour, fromRank, fineGridX, fineGridH, level );

}

void PeanoExecutor::adapters::PerformTimeStep::prepareSendToNeighbour(
  PeanoExecutor::Vertex&  vertex,
  int                                           toRank,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {
   _map2PerformTimeStep.prepareSendToNeighbour( vertex, toRank, x, h, level );

   _map2PerformTimeStep2MultiscaleLinkedCell_0.prepareSendToNeighbour( vertex, toRank, x, h, level );

}

void PeanoExecutor::adapters::PerformTimeStep::prepareCopyToRemoteNode(
  PeanoExecutor::Vertex&  localVertex,
  int                                           toRank,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {
   _map2PerformTimeStep.prepareCopyToRemoteNode( localVertex, toRank, x, h, level );

   _map2PerformTimeStep2MultiscaleLinkedCell_0.prepareCopyToRemoteNode( localVertex, toRank, x, h, level );

}

void PeanoExecutor::adapters::PerformTimeStep::prepareCopyToRemoteNode(
  PeanoExecutor::Cell&  localCell,
      int                                           toRank,
      const tarch::la::Vector<DIMENSIONS,double>&   x,
      const tarch::la::Vector<DIMENSIONS,double>&   h,
      int                                           level
) {
   _map2PerformTimeStep.prepareCopyToRemoteNode( localCell, toRank, x, h, level );

   _map2PerformTimeStep2MultiscaleLinkedCell_0.prepareCopyToRemoteNode( localCell, toRank, x, h, level );

}

void PeanoExecutor::adapters::PerformTimeStep::mergeWithRemoteDataDueToForkOrJoin(
  PeanoExecutor::Vertex&  localVertex,
  const PeanoExecutor::Vertex&  masterOrWorkerVertex,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {
   _map2PerformTimeStep2MultiscaleLinkedCell_0.mergeWithRemoteDataDueToForkOrJoin( localVertex, masterOrWorkerVertex, fromRank, x, h, level );


   _map2PerformTimeStep.mergeWithRemoteDataDueToForkOrJoin( localVertex, masterOrWorkerVertex, fromRank, x, h, level );

}

void PeanoExecutor::adapters::PerformTimeStep::mergeWithRemoteDataDueToForkOrJoin(
  PeanoExecutor::Cell&  localCell,
  const PeanoExecutor::Cell&  masterOrWorkerCell,
  int                                       fromRank,
  const tarch::la::Vector<DIMENSIONS,double>&  x,
  const tarch::la::Vector<DIMENSIONS,double>&  h,
  int                                       level
) {
   _map2PerformTimeStep2MultiscaleLinkedCell_0.mergeWithRemoteDataDueToForkOrJoin( localCell, masterOrWorkerCell, fromRank, x, h, level );


   _map2PerformTimeStep.mergeWithRemoteDataDueToForkOrJoin( localCell, masterOrWorkerCell, fromRank, x, h, level );

}

bool PeanoExecutor::adapters::PerformTimeStep::prepareSendToWorker(
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker
) {
  bool result = false;
   result |= _map2PerformTimeStep.prepareSendToWorker( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker );

   result |= _map2PerformTimeStep2MultiscaleLinkedCell_0.prepareSendToWorker( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker );

  return result;
}

void PeanoExecutor::adapters::PerformTimeStep::prepareSendToMaster(
  PeanoExecutor::Cell&                       localCell,
  PeanoExecutor::Vertex *                    vertices,
  const peano::grid::VertexEnumerator&       verticesEnumerator,
  const PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&       coarseGridVerticesEnumerator,
  const PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&   fineGridPositionOfCell
) {

   _map2PerformTimeStep.prepareSendToMaster( localCell, vertices, verticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

   _map2PerformTimeStep2MultiscaleLinkedCell_0.prepareSendToMaster( localCell, vertices, verticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

}

void PeanoExecutor::adapters::PerformTimeStep::mergeWithMaster(
  const PeanoExecutor::Cell&           workerGridCell,
  PeanoExecutor::Vertex * const        workerGridVertices,
  const peano::grid::VertexEnumerator& workerEnumerator,
  PeanoExecutor::Cell&                 fineGridCell,
  PeanoExecutor::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell,
  const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell,
  int                                                                  worker,
    const PeanoExecutor::State&          workerState,
  PeanoExecutor::State&                masterState
) {
   _map2PerformTimeStep2MultiscaleLinkedCell_0.mergeWithMaster( workerGridCell, workerGridVertices, workerEnumerator, fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker, workerState, masterState );


   _map2PerformTimeStep.mergeWithMaster( workerGridCell, workerGridVertices, workerEnumerator, fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell, worker, workerState, masterState );

}

void PeanoExecutor::adapters::PerformTimeStep::receiveDataFromMaster(
      PeanoExecutor::Cell&                        receivedCell,
      PeanoExecutor::Vertex *                     receivedVertices,
      const peano::grid::VertexEnumerator&        receivedVerticesEnumerator,
      PeanoExecutor::Vertex * const               receivedCoarseGridVertices,
      const peano::grid::VertexEnumerator&        receivedCoarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                        receivedCoarseGridCell,
      PeanoExecutor::Vertex * const               workersCoarseGridVertices,
      const peano::grid::VertexEnumerator&        workersCoarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                        workersCoarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&    fineGridPositionOfCell
) {
   _map2PerformTimeStep2MultiscaleLinkedCell_0.receiveDataFromMaster( receivedCell, receivedVertices, receivedVerticesEnumerator, receivedCoarseGridVertices, receivedCoarseGridVerticesEnumerator, receivedCoarseGridCell, workersCoarseGridVertices, workersCoarseGridVerticesEnumerator, workersCoarseGridCell, fineGridPositionOfCell );


   _map2PerformTimeStep.receiveDataFromMaster( receivedCell, receivedVertices, receivedVerticesEnumerator, receivedCoarseGridVertices, receivedCoarseGridVerticesEnumerator, receivedCoarseGridCell, workersCoarseGridVertices, workersCoarseGridVerticesEnumerator, workersCoarseGridCell, fineGridPositionOfCell );

}


void PeanoExecutor::adapters::PerformTimeStep::mergeWithWorker(
  PeanoExecutor::Cell&           localCell,
  const PeanoExecutor::Cell&     receivedMasterCell,
  const tarch::la::Vector<DIMENSIONS,double>&  cellCentre,
  const tarch::la::Vector<DIMENSIONS,double>&  cellSize,
  int                                          level
) {
   _map2PerformTimeStep2MultiscaleLinkedCell_0.mergeWithWorker( localCell, receivedMasterCell, cellCentre, cellSize, level );


   _map2PerformTimeStep.mergeWithWorker( localCell, receivedMasterCell, cellCentre, cellSize, level );

}

void PeanoExecutor::adapters::PerformTimeStep::mergeWithWorker(
  PeanoExecutor::Vertex&        localVertex,
  const PeanoExecutor::Vertex&  receivedMasterVertex,
  const tarch::la::Vector<DIMENSIONS,double>&   x,
  const tarch::la::Vector<DIMENSIONS,double>&   h,
  int                                           level
) {

   _map2PerformTimeStep.mergeWithWorker( localVertex, receivedMasterVertex, x, h, level );

   _map2PerformTimeStep2MultiscaleLinkedCell_0.mergeWithWorker( localVertex, receivedMasterVertex, x, h, level );

}
#endif

void PeanoExecutor::adapters::PerformTimeStep::touchVertexFirstTime(
      PeanoExecutor::Vertex&               fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                          fineGridH,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfVertex
) {
  _map2PerformTimeStep2MultiscaleLinkedCell_0.touchVertexFirstTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );


  _map2PerformTimeStep.touchVertexFirstTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::PerformTimeStep::touchVertexLastTime(
      PeanoExecutor::Vertex&         fineGridVertex,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridX,
      const tarch::la::Vector<DIMENSIONS,double>&                    fineGridH,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfVertex
) {

  _map2PerformTimeStep.touchVertexLastTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

  _map2PerformTimeStep2MultiscaleLinkedCell_0.touchVertexLastTime( fineGridVertex, fineGridX, fineGridH, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfVertex );

}


void PeanoExecutor::adapters::PerformTimeStep::enterCell(
      PeanoExecutor::Cell&                 fineGridCell,
      PeanoExecutor::Vertex * const        fineGridVertices,
      const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const        coarseGridVertices,
      const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&                 coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                             fineGridPositionOfCell
) {
  _map2PerformTimeStep2MultiscaleLinkedCell_0.enterCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );


  _map2PerformTimeStep.enterCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

}


void PeanoExecutor::adapters::PerformTimeStep::leaveCell(
      PeanoExecutor::Cell&           fineGridCell,
      PeanoExecutor::Vertex * const  fineGridVertices,
      const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
      PeanoExecutor::Vertex * const  coarseGridVertices,
      const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
      PeanoExecutor::Cell&           coarseGridCell,
      const tarch::la::Vector<DIMENSIONS,int>&                       fineGridPositionOfCell
) {

  _map2PerformTimeStep.leaveCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

  _map2PerformTimeStep2MultiscaleLinkedCell_0.leaveCell( fineGridCell, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell, fineGridPositionOfCell );

}


void PeanoExecutor::adapters::PerformTimeStep::beginIteration(
  PeanoExecutor::State&  solverState
) {
  _map2PerformTimeStep2MultiscaleLinkedCell_0.beginIteration( solverState );


  _map2PerformTimeStep.beginIteration( solverState );

}


void PeanoExecutor::adapters::PerformTimeStep::endIteration(
  PeanoExecutor::State&  solverState
) {

  _map2PerformTimeStep.endIteration( solverState );

  _map2PerformTimeStep2MultiscaleLinkedCell_0.endIteration( solverState );

}




void PeanoExecutor::adapters::PerformTimeStep::descend(
  PeanoExecutor::Cell * const          fineGridCells,
  PeanoExecutor::Vertex * const        fineGridVertices,
  const peano::grid::VertexEnumerator&                fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const        coarseGridVertices,
  const peano::grid::VertexEnumerator&                coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&                 coarseGridCell
) {
  _map2PerformTimeStep2MultiscaleLinkedCell_0.descend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );


  _map2PerformTimeStep.descend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );

}


void PeanoExecutor::adapters::PerformTimeStep::ascend(
  PeanoExecutor::Cell * const    fineGridCells,
  PeanoExecutor::Vertex * const  fineGridVertices,
  const peano::grid::VertexEnumerator&          fineGridVerticesEnumerator,
  PeanoExecutor::Vertex * const  coarseGridVertices,
  const peano::grid::VertexEnumerator&          coarseGridVerticesEnumerator,
  PeanoExecutor::Cell&           coarseGridCell
) {

  _map2PerformTimeStep.ascend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );

  _map2PerformTimeStep2MultiscaleLinkedCell_0.ascend( fineGridCells, fineGridVertices, fineGridVerticesEnumerator, coarseGridVertices, coarseGridVerticesEnumerator, coarseGridCell );

}
