#include "PeanoExecutor/State.h"
#include "PeanoExecutor/Cell.h"
#include "PeanoExecutor/Vertex.h"

#include "peano/grid/Checkpoint.h"



PeanoExecutor::State::State():
  Base() {
  // @todo Insert your code here
}


PeanoExecutor::State::State(const Base::PersistentState& argument):
  Base(argument) {
  // @todo Insert your code here
}


void PeanoExecutor::State::writeToCheckpoint( peano::grid::Checkpoint<PeanoExecutor::Vertex,PeanoExecutor::Cell>& checkpoint ) const {
  // @todo Insert your code here
}


void PeanoExecutor::State::readFromCheckpoint( const peano::grid::Checkpoint<PeanoExecutor::Vertex,PeanoExecutor::Cell>& checkpoint ) {
  // @todo Insert your code here
}
