// Do not modify any part of this file. It will be overwritten throughout the
// next pdt run.


#include "PeanoExecutor/VertexOperations.h"
#include "peano/utils/Loop.h"
#include "peano/grid/Checkpoint.h"


PeanoExecutor::VertexOperations::VertexOperations() {
}



























 tarch::la::Vector<TWO_POWER_D_TIMES_TWO_POWER_D,int>  PeanoExecutor::VertexOperations::readIndex(const peano::grid::VertexEnumerator& enumerator, const Vertex* const vertices)  { tarch::la::Vector<TWO_POWER_D_TIMES_TWO_POWER_D,int> result; dfor2(x) tarch::la::slice(result,vertices[ enumerator(x) ]._vertexData.getIndex(),xScalar*TWO_POWER_D); enddforx return result; }










 void PeanoExecutor::VertexOperations::writeIndex(const peano::grid::VertexEnumerator& enumerator, Vertex* const vertices, const tarch::la::Vector<TWO_POWER_D_TIMES_TWO_POWER_D,int>& values) { dfor2(x) tarch::la::Vector<TWO_POWER_D,int> temp = tarch::la::slice<TWO_POWER_D>(values,xScalar*TWO_POWER_D); vertices[ enumerator(x) ]._vertexData.setIndex( temp ); enddforx }



























