// This file is part of the Peano project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#ifndef _PEANOEXECUTOR_STATE_H_
#define _PEANOEXECUTOR_STATE_H_

#include "PeanoExecutor/records/State.h"
#include "peano/grid/State.h"

#include "peano/grid/Checkpoint.h"

#include "Simulation/Scenario.hpp"

namespace PeanoExecutor {
class State;
/**
 * Forward declaration
 */
class Vertex;
/**
 * Forward declaration
 */
class Cell;

namespace repositories {
/**
 * Forward declaration
 */
class RepositoryArrayStack;
class RepositorySTDStack;
}
}

/**
 * Blueprint for solver state.
 *
 * This file has originally been created by the PDT and may be manually extended
 * to
 * the needs of your application. We do not recommend to remove anything!
 */
class PeanoExecutor::State : public
                             peano::grid::State<PeanoExecutor::records::State> {
private:
  typedef class peano::grid::State<PeanoExecutor::records::State> Base;

  /**
   * Needed for checkpointing.
   */
  friend class PeanoExecutor::repositories::RepositoryArrayStack;
  friend class PeanoExecutor::repositories::RepositorySTDStack;

  void
  writeToCheckpoint(peano::grid::Checkpoint<Vertex, Cell>&  checkpoint) const;
  void
  readFromCheckpoint(const peano::grid::Checkpoint<Vertex, Cell>&  checkpoint);

public:
  typedef EulerEquations::Simulation::Scenario       Scenario;
  typedef EulerEquations::Simulation::ScenarioRunner ScenarioRunner;
  typedef EulerEquations::Simulation::ScenarioData   ScenarioData;

public:
  /**
   * Default Constructor
   *
   * This constructor is required by the framework's data container. Do not
   * remove it.
   */
  State();

  /**
   * Constructor
   *
   * This constructor is required by the framework's data container. Do not
   * remove it. It is kind of a copy constructor that converts an object which
   * comprises solely persistent attributes into a full attribute. This very
   * functionality is implemented within the super type, i.e. this constructor
   * has to invoke the correponsing super type's constructor and not the super
   * type standard constructor.
   */
  State(const Base::PersistentState& argument);

  void
  scenario(Scenario::Shared scenario) {
    _scenario     = scenario;
    _scenarioData = _scenario->createData();
  }

  Scenario::Shared
  scenario() const {
    return _scenario;
  }

  ScenarioData::Shared
  scenarioData() const {
    return _scenarioData;
  }

  void
  resetScenarioData() {
    _scenarioData = _scenario->createData();
  }

private:
  Scenario::Shared     _scenario;
  ScenarioData::Shared _scenarioData;
};

#endif
