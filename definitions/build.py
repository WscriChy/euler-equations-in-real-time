#!/usr/bin/env python
# encoding: utf-8

import inspect
import os
import sys
import subprocess
import shutil

currendDir = os.path.dirname(os.path.realpath(
    os.path.abspath(inspect.getfile(inspect.currentframe()))))

outputDir = os.path.join(currendDir, "output")

if (not os.path.exists(outputDir)):
  shutil.makedirs(outputDir)

pdt = "E:/BackUp/PeanoSource/pdt/pdt.jar"
dastgen = "E:/BackUp/PeanoSource/pdt/lib/DaStGen.jar"

templates = [
  "../3rdParty/Peano/multiscalelinkedcell"
  ]

subprocess.call("java -jar %s PeanoExecutor.peano-specification output %s" % (
  pdt, ",".join(templates)),
  shell = True,
  cwd=currendDir)

subprocess.call("java -jar %s SubgridDescriptor.def output/records" % (
  dastgen),
  shell = True,
  cwd=currendDir)

subprocess.call("java -jar %s Unknown.def output/records" % (
  dastgen),
  shell = True,
  cwd=currendDir)

#PDT_PATH=../peano3/pdt
#java -jar $PDT_PATH/lib/DaStGen.jar --plugin PeanoHeapSnippetGenerator --naming PeanoHeapNameTranslator --include $PWD/../../src src/peanoclaw/dastgen/CellDescription.def src/peanoclaw/records/
