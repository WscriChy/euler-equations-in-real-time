Real-Time 3D Simulation of a Fluid Flow Governed by the Euler Equations
=======================================================================

--------------------------------------------------------------------------------

Description
-----------

--------------------------------------------------------------------------------

In this project the 3D Euler Equations are solved numerically and the results are visualized.
The simulation and the visualization happens simultaneously in real-time.
The visualization shows the density field of the flow in a specified range using two colors for the highest density and for the lowest specify density of the range, these colors are blended inside the interval.

How it works
------------

--------------------------------------------------------------------------------

1.  [The Euler Equations](https://en.wikipedia.org/wiki/Euler_equations_%28fluid_dynamics%29) are solved using the [Roe Solver](https://en.wikipedia.org/wiki/Roe_solver).

    * The domain is discretized with a Cartesian grid, that is managed by [the Peano fraemowork](http://www5.in.tum.de/peano/releases/index.html).
    * The domain cells are resolved in parallel using [TBB library][TBB] on multiple cores.
 
2.  The visualization is processed through the [OpenGL][] pipeline based on the information received from the solver.

    * [Qt5 libraries][Qt5] or [Equalizer parallel rendering libary][Equalizer] provide the OpenGL context.
    * Vertex and fragment [GL shaders](https://www.opengl.org/wiki/Shader) are used to represent the fluid flow fields in 3D space.
    * A small window with several tabs based on [CEGUI library][CEGUI] for OpenGL graphical user interface allows to see some output and adjust parameters of the visualization in real time.

3.  Both, the solver and the visualization, run simultaneously (in different threads) and synchronize with each other to transfer the fluid solver fields to the visualization buffers for rendering.

Bubble Scenario
---------------

--------------------------------------------------------------------------------

In this scenario a rectangular domain has all boundary conditions set to a solid wall.
At the beginning in the middle of the domain there is a bubble with a lower density
than the rest of the domain, the velocity field is zero.

--------------------------------------------------------------------------------

![Bubble Scenario #1](https://dl.dropbox.com/s/c82o7nkyivk7mmm/Euler-Equations-1.png)

![Bubble Scenario #2](https://dl.dropbox.com/s/5g3ehyh9rrpm81o/Euler-Equations-2.png)

![Bubble Scenario #3](https://dl.dropbox.com/s/9ri58t2tdt7p1po/Euler-Equations-3.png)

![Bubble Scenario #4](https://dl.dropbox.com/s/kkrk2kjfjrx3m2b/Euler-Equations-4.png)

![Bubble Scenario #5](https://dl.dropbox.com/s/7a0pc4w106upwp4/Euler-Equations-5.png)

![Bubble Scenario #6](https://dl.dropbox.com/s/rr2619awpffhjhy/Euler-Equations-6.png)

![Bubble Scenario #7](https://dl.dropbox.com/s/64w39p9cqznlv8b/Euler-Equations-7.png)

![Bubble Scenario #8](https://dl.dropbox.com/s/xjdn7kysq11njd8/Euler-Equations-8.png)

![Bubble Scenario #9](https://dl.dropbox.com/s/m456i3inqy00hm5/Euler-Equations-9.png)

![Bubble Scenario #10](https://dl.dropbox.com/s/3jxmog8xz7ud4x7/Euler-Equations-10.png)

![Bubble Scenario #11](https://dl.dropbox.com/s/qol31rc39xa7iv5/Euler-Equations-11.png)

![Bubble Scenario #12](https://dl.dropbox.com/s/kvmosfiz4yagsqq/Euler-Equations-12.png)

![Bubble Scenario #13](https://dl.dropbox.com/s/304gh1fa40npgt3/Euler-Equations-13.png)

![Bubble Scenario #14](https://dl.dropbox.com/s/edp3pxf2wxwqd15/Euler-Equations-14.png)

![Bubble Scenario #15](https://dl.dropbox.com/s/ipsr0a5ie4czdh6/Euler-Equations-15.png)

![Bubble Scenario #16](https://dl.dropbox.com/s/1zsxbq1xg7lt61e/Euler-Equations-16.png)

![Bubble Scenario #17](https://dl.dropbox.com/s/jn0ya82nmts4snz/Euler-Equations-17.png)


--------------------------------------------------------------------------------

[Press here to see a short video.](https://www.dropbox.com/s/gs6xme6po56ckd6/Euler-Equations.mp4?dl=0)


Features
------------

--------------------------------------------------------------------------------

 * Highly optimized solver implementation for parallel running
 * Support of parallel rendering
 * Cross-platform solution
 * Special scenario description design allowing to add a large variety of new scenarios
 * Flexible architecture making easy to extend the solver as well as the visualization and control modules

Dependencies
------------

--------------------------------------------------------------------------------

 * [Eigen][] library for the vectorized linear algebra support
 * [TBB][] library or the multicore parallelization
 * [GLEW][] library for [OpenGL][] 4.0 API implementation
 * [Qt5][] Core and Gui components or [Equalizer] library for cross-platform window systems
 * [CEGUI][] library for OpenGL graphical user interface

Build instructions
------------------

--------------------------------------------------------------------------------

To build the project the common procedure for `cmake` is used.

* Options

    1. `Displaying system` option

        *Note*: Don't forget `-D` flag before specifying options with common
        `cmake` configuration method (Don't do this with custom wrappers
        method discussed below).

        This options allows to choose betwee Qt and Equalizer displaying
        systems.

        * `Qt=ON` is the default option for displaying.

        * `Equalizer=ON` switch to Equalizer framework for displaying instead
          of Qt.

## Using custom Python wrapper

1. Configure in `.build` directory / `./configure`

    1. Build tool

        * Default is `Ninja`.

        * It is possible to choose a build tool the same way it is done for
          `cmake`.

            In `cmake` to use `make`

                cmake -G "Unix Makefiles"

            here

                ./configure -G "Unix Makefiles"

    2. Options

        * `Qt=ON` is the default option for displaying.

                ./configure Qt=ON

            is equivalent to

                ./configure

        * `Equalizer=ON` switches to Equalizer framework for displaying instead
          of Qt.

                ./configure Equalizer=ON


2. Build in `.build` directory / `./build`

    * Extra options could be passed to the building tool.

        For example, to `ninja` being verbose:

            ./build -v

3. Install to `.install` directory / `./install`

    * Extra options could be passed to the building tool in the same way like
      for the build step.

--------------------------------------------------------------------------------

Copyright
---------

--------------------------------------------------------------------------------

Copyright (C) 2015, Viacheslav Mikerov

License
-------

--------------------------------------------------------------------------------

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.


[GLEW]:      http://glew.sourceforge.net/
[Eigen]:     http://eigen.tuxfamily.org/index.php?title=Main_Page
[TBB]:       https://www.threadingbuildingblocks.org/
[Qt5]:       http://doc.qt.io/qt-5/
[Equalizer]: http://www.equalizergraphics.com/
[CEGUI]:     https://en.wikipedia.org/wiki/CEGUI
[OpenGL]:    https://www.opengl.org/
