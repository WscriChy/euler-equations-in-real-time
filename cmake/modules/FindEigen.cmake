if ("${Eigen_INCLUDE_DIRS}" STREQUAL "")
  set(Eigen_INCLUDE_DIRS $ENV{Eigen3_INCLUDE_DIRS})
endif()

find_path(EIGEN_INCLUDE_DIR
  NAMES Eigen/Eigen
  PATHS ${Eigen3_INCLUDE_DIR} PATH_SUFFIXES eigen3)


set(EIGEN_INCLUDE_DIRS ${EIGEN_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args("Eigen3"
                                  REQUIRED_VARS
                                  EIGEN_INCLUDE_DIRS)

mark_as_advanced(EIGEN_INCLUDE_DIR EIGEN_INCLUDE_DIRS)
