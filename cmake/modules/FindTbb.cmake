if ("${TBB}" STREQUAL "")
  set(TBB $ENV{TBB})
endif ()

if (WIN32)
  set(TBB_LIBRARY_DIR "${TBB}/bin")
else()
  set(TBB_LIBRARY_DIR "${TBB}/lib")
endif ()

set(TBB_INCLUDE_DIR "${TBB}/include")

find_path(TBB_INCLUDE_DIR
  NAMES tbb/tbb.h
  PATHS ${_TBB_INCLUDE_DIR})

find_library(TBB_LIBRARY
             NAMES tbb
             PATHS ${TBB_LIBRARY_DIR})

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args("Tbb"
                                  DEFAULT_MSG
                                  TBB_INCLUDE_DIR
                                  TBB_LIBRARY)

mark_as_advanced(TBB_INCLUDE_DIR TBB_LIBRARY)
