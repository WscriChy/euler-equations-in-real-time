if ("${Cegui_INCLUDE_DIRS}" STREQUAL "")
  set(Cegui_INCLUDE_DIRS $ENV{Cegui_INCLUDE_DIRS})
endif()

find_path(CEGUI_INCLUDE_DIR
  NAMES "cegui-0/CEGUI/CEGUI.h"
  PATHS ${Cegui_INCLUDE_DIRS})

if (NOT "${CEGUI_INCLUDE_DIR}" STREQUAL "")
  file(TO_CMAKE_PATH "${CEGUI_INCLUDE_DIR}/cegui-0" CEGUI_INCLUDE_DIR)
endif()

set(CEGUI_INCLUDE_DIRS ${CEGUI_INCLUDE_DIR})

# Looking for libraries
if ("${Cegui_LIBRARIES}" STREQUAL "")
  set(Cegui_LIBRARIES $ENV{Cegui_LIBRARIES})
endif()

find_library(CEGUI_Base_LIBRARY
             NAMES CEGUIBase-0
             PATHS ${Cegui_LIBRARIES})

find_library(CEGUI_OpenGLRenderer_LIBRARY
             NAMES CEGUIOpenGLRenderer-0
             PATHS ${Cegui_LIBRARIES})

find_library(CEGUI_LibXMLParser_LIBRARY
             NAMES CEGUILibXMLParser
             PATHS ${Cegui_LIBRARIES}
             PATH_SUFFIXES cegui-0.8)

find_library(CEGUI_FreeImageImageCodec_LIBRARY
             NAMES CEGUIFreeImageImageCodec
             PATHS ${Cegui_LIBRARIES}
             PATH_SUFFIXES cegui-0.8)

find_library(CEGUI_CoreWindowRendererSet_LIBRARY
             NAMES CEGUICoreWindowRendererSet
             HINTS ${Cegui_LIBRARIES}
             PATH_SUFFIXES cegui-0.8)

find_library(CEGUI_CommonDialogs_LIBRARY
             NAMES CEGUICommonDialogs-0
             PATHS ${Cegui_LIBRARIES}
             )


set(CEGUI_LIBRARIES
  ${CEGUI_Base_LIBRARY}
  ${CEGUI_OpenGLRenderer_LIBRARY}
  ${CEGUI_LibXMLParser_LIBRARY}
  ${CEGUI_FreeImageImageCodec_LIBRARY}
  ${CEGUI_CoreWindowRendererSet_LIBRARY}
  ${CEGUI_CommonDialogs_LIBRARY}
  )

# Finalize

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args("CEGUI"
                                  REQUIRED_VARS
                                  CEGUI_INCLUDE_DIRS
                                  CEGUI_LIBRARIES
                                  )

mark_as_advanced(CEGUI_INCLUDE_DIR
                 CEGUI_INCLUDE_DIRS
                 CEGUI_LIBRARIES
                 CEGUI_Base_LIBRARY
                 CEGUI_OpenGLRenderer_LIBRARY
                 CEGUI_XercesParser_LIBRARY
                 CEGUI_TinyXMLParser_LIBRARY
                 CEGUI_LibXMLParser_LIBRARY
                 CEGUI_FreeImageImageCodec_LIBRARY
                 CEGUI_ExpatParser_LIBRARY
                 CEGUI_CoreWindowRendererSet_LIBRARY
                 CEGUI_CommonDialogs_LIBRARY
                 )
