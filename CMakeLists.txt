# General {{{
# ==============================================================================
cmake_minimum_required(VERSION 2.8.8)
# ------------------------------------------------------------------------------
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)
set(CMAKE_DISABLE_SOURCE_CHANGES  ON)

if ("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_BINARY_DIR}")
  message(SEND_ERROR "In-source builds are not allowed.")
endif ()
# ------------------------------------------------------------------------------
project("Real-Time 3D Simulation of a fluid flow governed by the Euler Equations" C CXX)
# ------------------------------------------------------------------------------
set(CMAKE_ERROR_DEPRECATED ON)
# ------------------------------------------------------------------------------
set(CMAKE_VERBOSE_MAKEFILE ON)
set(CMAKE_COLOR_MAKEFILE   ON)
# ------------------------------------------------------------------------------
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# ------------------------------------------------------------------------------
if (WIN32)
  set(CMAKE_SHARED_LIBRARY_PREFIX "")
endif ()
# ------------------------------------------------------------------------------
get_filename_component(PROJECT_DIR "." ABSOLUTE)
set(INSTALL_BINARY_DIR  bin)
set(INSTALL_INCLUDE_DIR include)
set(INSTALL_LIBRARY_DIR lib)
if (WIN32)
  set(INSTALL_CMAKE_DIR cmake)
else()
  set(INSTALL_CMAKE_DIR lib/cmake)
endif ()
# ==============================================================================
# }}} General

# Modules {{{
# ==============================================================================
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_SOURCE_DIR}/cmake/modules)
# ------------------------------------------------------------------------------
find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)
find_package(Eigen REQUIRED)
find_package(Tbb REQUIRED)
find_package(Cegui REQUIRED)
find_package(Threads REQUIRED)
find_package(FreeImage REQUIRED)
include(UseQt5)

find_package(Boost COMPONENTS filesystem locale regex REQUIRED)
set(Boost_USE_STATIC_LIBS    ON)
set(Boost_USE_MULTITHREADED  ON)
set(Boost_USE_STATIC_RUNTIME OFF)
# ==============================================================================
# }}} Modules

# Compilers {{{
# ==============================================================================
if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  set(COMPILER_IS_CLANG TRUE)
  set(LINKER_IS_LD      TRUE)
elseif (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  set(COMPILER_IS_GCC TRUE)
  set(LINKER_IS_LD    TRUE)
elseif (CMAKE_CXX_COMPILER_ID STREQUAL "Intel")
  set(COMPILER_IS_ICC TRUE)
  set(LINKER_IS_ICC    TRUE)
else()
  message(FATAL "Unsopported toolchain")
endif ()
# ==============================================================================
# }}} Compilers

# Definitions {{{
# ==============================================================================
add_definitions(-DMOC)
add_definitions(-DUNICODE)
# ==============================================================================
# }}} Definitions

# Flags {{{
# ==============================================================================
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
if (COMPILER_IS_CLANG OR COMPILER_IS_GCC OR COMPILER_IS_ICC)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pipe")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -w")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wextra")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pedantic")
  #set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fmessage-length=0")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

  #set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -fomit-frame-pointer")
  #set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -funroll-loops")

  set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0")
  set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -fno-inline")
  # set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -fsanitize=address")
  # set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -fsanitize=undefined")
  set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -fno-omit-frame-pointer")
  set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -ggdb3")
  set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g3")
  # set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -fsanitize=address")
  # set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -fsanitize=undefined")
  set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -ggdb3")
  set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -g3")
endif ()
# ------------------------------------------------------------------------------
if (LINKER_IS_LD)
  if (WIN32)
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static-libgcc")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static-libstdc++")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static")
  endif ()

  set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} -Wl,-O5")
  # ----------------------------------------------------------------------------
  if (WIN32)
    set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -static-libgcc")
    set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -static-libstdc++")
    set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -static")
  endif ()

  set(CMAKE_MODULE_LINKER_FLAGS_RELEASE "${CMAKE_MODULE_LINKER_FLAGS_RELEASE} -Wl,-O5")
  # ----------------------------------------------------------------------------
  if (WIN32)
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -static-libgcc")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -static-libstdc++")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -static")
  endif ()

  set(CMAKE_SHARED_LINKER_FLAGS_RELEASE "${CMAKE_SHARED_LINKER_FLAGS_RELEASE} -Wl,-O5")
elseif (LINKER_IS_ICC)
endif ()
# ==============================================================================
# }}} Flags

# Targets {{{
# ==============================================================================
file(GLOB_RECURSE CPP_FILES
  "${PROJECT_DIR}/source/*.cpp"
  "${PROJECT_DIR}/3rdParty/Peano/*.cpp"
  "${PROJECT_DIR}/3rdParty/Backtrace/*.cpp"
  "${PROJECT_DIR}/3rdParty/Uni/source/*.cpp"
  )

set(CONFIGURATION_STATE "Qt")
if (Equalizer)
  set(CONFIGURATION_STATE "Equalizer")
endif()

if (${CONFIGURATION_STATE} STREQUAL "Qt")
  get_filename_component(Ui_startEqualizer_cpp "source/Ui/startEqualizer.cpp" ABSOLUTE)
  list(REMOVE_ITEM CPP_FILES "${Ui_startEqualizer_cpp}")
  file(GLOB_RECURSE Equalizer_CPP
    "${PROJECT_DIR}/source/Ui/Equalizer/*.cpp")
  foreach(i ${Equalizer_CPP})
    list(REMOVE_ITEM CPP_FILES "${i}")
  endforeach()
else()
if (${CONFIGURATION_STATE} STREQUAL "Equalizer")
  get_filename_component(Ui_startQt_cpp_file "source/Ui/startQt.cpp" ABSOLUTE)
  list(REMOVE_ITEM CPP_FILES "${Ui_startQt_cpp_file}")
  file(GLOB_RECURSE Qt_CPP
    "${PROJECT_DIR}/source/Gui/Qt/*.cpp")
  foreach(i ${Qt_CPP})
    list(REMOVE_ITEM CPP_FILES "${i}")
  endforeach()
else()
  message(FATAL "Unknown build configuration")
endif()
endif()

#message(STATUS ${CPP_FILES})

if (${CONFIGURATION_STATE} STREQUAL "Qt")
  find_qt5_component(Core)
  qt5_wrap_cpp(CPP_MOC_FILES
    source/Ui/Qt/MainWindow.hpp
    )
endif()

add_executable(EulerEquations ${CPP_FILES} ${CPP_MOC_FILES})

include_directories("${PROJECT_DIR}/source")


include_directories(SYSTEM
  ${FREEIMAGE_INCLUDE_DIRS}
  ${EIGEN_INCLUDE_DIRS}
  ${GLEW_INCLUDE_DIRS}
  ${OPENGL_INCLUDE_DIRS}
  ${TBB_INCLUDE_DIR}
  ${CEGUI_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
  "${PROJECT_DIR}/3rdParty/Peano"
  "${PROJECT_DIR}/3rdParty/Uni/include"
  )

add_definitions(-DDim3)

if (${CONFIGURATION_STATE} STREQUAL "Qt")
  target_use_qt5(EulerEquations Core Gui)
endif()


if (${CONFIGURATION_STATE} STREQUAL "Equalizer")
  find_package(Equalizer REQUIRED)
  include_directories(SYSTEM
    ${EQUALIZER_INCLUDE_DIRS}
    )
  target_link_libraries(EulerEquations
    ${EQUALIZER_LIBRARIES}
    )
endif()

target_link_libraries(EulerEquations
  ${FREEIMAGE_LIBRARIES}
  ${TBB_LIBRARY}
  ${CEGUI_LIBRARIES}
  ${GLEW_LIBRARIES}
  ${OPENGL_LIBRARIES}
  ${Boost_LIBRARIES}
  ${CMAKE_THREAD_LIBS_INIT}
  )

file(COPY ${PROJECT_DIR}/resource/Gui DESTINATION ${CMAKE_BINARY_DIR})

install(DIRECTORY "${PROJECT_DIR}/resource/Gui/"
  DESTINATION "${INSTALL_BINARY_DIR}/Gui/")

install(TARGETS EulerEquations
        ARCHIVE DESTINATION ${INSTALL_LIBRARY_DIR}
        LIBRARY DESTINATION ${INSTALL_LIBRARY_DIR}
        RUNTIME DESTINATION ${INSTALL_BINARY_DIR})
# ==============================================================================
# }}} Targets

# vim:ft=cmake:fenc=utf-8:ff=unix:ts=2:sw=2:tw=80:et:
