#ifndef Uni_InputMapping_InputMapper_hpp
#define Uni_InputMapping_InputMapper_hpp

#include "Event.hpp"

#include <Uni/Core/Auxiliary/Delegate>

#include <Uni/String>

#include <functional>
#include <map>
#include <memory>
#include <utility>
#include <vector>

namespace Uni {
namespace InputMapping {
class InputMapper {
public:
  typedef std::shared_ptr<InputMapper> Shared;

  typedef std::pair<int, int> EventId;

private:
  typedef Core::Auxiliary::Delegate Delegate;
  typedef std::unique_ptr<Delegate> Handler;
  typedef std::vector<Handler>      List;
  typedef std::map<Event, List>     Map;
  typedef std::map<int, Map>        PriorityMap;

public:
  InputMapper() {}

  InputMapper(InputMapper const& other) = delete;

  virtual
  ~InputMapper() {}

  InputMapper const&
  operator=(InputMapper const& other) = delete;

  template <typename T>
  EventId
  handle(Event const&            event,
         std::function<T> const& function,
         int const&              priority = 0) {
    auto handler = Handler(new Delegate());
    Delegate::createFromFunction(*handler.get(), function);

    return addToMap(event, std::move(handler), priority);
  }

  template <typename T>
  EventId
  handle(Event const& event,
         T            lambda,
         int const&   priority = 0) {
    auto handler = Handler(new Delegate());
    Delegate::createFromLambda(*handler.get(), lambda);

    return addToMap(event, std::move(handler), priority);
  }

  template <typename ReturnType,
            typename... Args>
  EventId
  handle(Event const&  event,
         ReturnType (* functionPointer)(Args...),
         int const&    priority = 0) {
    auto handler = Handler(new Delegate());
    Delegate::createFromFunctionPointer(*handler.get(), functionPointer);

    return addToMap(event, std::move(handler), priority);
  }

  template <typename Pointer,
            typename ReturnType,
            typename ClassType,
            typename... Args>
  EventId
  handle(Event const&             event,
         Pointer*                 pointer,
         ReturnType (ClassType::* methodPointer)(Args...),
         int const&               priority = 0) {
    auto handler = Handler(new Delegate());
    Delegate::createFromMethodPointer(*handler.get(), pointer, methodPointer);

    return addToMap(event, std::move(handler), priority);
  }

  ////////////////////////////////////////////////////////////////////
  // Set up True Handler
  ////////////////////////////////////////////////////////////////////

  template <typename ReturnType,
            typename... Args>
  EventId
  handleAndReturnTrue(Event const&                              event,
                      std::function<ReturnType(Args...)> const& function,
                      int const&                                priority = 0) {
    return handle(event,
                  [ = ] (Args&& ... args) {
                    function(std::forward<Args>(args) ...);

                    return true;
                  }, priority);
  }

  template <typename T>
  EventId
  handleAndReturnTrue(Event const& event,
                      T            lambda,
                      int const&   priority = 0) {
    return handleAndReturnTrue(event,
                               Core::Auxiliary::Detail::toFunction<T>(lambda),
                               priority);
  }

  template <typename ReturnType,
            typename... Args>
  EventId
  handleAndReturnTrue(Event const&  event,
                      ReturnType (* functionPointer)(Args...),
                      int const&    priority = 0) {
    return handle(event,
                  [ = ] (Args&& ... args) {
                    (*functionPointer)(std::forward<Args>(args) ...);

                    return true;
                  }, priority);
  }

  template <typename Pointer,
            typename ReturnType,
            typename ClassType,
            typename... Args>
  EventId
  handleAndReturnTrue(Event const&             event,
                      Pointer                  pointer,
                      ReturnType (ClassType::* methodPointer)(Args...),
                      int const&               priority = 0) {
    return handle(event,
                  [ = ] (Args&& ... args) {
                    ((*pointer).*methodPointer)(std::forward<Args>(args) ...);

                    return true;
                  }, priority);
  }

  ////////////////////////////////////////////////////////////////////
  // Set up False Handler
  ////////////////////////////////////////////////////////////////////

  template <typename ReturnType,
            typename... Args>
  EventId
  handleAndReturnFalse(Event const&                              event,
                       std::function<ReturnType(Args...)> const& function,
                       int const&                                priority = 0) {
    return handle(event,
                  [ = ] (Args&& ... args) {
                    function(std::forward<Args>(args) ...);

                    return false;
                  }, priority);
  }

  template <typename T>
  EventId
  handleAndReturnFalse(Event const& event,
                       T            lambda,
                       int const&   priority = 0) {
    return handleAndReturnFalse(event,
                                Core::Auxiliary::Detail::toFunction<T>(lambda),
                                priority);
  }

  template <typename ReturnType,
            typename... Args>
  EventId
  handleAndReturnFalse(Event const&  event,
                       ReturnType (* functionPointer)(Args...),
                       int const&    priority = 0) {
    return handle(event,
                  [ = ]  (Args&& ... args) {
                    (*functionPointer)(std::forward<Args>(args) ...);

                    return false;
                  }, priority);
  }

  template <typename Pointer,
            typename ReturnType,
            typename ClassType,
            typename... Args>
  EventId
  handleAndReturnFalse(Event const&             event,
                       Pointer                  pointer,
                       ReturnType (ClassType::* methodPointer)(Args...),
                       int const&               priority = 0) {
    return handle(event,
                  [ = ] (Args&& ... args) {
                    ((*pointer).*methodPointer)(std::forward<Args>(args) ...);

                    return false;
                  }, priority);
  }

  template <typename... Args>
  void
  fire(Event const& event,
       Args&& ...   args) const {
    for (auto const& map : _map) {
      auto it = map.second.find(event);

      bool isHandled = false;

      if (it != map.second.end()) {
        for (auto const& handler : it->second) {
          if (!isHandled) {
            isHandled = handler->invoke<bool, Args...>(
              std::forward<Args>(args) ...);
          } else {
            handler->invoke<bool, Args...>(
              std::forward<Args>(args) ...);
          }
        }
      }

      if (isHandled) {
        break;
      }
    }
  }

private:
  EventId
  addToMap(Event const& event,
           Handler&&    handler,
           int const&   priority = 0) {
    Map* mapPtr;
    auto itRoot = _map.find(priority);

    if (itRoot == _map.end()) {
      auto result = _map.insert(std::make_pair(priority, Map()));
      mapPtr = &(result.first->second);
    } else {
      mapPtr = &(itRoot->second);
    }

    auto it = mapPtr->find(event);

    if (it == mapPtr->end()) {
      List list;
      list.push_back(std::move(handler));
      mapPtr->insert(std::make_pair(event, std::move(list)));

      return std::make_pair(priority, 0);
    } else {
      it->second.push_back(std::move(handler));

      return std::make_pair(priority, it->second.size() - 1);
    }
  }

  PriorityMap _map;
};
}
}

#endif
