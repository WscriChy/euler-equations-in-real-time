#pragma once

#include "Auxiliary/Delegate.hpp"

#include <Uni/ExecutionControl/exception>
#include <Uni/Logging/macros>

#include <functional>
#include <map>
#include <memory>
#include <utility>
#include <vector>

namespace Uni {
namespace Core {
template <typename Property>
class Observable {
private:
  typedef Auxiliary::Delegate         Observer;
  typedef std::unique_ptr<Observer>   UniqueObserver;
  typedef std::vector<UniqueObserver> List;
  typedef std::map<Property, List>    Map;

public:
  template <typename T>
  void
  observe(Property const&         property,
          std::function<T> const& function) {
    auto observer = UniqueObserver(new Observer());
    Observer::createFromFunction(*observer.get(), function);
    addToMap(property, std::move(observer));
  }

  template <typename T>
  void
  observe(Property const& property,
          T&&             lambda) {
    auto observer = UniqueObserver(new Observer());
    Observer::createFromLambda(*observer.get(), lambda);
    addToMap(property, std::move(observer));
  }

  template <typename ReturnType,
            typename... Args>
  void
  observe(Property const& property,
          ReturnType      (*   functionPointer)(Args...)) {
    auto observer = UniqueObserver(new Observer());
    Observer::createFromFunctionPointer(*observer.get(), functionPointer);
    addToMap(property, std::move(observer));
  }

  template <typename Pointer,
            typename ReturnType,
            typename ClassType,
            typename... Args>
  void
  observe(Property const& property,
          Pointer         pointer,
          ReturnType      (ClassType::* methodPointer)(Args...)) {
    auto observer = UniqueObserver(new Observer());
    Observer::createFromMethodPointer(*observer.get(), pointer, methodPointer);
    addToMap(property, std::move(observer));
  }

protected:
  template <typename... Args>
  void
  notify(Property const& property,
         Args&& ...      args) const {
    auto it = _map.find(property);

    if (it != _map.end()) {
      for (auto const& observer : it->second) {
        observer->invoke(std::forward<Args>(args) ...);
      }
    }
  }

private:
  void
  addToMap(Property const&  property,
           UniqueObserver&& observer) {
    auto it = _map.find(property);

    if (it == _map.end()) {
      List list;
      list.push_back(std::move(observer));
      _map.insert(std::make_pair(property, std::move(list)));
    } else {
      it->second.push_back(std::move(observer));
    }
  }

  Map _map;
};
}

template <typename Property>
using Observable = Uni::Core::Observable<Property>;
}
