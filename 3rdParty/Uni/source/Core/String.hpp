#ifndef Uni_Core_String_hpp
#define Uni_Core_String_hpp

#include <boost/algorithm/string.hpp>
#include <boost/functional/hash.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>

#include <string>

namespace Uni {
namespace Core {
namespace Detail {
template <class T>
class String {
public:
  typedef T                         Base;
  typedef typename Base::value_type Value;
  typedef std::shared_ptr<String>   Shared;

  typedef typename Base::iterator       Iterator;
  typedef typename Base::const_iterator ConstIterator;

public:
  explicit
  String() {}
  String(T const& str) : _base(str) {}
  String(char const* s) : _base(s) {}
  String(const String& str) : _base(str._base) {}
  // String(int const& num): _base(boost::lexical_cast<T>(num)) {}

  String&
  operator=(const String& str) {
    _base = str._base; return *this;
  }

  T const&
  reveal() const { return _base; }

  char const*
  chars() const { return _base.c_str(); }

  Value const&
  get(int const& index) const {
    return _base.at(index);
  }

  Iterator
  begin() { return _base.begin(); }

  ConstIterator
  begin() const { return _base.begin(); }

  Iterator
  end() { return _base.end(); }

  ConstIterator
  end() const { return _base.end(); }

  String&
  append(const String& str) { _base.append(str._base); return *this; }

  size_t
  length() const { return _base.length(); }

  bool
  isEmpty() const { return _base.empty(); }

  size_t
  find(const String& str, size_t pos = 0) const {
    return _base.find(str._base,
                      pos);
  }

  unsigned int
  hash() const {
    std::hash<Base> hasher;

    return hasher(_base);
  }

  int
  compare(const String& str) const { return _base.compare(str._base); }

  bool
  equals(String const& other) const { return _base.compare(other._base) == 0; }

  bool
  equals(Shared const& other) const {
    return _base.compare(other->_base) == 0;
  }

  bool
  equalsIgnoreCase(String const& other) const {
    return boost::iequals(_base,
                          other._base);
  }

  bool
  equalsIgnoreCase(Shared const& other) const {
    return boost::iequals(_base,
                          other->_base);
  }

  operator T() { return _base; }

  operator char const*() const { return chars(); }

  char const*
  operator()() const { return chars(); }

  bool
  operator==(String const& other) const { return _base == other._base; }

  bool
  operator==(T const& other) const { return _base == other; }

  bool
  operator==(char const* const& other) const { return _base == other; }

  bool
  operator==(Shared const& other) const { return _base == other->_base; }

  bool
  operator!=(String const& other) const { return _base != other._base; }

  bool
  operator<(String const& other) const { return _base < other._base; }

  bool
  operator<(Shared const& other) const { return _base < other->_base; }

  bool
  operator>(String const& other) const { return _base > other._base; }

  bool
  operator<=(String const& other) const { return _base <= other._base; }

  bool
  operator>=(String const& other) const { return _base >= other._base; }

  String
  operator+(char const* other) const {
    String result(_base + other);

    return result;
  }

  String
  operator+(String const& other) const {
    String result(_base + other._base);

    return result;
  }

  void
  operator+=(char const* other) {
    _base += other;
  }

  void
  operator+=(String const& other) {
    _base += other;
  }

  Value const&
  operator()(int const& index) const {
    return get(index);
  }

  Value const&
  operator[](int const& index) const {
    return get(index);
  }

  static Shared
  createShared() {
    return Shared(new String());
  }

  static Shared
  createShared(String const& other) {
    return Shared(new String(other));
  }

  static Shared
  createShared(char const* chars) {
    return Shared(new String(chars));
  }

private:
  Base _base;
};

template <class T>
bool
operator<(typename String<T>::Shared const& sharedString,
          String<T> const&                  string) {
  return *sharedString < string;
}

template <class T>
bool
operator<(typename String<T>::Shared const& sharedString1,
          typename String<T>::Shared const& sharedString2) {
  return *sharedString1 < *sharedString2;
}

template <class T>
bool
operator==(typename String<T>::Shared const& sharedString,
           String<T> const&                  string) {
  return *sharedString == string;
}

template <class T>
bool
operator==(typename String<T>::Shared const& sharedString1,
           typename String<T>::Shared const& sharedString2) {
  return *sharedString1 == *sharedString2;
}

template <class T>
std::ostream&
operator<<(std::ostream& os, String<T> const& string) {
  return os << string.chars();
}

template <class T>
struct StringHash {
  inline std::size_t
  operator()(String<T> const& string) const {
    return string.hash();
  }

  inline std::size_t
  operator()(typename String<T>::Shared const& sharedString) const {
    return sharedString->hash();
  }
};

template <class T>
struct StringEqual {
  bool
  operator()(
    typename String<T>::Shared const& sharedString1,
    typename String<T>::Shared const& sharedString2) const {
    return *sharedString1 == sharedString2;
  }

  bool
  operator()(
    String<T> const&                  string,
    typename String<T>::Shared const& sharedString) const {
    return *sharedString == string;
  }

  bool
  operator()(
    typename String<T>::Shared const& sharedString,
    String<T> const&                  string) const {
    return *sharedString == string;
  }
};
}

typedef::Uni::Core::Detail::String<std::string>      String;
typedef::Uni::Core::Detail::StringHash<std::string>  StringHash;
typedef::Uni::Core::Detail::StringEqual<std::string> StringEqual;
}
}

namespace boost {
template <class T>
struct hash < Uni::Core::Detail::String < T >>  {
  inline std::size_t
  operator()(Uni::Core::Detail::String<T> const& string) const {
    return string.hash();
  }
};

template <class T>
struct hash < boost::shared_ptr < Uni::Core::Detail::String<T >>>  {
  inline std::size_t
  operator()(typename Uni::Core::Detail::String<T>::Shared
             const& sharedString) const {
    return sharedString->hash();
  }
};
}

typedef Uni::Core::String      String;
typedef Uni::Core::StringHash  StringHash;
typedef Uni::Core::StringEqual StringEqual;
#endif
