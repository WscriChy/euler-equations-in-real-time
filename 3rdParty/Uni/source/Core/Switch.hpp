#ifndef Uni_Core_Switch_hpp
#define Uni_Core_Switch_hpp

namespace Uni {
namespace Core {
class Switch {
public:
  Switch(bool value = false) : _value(value) {}

  Switch(Switch const& other) : _value(other._value) {}

  ~Switch() {}

  Switch const&
  operator=(Switch const& other) {
    _value = other._value;

    return *this;
  }

  bool
  equals(Switch const& other) const {
    return _value == other._value;
  }

  bool
  value() const {
    return _value;
  }

  bool
  get() const {
    return _value;
  }

  void
  value(bool const& value) {
    _value = value;
  }

  void
  toggle() {
    _value = !_value;
  }

  bool
  operator==(Switch const& other) const {
    return equals(other);
  }

  operator bool() const {
    return value();
  }

  void
  operator()(bool const& value) {
    this->value(value);
  }

  void
  operator()() {
    toggle();
  }

private:
  bool _value;
};
}
}

typedef Uni::Core::Switch Switch;

#endif
