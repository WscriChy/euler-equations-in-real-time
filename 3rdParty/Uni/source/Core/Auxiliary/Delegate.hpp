#pragma once

#include <Uni/ExecutionControl/exception>

#include <cxxabi.h>
#include <functional>
#include <sstream>
#include <utility>

namespace Uni {
namespace Core {
namespace Auxiliary {
namespace Detail {
template <typename Function>
struct FunctionTraits : public FunctionTraits<decltype (
                                                & Function::operator())>
{};

template <typename TReturnType, typename TClassType, typename... TArgs>
struct FunctionTraits<TReturnType (TClassType::*)(TArgs...) const> {
  typedef TReturnType (* pointer)(TArgs...);
  typedef std::function<TReturnType(TArgs...)> function;
  typedef TReturnType                          ReturnType;
  typedef TClassType                           ClassType;
};

template <typename Function>
typename FunctionTraits<Function>::pointer
toFunctionPointer(Function& lambda) {
  return static_cast<typename FunctionTraits<Function>::pointer>(lambda);
}

template <typename Function>
typename FunctionTraits<Function>::function
toFunction(Function& lambda) {
  return typename FunctionTraits<Function>::function(lambda);
}

template <typename Pointer,
          typename ReturnType,
          typename ClassType,
          typename... Args>
struct Delegate {
  Delegate(Pointer pointer_,
           ReturnType(ClassType::* function_)(Args...)) : pointer(pointer_),
    function(function_) {}

  Pointer    pointer;
  ReturnType (ClassType::* function)(Args...);

  template <typename... Ts>
  ReturnType
  operator()(Ts&& ... args) {
    return ((*pointer).*function)(std::forward<Ts>(args) ...);
  }
};

template <typename Pointer,
          typename ReturnType,
          typename ClassType,
          typename... Args>
auto makeDelegate(Pointer pointer,
                  ReturnType (ClassType::* methodPointer)(Args...))
->Delegate<Pointer, ReturnType, ClassType, Args...>
{
  Delegate<Pointer, ReturnType, ClassType, Args...> result(
    pointer, methodPointer);

  return result;
}

template <typename Pointer,
          typename ReturnType,
          typename ClassType,
          typename... Args>
std::function<ReturnType(Args...)>
makeFunction(Pointer                  pointer,
             ReturnType (ClassType::* methodPointer)(Args...)) {
  Delegate<Pointer, ReturnType, ClassType, Args...> result(
    pointer, methodPointer);

  return std::function<ReturnType(Args...)>(result);
}
}

class Delegate final {
public:
  Delegate() {
    function = static_cast<void*>(
      new std::function<void()>([] () { INFO << "Dummy call"; }));
    signature = &typeid (std::function<void()> );
  }

  ~Delegate() {
    delete static_cast<std::function<void()>*>(function);
  }

  template <typename ReturnType = void,
            typename... Args>
  ReturnType
  invoke(Args&& ... args) const {
    auto function_ = static_cast<std::function<ReturnType(Args...)>*>(function);

    if (typeid (function_) != *(signature)) {
      auto               typeidName = typeid (function_).name();
      std::ostringstream oss;
      int                a[] = { 0, ((void)(oss << args << " "), 0) ... };
      ((void)a);
      int status;

      throwException("Bad Type Id: Expected '{1}', but got '{2}', '{3}'",
                     abi::__cxa_demangle(signature->name(), 0, 0,
                                         &status),
                     abi::__cxa_demangle(typeidName, 0, 0, &status),
                     oss.str().c_str());
    }

    return (*function_)(std::forward<Args>(args) ...);
  }

  template <typename T>
  static
  Delegate&
  createFromFunction(Delegate&               delegate,
                     std::function<T> const& function_) {
    auto function = new std::function<T>(function_);
    delegate.function  = static_cast<void*>(function);
    delegate.signature = &typeid (function);

    return delegate;
  }

  template <typename T>
  static
  Delegate&
  createFromLambda(Delegate& delegate, T&&       lambda) {
    auto function = new decltype (Detail::toFunction(lambda))(
      Detail::toFunction(lambda));
    delegate.function  = static_cast<void*>(function);
    delegate.signature = &typeid (function);

    return delegate;
  }

  template <typename ReturnType,
            typename... Args>
  static
  Delegate&
  createFromFunctionPointer(Delegate&     delegate,
                            ReturnType (* functionPointer)(Args...)) {
    auto function = new std::function<ReturnType(Args...)>(functionPointer);
    delegate.function  = static_cast<void*>(function);
    delegate.signature = &typeid (function);

    return delegate;
  }

  template <typename Pointer,
            typename ReturnType,
            typename ClassType,
            typename... Args>
  static
  Delegate&
  createFromMethodPointer(Delegate&                delegate,
                          Pointer                  pointer,
                          ReturnType (ClassType::* methodPointer)(Args...)) {
    auto function = new std::function<ReturnType(Args...)>(
      Detail::makeDelegate(pointer, methodPointer));
    delegate.function  = static_cast<void*>(function);
    delegate.signature = &typeid (function);

    return delegate;
  }

  void*                 function;
  std::type_info const* signature;
};
}
}
}
