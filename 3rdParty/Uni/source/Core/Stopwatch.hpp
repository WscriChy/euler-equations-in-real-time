#ifndef Uni_Core_Stopwatch_hpp
#define Uni_Core_Stopwatch_hpp

#include <chrono>

namespace Uni {
namespace Core {
typedef std::chrono::high_resolution_clock HighResolutionClock;
typedef std::chrono::steady_clock          SteadyClock;
typedef std::chrono::system_clock          SystemClock;
typedef std::chrono::nanoseconds           Nanoseconds;
typedef std::chrono::microseconds          Microseconds;
typedef std::chrono::milliseconds          Milliseconds;
typedef std::chrono::seconds               Seconds;
typedef std::chrono::minutes               Minutes;
typedef std::chrono::hours                 Hours;

template <typename TDuration = Nanoseconds,
          typename TClock    = HighResolutionClock>
class Stopwatch {
public:
  typedef typename TClock::duration   ClockDuration;
  typedef typename TClock::time_point ClockTimePoint;
  typedef typename TDuration::rep     Duration;
  typedef  TDuration                  DurationType;

public:
public:
  Stopwatch() :  _running(false), _elapsed() {}

  void
  start() {
    if (_running) {
      return;
    }

    _running = true;
    _start   = TClock::now();
  }

  void
  stop() {
    if (!_running) {
      return;
    }

    _elapsed += TClock::now() - _start;
    _running  = false;
  }

  void
  reset() {
    _elapsed = TClock::duration::zero();
    _start   = TClock::now();
  }

  bool
  isRunning() const
  { return _running; }

  template <typename UDuration = TDuration>
  bool
  hasExpired(typename UDuration::rep const& interval) const {
    using std::chrono::duration_cast;

    return elapsed<UDuration>() > interval;
  }

  template <typename UDuration = TDuration>
  typename UDuration::rep
  elapsed() const {
    using std::chrono::duration_cast;

    return duration_cast<UDuration>(
      _running ? _elapsed + (TClock::now() - _start) :
      _elapsed).count();
  }

private:
  bool           _running;
  ClockTimePoint _start;
  ClockDuration  _elapsed;
};
}
}

using HighResolutionClock = Uni::Core::HighResolutionClock;
using Hours               = Uni::Core::Hours;
using Microseconds        = Uni::Core::Microseconds;
using Milliseconds        = Uni::Core::Milliseconds;
using Minutes             = Uni::Core::Minutes;
using Nanoseconds         = Uni::Core::Nanoseconds;
using Seconds             = Uni::Core::Seconds;
using SteadyClock         = Uni::Core::SteadyClock;
using SystemClock         = Uni::Core::SystemClock;

template <typename TDuration = Nanoseconds,
          typename TClock    = HighResolutionClock>
using Stopwatch              = Uni::Core::Stopwatch<TDuration, TClock>;

template <typename TDuration  = Nanoseconds>
using HighResolutionStopwatch = Uni::Core::Stopwatch<TDuration,
                                                     HighResolutionClock>;
template <typename TDuration = Seconds>
using SteadyStopwatch        = Uni::Core::Stopwatch<TDuration, SteadyClock>;
template <typename TDuration = Seconds>
using SystemStopwatch        = Uni::Core::Stopwatch<TDuration, SteadyClock>;
#endif
