#ifndef Uni_Core_IteratorFacade_hpp
#define Uni_Core_IteratorFacade_hpp

#include <iterator>

namespace Uni {
template <typename Derived,
          typename TValue,
          typename TReference      = TValue&,
          typename TConstReference = TValue const&,
          typename TPointer        = TValue*,
          typename TConstPointer   = TValue const*>
class IteratorFacade {
public:
  typedef TValue                          Value;
  typedef TValue                          value_type;
  typedef TReference                      Reference;
  typedef TConstReference                 ConstReference;
  typedef Reference                       reference;
  typedef ConstReference                  const_reference;
  typedef TPointer                        Pointer;
  typedef TConstPointer                   ConstPointer;
  typedef Pointer                         pointer;
  typedef ConstPointer                    const_pointer;
  typedef int                             Difference;
  typedef int                             difference_type;
  typedef std::random_access_iterator_tag iterator_category;

public:
  ConstReference
  operator*() const {
    return this->derived().dereference();
  }

  Reference
  operator*() {
    return this->derived().dereference();
  }

  ConstPointer
  operator->() const {
    return &this->derived().dereference();
  }

  Pointer
  operator->() {
    return &this->derived().dereference();
  }

  bool
  operator==(Derived const& other) const {
    return this->derived().equals(other);
  }

  bool
  operator!=(Derived const& other) const {
    return !this->derived().equals(other);
  }

  bool
  operator<(Derived const& other) const {
    return this->derived().distanceTo(other) > 0;
  }

  bool
  operator<=(Derived const& other) const {
    return this->derived().distanceTo(other) >= 0;
  }

  bool
  operator>(Derived const& other) const {
    return this->derived().distanceTo(other) < 0;
  }

  bool
  operator>=(Derived const& other) const {
    return this->derived().distanceTo(other) <= 0;
  }

  Derived&
  operator++() {
    this->derived().increment();

    return this->derived();
  }

  Derived
  operator++(int) {
    Derived result(this->derived());
    this->derived().increment();

    return result;
  }

  Derived&
  operator--() {
    this->derived().decrement();

    return this->derived();
  }

  Derived
  operator--(int) {
    Derived result(this->derived());
    this->derived().decrement();

    return result;
  }

  Derived&
  operator+=(int const& dist) {
    this->derived().advance(dist);

    return this->derived();
  }

  Derived&
  operator-=(int const& dist) {
    this->derived().advance(-dist);

    return this->derived();
  }

  int
  operator-(Derived const& other) const {
    return other.distanceTo(this->derived());
  }

  Derived
  operator+(int const& dist) const {
    Derived result(this->derived());
    result.advance(dist);

    return result;
  }

  Derived
  operator-(int const& dist) const {
    Derived result(this->derived());
    result.advance(-dist);

    return result;
  }

  Value
  operator[](int const& dist) const {
    return *(this->derived() + dist);
  }

  Value
  operator()(int const& dist) const {
    return *(this->derived() + dist);
  }

  int
  operator()() const {
    return this->derived().distance();
  }

private:
  Derived&
  derived() {
    return *static_cast<Derived*>(this);
  }

  Derived const&
  derived() const {
    return *static_cast<Derived const*>(this);
  }
};

template <class Derived, class Value>
Derived
operator+(int const& dist, IteratorFacade<Derived, Value> const& iterator) {
  return iterator + dist;
}

template <class Derived, class Value>
Derived
operator-(int const& dist, IteratorFacade<Derived, Value> const& iterator) {
  return -(iterator - dist);
}
}
#endif
