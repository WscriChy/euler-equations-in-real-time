#ifndef Uni_Core_RingStack_hpp
#define Uni_Core_RingStack_hpp

#include <Uni/IteratorFacade>

#include <vector>

namespace Uni {
namespace Core {
namespace RingStackDetail {
template <class T,
          class U = typename T::Value>
class Iterator : public IteratorFacade<Iterator<T, U>, U> {
  friend class IteratorFacade<Iterator<T, U>, U>;

private:
  typedef IteratorFacade<Iterator<T, U>, U> Base;

public:
  typedef typename Base::Reference Reference;
  typedef typename Base::Pointer   Pointer;

public:
  Iterator(T* holder) : _holder(holder),
                        _index(0) {}

  Iterator(T*         holder,
           int const& index) : _holder(holder),
                               _index(_holder->size()) {}

  Iterator(Iterator const& other) : _holder(other._holder),
                                    _index(other._index) {}

private:
  void
  increment() {
    if (_index >= _holder->size()) {
      return;
    }
    ++_index;
  }

  void
  decrement() {
    if (_index <= 0) {
      return;
    }
    --_index;
  }

  bool
  equals(Iterator const& other) const {
    return _holder == other._holder &&
           _index == other._index;
  }

  Reference
  dereference() const {
    return _holder->get(_index);
  }

private:
  T* _holder;
  typename T::Size _index;
};
}

template <typename T>
class RingStack {
public:
  typedef T                          Value;
  typedef std::vector<Value>         Vector;
  typedef typename Vector::size_type Size;

  typedef RingStackDetail::Iterator<RingStack, Value>             Iterator;
  typedef RingStackDetail::Iterator<RingStack const, Value const> ConstIterator;

public:
  RingStack() : _length(1),
                _position(0) {}

  RingStack(RingStack const& other) {}

  ~RingStack() {}

  RingStack&
  operator=(RingStack const& other) {
    return *this;
  }

  ConstIterator
  begin() const {
    return ConstIterator(this);
  }

  ConstIterator
  end() const {
    return ConstIterator(this, 0);
  }

  Size const&
  length() const {
    return _length;
  }

  void
  length(Size const& length) {
    _length = length;
    reduceSize();
  }

  int
  size() const {
    return _vector.size();
  }

  /**
   * _position <-> index = 0;
   */
  Value const&
  get(Size const& index) const {
    int tempPosition = _position - index - 1;

    while (tempPosition < 0) {
      tempPosition = _vector.size() + tempPosition;
    }

    return _vector.at(tempPosition);
  }

  Value&
  get(Size const& index) {
    int tempPosition = _position - index;

    while (tempPosition < 0) {
      tempPosition = _vector.size() + tempPosition;
    }

    return _vector.at(tempPosition);
  }

  void
  add(Value const& value) {
    if (_vector.size() == _length) {
      if (_position >= _length) {
        _position = 0;
      }
      _vector.at(_position) = value;
      ++_position;
    } else if (_vector.size() < _length) {
      _vector.push_back(value);
    } else {
      reduceSize();
      _vector.at(_position) = value;
      ++_position;
    }
  }

  Value const&
  operator()(int const& index) const {
    return get(index);
  }

  Value const&
  operator[](int const& index) const {
    return get(index);
  }

private:
  void
  reduceSize() {
    if (_vector.size() <= _length) {
      return;
    }
    Vector newVector;
    int    temp = _position - _length;

    if (temp < 0) {
      temp = _vector.size() + temp;
    }

    Size i = 0;
    Size j = temp;

    for (; i < _length; ++i, ++j) {
      if (j >= _vector.size()) {
        j = 0;
      }
      newVector.push_back(_vector[j]);
    }
    _vector   = newVector;
    _position = 0;
  }

  Size   _length;
  Size   _position;
  Vector _vector;
};
}
}

template <typename T>
using RingStack = Uni::Core::RingStack<T>;

#endif
