#ifndef Uni_MotionSystem_Direction_hpp
#define Uni_MotionSystem_Direction_hpp

#include "Handedness.hpp"

#include <Eigen/Geometry>

namespace Uni {
namespace MotionSystem {
namespace Math {
/**
 * \brief
 * Template empty class which represents a storage (state) of 3d directions
 *  dependent on a chosen handedness of the coordinate system of a space.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
template <typename T,
          Handedness handedness = Handedness::right>
struct Direction {
  using Vector3 = Eigen::Matrix<T, 3, 1>;
};

/**
 * \brief
 * Template empty class which represents a storage (state) of 3d directions
 *  for the left handedness of the coordinate system of a space.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
template <class T>
struct Direction<T, Handedness::left> {
  using Vector3 = Eigen::Matrix<T, 3, 1>;

  static Vector3
  left()
  { return -Vector3::UnitX(); }

  static Vector3
  right()
  { return Vector3::UnitX(); }

  static Vector3
  up()
  { return Vector3::UnitY(); }

  static Vector3
  down()
  { return -Vector3::UnitY(); }

  static Vector3
  forward()
  { return Vector3::UnitZ(); }

  static Vector3
  backward()
  { return -Vector3::UnitZ(); }
};

/**
 * \brief
 * Template empty class which represents a storage (state) of 3d directions
 *  for the right handedness of the coordinate system of a space.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
template <class T>
struct Direction<T, Handedness::right> {
  using Vector3 = Eigen::Matrix<T, 3, 1>;

  static Vector3
  left()
  { return Vector3::UnitX(); }

  static Vector3
  right()
  { return -Vector3::UnitX(); }

  static Vector3
  up()
  { return Vector3::UnitY(); }

  static Vector3
  down()
  { return -Vector3::UnitY(); }

  static Vector3
  forward()
  { return Vector3::UnitZ(); }

  static Vector3
  backward()
  { return -Vector3::UnitZ(); }
};
}
}
}

#endif
