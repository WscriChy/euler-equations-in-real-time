#ifndef Uni_MotionSystem_Handedness_hpp
#define Uni_MotionSystem_Handedness_hpp

namespace Uni {
namespace MotionSystem {
/**
 * \brief
 *  Class which represents a entity of a handedness of the coordinate system of
 * a space.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
enum class Handedness {
  right,
  left
};
}
}

#endif
