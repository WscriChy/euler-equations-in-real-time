#ifndef Uni_MotionSystem_Basic_SixScalarsMotion_hpp
#define Uni_MotionSystem_Basic_SixScalarsMotion_hpp

#include "MultiScalarProvider.hpp"

#include <Uni/MotionSystem/Motion>

#include <Uni/Logging/macros>

#include <memory>

namespace Uni {
namespace MotionSystem {
namespace Basic {
/**
 * \brief
 * Template class which represents an entity of a 3D motion based on six
 * scalar values that could be updated and provide this information as well as
 * values themselves on request.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
template <
  typename T,
  typename U = T>
class SixScalarsMotion : public Motion<T> {
private:
  typedef Motion<T>                 Base;
  typedef MultiScalarProvider<U, 6> SixScalarsProvider;

public:
  typedef typename Base::Vector Vector;
  typedef typename SixScalarsProvider::ConcreteScalarProvider
    ConcreteScalarProvider;
  typedef typename SixScalarsProvider::ScalarProviderShared
    ScalarProviderShared;
  typedef typename SixScalarsProvider::ScalarProviders ScalarProviders;

public:
  SixScalarsMotion() : Base() {
    _speed.fill(1.0);
  }

  SixScalarsMotion(SixScalarsMotion const& other) = delete;

  ~SixScalarsMotion() {}

  SixScalarsMotion const&
  operator=(SixScalarsMotion const& other) = delete;

  void
  scalarProviders(ScalarProviders const& scalarProviders) {
    _sixScalarsProvider.scalarProviders(scalarProviders);
  }

  void
  scalarProvider(int const&           index,
                 ScalarProviderShared scalarProvider) {
    _sixScalarsProvider.scalarProvider(index, scalarProvider);
  }

  Vector const&
  speed() const {
    return _speed;
  }

  void
  speed(Vector const& speed) {
    _speed = speed;
  }

  bool
  isUpdated() const {
    return _sixScalarsProvider.isUpdated();
  }

  Vector
  xyz() const {
    return Vector(x(), y(), z());
  }

  T
  x() const {
    return _speed(0) * (_sixScalarsProvider(1) - _sixScalarsProvider(0));
  }

  T
  y() const {
    return _speed(1) * (_sixScalarsProvider(3) - _sixScalarsProvider(2));
  }

  T
  z() const {
    return _speed(2) * (_sixScalarsProvider(5) - _sixScalarsProvider(4));
  }

  void
  reset() {
    _sixScalarsProvider.reset();
  }

private:
  Vector             _speed;
  SixScalarsProvider _sixScalarsProvider;
};
typedef SixScalarsMotion<double> SixScalarsMotiond;
}
}
}

#endif
