#ifndef Uni_MotionSystem_Basic_OneVector2TwoScalarsMotion_hpp
#define Uni_MotionSystem_Basic_OneVector2TwoScalarsMotion_hpp

#include "MultiScalarProvider.hpp"
#include "VectorProvider.hpp"

#include <Uni/MotionSystem/Motion>

#include <memory>

namespace Uni {
namespace MotionSystem {
namespace Basic {
/**
 * \brief
 * Template class which represents an entity of a 3D motion based on one 2D
 * vector value and two scalar values that could be updated and provide this
 * information as well as values themselves on request.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */
template <
  typename T,
  typename U = T,
  typename G = T>
class OneVector2TwoScalarsMotion : public Motion<T> {
private:
  typedef Motion<T>                 Base;
  typedef MultiScalarProvider<U, 2> TwoScalarsProvider;

public:
  typedef typename Base::Vector Vector;
  typedef VectorProvider<G, 2>  Vector2Provider;

  typedef std::shared_ptr<Vector2Provider> Vector2ProviderShared;

  typedef typename TwoScalarsProvider::ScalarProviderShared
    ScalarProviderShared;
  typedef typename TwoScalarsProvider::ScalarProviders
    ScalarProviders;

public:
  OneVector2TwoScalarsMotion() : Base() {
    _speed.fill(1.0);
  }

  OneVector2TwoScalarsMotion(OneVector2TwoScalarsMotion const& other) = delete;

  ~OneVector2TwoScalarsMotion() {}

  OneVector2TwoScalarsMotion const&
  operator=(OneVector2TwoScalarsMotion const& other) = delete;

  void
  vector2Provider(Vector2ProviderShared const& vector2ProviderShared) {
    _vector2ProviderShared = vector2ProviderShared;
  }

  void
  scalarProviders(ScalarProviders const& scalarProviders) {
    _twoScalarsProvider.scalarProviders(scalarProviders);
  }

  void
  scalarProvider(int const&           index,
                 ScalarProviderShared scalarProvider) {
    _twoScalarsProvider.scalarProvider(index, scalarProvider);
  }

  Vector const&
  speed() const {
    return _speed;
  }

  void
  speed(Vector const& speed) {
    _speed = speed;
  }

  bool
  isUpdated() const {
    return _twoScalarsProvider.isUpdated() ||
           _vector2ProviderShared->isUpdated();
  }

  Vector
  xyz() const {
    return Vector(x(), y(), z());
  }

  T
  x() const {
    return _speed(0) * _vector2ProviderShared->value(0);
  }

  T
  y() const {
    return _speed(1) * _vector2ProviderShared->value(1);
  }

  T
  z() const {
    return _speed(2) * (_twoScalarsProvider(1) - _twoScalarsProvider(0));
  }

  void
  reset() {
    _twoScalarsProvider.reset();
    _vector2ProviderShared->reset();
  }

private:
  Vector                _speed;
  Vector2ProviderShared _vector2ProviderShared;
  TwoScalarsProvider    _twoScalarsProvider;
};
}
}
}

#endif
