#ifndef Uni_MotionSystem_MotionController_hpp
#define Uni_MotionSystem_MotionController_hpp

#include "Camera.hpp"
#include "Direction.hpp"
#include "Motion.hpp"
#include "ScreenState.hpp"

#include "Basic/DummyMotion.hpp"

#include <Uni/Logging/macros>

#include <Eigen/Core>

#include <memory>

namespace Uni {
namespace MotionSystem {
/**
 * \brief
 * Template class which represents a controller of motions in 3D space.
 *
 * \details
 *
 * \author Viacheslav Mikerov <SlavaMikerov@gmail.com>
 * \copyright GNU Public License.
 * \version 1.0 \date 2014
 */

template <
  typename T,
  typename U            = T,
  Handedness handedness = Handedness::left>
class MotionController {
private:
  typedef ScreenState<U>        ScreenStateConcrete;
  typedef Camera<T, handedness> CameraConcrete;

public:
  typedef std::shared_ptr<MotionController>    Shared;
  typedef Eigen::Matrix<T, 4, 4>               Matrix4x4;
  typedef typename ScreenStateConcrete::Shared ScreenStateShared;
  typedef MotionSystem::Motion<T>              Motion;
  typedef std::shared_ptr<Motion>              MotionShared;

public:
  MotionController() : _updateProjection(false),
                       _movement(new Basic::DummyMotion<T>()),
                       _rotation(new Basic::DummyMotion<T>()),
                       _view(_camera.view()),
                       _projection(_camera.projection(100,
                                                      100,
                                                      3.1415926 / 3.0,
                                                      0.01,
                                                      1000)) {}

  MotionController(MotionController const& other) = delete;

  MotionController const&
  operator==(MotionController const& other) = delete;

  void
  screenState(ScreenStateShared const& screenState) {
    _screenState = screenState;
    _screenState->observe(ScreenStateProperty::Width,
                          [&] (U const&) { _updateProjection = true; });
    _screenState->observe(ScreenStateProperty::Height,
                          [&] (U const&) { _updateProjection = true; });
    _screenState->observe(ScreenStateProperty::FovY,
                          [&] (U const&) { _updateProjection = true; });
    _screenState->observe(ScreenStateProperty::NearZ,
                          [&] (U const&) { _updateProjection = true; });
    _screenState->observe(ScreenStateProperty::FarZ,
                          [&] (U const&) { _updateProjection = true; });
    _updateProjection = true;
  }

  ScreenStateShared
  screenState() const {
    return _screenState;
  }

  void
  rotation(MotionShared const& rotation) {
    _rotation = rotation;
  }

  void
  movement(MotionShared const& movement) {
    _movement = movement;
  }

  Matrix4x4
  projection() {
    if (_updateProjection) {
      _projection = _camera.projection(_screenState->width(),
                                       _screenState->height(),
                                       _screenState->fovY(),
                                       _screenState->nearZ(),
                                       _screenState->farZ());
      _updateProjection = false;
    }

    return _projection;
  }

  Matrix4x4
  view() {
    bool isUpdated = false;

    if (_rotation->isUpdated()) {
      _camera.pitch(_rotation->x());
      _camera.yaw(_rotation->y());
      _camera.roll(_rotation->z());
      _rotation->reset();
      isUpdated = true;
    }

    if (_movement->isUpdated()) {
      _camera.move(_movement->xyz());
      _movement->reset();
      isUpdated = true;
    }

    if (isUpdated) {
      _view = _camera.view();
    }

    return _view;
  }

  Matrix4x4
  viewProjection() { return projection() * view(); }

private:
  bool              _updateProjection;
  CameraConcrete    _camera;
  ScreenStateShared _screenState;
  MotionShared      _movement;
  MotionShared      _rotation;

  Matrix4x4 _view;
  Matrix4x4 _projection;
};

template <typename T>
using SharedMotionController = typename MotionController<T>::Shared;

typedef MotionController<float> MotionControllerf;

typedef SharedMotionController<float> SharedMotionControllerf;
}
}
#endif
