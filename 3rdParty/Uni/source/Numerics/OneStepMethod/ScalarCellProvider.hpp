#pragma once

namespace Uni {
namespace Numerics {
namespace OneStepMethod {
/**
 * Represents the controller of a one-step method with scalar (one-dimensional)
 *  access to cells of a structured grid.
 *
 * 1. Controls the execution of a one-step method - inherited from Controller
 *    class.
 * 2. Provides access for cells of a structured grid by scalar index.
 *    The scalar index identifies the current cell and the methods of the class
 *    provide access for neighboring (adjacent) cells relative to this one.
 *
 * The scalar index could be appropriate, for example, when the memory layout is
 *  one-dimensional.
 * In different circumstances, for example, with three-dimensional memory layout
 *  there could be necessity in different access means to the grid elements.
 */
template <typename Cell>
class ScalarCellProvider {
public:
  virtual
  ~ScalarCellProvider() {}

  virtual Cell
  leftCell(int const& index) const = 0;

  virtual Cell
  rightCell(int const& index) const = 0;

  virtual Cell
  bottomCell(int const& index) const = 0;

  virtual Cell
  topCell(int const& index) const = 0;

  virtual Cell
  backCell(int const& index) const = 0;

  virtual Cell
  frontCell(int const& index) const = 0;

  virtual Cell
  currentCell(int const& index) const = 0;

  virtual Cell
  leftNewCell(int const& index) const = 0;

  virtual Cell
  rightNewCell(int const& index) const = 0;

  virtual Cell
  bottomNewCell(int const& index) const = 0;

  virtual Cell
  topNewCell(int const& index) const = 0;

  virtual Cell
  backNewCell(int const& index) const = 0;

  virtual Cell
  frontNewCell(int const& index) const = 0;

  virtual Cell
  currentNewCell(int const& index) const = 0;
};
}
}
}
