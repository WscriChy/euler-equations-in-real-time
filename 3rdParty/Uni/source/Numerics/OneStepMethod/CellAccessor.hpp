#pragma once

namespace Uni {
namespace Numerics {
namespace OneStepMethod {
/**
 * Represent the contoller of access to a structured grid of a one-step
 *  method from the current position.
 *
 * Is an accessor to cells of a one-step method on a structured grid.
 */
template <typename Cell>
class CellAccessor {
public:
  virtual
  ~CellAccessor() {}

  virtual Cell
  leftCell() const = 0;

  virtual Cell
  rightCell() const = 0;

  virtual Cell
  bottomCell() const = 0;

  virtual Cell
  topCell() const = 0;

  virtual Cell
  backCell() const = 0;

  virtual Cell
  frontCell() const = 0;

  virtual Cell
  currentCell() const = 0;

  virtual Cell
  leftNewCell() const = 0;

  virtual Cell
  rightNewCell() const = 0;

  virtual Cell
  bottomNewCell() const = 0;

  virtual Cell
  topNewCell() const = 0;

  virtual Cell
  backNewCell() const = 0;

  virtual Cell
  frontNewCell() const = 0;

  virtual Cell
  currentNewCell() const = 0;
};
}
}
}
