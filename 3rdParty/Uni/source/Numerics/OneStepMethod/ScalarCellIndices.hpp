#pragma once

namespace Uni {
namespace Numerics {
namespace OneStepMethod {
/**
 * Represents the entity of Cartesian grid for one-step methods with scalar
 *  (one-dimensional) access to cells of a Cartesian grid.
 *
 *
 */
template <typename TGrid>
class ScalarCellIndices {
public:
  ScalarCellIndices(TGrid const* grid) : _grid(grid) {}

  virtual
  ~ScalarCellIndices() {}

  virtual int
  leftIndex(int const& index) const {
    return index - 1;
  }

  virtual int
  rightIndex(int const& index) const {
    return index + 1;
  }

  virtual int
  bottomIndex(int const& index) const {
    return index - _grid->size(0);
  }

  virtual int
  topIndex(int const& index) const {
    return index + _grid->size(0);
  }

  virtual int
  backIndex(int const& index) const {
    return index - _grid->size(0) * _grid->size(1);
  }

  virtual int
  frontIndex(int const& index) const {
    return index + _grid->size(0) * _grid->size(1);
  }

  virtual int
  currentIndex(int const& index) const {
    return index;
  }

private:
  TGrid const* _grid;
};
}
}
}
