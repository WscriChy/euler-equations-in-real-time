#ifndef Uni_Numerics_OneStepMethod_Controller_hpp
#define Uni_Numerics_OneStepMethod_Controller_hpp

namespace Uni {
namespace Numerics {
namespace OneStepMethod {
/**
 * Represents the basic controller of a one-step method.
 *
 * Controls the execution of a one-step method.
 */
template <typename T = double>
class Controller {
private:
public:
  virtual
  T
  previousTimeStamp() const { return T(0); }

  virtual
  T
  currentTimeStamp() const { return T(0); }

  virtual
  void
  newTimeStamp(T const& value) { ((void)value); }

  virtual
  T
  timeStepSize() const { return T(0); }

  virtual
  void
  timeStepSize(double const& value) { ((void)value); }

  virtual
  void
  flip() {}

  virtual
  void
  resetTimeStep() {}
};
}
}
}

#endif
