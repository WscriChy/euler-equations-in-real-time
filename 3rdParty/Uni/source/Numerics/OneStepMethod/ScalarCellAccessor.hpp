#pragma once

#include "CellAccessor.hpp"
#include "ScalarCellProvider.hpp"

#include <Uni/StructuredGrid/Basic/GlobalMultiIndex>
#include <Uni/StructuredGrid/Basic/Grid>

#include <Eigen/Core>

namespace Uni {
namespace Numerics {
namespace OneStepMethod {
template <typename Derived, typename TGrid, typename Cell, unsigned TDimensions>
class ScalarCellAccessor
  : public Uni::StructuredGrid::Basic::GlobalMultiIndex<Derived, TGrid,
                                                        TDimensions>,
    public CellAccessor<Cell> {
public:

  enum {
    Dimensions = TDimensions
  };

  typedef StructuredGrid::Basic::GlobalMultiIndex<Derived, TGrid,
                                                  Dimensions> Base;
  typedef CellAccessor<Cell>
    Base2;

  typedef typename Base::GridType GridType;

  using VectorDi = Eigen::Matrix<int, Dimensions, 1>;

  typedef ScalarCellProvider<Cell> CellProvider;

public:
  ScalarCellAccessor(
    GridType const* grid,
    CellProvider*   cellProvider) : Base(grid),
    _cellProvider(cellProvider) {}

  ScalarCellAccessor(
    GridType const*                grid,
    VectorDi const& i,
    CellProvider*                  cellProvider) : Base(grid, i),
    _cellProvider(cellProvider) {}

  ScalarCellAccessor(
    ScalarCellAccessor const& other) : Base(other),
    _cellProvider(other._cellProvider) {}

  virtual
  ~ScalarCellAccessor() {}

  ScalarCellAccessor const&
  operator=(ScalarCellAccessor const& other) {
    Base::operator=(other);
    _cellProvider = other._cellProvider;

    return *this;
  }

  virtual Cell
  leftCell() const {
    return _cellProvider->leftCell(this->indexValue(3));
  }

  virtual Cell
  rightCell() const {
    return _cellProvider->rightCell(this->indexValue(3));
  }

  virtual Cell
  bottomCell() const {
    return _cellProvider->bottomCell(this->indexValue(3));
  }

  virtual Cell
  topCell() const {
    return _cellProvider->topCell(this->indexValue(3));
  }

  virtual Cell
  backCell() const {
    return _cellProvider->backCell(this->indexValue(3));
  }

  virtual Cell
  frontCell() const {
    auto index = this->indexValue(3);

    auto result = _cellProvider->frontCell(this->indexValue(3));

    return result;
  }

  virtual Cell
  currentCell() const {
    return _cellProvider->currentCell(this->indexValue(3));
  }

  virtual Cell
  leftNewCell() const {
    return _cellProvider->leftNewCell(this->indexValue(3));
  }

  virtual Cell
  rightNewCell() const {
    return _cellProvider->rightNewCell(this->indexValue(3));
  }

  virtual Cell
  bottomNewCell() const {
    return _cellProvider->bottomNewCell(this->indexValue(3));
  }

  virtual Cell
  topNewCell() const {
    return _cellProvider->topNewCell(this->indexValue(3));
  }

  virtual Cell
  backNewCell() const {
    return _cellProvider->backNewCell(this->indexValue(3));
  }

  virtual Cell
  frontNewCell() const {
    return _cellProvider->frontNewCell(this->indexValue(3));
  }

  virtual Cell
  currentNewCell() const {
    return _cellProvider->currentNewCell(this->indexValue(3));
  }

private:
  CellProvider* _cellProvider;
};
}
}
}
