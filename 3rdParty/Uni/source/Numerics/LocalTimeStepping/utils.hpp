#ifndef Uni_Numerics_LocalTimeStepping_utils_hpp
#define Uni_Numerics_LocalTimeStepping_utils_hpp

#include <Uni/Logging/macros>

#include <cstdio>
#include <cstdlib>
#include <limits>
#include <vector>

namespace Uni {
namespace Numerics {
namespace LocalTimeStepping {
template <typename T>
bool
isLessOrEqual(T one, std::vector<T> const& others) {
  for (auto const& other : others) {
    if (one > other) {
      return false;
    }
  }

  return true;
}

template <typename T>
void
computeTimeStepPlotCount(T const& timeStamp,
                         T const  interval,
                         int&     timeStepPlotCount,
                         T&       timeStepSize) {
  int const plotCount    = (timeStamp / interval);
  T const   newTimeStamp = timeStamp + timeStepSize;
  int const newPlotCount = (newTimeStamp / interval);

  timeStepPlotCount = newPlotCount - plotCount;

  if (newPlotCount > plotCount) {
    timeStepSize = (plotCount + 1) * interval - timeStamp;
    --timeStepPlotCount;
  }
}

template <typename T>
T
interpolate(T const& t1,
            T const& t2,
            T const& t) {
  T denominator = t2 - t1;

  if (t > t2) {
    logFatal("Local Time Stepping Failed t = %f > t2 = %f", t, t2);
    // exit(-1);
  }

  T numerator = t - t1;

  if (denominator > std::numeric_limits<T>::epsilon() &&
      denominator < 0) {
    logFatal("Local Time Stepping Failed t = %f < t1 = %f", t, t1);
    // exit(-1);
  }

  T coeff = 0.0;

  if (denominator <= std::numeric_limits<T>::epsilon()) {
    // coeff = 0.0;
  } else if (numerator <= std::numeric_limits<T>::epsilon()) {
    // coeff = 0.0;
  } else {
    T coeff = numerator / denominator;

    if (coeff != coeff) {
      coeff = 0.0;
    }
  }

  return coeff;
}
}
}
}

#endif
