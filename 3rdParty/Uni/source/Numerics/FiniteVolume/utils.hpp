#ifndef Uni_Numerics_FiniteVolume_utils_hpp
#define Uni_Numerics_FiniteVolume_utils_hpp

#include <Uni/Logging/macros>

#include <Eigen/Core>

namespace Uni {
namespace Numerics {
template <typename T, int D>
using VectorT3 = Eigen::Matrix<double, D, 1>;

template <typename T, int D>
using VectorT4 = Eigen::Matrix<double, D, 1>;

namespace FiniteVolume {
template <typename T, int D>
T
computeTimeStepSize(VectorT3<T, D> const& gridWidth,
                    T const&           waveSpeed,
                    T const&           cfl = 0.5) {
  if (waveSpeed <= 0 || waveSpeed != waveSpeed) {
    logFatal("Wave speed (%f) is out of possible values", waveSpeed);
  }

  auto vector = (cfl / waveSpeed) * gridWidth;

  return vector.minCoeff();
}
}
}
}

#endif
