#ifndef Uni_EulerEquations_Cell_hpp
#define Uni_EulerEquations_Cell_hpp

#include <Uni/Logging/macros>

#include <Uni/ExecutionControl/exception>

#include <Eigen/Core>

#include <cmath>
#include <cstdio>

namespace Uni {
namespace EulerEquations {
template <typename T = double, unsigned D = 3>
class Cell {
private:
public:
  typedef Eigen::Matrix<T, D, 1> Vector;

public:
  virtual
  ~Cell() {}

  virtual
  T
  density() const = 0;

  virtual
  void
  density(T const& value) = 0;

  virtual
  Vector
  velocity() const = 0;

  virtual
  T
  velocity(int const& index) const {
    return velocity()(index);
  }

  virtual
  void
  velocity(Vector const& value) = 0;

  virtual
  void
  velocity(int const& index, T const& value) = 0;

  virtual
  T
  energy() const = 0;

  virtual
  void
  energy(T const& value) = 0;

  virtual
  T
  marker() const = 0;

  virtual
  void
  marker(T const& value) = 0;

  virtual
  T
  gasConstant() const {
    // R is a constant obtained by dividing the universal gas constant R
    // by the molecular weight of the gas

    return T(8.3144621757575);
  }

  virtual
  T
  adiabaticConstant() const = 0;

  virtual
  T
  temperature() const {
    throwException("Not supported");

    return 0;
  }

  virtual
  void
  temperature(T const& value) {
    throwException("Not supported");
  }

  virtual
  T
  pressure() const {
    throwException("Not supported");

    return 0;
  }

  virtual
  void
  pressure(T const& pressure) {
    throwException("Not supported");
  }

  virtual
  T
  enthalpy() const {
    throwException("Not supported");

    return 0;
  }

  virtual
  void
  enthalpy(T const& enthalpy) {
    throwException("Not supported");
  }

  virtual
  T
  soundSpeed() const {
    throwException("Not supported");

    return 0;
  }

  virtual
  void
  soundSpeed(T const& soundSpeed) const {
    throwException("Not supported");
  }

  virtual
  T
  computeMaxWaveVelocity() const {
    T _soundSpeed = soundSpeed();

    T result = std::max(
      std::max(
        std::max(
          std::max(
            std::max(
              std::abs(velocity(0) + _soundSpeed),
              std::abs(velocity(0) - _soundSpeed)),
            std::abs(velocity(1) + _soundSpeed)),
          std::abs(velocity(1) - _soundSpeed)),
        std::abs(velocity(2) + _soundSpeed)),
      std::abs(velocity(2) - _soundSpeed));

    return result;
  }

  virtual
  void
  copy(Cell const* cell) {
    density(cell->density());
    energy(cell->energy());
    velocity(cell->velocity());
    marker(cell->marker());
  }

  virtual
  Cell&
  operator=(Cell const& other) {
    copy(&other);

    return *this;
  }

  virtual
  void
  reflect(Cell const* cell) {
    density(cell->density());
    energy(cell->energy());
    velocity((-1.0) * cell->velocity());
    marker(cell->marker());
  }

  virtual
  void
  interpolate(Cell const* cell1, Cell const* cell2, double const& value) {
    density(cell1->density() + value * (cell2->density() - cell1->density()));
    velocity(cell1->velocity() + value * (cell2->velocity() -
                                          cell1->velocity()));
    energy(cell1->energy() + value * (cell2->energy() - cell1->energy()));
    marker(cell1->marker() + value * (cell2->marker() - cell1->marker()));
  }

public:
  static
  T
  computeTemperatureFromDensityVelocityEnergy(
    T const&      density,
    Vector const& velocity,
    T const&      energy,
    T const&      adiabaticConstant,
    T const&      gasConstant) {
    return (energy / density - 0.5 * velocity.dot(velocity)) *
           (adiabaticConstant - 1) / gasConstant;
  }

  static
  T
  computeEnergyFromDensityVelocityTemperature(
    T const&      density,
    Vector const& velocity,
    T const&      temperature,
    T const&      adiabaticConstant,
    T const&      gasConstant) {
    return density *
           (gasConstant * temperature / (adiabaticConstant - 1)
            + 0.5 * velocity.dot(velocity));
  }

  static
  T
  computePressureFromDensityVelocityEnergy(
    T const&      density,
    Vector const& velocity,
    T const&      energy,
    T const&      adiabaticConstant) {
    return (adiabaticConstant - 1.0) *
           (energy - 0.5 * density * velocity.dot(velocity));
  }

  static
  T
  computeEnergyFromDensityVelocityPressure(
    T const&      density,
    Vector const& velocity,
    T const&      pressure,
    T const&      adiabaticConstant) {
    return (pressure / (adiabaticConstant - 1.0)) +
           0.5 * density * velocity.dot(velocity);
  }

  static
  T
  computeEnthalpyFromDensityVelocityEnergy(
    T const&      density,
    Vector const& velocity,
    T const&      energy,
    T const&      adiabaticConstant) {
    return ((adiabaticConstant - 1.0) *
            (energy - 0.5 * density * velocity.dot(velocity)) + energy) /
           density;
  }

  static
  T
  computeEnergyFromDensityVelocityEnthalpy(
    T const&      density,
    Vector const& velocity,
    T const&      enthalpy,
    T const&      adiabaticConstant) {
    return (enthalpy +
            (adiabaticConstant - 1.0) * 0.5 * velocity.dot(velocity)) *
           density / adiabaticConstant;
  }

  static
  T
  computeSoundSpeedFromDensityVelocityEnergy(
    T const&      density,
    Vector const& velocity,
    T const&      energy,
    T const&      adiabaticConstant) {
    T soundSpeed = adiabaticConstant *
                   (adiabaticConstant - 1.0) *
                   (energy / density - 0.5 * velocity.dot(velocity));

    if (soundSpeed < 0.0) {
      logFatal(
        "Sound speed is less than 0.0: energy = {1}, velosicy square norm =  {2}",
        energy / density, velocity.dot(velocity));
      exit(-1);
    }

    return std::sqrt(soundSpeed);
  }
};

template <typename T, int D>
std::ostream&
operator<<(std::ostream& os, Cell<T, D> const& cell) {
  os << "density = " <<
    cell.density() << "\nvelocity = " <<
    cell.velocity() << "\nenergy = " <<
    cell.energy() << "\nmarker = " <<
    cell.marker();

  return os;
}
}
}

#endif
