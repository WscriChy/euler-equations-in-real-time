#ifndef Uni_EulerEquations_Basic_Cell_hpp
#define Uni_EulerEquations_Basic_Cell_hpp

#include <Uni/EulerEquations/Cell>

namespace Uni {
namespace EulerEquations {
namespace Basic {
template <
  typename T = double,
  int D      = 3>
class Cell : public Uni::EulerEquations::Cell<T, D> {
private:
public:
  typedef Uni::EulerEquations::Cell<T, D> Base;
  typedef typename Base::Vector           Vector;

public:
  Cell() {}

  Cell(Cell const& other) { (*this) = other; }

  virtual
  Cell&
  operator=(Cell const& other) {
    Base::operator=(other);

    return *this;
  }

  virtual
  ~Cell() {}

  virtual
  T
  density() const {
    return _density;
  }

  virtual
  void
  density(T const& value) {
    _density = value;
  }

  virtual
  Vector
  velocity() const {
    return _velocity;
  }

  virtual
  T
  velocity(int const& index) const {
    return _velocity(index);
  }

  virtual
  void
  velocity(Vector const& value) {
    _velocity = value;
  }

  virtual
  void
  velocity(int const& index, T const& value) {
    _velocity(index) = value;
  }

  virtual
  T
  energy() const {
    return _energy;
  }

  virtual
  void
  energy(T const& value) {
    _energy = value;
  }

  virtual
  T
  marker() const {
    return _marker;
  }

  virtual
  void
  marker(T const& value) {
    _marker = value;
  }

  virtual
  T
  gasConstant() const {
    // R is a constant obtained by dividing the universal gas constant R
    // by the molecular weight of the gas

    return Base::gasConstant() / T(18.0);
  }

  virtual
  T
  adiabaticConstant() const {
    return 1.4;
  }

  virtual
  T
  temperature() const {
    return Base::computeTemperatureFromDensityVelocityEnergy(
      density(),
      velocity(),
      energy(),
      adiabaticConstant(),
      gasConstant());
  }

  virtual
  void
  temperature(T const& temperature) {
    energy(Base::computeEnergyFromDensityVelocityTemperature(
             density(),
             velocity(),
             temperature,
             adiabaticConstant(),
             gasConstant()));
  }

  virtual
  T
  pressure() const {
    return Base::computePressureFromDensityVelocityEnergy(
      density(),
      velocity(),
      energy(),
      adiabaticConstant());
  }

  virtual
  void
  pressure(T const& pressure) {
    energy(Base::computeEnergyFromDensityVelocityPressure(
             density(),
             velocity(),
             pressure,
             adiabaticConstant()));
  }

  virtual
  T
  enthalpy() const {
    return Base::computeEnthalpyFromDensityVelocityEnergy(
      density(),
      velocity(),
      energy(),
      adiabaticConstant());
  }

  virtual
  void
  enthalpy(T const& enthalpy) {
    energy(Base::computeEnergyFromDensityVelocityEnthalpy(
             density(),
             velocity(),
             enthalpy,
             adiabaticConstant()));
  }

  virtual
  T
  soundSpeed() const {
    return Base::computeSoundSpeedFromDensityVelocityEnergy(
      density(),
      velocity(),
      energy(),
      adiabaticConstant());
  }

private:
  T      _density;
  Vector _velocity;
  T      _energy;
  T      _marker;
};
}
}
}

#endif
