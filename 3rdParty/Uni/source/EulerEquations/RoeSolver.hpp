#ifndef Uni_EulerEquations_RoeSolver_hpp
#define Uni_EulerEquations_RoeSolver_hpp

#include "Cell.hpp"

#include "Basic/Cell.hpp"

#include <Eigen/Core>

namespace Uni {
namespace EulerEquations {
class RoeSolver {
public:
  typedef Cell<double, 3> ConcreteCell;

  typedef Eigen::Matrix<double, 3, 1> Vector3d;
  typedef Eigen::Matrix<double, 5, 1> Vector5d;
  typedef Eigen::Matrix<double, 5, 5> Matrix5d;

private:
  typedef double Aligned32DoubleArray[4] __attribute__((__aligned__(32)));

public:
  RoeSolver();

  RoeSolver(RoeSolver const& other);

  ~RoeSolver();

  void
  courantNumber(double const& t,
                double const& x,
                double const& y,
                double const& z);

  void
  apply(ConcreteCell const& leftCell,
        ConcreteCell const& rightCell,
        ConcreteCell const& bottomCell,
        ConcreteCell const& topCell,
        ConcreteCell const& backCell,
        ConcreteCell const& frontCell,
        ConcreteCell const& currentCell,
        ConcreteCell&       newCell,
        double&             maxLambda) const;

private:
  void
  computeFluxX(ConcreteCell const& cell,
               Vector5d&           flux) const;

  void
  computeFluxY(ConcreteCell const& cell,
               Vector5d&           flux) const;

  void
  computeFluxZ(ConcreteCell const& cell,
               Vector5d&           flux) const;

  void
  composeMatrices(ConcreteCell const& leftCell,
                  ConcreteCell const& rightCell,
                  Vector5d&           result,
                  Vector3d const&     n,
                  Vector3d const&     l,
                  Vector3d const&     m) const;

  Aligned32DoubleArray _courantNumber;

  static Vector3d nx;
  static Vector3d lx;
  static Vector3d mx;
  static Vector3d ny;
  static Vector3d ly;
  static Vector3d my;
  static Vector3d nz;
  static Vector3d lz;
  static Vector3d mz;
};
}
}

#endif
