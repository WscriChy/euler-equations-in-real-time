#ifdef Uni_EulerEquations_Unavailable
#include "LaxFriedrichsScheme.hpp"

#include <immintrin.h>
#include <math.h>

#define GAS_CONST       8.3144621
#define TEMPERATURE     283.0
#define ADIABATIC_CONST 1.4
#define SOUND_SPEED     (sqrt(GAS_CONST * TEMPERATURE * ADIABATIC_CONST))

using namespace euler::solver;

namespace euler {
namespace solver {
namespace detail {
template <>
struct Private<LaxFriedrichsScheme> {
  typedef __m256d Vector4d;

  Vector4d _xCourantNumberVector;
  Vector4d _yCourantNumberVector;
  Vector4d _zCourantNumberVector;

  static Vector4d const oneSixthVector;
  static Vector4d const rtVector;

  void
  operator delete(void* ptr) {
    _aligned_free(ptr);
  }
} __attribute__((__aligned__(32)));
}
}
}

LaxFriedrichsScheme::
LaxFriedrichsScheme() : _im((Private*)_aligned_malloc(sizeof (Private), 32)) {}

LaxFriedrichsScheme::
~LaxFriedrichsScheme() {}

double
LaxFriedrichsScheme::
soundSpeed() const { return SOUND_SPEED; }

void
LaxFriedrichsScheme::
courantNumber(double const& t,
              double const& x,
              double const& y,
              double const& z) {
  _courantNumber[1]          = t / (2.0 * x);
  _courantNumber[2]          = t / (2.0 * y);
  _courantNumber[3]          = t / (2.0 * z);
  _im->_xCourantNumberVector = _mm256_set1_pd(_courantNumber[1]);
  _im->_yCourantNumberVector = _mm256_set1_pd(_courantNumber[2]);
  _im->_zCourantNumberVector = _mm256_set1_pd(_courantNumber[3]);
}

void
LaxFriedrichsScheme::
apply(Cell const& leftCell,
      Cell const& rightCell,
      Cell const& bottomCell,
      Cell const& topCell,
      Cell const& backCell,
      Cell const& frontCell,
      Cell&       newCell,
      double&     maxLambda) const {
  Private::Vector4d leftUnknownVector =
    _mm256_mul_pd(_mm256_set1_pd(leftCell.getRho()),
                  _mm256_setr_pd(1.0,
                                 leftCell.getU()[0],
                                 leftCell.getU()[1],
                                 leftCell.getU()[2]));
  Private::Vector4d rightUnknownVector =
    _mm256_mul_pd(_mm256_set1_pd(rightCell.getRho()),
                  _mm256_setr_pd(1.0,
                                 rightCell.getU()[0],
                                 rightCell.getU()[1],
                                 rightCell.getU()[2]));
  Private::Vector4d bottomUnknownVector =
    _mm256_mul_pd(_mm256_set1_pd(bottomCell.getRho()),
                  _mm256_setr_pd(1.0,
                                 bottomCell.getU()[0],
                                 bottomCell.getU()[1],
                                 bottomCell.getU()[2]));
  Private::Vector4d topUnknownVector =
    _mm256_mul_pd(_mm256_set1_pd(topCell.getRho()),
                  _mm256_setr_pd(1.0,
                                 topCell.getU()[0],
                                 topCell.getU()[1],
                                 topCell.getU()[2]));
  Private::Vector4d backUnknownVector =
    _mm256_mul_pd(_mm256_set1_pd(backCell.getRho()),
                  _mm256_setr_pd(1.0,
                                 backCell.getU()[0],
                                 backCell.getU()[1],
                                 backCell.getU()[2]));
  Private::Vector4d frontUnknownVector =
    _mm256_mul_pd(_mm256_set1_pd(frontCell.getRho()),
                  _mm256_setr_pd(1.0,
                                 frontCell.getU()[0],
                                 frontCell.getU()[1],
                                 frontCell.getU()[2]));

  Private::Vector4d resultVector =
    _mm256_sub_pd(
      _mm256_mul_pd(LaxFriedrichsScheme::Private::oneSixthVector,
                            _mm256_add_pd(
                      leftUnknownVector,
                            _mm256_add_pd(
                        rightUnknownVector,
                            _mm256_add_pd(
                          bottomUnknownVector,
                            _mm256_add_pd(
                            topUnknownVector,
                            _mm256_add_pd(
                              backUnknownVector,
                              frontUnknownVector)))))),
                            _mm256_add_pd(
                            _mm256_add_pd(
          _mm256_mul_pd(_im->_xCourantNumberVector,
                        _mm256_sub_pd(
                          _mm256_mul_pd(_mm256_set1_pd(rightCell.getU()[0]),
                                        rightUnknownVector),
                          _mm256_mul_pd(_mm256_set1_pd(leftCell.getU()[0]),
                                        leftUnknownVector))),
          _mm256_add_pd(
            _mm256_mul_pd(_im->_yCourantNumberVector,
                          _mm256_sub_pd(
                            _mm256_mul_pd(_mm256_set1_pd(topCell.getU()[1]),
                                          topUnknownVector),

                            _mm256_mul_pd(_mm256_set1_pd(bottomCell.getU()[1]),
                                          bottomUnknownVector))),
                            _mm256_mul_pd(_im->_zCourantNumberVector,
                          _mm256_sub_pd(

                            _mm256_mul_pd(_mm256_set1_pd(frontCell.getU()[2]),
                                          frontUnknownVector),

                            _mm256_mul_pd(_mm256_set1_pd(backCell.getU()[2]),
                                          backUnknownVector))))),
                            _mm256_mul_pd(
          LaxFriedrichsScheme::Private::rtVector,
          _mm256_setr_pd(
            0.0,
            _courantNumber[1] * (
              rightCell.getRho() - leftCell.getRho()),
            _courantNumber[2] * (
              topCell.getRho() - bottomCell.getRho()),
            _courantNumber[3] * (
              frontCell.getRho() - backCell.getRho())
            )
          )
        )
      );

  Aligned32DoubleArray resultArray;
  _mm256_store_pd(resultArray, resultVector);

  double resultMarker =
    LaxFriedrichsScheme::oneSixth *
    (leftCell.getMarker() + rightCell.getMarker()
     + bottomCell.getMarker() + topCell.getMarker()
     + backCell.getMarker() + frontCell.getMarker())
    - _courantNumber[1] *
    (rightCell.getMarker() * rightCell.getU()[0]
     - leftCell.getMarker() * leftCell.getU()[0])
    - _courantNumber[2] *
    (topCell.getMarker() * topCell.getU()[1]
     - bottomCell.getMarker() * bottomCell.getU()[1])
    - _courantNumber[3] *
    (frontCell.getMarker() * frontCell.getU()[2]
     - backCell.getMarker() * backCell.getU()[2]);

  newCell.setRho(resultArray[0]);

  if (resultArray[0] != 0) {
    newCell.setU(0, resultArray[1] / resultArray[0]);
    newCell.setU(1, resultArray[2] / resultArray[0]);
    newCell.setU(2, resultArray[3] / resultArray[0]);
  } else {
    printf("Density is 0\n");
    newCell.setU(0, resultArray[1]);
    newCell.setU(1, resultArray[2]);
    newCell.setU(2, resultArray[3]);
  }

  newCell.setMarker(resultMarker);

  maxLambda = fmax(
            fmax(
            fmax(
            fmax(
            fmax(
            fmax(abs(newCell.getU()[0] + SOUND_SPEED),
                 abs(newCell.getU()[0] - SOUND_SPEED)),
                 abs(newCell.getU()[0] + SOUND_SPEED)),
                 abs(newCell.getU()[1] - SOUND_SPEED)),
                 abs(newCell.getU()[1] + SOUND_SPEED)),
                 abs(newCell.getU()[2] - SOUND_SPEED)),
                 abs(newCell.getU()[2] + SOUND_SPEED));
}

double const
LaxFriedrichsScheme::oneSixth = 1.0 / 6.0;

double const
LaxFriedrichsScheme::rt = GAS_CONST * TEMPERATURE;

LaxFriedrichsScheme::Private::Vector4d const
LaxFriedrichsScheme::Private::oneSixthVector =
  _mm256_set1_pd(LaxFriedrichsScheme::oneSixth);

LaxFriedrichsScheme::Private::Vector4d const
LaxFriedrichsScheme::Private::rtVector =
  _mm256_set1_pd(LaxFriedrichsScheme::rt);
#endif
