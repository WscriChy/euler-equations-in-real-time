#include "RoeSolver.hpp"

#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

#include <iomanip>
#include <iostream>
#include <limits>
#include <math.h>
#include <stdio.h>

using Uni::EulerEquations::RoeSolver;

void
RoeSolver::
computeFluxX(ConcreteCell const& cell,
             Vector5d&           flux) const {
  double pressure = cell.pressure();
  flux << 1.0, cell.velocity(), 0.0;
  flux    *=  cell.density();
  flux(4)  = cell.energy() + pressure;
  flux    *=  cell.velocity(0);
  flux(1) += pressure;
}

void
RoeSolver::
computeFluxY(ConcreteCell const& cell,
             Vector5d&           flux) const {
  double pressure = cell.pressure();
  flux << 1.0, cell.velocity(), 0.0;
  flux     = flux * cell.density();
  flux(4)  = cell.energy() + pressure;
  flux    *= cell.velocity(1);
  flux(2) += pressure;
}

void
RoeSolver::
computeFluxZ(ConcreteCell const& cell,
             Vector5d&           flux) const {
  double pressure = cell.pressure();
  flux << 1.0, cell.velocity(), 0.0;
  flux     = flux * cell.density();
  flux(4)  = cell.energy() + pressure;
  flux    *= cell.velocity(2);
  flux(3) += pressure;
}

void
RoeSolver::
composeMatrices(ConcreteCell const& leftCell,
                ConcreteCell const& rightCell,
                Vector5d&           result,
                Vector3d const&     n,
                Vector3d const&     l,
                Vector3d const&     m) const {
  double tempRhoLeft    = std::sqrt(leftCell.density());
  double tempRhoRight   = std::sqrt(rightCell.density());
  double tempRhoAverage = tempRhoLeft + tempRhoRight;

  double const u = (tempRhoLeft * leftCell.velocity(0) +
                    tempRhoRight * rightCell.velocity(0)) / tempRhoAverage;
  double const v = (tempRhoLeft * leftCell.velocity(1) +
                    tempRhoRight * rightCell.velocity(1)) / tempRhoAverage;
  double const w = (tempRhoLeft * leftCell.velocity(2) +
                    tempRhoRight * rightCell.velocity(2)) / tempRhoAverage;
  double const H = (tempRhoLeft * leftCell.enthalpy() +
                    tempRhoRight * rightCell.enthalpy()) / tempRhoAverage;

  Vector5d difference;
  difference <<
    rightCell.density() - leftCell.density(),
    rightCell.density() * rightCell.velocity() -
    leftCell.density() * leftCell.velocity(),
    rightCell.energy() - leftCell.energy();

  double   cLeft   = leftCell.soundSpeed();
  double   cRight  = rightCell.soundSpeed();
  double   qnLeft  = leftCell.velocity().dot(n);
  double   qnRight = rightCell.velocity().dot(n);
  Vector5d leftEigenValues;
  leftEigenValues <<
    qnLeft - cLeft,
    qnLeft,
    qnLeft + cLeft,
    qnLeft,
    qnLeft;
  Vector5d rightEigenValues;
  rightEigenValues <<
    qnRight - cRight,
    qnRight,
    qnRight + cRight,
    qnRight,
    qnRight;

  double const k  = leftCell.adiabaticConstant() - 1.0;
  double const q2 = u * u + v * v + w * w;
  double       c  = k * (H - 0.5 * q2);

  if (c <= 0.0) {
    FATAL << "Sound speed is less or equal zero";
    exit(-1);
  } else {
    c = std::sqrt(c);
  }
  double   c2 = c * c;
  double   qn = u * n(0) + v * n(1) + w * n(2);
  double   ql = u * l(0) + v * l(1) + w * l(2);
  double   qm = u * m(0) + v * m(1) + w * m(2);
  Vector3d cn = c * n;
  Matrix5d rn;
  Matrix5d ln;
  rn <<
    1.0, 1.0, 1.0, 0.0, 0.0,
    u - cn(0), u, u + cn(0), l(0), m(0),
    v - cn(1), v, v + cn(1), l(1), m(1),
    w - cn(2), w, w + cn(2), l(2), m(2),
    H - qn * c, q2 / 2.0, H + qn * c, ql, qm;
  ln <<
    // row 1
    k * q2 /  (4.0 * c2) + qn / (2.0 * c),
    -(k * u / (2.0 * c2) + n(0) / (2.0 * c)),
    -(k * v / (2.0 * c2) + n(1) / (2.0 * c)),
    -(k * w / (2.0 * c2) + n(2) / (2.0 * c)),
    k / (2.0 * c2),
    // row 2
    1.0 - (k * q2 / (2.0 * c2)),
    k * u / c2,
    k * v / c2,
    k * w / c2,
    -k / c2,
    // row 3
    k * q2 /  (4.0 * c2) - qn / (2.0 * c),
    -(k * u / (2.0 * c2) - n(0) / (2.0 * c)),
    -(k * v / (2.0 * c2) - n(1) / (2.0 * c)),
    -(k * w / (2.0 * c2) - n(2) / (2.0 * c)),
    k / (2.0 * c2),
    // row 4
    -ql, l(0), l(1), l(2), 0,
    // row 5
    -qm, m(0), m(1), m(2), 0
  ;

  Vector5d eigenValues;

  eigenValues(0) = std::abs(qn - c);

  if (leftEigenValues(0) < 0.0 && rightEigenValues(0) > 0.0) {
    eigenValues(0) = (((rightEigenValues(0) - eigenValues(0)) *
                       leftEigenValues(0)) /
                      (rightEigenValues(0) - eigenValues(0)));
  }

  eigenValues(2) = std::abs(qn + c);

  if (leftEigenValues(2) < 0.0 && rightEigenValues(2) > 0.0) {
    eigenValues(2) = (((eigenValues(2) - leftEigenValues(2)) *
                       rightEigenValues(2)) /
                      (rightEigenValues(2) - eigenValues(2)));
  }

  eigenValues(1) = std::abs(qn);
  eigenValues(3) = eigenValues(1);
  eigenValues(4) = eigenValues(1);
  result         = rn * eigenValues.asDiagonal() * ln * difference;
}

RoeSolver::Vector3d RoeSolver::nx({ 1.0, 0.0, 0.0 });
RoeSolver::Vector3d RoeSolver::lx({ 0.0, 1.0, 0.0 });
RoeSolver::Vector3d RoeSolver::mx({ 0.0, 0.0, 1.0 });
RoeSolver::Vector3d RoeSolver::ny({ 0.0, 1.0, 0.0 });
RoeSolver::Vector3d RoeSolver::ly({ 0.0, 0.0, 1.0 });
RoeSolver::Vector3d RoeSolver::my({ 1.0, 0.0, 0.0 });
RoeSolver::Vector3d RoeSolver::nz({ 0.0, 0.0, 1.0 });
RoeSolver::Vector3d RoeSolver::lz({ 1.0, 0.0, 0.0 });
RoeSolver::Vector3d RoeSolver::mz({ 0.0, 1.0, 0.0 });

RoeSolver::
RoeSolver() {}

RoeSolver::
RoeSolver(RoeSolver const& other) {
  _courantNumber[1] = other._courantNumber[1];
  _courantNumber[2] = other._courantNumber[2];
  _courantNumber[3] = other._courantNumber[3];
}

RoeSolver::
~RoeSolver() {}

void
RoeSolver::
courantNumber(double const& t,
              double const& x,
              double const& y,
              double const& z) {
  _courantNumber[1] = t / (x);
  _courantNumber[2] = t / (y);
  _courantNumber[3] = t / (z);
}

void
RoeSolver::
apply(ConcreteCell const& leftCell,
      ConcreteCell const& rightCell,
      ConcreteCell const& bottomCell,
      ConcreteCell const& topCell,
      ConcreteCell const& backCell,
      ConcreteCell const& frontCell,
      ConcreteCell const& currentCell,
      ConcreteCell&       newCell,
      double&             maxLambda) const {
  Vector5d flux1;
  Vector5d diff1;
  composeMatrices(leftCell, currentCell, diff1, nx, lx, mx);
  computeFluxX(leftCell, flux1);
  Vector5d result1 = 0.5 * (flux1 - diff1);

  Vector5d flux2;
  Vector5d diff2;
  composeMatrices(currentCell, rightCell, diff2, nx, lx, mx);
  computeFluxX(rightCell, flux2);
  Vector5d result2 = 0.5 * (flux2 - diff2);

  Vector5d flux3;
  Vector5d diff3;
  composeMatrices(bottomCell, currentCell, diff3, ny, ly, my);
  computeFluxY(bottomCell, flux3);
  Vector5d result3 = 0.5 * (flux3 - diff3);

  Vector5d flux4;
  Vector5d diff4;
  composeMatrices(currentCell, topCell, diff4, ny, ly, my);
  computeFluxY(topCell, flux4);
  Vector5d result4 = 0.5 * (flux4 - diff4);

  Vector5d flux5;
  Vector5d diff5;
  composeMatrices(backCell, currentCell, diff5, nz, lz, mz);
  computeFluxZ(backCell, flux5);
  Vector5d result5 = 0.5 * (flux5 - diff5);

  Vector5d flux6;
  Vector5d diff6;
  composeMatrices(currentCell, frontCell, diff6, nz, lz, mz);
  computeFluxZ(frontCell, flux6);
  Vector5d result6 = 0.5 * (flux6 - diff6);

  Vector5d result;
  result <<
    1.0,
    currentCell.velocity(),
    0.0;
  result    = currentCell.density() * result;
  result(4) = currentCell.energy();
  result   -= _courantNumber[1] * (result2 - result1) +
              _courantNumber[2] * (result4 - result3) +
              _courantNumber[3] * (result6 - result5);

  newCell.density(result(0));
  newCell.energy(result(4));

  if (result(0) <= 0.0) {
    logFatal("density is equal or less than 0");
  }

  result /= result(0);
  newCell.velocity(0, result(1));
  newCell.velocity(1, result(2));
  newCell.velocity(2, result(3));

  maxLambda =  newCell.computeMaxWaveVelocity();
}
