#ifdef Uni_EulerEquations_Unavailable
#include "LaxFriedrichsScheme.hpp"

#include <math.h>
#include <stdio.h>

#define GAS_CONST       8.3144621
#define TEMPERATURE     283.0
#define ADIABATIC_CONST 1.4
#define SOUND_SPEED     (sqrt(GAS_CONST * TEMPERATURE * ADIABATIC_CONST))

using namespace euler::solver;

namespace detail {
template <>
struct Private<LaxFriedrichsScheme> {};
}

LaxFriedrichsScheme::
LaxFriedrichsScheme() : _im(new Private()) {}

LaxFriedrichsScheme::
~LaxFriedrichsScheme() {}

double
LaxFriedrichsScheme::
soundSpeed() const { return SOUND_SPEED; }

void
LaxFriedrichsScheme::
courantNumber(double const& t,
              double const& x,
              double const& y,
              double const& z) {
  _courantNumber[1] = t / (2.0 * x);
  _courantNumber[2] = t / (2.0 * y);
  _courantNumber[3] = t / (2.0 * z);
}

void
LaxFriedrichsScheme::
apply(Cell const& leftCell,
      Cell const& rightCell,
      Cell const& bottomCell,
      Cell const& topCell,
      Cell const& backCell,
      Cell const& frontCell,
      Cell&       newCell,
      double&     maxLambda) const {
  double const aLeftUnknown = leftCell.getDensity();
  double const bLeftUnknown = leftCell.getDensity() * leftCell.getVelocity()[0];
  double const cLeftUnknown = leftCell.getDensity() * leftCell.getVelocity()[1];
  double const dLeftUnknown = leftCell.getDensity() * leftCell.getVelocity()[2];

  double const aRightUnknown = rightCell.getDensity();
  double const bRightUnknown = rightCell.getDensity() *
                               rightCell.getVelocity()[0];
  double const cRightUnknown = rightCell.getDensity() *
                               rightCell.getVelocity()[1];
  double const dRightUnknown = rightCell.getDensity() *
                               rightCell.getVelocity()[2];

  double const aBottomUnknown = bottomCell.getDensity();
  double const bBottomUnknown = bottomCell.getDensity() *
                                bottomCell.getVelocity()[0];
  double const cBottomUnknown = bottomCell.getDensity() *
                                bottomCell.getVelocity()[1];
  double const dBottomUnknown = bottomCell.getDensity() *
                                bottomCell.getVelocity()[2];

  double const aTopUnknown = topCell.getDensity();
  double const bTopUnknown = topCell.getDensity() * topCell.getVelocity()[0];
  double const cTopUnknown = topCell.getDensity() * topCell.getVelocity()[1];
  double const dTopUnknown = topCell.getDensity() * topCell.getVelocity()[2];

  double const aBackUnknown = backCell.getDensity();
  double const bBackUnknown = backCell.getDensity() * backCell.getVelocity()[0];
  double const cBackUnknown = backCell.getDensity() * backCell.getVelocity()[1];
  double const dBackUnknown = backCell.getDensity() * backCell.getVelocity()[2];

  double const aFrontUnknown = frontCell.getDensity();
  double const bFrontUnknown = frontCell.getDensity() *
                               frontCell.getVelocity()[0];
  double const cFrontUnknown = frontCell.getDensity() *
                               frontCell.getVelocity()[1];
  double const dFrontUnknown = frontCell.getDensity() *
                               frontCell.getVelocity()[2];

  double aResult = LaxFriedrichsScheme::oneSixth * (aLeftUnknown +
                                                    aRightUnknown +
                                                    aBottomUnknown +
                                                    aTopUnknown +
                                                    aBackUnknown +
                                                    aFrontUnknown);

  aResult -= _courantNumber[1] * (bRightUnknown - bLeftUnknown);
  aResult -= _courantNumber[2] * (cTopUnknown - cBottomUnknown);
  aResult -= _courantNumber[3] * (dFrontUnknown - dBackUnknown);

  double bResult = LaxFriedrichsScheme::oneSixth * (bLeftUnknown +
                                                    bRightUnknown +
                                                    bBottomUnknown +
                                                    bTopUnknown +
                                                    bBackUnknown +
                                                    bFrontUnknown);

  bResult -= _courantNumber[1] * (
    rightCell.getVelocity()[0] * bRightUnknown - leftCell.getVelocity()[0] *
    bLeftUnknown +
    LaxFriedrichsScheme::rt * (aRightUnknown - aLeftUnknown));
  bResult -= _courantNumber[2] * (
    topCell.getVelocity()[0] * cTopUnknown - bottomCell.getVelocity()[0] *
    cBottomUnknown);
  bResult -= _courantNumber[3] * (
    frontCell.getVelocity()[0] * dFrontUnknown - backCell.getVelocity()[0] *
    dBackUnknown);

  double cResult = LaxFriedrichsScheme::oneSixth * (cLeftUnknown +
                                                    cRightUnknown +
                                                    cBottomUnknown +
                                                    cTopUnknown +
                                                    cBackUnknown +
                                                    cFrontUnknown);

  cResult -= _courantNumber[1] * (
    rightCell.getVelocity()[1] * bRightUnknown - leftCell.getVelocity()[1] *
    bLeftUnknown);
  cResult -= _courantNumber[2] * (
    topCell.getVelocity()[1] * cTopUnknown - bottomCell.getVelocity()[1] *
    cBottomUnknown +
    LaxFriedrichsScheme::rt * (aTopUnknown - aBottomUnknown));
  cResult -= _courantNumber[3] * (
    frontCell.getVelocity()[1] * dFrontUnknown - backCell.getVelocity()[1] *
    dBackUnknown);

  double dResult = LaxFriedrichsScheme::oneSixth * (dLeftUnknown +
                                                    dRightUnknown +
                                                    dBottomUnknown +
                                                    dTopUnknown +
                                                    dBackUnknown +
                                                    dFrontUnknown);

  dResult -= _courantNumber[1] * (
    rightCell.getVelocity()[2] * bRightUnknown - leftCell.getVelocity()[2] *
    bLeftUnknown);
  dResult -= _courantNumber[2] * (
    topCell.getVelocity()[2] * cTopUnknown - bottomCell.getVelocity()[2] *
    cBottomUnknown);
  dResult -= _courantNumber[3] * (
    frontCell.getVelocity()[2] * dFrontUnknown - backCell.getVelocity()[2] *
    dBackUnknown +
    LaxFriedrichsScheme::rt * (aFrontUnknown - aBackUnknown));

  double resultMarker =
    LaxFriedrichsScheme::oneSixth *
    (leftCell.getMarker() + rightCell.getMarker()
     + bottomCell.getMarker() + topCell.getMarker()
     + backCell.getMarker() + frontCell.getMarker())
    - _courantNumber[1] *
    (rightCell.getMarker() * rightCell.getVelocity()[0]
     - leftCell.getMarker() * leftCell.getVelocity()[0])
    - _courantNumber[2] *
    (topCell.getMarker() * topCell.getVelocity()[1]
     - bottomCell.getMarker() * bottomCell.getVelocity()[1])
    - _courantNumber[3] *
    (frontCell.getMarker() * frontCell.getVelocity()[2]
     - backCell.getMarker() * backCell.getVelocity()[2]);

  newCell.setDensity(aResult);

  if (aResult != 0) {
    std::vector<double> velocityVector;
    velocityVector[0] =  bResult / aResult;
    velocityVector[1] =  cResult / aResult;
    velocityVector[2] =  dResult / aResult;
    newCell.setVelocity(velocityVector);
  } else {
    printf("Density is 0\n");
    newCell.setVelocity(0, bResult);
    newCell.setVelocity(1, cResult);
    newCell.setVelocity(2, dResult);
  }

  newCell.setMarker(resultMarker);

  maxLambda = std::max(
    std::max(
      std::max(
        std::max(
          std::max(
            std::abs(newCell.getVelocity()[0] + SOUND_SPEED),
            std::abs(newCell.getVelocity()[0] - SOUND_SPEED)),
          std::abs(newCell.getVelocity()[1] + SOUND_SPEED)),
        std::abs(newCell.getVelocity()[1] - SOUND_SPEED)),
      std::abs(newCell.getVelocity()[2] + SOUND_SPEED)),
    std::abs(newCell.getVelocity()[2] - SOUND_SPEED));
}

double const
LaxFriedrichsScheme::oneSixth = 1.0 / 6.0;

double const
LaxFriedrichsScheme::rt = GAS_CONST * TEMPERATURE;
#endif
