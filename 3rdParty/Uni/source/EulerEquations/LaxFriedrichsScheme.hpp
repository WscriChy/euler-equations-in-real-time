#ifndef Uni_EulerEquations_LaxFriedrichsScheme_hpp
#define Uni_EulerEquations_LaxFriedrichsScheme_hpp
#ifdef Uni_EulerEquations_Unavailable

#include "Firewall.hpp"

#include "PeanoExecutor/records/Unknown.h"

namespace Uni {
namespace EulerEquations {
class LaxFriedrichsScheme {
  friend class detail::Private<LaxFriedrichsScheme>;

private:
  typedef detail::Implementation<LaxFriedrichsScheme> Implementation;
  typedef detail::Private<LaxFriedrichsScheme>        Private;
  typedef PeanoExecutor::records::Unknown             Cell;

  typedef double Aligned32DoubleArray[4] __attribute__((__aligned__(32)));

public:
  LaxFriedrichsScheme();

  ~LaxFriedrichsScheme();

  double
  soundSpeed() const;

  void
  courantNumber(double const& t,
                double const& x,
                double const& y,
                double const& z);

  void
  apply(Cell const& leftCell,
        Cell const& rightCell,
        Cell const& bottomCell,
        Cell const& topCell,
        Cell const& backCell,
        Cell const& frontCell,
        Cell&       newCell,
        double&     maxLambda) const;

private:
  Implementation _im;

  Aligned32DoubleArray _courantNumber;

  static double const oneSixth;
  static double const rt;
};
}
}

#endif
#endif
