#include "multiscalelinkedcell/SAMRTools.h"


int multiscalelinkedcell::SAMRTools::getNumberOfCellsPerPatch(
  const tarch::la::Vector<DIMENSIONS, int>&  cells,
  const tarch::la::Vector<DIMENSIONS, int>&  haloCells
) {
  int result = 1;
  for (int d=0; d<DIMENSIONS; d++) {
    result *= (cells(d)+2*haloCells(d));
  }
  return result;
}



int multiscalelinkedcell::SAMRTools::getNumberOfVerticesPerPatch(
  const tarch::la::Vector<DIMENSIONS, int>&  cells,
  const tarch::la::Vector<DIMENSIONS, int>&  haloCells
) {
  int result = 1;
  for (int d=0; d<DIMENSIONS; d++) {
    result *= (cells(d)+2*haloCells(d))+1;
  }
  return result;
}



void multiscalelinkedcell::SAMRTools::computePatchOverlapWithGhostLayer(
  const tarch::la::Vector<DIMENSIONS, double>&   destOffset,
  const tarch::la::Vector<DIMENSIONS, double>&   destH,
  const tarch::la::Vector<DIMENSIONS, double>&   srcOffset,
  const tarch::la::Vector<DIMENSIONS, double>&   srcH,
  const tarch::la::Vector<DIMENSIONS, float>&    dxdy,
  tarch::la::Vector<DIMENSIONS, double>&         leftBottom,
  tarch::la::Vector<DIMENSIONS, double>&         rightTop
) {
  for (int d=0; d<DIMENSIONS; d++) {
    if (tarch::la::greater(srcOffset(d),destOffset(d))) {
      assertionNumericalEquals(destOffset(d) + destH(d), srcOffset(d) );
      leftBottom(d) =  srcOffset(d);
    }
    else if (tarch::la::smaller(srcOffset(d),destOffset(d))) {
      leftBottom(d) =  destOffset(d) - dxdy(d);
    }
    else {
      leftBottom(d) =  destOffset(d);
    }
  }

  for (int d=0; d<DIMENSIONS; d++) {
    if (tarch::la::greater(srcOffset(d),destOffset(d))) {
      rightTop(d) = srcOffset(d) + dxdy(d);
    }
    else if (tarch::la::smaller(srcOffset(d),destOffset(d))) {
      rightTop(d) = destOffset(d);
    }
    else {
      rightTop(d) = destOffset(d) + destH(d);
    }
  }
}


void multiscalelinkedcell::SAMRTools::computeIterationRangeFromPatchOverlap(
  const tarch::la::Vector<DIMENSIONS, double>&   destOffset,
  const tarch::la::Vector<DIMENSIONS, double>&   srcOffset,
  const tarch::la::Vector<DIMENSIONS, float>&    dxdy,
  const tarch::la::Vector<DIMENSIONS, double>&   leftBottom,
  const tarch::la::Vector<DIMENSIONS, double>&   rightTop,
  tarch::la::Vector<DIMENSIONS, int>&  cellOffsetSrc,
  tarch::la::Vector<DIMENSIONS, int>&  cellOffsetDest,
  tarch::la::Vector<DIMENSIONS, int>&  range
) {
  for (int d=0; d<DIMENSIONS; d++) {
    cellOffsetSrc(d) = tarch::la::round( (leftBottom(d)-srcOffset(d)+dxdy(d)) / dxdy(d));
  }
  for (int d=0; d<DIMENSIONS; d++) {
    cellOffsetDest(d) = tarch::la::round( (leftBottom(d)-destOffset(d)+dxdy(d)) / dxdy(d));
  }
  for (int d=0; d<DIMENSIONS; d++) {
    range(d) = tarch::la::round( (rightTop(d)-leftBottom(d))/dxdy(d) );
  }

  assertion3( cellOffsetSrc(0)>=1, cellOffsetSrc, cellOffsetDest, range );
  assertion3( cellOffsetSrc(1)>=1, cellOffsetSrc, cellOffsetDest, range );
  assertion3( cellOffsetDest(0)>=0, cellOffsetSrc, cellOffsetDest, range );
  assertion3( cellOffsetDest(1)>=0, cellOffsetSrc, cellOffsetDest, range );
  assertion3( range(0)>=0, cellOffsetSrc, cellOffsetDest, range );
  assertion3( range(1)>=0, cellOffsetSrc, cellOffsetDest, range );

  assertion3( range(0)==1 || range(1)==1, cellOffsetSrc, cellOffsetDest, range );

  assertion3( cellOffsetSrc(0)+range(0)>=1, cellOffsetSrc, cellOffsetDest, range );
  assertion3( cellOffsetSrc(1)+range(1)>=1, cellOffsetSrc, cellOffsetDest, range );
}
