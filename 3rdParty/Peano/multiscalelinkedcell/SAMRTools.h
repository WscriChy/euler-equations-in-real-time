// This file is part of the Peano project. For conditions of distribution and
// use, please see the copyright notice at www.peano-framework.org
#ifndef _MULISCALE_LINKED_CELL_SAMR_TOOLS_H_
#define _MULISCALE_LINKED_CELL_SAMR_TOOLS_H_


#include "tarch/logging/Log.h"

#include "tarch/la/Vector.h"

#include "peano/utils/Globals.h"
#include "peano/grid/VertexEnumerator.h"


namespace multiscalelinkedcell {
  class SAMRTools;
}



class multiscalelinkedcell::SAMRTools {
  public:
    static int getNumberOfCellsPerPatch( const tarch::la::Vector<DIMENSIONS, int>&  cells, const tarch::la::Vector<DIMENSIONS, int>&  haloCells);
    static int getNumberOfVerticesPerPatch( const tarch::la::Vector<DIMENSIONS, int>&  cells, const tarch::la::Vector<DIMENSIONS, int>&  haloCells);

    static void computePatchOverlapWithGhostLayer(
      const tarch::la::Vector<DIMENSIONS, double>&   destOffset,
      const tarch::la::Vector<DIMENSIONS, double>&   destH,
      const tarch::la::Vector<DIMENSIONS, double>&   srcOffset,
      const tarch::la::Vector<DIMENSIONS, double>&   srcH,
      const tarch::la::Vector<DIMENSIONS, float>&    dxdy,
      tarch::la::Vector<DIMENSIONS, double>&         leftBottom,
      tarch::la::Vector<DIMENSIONS, double>&         rightTop
    );

    static void computeIterationRangeFromPatchOverlap(
      const tarch::la::Vector<DIMENSIONS, double>&   destOffset,
      const tarch::la::Vector<DIMENSIONS, double>&   srcOffset,
      const tarch::la::Vector<DIMENSIONS, float>&    dxdy,
      const tarch::la::Vector<DIMENSIONS, double>&   leftBottom,
      const tarch::la::Vector<DIMENSIONS, double>&   rightTop,
      tarch::la::Vector<DIMENSIONS, int>&  cellOffsetSrc,
      tarch::la::Vector<DIMENSIONS, int>&  cellOffsetDest,
      tarch::la::Vector<DIMENSIONS, int>&  range
    );
};


#endif
